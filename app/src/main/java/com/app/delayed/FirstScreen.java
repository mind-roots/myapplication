package com.app.delayed;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelCountry;
import com.app.delayed.tutorial.ViewpagerActivity;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Balvinder on 1/29/2016.
 */
public class FirstScreen extends Activity {

    public static ArrayList<ModelCountry> arr_cntry = new ArrayList<>();
    TextView tap_here;
    RelativeLayout rl_taphere;
   // public static boolean logdirect = false;
    SharedPreferences prefs;
    private static int SPLASH_TIME_OUT = 3000;

    String value;
    boolean b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_screen);
        init();
        loaddata();
        AllowPermission();

      /*  new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

            }
        }, SPLASH_TIME_OUT);*/


    }

    private void loaddata() {
        if (!b) {
            Methods.conDialog(FirstScreen.this);

        } else {

            new country().execute();
        }

    }

    private void init() {
        tap_here = (TextView) findViewById(R.id.txt_content);
        rl_taphere=(RelativeLayout) findViewById(R.id.rl_taphere);
        prefs = getSharedPreferences("delayed", FirstScreen.this.MODE_PRIVATE);

        value = prefs.getString("pin_match", "");
        b = Methods.isNetworkConnected(FirstScreen.this);
        ActivityStack.activity.add(FirstScreen.this);
    }

    //********************************************Get Country parsing*******************************

    public class country extends AsyncTask {
        String status = "false", result;
        String service_type = "get_country";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tap_here.setText("Content Loading...");
            arr_cntry.clear();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = CountryMethod(service_type);


            System.out.println("LOGIN RESPONCE:::" + response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");

                if (status.equals("true")) {

                    //************************saving data to  model class***************************************

                    JSONArray data_obj = obj.getJSONArray("data");
                    for (int j = 0; j < data_obj.length(); j++) {
                        JSONObject obj1 = data_obj.getJSONObject(j);
                        ModelCountry modelcountry = new ModelCountry();
                        modelcountry.country = obj1.getString("country");
                        modelcountry.code = obj1.getString("code");
                        arr_cntry.add(modelcountry);
                    }
                } else {


                    JSONObject data_obj = obj.getJSONObject("data");
                    result = data_obj.getString("result");


                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (status.equals("true")) {
                tap_here.setText("Tap here to start");
                rl_taphere.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(FirstScreen.this, RegisterScreen.class));
                        finish();

                    }
                });

            } else {
                tap_here.setText("Please try again later");
                Snackbar.make(tap_here, "Response Error", Snackbar.LENGTH_LONG).show();
            }
        }
    }


    //************************Webservice parameter CountryMethod************************************

    public String CountryMethod(String service_type) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }


    /**************************************************
     * Allow Permissions
     **************************************************/
    private void AllowPermission() {
        int hasStoragePermission = ActivityCompat.checkSelfPermission(FirstScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasCameraPermission = ActivityCompat.checkSelfPermission(FirstScreen.this, android.Manifest.permission.CAMERA);
        int hasContactsPermission = ActivityCompat.checkSelfPermission(FirstScreen.this, android.Manifest.permission.READ_CONTACTS);
        int hasRecordPermission = ActivityCompat.checkSelfPermission(FirstScreen.this, android.Manifest.permission.RECORD_AUDIO);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(FirstScreen.this, Manifest.permission.SEND_SMS);
        List<String> permissions = new ArrayList<String>();
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }
        if (hasContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.READ_CONTACTS);
        }

        if (hasRecordPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.RECORD_AUDIO);
        }
        if (hasRecordPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.SEND_SMS);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(FirstScreen.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            //imagedialog();
            // startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        //  startActivityForResult(getPickImageChooserIntent(), 200);
                       // Snackbar.make(tap_here, "Permissions Accepted", Snackbar.LENGTH_SHORT).show();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                       // Snackbar.make(tap_here, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
