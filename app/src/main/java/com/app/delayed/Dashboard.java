package com.app.delayed;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.delayed.adapters.PagerAdapter;
import com.app.delayed.chatExtended.util.TabUtils;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelCountry;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Person;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Dashboard extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;
    TextView title;
    Fragment fragment;
    PagerAdapter adapter;
    FrameLayout container;
    public static int tab_no = 0;
    List<String> titles = new ArrayList<>();
    public static boolean isFb = false;
    public static boolean isTw = false;
    public static boolean newsms = false, newemail = false, newfb = false, newtweet = false, newchat = true;
    public static boolean leavegrp = false;
    ArrayList<ModelCountry> emails = new ArrayList<>();
    public static TabLayout.Tab tabChat;
    Person item;
    public static SharedPreferences prefs;
    DatabaseQueries dataqyery;
    List<ModelContacts> arr_cont = new ArrayList<>();
     boolean isloademail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        init();


        for (int i = 0; i < ActivityStack.activity.size(); i++) {
            ActivityStack.activity.get(i).finish();
        }

        ActivityStack.activity.add(Dashboard.this);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                // setTitle(titles.get(tab.getPosition()));
                title.setText(titles.get(tab.getPosition()));
                viewPager.setCurrentItem(tab.getPosition());
                //page = tab.getPosition();
                tab_no = tab.getPosition();
                // inflateFragments(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


//        //***********get email and contacts*******************
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (!isloademail) {
////                    if (Build.VERSION.SDK_INT >= 23) {
////                        AllowPermission();
////                    } else {
////                        new getemail().execute();
////                      //  new contactsync().execute();
////                    }
//                }
//            }
//        }, 10000);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            startActivity(new Intent(Dashboard.this, Settings.class));
            return true;
        } else if (id == R.id.action_create) {

            if (tab_no == 0) {
                startActivity(new Intent(Dashboard.this, ChatContactsScreen.class));
               // Snackbar.make(toolbar, "Chat View", Snackbar.LENGTH_SHORT).show();

            } else if (tab_no == 1) {
                Intent intent = new Intent(Dashboard.this, NewSMS.class);
                intent.putExtra("intenttype", "new");
                startActivity(intent);
            } else if (tab_no == 2) {
                Intent intent = new Intent(Dashboard.this, NewEmail.class);
                intent.putExtra("intenttype", "new");
                startActivity(intent);

            } else if (tab_no == 3) {
               /* Intent intent = new Intent(Dashboard.this, NewFacebook.class);
                intent.putExtra("intenttype", "new");
                startActivity(intent);*/
                //  if (isFb) {
                if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                    Intent intent = new Intent(Dashboard.this, NewFacebook.class);
                    intent.putExtra("intenttype", "new");
                    startActivity(intent);

                    //isFb = false;
                }
            } else if (tab_no == 4) {
                if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                    Intent intent = new Intent(Dashboard.this, NewTweet.class);
                    intent.putExtra("intenttype", "new");
                    startActivity(intent);
                    // isTw = false;
                }
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.chat_section = 0;
        FragmentChat.contenttype = "recent";
        newchat = true;
        isloademail = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {


            NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notifManager.cancelAll();


        } catch (Exception e) {
        }
    }

    /*********************************
     * Initialize UI
     *******************************/
    private void init() {
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        dataqyery = new DatabaseQueries(Dashboard.this);
        setSupportActionBar(toolbar);
        setTitle("");

        tabs();
        pagerset();
    }

    //*************Generate Tabls*******************
    private void tabs() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        tabChat = tabLayout.newTab().setCustomView(TabUtils.renderTabView(Dashboard.this, 0, 0, 0));
        tabLayout.addTab(tabChat);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.select_sms));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.select_email));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.select_facebook));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.select_twitter));
        tabLayout.setBackgroundColor((Color.parseColor("#f9f9f9")));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        title.setText("Delayd");
        titles.add("Delayd");
        titles.add("SMS");
        titles.add("Email");
        titles.add("Facebook");
        titles.add("Twitter");

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(5);

    }


    //************Setup pager******************
    private void pagerset() {
        adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        //  tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);


            }
        });


        //***************************************************
    }

    /*************************************
     * Load emails
     ***********************************/
    class getemail extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            Methods.getNameEmailDetails(Dashboard.this);
           /* for (int i = 0; i < emails.size(); i++) {
                item = new Person(emails.get(i).email, emails.get(i).email);
                userlist.add(item);

            }*/

            isloademail = true;
            return null;
        }
    }

    /*************************************
     * Update contacts
     ***********************************/
    class contactsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Methods.FetchContacts(Dashboard.this);
                arr_cont = dataqyery.getupdateduserlist();

                if (arr_cont.size() > 0) {
                    String json = new Gson().toJson(arr_cont);
                    //
                    String auth_code = prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String status = object.getString("status");
                    if (status.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject obj = jsonArray.getJSONObject(i);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = obj.getString("delayd_user_id");
                            model.profile_pic = obj.getString("profile_pic");
                            model.last_updated_date = obj.getString("last_updated_date");
                            model.contact_no = obj.getString("no");
                            model.country_code = obj.getString("country_code");
                            model.delayed_user = "1";
                            //delayedContacts.add(model);
                            dataqyery.UpdateContacts(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }


    private String ResponseMethod(String auth_code, String data) {
        String res = "";

        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }

    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermission() {

        int hasSMSPermission = ActivityCompat.checkSelfPermission(Dashboard.this, android.Manifest.permission.READ_CONTACTS);
        List<String> permissions = new ArrayList<String>();

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.READ_CONTACTS);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(Dashboard.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            new getemail().execute();
            new contactsync().execute();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        new getemail().execute();
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(tabLayout, "Your contacts will not loaded in the app", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
