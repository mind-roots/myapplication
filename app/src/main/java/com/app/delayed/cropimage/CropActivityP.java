package com.app.delayed.cropimage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import com.app.delayed.R;
import com.app.delayed.utils.Utils;

import java.io.ByteArrayOutputStream;


/**
 * Created by abc on 11/21/2015.
 */
public class CropActivityP extends Activity {

    CropImageView cropImageView;
    FrameLayout btn_cancel,btn_done;
    Uri uri;

    @SuppressLint("NewApi")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop__activity_crop);
        cropImageView=(CropImageView) findViewById(R.id.crop_image);
        cropImageView.setImageUri(Utils.selected_image);
        cropImageView.setAspectRatio(1,1);
        cropImageView.setFixedAspectRatio(true);
        btn_cancel = (FrameLayout)findViewById(R.id.btn_cancel);
        btn_done = (FrameLayout)findViewById(R.id.btn_done);


        btn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Utils.croppedImage = cropImageView.getCroppedImage();
                Utils.pimageUri = getImageUri(getApplicationContext(), Utils.croppedImage);
                Utils.croppedImage.recycle();
                Intent in = new Intent();
                setResult(RESULT_OK, in);
                finish();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.change_image = false;
                Intent in = new Intent();
                setResult(RESULT_CANCELED, in);
                finish();
            }
        });


    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Log.i("Width: ",""+inImage.getWidth());
        if(inImage.getWidth()>500){
            inImage = Bitmap.createScaledBitmap(inImage,500,500,false);
        }
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }
}