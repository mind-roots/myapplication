package com.app.delayed;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by Balvinder on 4/6/2016.
 */
public class InstaShake extends Activity {

    RelativeLayout mGotIt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shake_view);

        mGotIt = (RelativeLayout)findViewById(R.id.rl_gotit);

        mGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
