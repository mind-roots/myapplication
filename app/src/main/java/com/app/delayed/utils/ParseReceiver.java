package com.app.delayed.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;

import com.app.delayed.SplashActivity;
import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.chatExtended.CustomAdapters.ChatAdapter;
import com.app.delayed.chatExtended.CustomAdapters.ContactAdapter;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.chatExtended.util.CustomComparator;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.model.JSONParser;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * Created by user on 12/2/2015.
 */
public class ParseReceiver extends ParsePushBroadcastReceiver {
    Intent activityIntent;
    String auth_code, userid, timeformat, username, userpic;
    String content_sync_id, thread_sync_id, current_thread;
    SharedPreferences prefs;
    boolean network;
    DatabaseQueries dataquery;
    Context con;
    List<ActivityManager.RunningTaskInfo> tasks;
    Intent parentintent;
    Class<? extends Activity> cls;
    String mThreadId;
    boolean classtatus;
    int changeofse = 0;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.con = context;

        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            mThreadId = pushData.optString("thread_id");
            System.out.print("push response = " + pushData);
        } catch (JSONException e) {
            e.printStackTrace();
            //type = "no";
        }

        classtatus = isApplicationSentToBackground(context);

        prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
        network = Methods.isNetworkConnected(context);
        auth_code = prefs.getString("auth_code", null);
        timeformat = prefs.getString("timeformat", null);
        userid = prefs.getString("id", null);
        username = prefs.getString("name", null);
        userpic = prefs.getString("chat_icon", null);
        dataquery = new DatabaseQueries(context);
        current_thread = prefs.getString("current_thread", null);
        if (classtatus) {

            if (!mThreadId.equals(current_thread)) {
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(context, notification);
                    r.play();
                    vibrate(context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            super.onReceive(context, intent);
        }
        content_sync_id = dataquery.getsynchid("message_thread_content", "content_sync_id");
        thread_sync_id = dataquery.getsynchid("message_threads", "thread_sync_id");
        if (!network) {
            Methods.conDialog(context);

        } else {

            new getthreadcontent(content_sync_id).execute();

        }
    }

    @Override
    public void onPushOpen(Context context, Intent intent) {
        Log.e("Push", "Clicked");
        con = context;
        parentintent = intent;
        ParseAnalytics.trackAppOpenedInBackground(intent);

        cls = getActivity(context, intent);
        if (classtatus) {
        } else {
            activityIntent = new Intent(context, SplashActivity.class);
            passIntent();
        }

        //  Toast.makeText(context, "push open", Toast.LENGTH_SHORT).show();


      /*  if (uriString != null && !uriString.isEmpty()) {
            activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
        } else {
            if (statust) {

                if (type.equals("message_thread")) {
                   getmessagesopend();

                } else if (type.equals("appointment")) {
                    getAppointmentopend();
                }else if (type.equals("new_listings")){
                    getlistingopend();
                }else
                {}
            } else {
                SplashActivity.push=true;
                activityIntent = new Intent(context, SplashActivity.class);
                if (type.equals("message_thread")) {
                    activityIntent.putExtra("Intenttype", "parsmsg");
                } else if (type.equals("appointment")) {
                    activityIntent.putExtra("Intenttype", "parsapoint");
                }else if (type.equals("new_listings")){
                    activityIntent.putExtra("Intenttype", "newhome");
                }else{
                    activityIntent.putExtra("Intenttype", "home");
                }
                activityIntent.putExtras(intent.getExtras());
                passIntent();

            }

        }
*/


    }

    class getthreadcontent extends AsyncTask<String, Void, String> {
        String response, syncid;

        public getthreadcontent(String content_sync_id) {
            this.syncid = content_sync_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            response = schdule_method("get_new_content", syncid);
            System.out.print(response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    ModelMsgThrdContent item = new ModelMsgThrdContent();
                    JSONArray jarr = obj.optJSONArray("data");
                    changeofse = jarr.length();
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        String content_id = dobj.optString("content_id");
                        item.content_id = content_id;
                        item.user_id = dobj.optString("user_id");
                        item.type = dobj.optString("type");
                        item.content = dobj.optString("content");
                        item.content_time = dobj.optString("content_time");
                        item.thumb_url = dobj.optString("thumb_url");
                        //  new downloadimage(item.thumb_url, item.thread_id, item.content_id).execute();
                        String cstatus= dobj.optString("status");
                        item.status = dobj.optString("status");
                        String content_sync = dobj.optString("content_sync_id");
                        item.content_sync_id = content_sync;
                        item.last_updated = dobj.optString("last_updated");
                        item.schduled_date = "";
                        Log.i("Last_updated : ", "" + dobj.optString("last_updated"));
                        if (classtatus) {
                            if (current_thread.equals("all")) {
                                item.read_status = "0";
                            } else {
                                if (Utils.chat_section == 0) {
                                    if (item.thread_id.equals(current_thread)) {
                                        item.read_status = "1";
                                        JSONParser parser = new JSONParser();
                                        Uri.Builder builder = new Uri.Builder()
                                                .appendQueryParameter("service_type", "content_read")
                                                .appendQueryParameter("auth_code", auth_code)
                                                .appendQueryParameter("content_id", content_id);
                                        parser.getJSONFromUrl(Utils.base_url, builder);
                                    } else {
                                        item.read_status = "0";
                                    }
                                } else {
                                    item.read_status = "0";
                                }
                            }
                        } else {
                            item.read_status = "0";
                        }
                        if (item.type.equals("0")) {
                            //   publishProgress(item.content);/
                        } else {
                        }
                        if (dataquery.contentexist(content_id)) {
                            dataquery.updatemsgcontent(content_id, content_sync,cstatus);
                        } else {
                            dataquery.insertmsgthreadcontent(item);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new getthread(thread_sync_id).execute();
        }
    }

    private String schdule_method(String service_type, String id) {
        String res = null;
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("content_sync_id", id);


        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;

    }

    class getthread extends AsyncTask<String, Void, String> {
        String schedule_response, sync_ids;

        public getthread(String ids) {
            this.sync_ids = ids;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {


            //--------------------get_schedule_email----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Get_thread_Method("get_new_threads", auth_code, sync_ids);
            System.out.println("schedule response:: " + schedule_response);


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {
                JSONObject obj = new JSONObject(schedule_response);
                String status = obj.optString("status");
                if (status.equals("true")) {

                    JSONArray jarr = obj.optJSONArray("data");
                    for (int i = 0; i < jarr.length(); i++) {
                        ModelMsgThread item = new ModelMsgThread();
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        item.participant_ids = dobj.optString("participant_ids");
                        item.participant_numbers = dobj.optString("participant_nos");
                        item.group_name = dobj.optString("group_name");
                        item.group_icon = dobj.optString("group_icon");
                        item.thread_sync_id = dobj.optString("thread_sync_id");
                        item.last_updated = dobj.optString("last_updated");
                        item.notification_status = dobj.optString("notification_status");
                        item.blocked_status = dobj.optString("blocked_status");
                        // thread_list.add(item);
                        if (dataquery.isthreadexist(item.thread_id)) {
                        } else {
                            dataquery.insertmsgthread(item);
                        }
                    }

                }

                if (classtatus) {
                    if (Utils.chat_section == 0) {
                        if (current_thread.equals("all")) {

                            if (Utils.chat_section == 0) {
                                FragmentChat.thread_list = dataquery.getallmsgs(userid, "0",username,userpic);
                            } else if (Utils.chat_section == 1) {
                                FragmentChat.thread_list = dataquery.getallmsgs(userid, "1",username,userpic);
                            } else {
                                FragmentChat.thread_list = dataquery.getallmsgs(userid, "2",username,userpic);
                            }
                            if (FragmentChat.thread_list.size() > 0) {
                                FragmentChat.mDummyView.setVisibility(View.GONE);
                                Collections.sort(FragmentChat.thread_list, new CustomComparator());
                                Collections.reverse(FragmentChat.thread_list);
                                FragmentChat.adapter = new ContactAdapter(con, FragmentChat.thread_list);
                                FragmentChat.contact_list.setAdapter(FragmentChat.adapter);
                            } else {
                                FragmentChat.mDummyView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            //      ChattingActivity.listMessage = dataquery.getthreadchat(current_thread, con, userid, ChattingActivity.mPic, ChattingActivity.mName, userpic, username, "0");


                            int offset = ChattingActivity.offset + 20 + changeofse;

                            ChattingActivity.listMessage = dataquery.getlimitthreadchat(current_thread, con, userid, ChattingActivity.mPic, ChattingActivity.mName, userpic, username, "0", 0, offset);
                            ChattingActivity.offset = offset - 20;
                            //  ChattingActivity.chatAdapter.notifyDataSetChanged();
                            ChattingActivity.chatAdapter = new ChatAdapter(con, ChattingActivity.listMessage);
                            ChattingActivity.listView.setAdapter(ChattingActivity.chatAdapter);
                            if (ChattingActivity.update) {
                                ChattingActivity.listView.setSelection(ChattingActivity.listMessage.size() - 1);

                            } else {
                                ChattingActivity.listView.setSelection(ChattingActivity.lastpos);

                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public String Get_thread_Method(String service_type, String auth_code, String syncid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_sync_id", syncid);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void passIntent() {
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(con);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            con.startActivity(activityIntent);
        }


    }


    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }


    /**************************************
     * Vibrate device
     ************************************/
    private void vibrate(Context ctx) {
        // TODO Auto-generated method stub
        try {
            Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}