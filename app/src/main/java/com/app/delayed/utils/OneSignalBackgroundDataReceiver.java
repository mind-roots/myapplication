package com.app.delayed.utils;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;

import com.app.delayed.Dashboard;
import com.app.delayed.R;
import com.app.delayed.adapters.Facebook_Adapter;
import com.app.delayed.adapters.ScheduleEmailAdapter;
import com.app.delayed.adapters.ScheduleSmsAdapter;
import com.app.delayed.adapters.TweeterAdapter;
import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.chatExtended.CustomAdapters.ChatAdapter;
import com.app.delayed.chatExtended.CustomAdapters.ContactAdapter;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.chatExtended.MyAlertDialog;
import com.app.delayed.chatExtended.util.ChatComparator;
import com.app.delayed.chatExtended.util.CustomComparator;
import com.app.delayed.chatExtended.util.TabUtils;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.fragments.FragmentFacebook;
import com.app.delayed.fragments.FragmentSms;
import com.app.delayed.fragments.FragmentTwitter;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.model.ModelScheduleEmail;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.model.ModelTwitterData;
import com.app.delayed.model.Model_facebook;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class OneSignalBackgroundDataReceiver extends WakefulBroadcastReceiver {
    public static boolean isPush = false;

    String gettype, getthread_id, getcontent_id, getcontent_sync_id, status;
    List<ModelContacts> delayedContacts = new ArrayList<>();

    Intent activityIntent;
    String auth_code, userid, timeformat, username, userpic;
    String content_sync_id, thread_sync_id, current_thread;
    SharedPreferences prefs;
    boolean network;
    DatabaseQueries dataquery;
    Context con;
    List<ActivityManager.RunningTaskInfo> tasks;
    Intent parentintent;
    Class<? extends Activity> cls;

    boolean classtatus;
    int changeofse = 0;
    Context context;
    String notification_status;
    String confirmation;
    boolean isScreenOn;
    String message;

    public void onReceive(Context context, Intent intent) {
        this.context = context;
        try {
            PowerManager pm = (PowerManager)
                    context.getSystemService(Context.POWER_SERVICE);
            isScreenOn = pm.isScreenOn();

            Bundle dataBundle = intent.getBundleExtra("data");
            con = context;
            prefs = context.getSharedPreferences("delayed", Context.MODE_PRIVATE);
            Log.i("OneSignalExample", "Is Your App Active: " + dataBundle.getBoolean("isActive"));
            message = dataBundle.getString("alert");
            //      if (dataBundle.getBoolean("isActive")) {
            Log.i("OneSignalExample", "data additionalData: " + dataBundle.getString("custom"));
            JSONObject customJSON = new JSONObject(dataBundle.getString("custom"));
            if (customJSON.has("a")) {

                JSONObject additionalData = customJSON.getJSONObject("a");
                gettype = additionalData.optString("type");
                getthread_id = additionalData.optString("thread_id");
                if (gettype.equals("chat")) {

                    getcontent_id = additionalData.optString("content_id");
                    getcontent_sync_id = additionalData.optString("content_sync_id");
                    confirmation = additionalData.optString("confirmation");
                }/*else if (gettype.equals("SMS")){

                    message = additionalData.optString("");

                }else if (gettype.equals("Email")){

                    message = additionalData.optString("");

                }else if (gettype.equals("Facebook")) {

                    message = additionalData.optString("");

                }else if (gettype.equals("Twitter")){

                    message = additionalData.optString("");

                }*/

            }


            //-----------initialization---------------------//
            classtatus = isApplicationSentToBackground(context);

            prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
            network = Methods.isNetworkConnected(context);
            auth_code = prefs.getString("auth_code", null);
            timeformat = prefs.getString("timeformat", null);
            userid = prefs.getString("id", null);
            username = prefs.getString("name", null);
            userpic = prefs.getString("chat_icon", null);
            dataquery = new DatabaseQueries(context);
            current_thread = prefs.getString("current_thread", null);

            System.out.println("noti=" + notification_status);
         /*   if (classtatus) {

                if (!getthread_id.equals(current_thread)) {
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(context, notification);
                        r.play();
                        vibrate(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }*/
            content_sync_id = dataquery.getsynchid("message_thread_content", "content_sync_id");
            thread_sync_id = dataquery.getsynchid("message_threads", "thread_sync_id");
            if (!network) {
                Methods.conDialog(context);

            } else {
                if (gettype.equals("chat")) {
                    new getthreadcontent(content_sync_id).execute();
                } else if (gettype.equals("SMS")) {
                   /* if (classtatus && isScreenOn) {

                        Intent i=new Intent(context.getApplicationContext(),MyAlertDialog.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("msg","Your SMS has been sent.");
                        context.startActivity(i);
                    }*/
                   /* new getsms().execute();*/

                    setAlarms(context);

                } else if (gettype.equals("Email")) {
                    if (classtatus && isScreenOn) {

                        Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("msg", message);
                        context.startActivity(i);
                    }
                    new getEmail().execute();
                } else if (gettype.equals("Facebook")) {
                    if (classtatus && isScreenOn) {

                        Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("msg", message);
                        context.startActivity(i);
                    }
                    new facebook_thread().execute();
                } else if (gettype.equals("Twitter")) {
                    if (classtatus && isScreenOn) {

                        Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("msg", message);
                        context.startActivity(i);
                    }
                    new tweeter_thread().execute();
                }
            }

            //   }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    //----------------------chat views----------------------//
    class getthreadcontent extends AsyncTask<String, Void, String> {
        String response, syncid;
        String cstatus = "";

        public getthreadcontent(String content_sync_id) {
            this.syncid = content_sync_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            response = schdule_method("get_new_content", syncid);
            System.out.print("rec=" + response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    ModelMsgThrdContent item = new ModelMsgThrdContent();
                    JSONArray jarr = obj.optJSONArray("data");
                    changeofse = jarr.length();
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        String content_id = dobj.optString("content_id");
                        item.content_id = content_id;
                        item.user_id = dobj.optString("user_id");
                        item.type = dobj.optString("type");
                        //  String content = URLDecoder.decode(dobj.optString("content"), "UTF-8");
                        item.content = dobj.optString("content");
                        item.content_time = dobj.optString("content_time");
                        item.thumb_url = dobj.optString("thumb_url");
                        //  new downloadimage(item.thumb_url, item.thread_id, item.content_id).execute();
                        cstatus = dobj.optString("status");
                        item.status = dobj.optString("status");
                        String content_sync = dobj.optString("content_sync_id");
                        item.scheduled_type = dobj.optString("scheduled_type");
                        item.content_sync_id = content_sync;
                        item.last_updated = dobj.optString("last_updated");
                        item.schduled_date = "";
                        Log.i("Last_updated : ", "" + dobj.optString("last_updated"));

                        /*******************************************************************
                         * Sync contact if it is in your user table but not as delayed user
                         * *****************************************************************/
                        if (!item.user_id.equals(userid)) {

                            if (!dataquery.contactexist(item.user_id)) {
                                fetchSingleContact();
                            }
                            //  ********************************************************
                        }


                        //-------------new---------------//
                        String participants = dataquery.getParticipents(item.thread_id);
                        if (classtatus) {
                            if (current_thread.equals("all")) {


                                if (item.user_id.equals(userid)) {
                                    item.read_status = "1";
                                } else {
                                    item.read_status = "0";
                                }

                               /* if (item.scheduled_type.equals(null)) {
                                    if (item.user_id.equals(userid)) {
                                        item.read_status = "1";
                                    } else {
                                        item.read_status = "0";
                                    }
                                } else {
                                    item.read_status = "1";
                                }*/

                            } else {
                                if (Utils.chat_section == 0) {
                                    if (item.thread_id.equals(current_thread)) {
                                        item.read_status = "1";
                                        JSONParser parser = new JSONParser();
                                        Uri.Builder builder = new Uri.Builder()
                                                .appendQueryParameter("service_type", "content_read")
                                                .appendQueryParameter("auth_code", auth_code)
                                                .appendQueryParameter("content_id", content_id);
                                        parser.getJSONFromUrl(Utils.base_url, builder);
                                    } else {
                                        if (item.user_id.equals(userid)) {
                                            item.read_status = "1";
                                        } else {
                                            item.read_status = "0";
                                        }

                                    }
                                } else {
                                    if (item.user_id.equals(userid)) {
                                        item.read_status = "1";
                                    } else {
                                        item.read_status = "0";
                                    }

                                }
                            }
                        } else {
                            if (item.user_id.equals(userid)) {
                                item.read_status = "1";
                            } else {
                                item.read_status = "0";
                            }

                        }
                        if (item.type.equals("0")) {
                            //   publishProgress(item.content);/
                        } else {
                        }
                        if (dataquery.contentexist(content_id)) {
                            if (participants.equals(userid) && !participants.equals("")) {
                                dataquery.updatemyread(content_id, content_sync, cstatus);
                            }
                            dataquery.updatemsgcontent(content_id, content_sync, cstatus);
                        } else {
                            dataquery.insertmsgthreadcontent(item);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        private void fetchSingleContact() {
            try {
                delayedContacts = dataquery.getupdateduserlist();

                if (delayedContacts.size() > 0) {
                    String json = new Gson().toJson(delayedContacts);
                    //
                    String auth_code = FragmentChat.prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String statusc = object.getString("status");
                    if (statusc.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int k = 0; k < jsonArray.length(); k++) {

                            JSONObject objc = jsonArray.getJSONObject(k);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = objc.getString("delayd_user_id");
                            model.name = objc.getString("name");
                            model.profile_pic = objc.getString("profile_pic");
                            model.last_updated_date = objc.getString("last_updated_date");
                            model.contact_no = objc.getString("no");
                            model.country_code = objc.getString("country_code");
                            model.delayed_user = "1";

                            //delayedContacts.add(model);
                            dataquery.UpdateContacts2(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                if (cstatus.equals("4")) {
                    new updateinfo(getthread_id).execute();
                }

            } catch (Exception e) {
            }

            new getthread(thread_sync_id).execute();
        }
    }

    private String schdule_method(String service_type, String id) {
        String res = null;
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("content_sync_id", id);


        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;

    }

    class getthread extends AsyncTask<String, Void, String> {
        String schedule_response, sync_ids;

        public getthread(String ids) {
            this.sync_ids = ids;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {


            //--------------------get_schedule_email----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Get_thread_Method("get_new_threads", auth_code, sync_ids);
            System.out.println("schedule response:: " + schedule_response);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {
                JSONObject obj = new JSONObject(schedule_response);
                String status = obj.optString("status");
                if (status.equals("true")) {

                    JSONArray jarr = obj.optJSONArray("data");
                    for (int i = 0; i < jarr.length(); i++) {
                        ModelMsgThread item = new ModelMsgThread();
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        item.participant_ids = dobj.optString("participant_ids");
                        item.participant_numbers = dobj.optString("participant_nos");
                        // String name = URLDecoder.decode(dobj.optString("group_name"),"UTF-8");
                        item.group_name = dobj.optString("group_name");
                        item.group_icon = dobj.optString("group_icon");
                        item.thread_sync_id = dobj.optString("thread_sync_id");
                        item.last_updated = dobj.optString("last_updated");
                        String status_n = dobj.optString("notification_status");
                        item.notification_status = dobj.optString("notification_status");
                        item.blocked_status = dobj.optString("blocked_status");
                        // thread_list.add(item);
                        if (dataquery.isthreadexist(item.thread_id)) {
                        } else {
                            dataquery.insertmsgthread(item);
                        }
                    }

                }
                notification_status = dataquery.getnotification(getthread_id);


                if (classtatus) {
                    if (isScreenOn) {
                        if (!getthread_id.equals(current_thread)) {
                            if (notification_status.equals("0")) {
                                try {
                                    if (confirmation.equals("false")) {

                                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                        Ringtone r = RingtoneManager.getRingtone(context, notification);
                                        r.play();
                                        vibrate(context);
//                                        Intent i=new Intent(context.getApplicationContext(),MyAlertDialog.class);
//                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        context.startActivity(i);

                                    } else if (confirmation.equals("true")) {

                                        Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        i.putExtra("msg", "Your Delayd has been sent");
                                        context.startActivity(i);
                                        // Toast.makeText(context, "Your Delayd message has been sent", Toast.LENGTH_SHORT).show();
//                                        new android.support.v7.app.AlertDialog.Builder(((Activity) context), R.style.AppTheme_Dialog)
//                                                .setTitle("Confirmation")
//                                                .setMessage("Your Delayd message has been sent")
//
//                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        dialog.dismiss();
//                                                    }
//                                                }).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    // if (Utils.chat_section == 0) {
                    if (current_thread.equals("all")) {

                        if (Utils.chat_section == 0) {
                            FragmentChat.thread_list = dataquery.getallmsgs(userid, "0", username, userpic);
                        } else if (Utils.chat_section == 1) {
                            FragmentChat.thread_list = dataquery.getallmsgs(userid, "1", username, userpic);
                        } else {
                            FragmentChat.thread_list = dataquery.getallmsgs(userid, "2", username, userpic);
                        }
                        if (FragmentChat.thread_list.size() > 0) {
                            FragmentChat.contact_list.setVisibility(View.VISIBLE);
                            FragmentChat.mDummyView.setVisibility(View.GONE);
                            Collections.sort(FragmentChat.thread_list, new CustomComparator());
                            Collections.reverse(FragmentChat.thread_list);
                            FragmentChat.adapter = new ContactAdapter(con, FragmentChat.thread_list);
                            FragmentChat.contact_list.setAdapter(FragmentChat.adapter);
                            TabUtils.updateTabBadge(Dashboard.tabChat, DatabaseQueries.unreadcount);
                        } else {
                            FragmentChat.contact_list.setVisibility(View.GONE);
                            FragmentChat.mDummyView.setVisibility(View.VISIBLE);
                            if (Utils.chat_section == 2) {
                                FragmentChat.mDummyText.setText("You have no recurring chats");
                                FragmentChat.mBtnCompose.setVisibility(View.INVISIBLE);
                            } else if (Utils.chat_section == 1) {
                                FragmentChat.mDummyText.setText("You have no scheduled chats");
                                FragmentChat.mBtnCompose.setVisibility(View.INVISIBLE);
                            } else {
                                FragmentChat.mDummyText.setText("You have no recent chats");
                                FragmentChat.mBtnCompose.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        //      ChattingActivity.listMessage = dataquery.getthreadchat(current_thread, con, userid, ChattingActivity.mPic, ChattingActivity.mName, userpic, username, "0");

                        int offset = ChattingActivity.offset + 20 + changeofse;
                        String delayed = "0";
                        if (Utils.chat_section == 0) {
                            delayed = "0";
                        } else if (Utils.chat_section == 1) {
                            delayed = "1";
                        } else {
                            delayed = "2";
                        }
                        ChattingActivity.listMessage = dataquery.getlimitthreadchat(current_thread, con, userid, ChattingActivity.mPic, ChattingActivity.mName, userpic, username, delayed, 0, offset);
                        ChattingActivity.offset = offset - 20;
                        //  ChattingActivity.chatAdapter.notifyDataSetChanged();
                        Collections.sort(ChattingActivity.listMessage, new ChatComparator());
                        //Collections.reverse(ChattingActivity.listMessage);
                        ChattingActivity.chatAdapter = new ChatAdapter(con, ChattingActivity.listMessage);
                        ChattingActivity.listView.setAdapter(ChattingActivity.chatAdapter);
                        if (ChattingActivity.update) {
                            System.out.println("ids1111=");
                            ChattingActivity.listView.setSelection(ChattingActivity.listMessage.size());

                            // ChattingActivity.listView.setStackFromBottom(true);
                        } else {
                            //    ChattingActivity.listView.setStackFromBottom(false);
                            System.out.println("ids2222=");
                            ChattingActivity.listView.setSelection(ChattingActivity.lastpos);

                        }
                        //   }
                    }

//                    if (FragmentChat.thread_list.size() > 0) {
//                        TabUtils.updateTabBadge(Dashboard.tabChat, DatabaseQueries.unreadcount);
//                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public String Get_thread_Method(String service_type, String auth_code, String syncid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_sync_id", syncid);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void passIntent() {
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(con);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            con.startActivity(activityIntent);
        }


    }


    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }


    /**************************************
     * Vibrate device
     ************************************/
    private void vibrate(Context ctx) {
        try {
            Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------------------SMS View Update---------------------------//
    class getsms extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentSms.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Getsms_Method("get_schedule_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentSms.schdulesmslist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.schdulesmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            // String gname = URLDecoder.decode(iobj.optString("group_name"),"UTF-8");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.schdulesmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentSms.recurringsmslist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.recurringsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.recurringsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getsms_Method("get_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentSms.sentsmslist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.sentsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.sentsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentSms.progressbar.setVisibility(View.GONE);
            try {
                if (FragmentSms.adaptertype.equals("0")) {
                    if (statuss.equals("true")) {
                        if (FragmentSms.schdulesmslist.size() != 0) {
                            FragmentSms.view_dummy.setVisibility(View.GONE);
                            FragmentSms.view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibility();
                            /*if (Dashboard.viewshow) {
                                Dashboard.viewshow = false;
                                if (schdulesmslist.size() == 1) {
                                    startActivity(new Intent(getActivity(), FirstSchduled.class));

                                }
                            }*/
                            FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.schdulesmslist, "sch");
                            FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                        } else {
                            FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                            FragmentSms.compose.setVisibility(View.VISIBLE);
                            FragmentSms.view_scheduled.setVisibility(View.GONE);
                            FragmentSms.dummy_text.setText("You have no SMS scheduled");
                        }
                    } else {
                        FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                        FragmentSms.compose.setVisibility(View.VISIBLE);
                        FragmentSms.view_scheduled.setVisibility(View.GONE);
                        FragmentSms.dummy_text.setText("You have no SMS scheduled");
                    }
                } else {

                    if (statusr.equals("true")) {
                        if (FragmentSms.recurringsmslist.size() != 0) {
                            FragmentSms.view_dummy.setVisibility(View.GONE);
                            FragmentSms.view_scheduled.setVisibility(View.VISIBLE);
                            recurrvisibility();
                            /*if (Dashboard.viewshow) {
                                Dashboard.viewshow = false;
                                if (recurringsmslist.size() == 1) {
                                    startActivity(new Intent(getActivity(), FirstSchduled.class));

                                }
                            }*/
                            FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.recurringsmslist, "rec");
                            FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                        } else {
                            FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                            FragmentSms.compose.setVisibility(View.VISIBLE);
                            FragmentSms.view_scheduled.setVisibility(View.GONE);
                            FragmentSms.dummy_text.setText("You have no SMS recurring");
                        }
                    } else {
                        FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                        FragmentSms.compose.setVisibility(View.VISIBLE);
                        FragmentSms.view_scheduled.setVisibility(View.GONE);
                        FragmentSms.dummy_text.setText("You have no SMS recurring");
                    }

                }
            } catch (Exception e) {
            }
        }
    }

    public String Getsms_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void recurrvisibility() {

        FragmentSms.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        FragmentSms.rl_schdule.setBackgroundResource(R.drawable.white_left);
        FragmentSms.rl_sent.setBackgroundResource(R.drawable.white_right);


        FragmentSms.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        FragmentSms.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentSms.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibility() {
        FragmentSms.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        FragmentSms.rl_schdule.setBackgroundResource(R.drawable.blue_left);
        FragmentSms.rl_sent.setBackgroundResource(R.drawable.white_right);

        FragmentSms.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        FragmentSms.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentSms.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

    //------------------------email update----------------------//
    class getEmail extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, email_response, status_schedule, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentEmail.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {


            //--------------------get_schedule_email----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Getsms_Method("get_schedule_email", auth_code);
            System.out.println("schedule response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(schedule_response);
                status_schedule = jobj.optString("status");
                FragmentEmail.schduleemaillist.clear();
                if (status_schedule.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iterator = obj.keys();
                    while (iterator.hasNext()) {

                        String key = iterator.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.schduleemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String msg = URLDecoder.decode(iobj.optString("message"),"UTF-8");
                            item.message = iobj.optString("message");
                            //String sub = URLDecoder.decode(iobj.optString("subject"),"UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.schduleemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            //   }
            //--------------------get_recurring_email----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_email", auth_code);
            System.out.println("recurring response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentEmail.recurringemaillist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.recurringemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String msg = URLDecoder.decode(iobj.optString("message"),"UTF-8");
                            item.message = iobj.optString("message");
                            // String sub = URLDecoder.decode(iobj.optString("subject"),"UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.recurringemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_email-------------------------------//
            email_response = Getsms_Method("get_email", auth_code);
            System.out.println("email response:: " + schedule_response);

            try {
                JSONObject jobj = new JSONObject(email_response);
                statust = jobj.optString("status");
                FragmentEmail.sentemaillist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.sentemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String msg = URLDecoder.decode(iobj.optString("message"),"UTF-8");
                            item.message = iobj.optString("message");
                            // String sub = URLDecoder.decode(iobj.optString("subject"),"UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.sentemaillist.add(item);

                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentEmail.progressbar.setVisibility(View.GONE);
            try {
                if (FragmentEmail.adaptertype.equals("0")) {
                    if (status_schedule.equals("true")) {
                        if (FragmentEmail.schduleemaillist.size() != 0) {
                            FragmentEmail.view_dummy.setVisibility(View.GONE);
                            FragmentEmail.view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibilityy();
                            FragmentEmail.adapter = new ScheduleEmailAdapter(con, FragmentEmail.schduleemaillist, "sch");
                            FragmentEmail.recycler_list_email.setAdapter(FragmentEmail.adapter);
                        } else {
                            FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                            FragmentEmail.compose.setVisibility(View.VISIBLE);
                            FragmentEmail.view_scheduled.setVisibility(View.GONE);
                            FragmentEmail.dummy_text.setText("You have no Emails scheduled");
                        }
                    } else {
                        FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                        FragmentEmail.compose.setVisibility(View.VISIBLE);
                        FragmentEmail.view_scheduled.setVisibility(View.GONE);
                        FragmentEmail.dummy_text.setText("You have no Emails scheduled");
                    }
                } else {

                    if (statusr.equals("true")) {
                        if (FragmentEmail.recurringemaillist.size() != 0) {
                            FragmentEmail.view_dummy.setVisibility(View.GONE);
                            FragmentEmail.view_scheduled.setVisibility(View.VISIBLE);
                            recurrvisibilityy();
                            FragmentEmail.adapter = new ScheduleEmailAdapter(con, FragmentEmail.recurringemaillist, "rec");
                            FragmentEmail.recycler_list_email.setAdapter(FragmentEmail.adapter);
                        } else {
                            FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                            FragmentEmail.compose.setVisibility(View.VISIBLE);
                            FragmentEmail.view_scheduled.setVisibility(View.GONE);
                            FragmentEmail.dummy_text.setText("You have no Emails recurring");
                        }
                    } else {
                        FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                        FragmentEmail.compose.setVisibility(View.VISIBLE);
                        FragmentEmail.view_scheduled.setVisibility(View.GONE);
                        FragmentEmail.dummy_text.setText("You have no Emails recurring");
                    }

                }
            } catch (Exception e) {

            }


        }
    }

    private void recurrvisibilityy() {
        FragmentEmail.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        FragmentEmail.rl_schdule.setBackgroundResource(R.drawable.white_left);
        FragmentEmail.rl_sent.setBackgroundResource(R.drawable.white_right);


        FragmentEmail.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        FragmentEmail.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentEmail.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibilityy() {
        FragmentEmail.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        FragmentEmail.rl_schdule.setBackgroundResource(R.drawable.blue_left);
        FragmentEmail.rl_sent.setBackgroundResource(R.drawable.white_right);

        FragmentEmail.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        FragmentEmail.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentEmail.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

//****************************facebook_thread***************************************************

    class facebook_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentFacebook.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            System.out.println("Authcode::::::: " + auth_code);
            schedule_response = Getfb_Method("fb_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentFacebook.schdulefblist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.schdulefblist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook.schdulefblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String post = URLDecoder.decode( iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            FragmentFacebook.schdulefblist.add(item);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getfb_Method("facebook_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentFacebook.recurringfblist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.recurringfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook.recurringfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //  String post = URLDecoder.decode( iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            FragmentFacebook.recurringfblist.add(item);
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getfb_Method("get_facebook_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentFacebook.postfblist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.postfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook.postfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //   String post = URLDecoder.decode( iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            FragmentFacebook.postfblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentFacebook.progressbar.setVisibility(View.GONE);

            if (FragmentFacebook.adaptertype.equals("0")) {
                try {


                    if (statuss.equals("true")) {
                        if (FragmentFacebook.schdulefblist.size() != 0) {
                            FragmentFacebook.view_dummy.setVisibility(View.GONE);
                            FragmentFacebook.view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibilityf();
                            FragmentFacebook.adapter = new Facebook_Adapter(con, FragmentFacebook.schdulefblist, "sch");
                            FragmentFacebook.list_scheduled.setAdapter(FragmentFacebook.adapter);
                        } else {
                            FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                            FragmentFacebook.btn_compose.setVisibility(View.VISIBLE);
                            FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                            FragmentFacebook.dummy_text.setText("You have no Facebook posts scheduled");
                        }
                    } else {
                        FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                        FragmentFacebook.btn_compose.setVisibility(View.VISIBLE);
                        FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                        FragmentFacebook.dummy_text.setText("You have no Facebook posts scheduled");
                    }
                } catch (Exception e) {
                    FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                    FragmentFacebook.btn_compose.setVisibility(View.VISIBLE);
                    FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                    FragmentFacebook.dummy_text.setText("You have no Facebook posts scheduled");
                }
            } else {

                if (statusr.equals("true")) {
                    if (FragmentFacebook.recurringfblist.size() != 0) {
                        FragmentFacebook.view_dummy.setVisibility(View.GONE);
                        FragmentFacebook.view_scheduled.setVisibility(View.VISIBLE);
                        recurrvisibilityf();
                        FragmentFacebook.adapter = new Facebook_Adapter(con, FragmentFacebook.recurringfblist, "rec");
                        FragmentFacebook.list_scheduled.setAdapter(FragmentFacebook.adapter);
                    } else {
                        FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                        FragmentFacebook.btn_compose.setVisibility(View.VISIBLE);
                        FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                        FragmentFacebook.dummy_text.setText("You have no Facebook posts recurring");
                    }
                } else {
                    FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                    FragmentFacebook.btn_compose.setVisibility(View.VISIBLE);
                    FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                    FragmentFacebook.dummy_text.setText("You have no Facebook posts recurring");
                }

            }


        }
    }


    private void recurrvisibilityf() {
        FragmentFacebook.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        FragmentFacebook.rl_schdule.setBackgroundResource(R.drawable.white_left);
        FragmentFacebook.rl_sent.setBackgroundResource(R.drawable.white_right);


        FragmentFacebook.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        FragmentFacebook.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentFacebook.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibilityf() {
        FragmentFacebook.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        FragmentFacebook.rl_schdule.setBackgroundResource(R.drawable.blue_left);
        FragmentFacebook.rl_sent.setBackgroundResource(R.drawable.white_right);

        FragmentFacebook.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        FragmentFacebook.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentFacebook.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

    public String Getfb_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    //------------------------twitter----------------------//
    class tweeter_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentTwitter.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            schedule_response = Gettweet_Method("twitter_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentTwitter.schduletweetlist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.schduletweetlist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.schduletweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String post = URLDecoder.decode( iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.schduletweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Gettweet_Method("twitter_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentTwitter.recurringtweetlist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.recurringtweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.recurringtweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String post = URLDecoder.decode( iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.recurringtweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Gettweet_Method("get_twitter_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentTwitter.posttweetlist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.posttweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.posttweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                            // String post = URLDecoder.decode( iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.posttweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentTwitter.progressbar.setVisibility(View.GONE);

            if (FragmentTwitter.adaptertype.equals("0")) {
                try {
                    if (statuss.equals("true")) {
                        if (FragmentTwitter.schduletweetlist.size() != 0) {
                            FragmentTwitter.view_dummy.setVisibility(View.GONE);
                            FragmentTwitter.view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibilityt();
                            FragmentTwitter.adapter = new TweeterAdapter(con, FragmentTwitter.schduletweetlist, "sch");
                            FragmentTwitter.list_tw.setAdapter(FragmentTwitter.adapter);
                        } else {
                            FragmentTwitter.view_dummy.setVisibility(View.VISIBLE);
                            FragmentTwitter.btn_compose.setVisibility(View.VISIBLE);
                            FragmentTwitter.view_scheduled.setVisibility(View.GONE);
                            FragmentTwitter.dummy_text.setText("You have no Tweets scheduled");
                        }
                    } else {
                        FragmentTwitter.view_dummy.setVisibility(View.VISIBLE);
                        FragmentTwitter.btn_compose.setVisibility(View.VISIBLE);
                        FragmentTwitter.view_scheduled.setVisibility(View.GONE);
                        FragmentTwitter.dummy_text.setText("You have no Tweets scheduled");
                    }
                } catch (Exception e) {
                }
            } else {

                if (statusr.equals("true")) {
                    if (FragmentTwitter.recurringtweetlist.size() != 0) {
                        FragmentTwitter.view_dummy.setVisibility(View.GONE);
                        FragmentTwitter.view_scheduled.setVisibility(View.VISIBLE);
                        recurrvisibilityt();
                        FragmentTwitter.adapter = new TweeterAdapter(con, FragmentTwitter.recurringtweetlist, "rec");
                        FragmentTwitter.list_tw.setAdapter(FragmentTwitter.adapter);
                    } else {
                        FragmentTwitter.view_dummy.setVisibility(View.VISIBLE);
                        FragmentTwitter.btn_compose.setVisibility(View.VISIBLE);
                        FragmentTwitter.view_scheduled.setVisibility(View.GONE);
                        FragmentTwitter.dummy_text.setText("You have no Tweets recurring");
                    }
                } else {
                    FragmentTwitter.view_dummy.setVisibility(View.VISIBLE);
                    FragmentTwitter.btn_compose.setVisibility(View.VISIBLE);
                    FragmentTwitter.view_scheduled.setVisibility(View.GONE);
                    FragmentTwitter.dummy_text.setText("You have no Tweets recurring");
                }

            }


        }
    }

    public String Gettweet_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void recurrvisibilityt() {
        FragmentTwitter.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        FragmentTwitter.rl_schdule.setBackgroundResource(R.drawable.white_left);
        FragmentTwitter.rl_sent.setBackgroundResource(R.drawable.white_right);


        FragmentTwitter.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        FragmentTwitter.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentTwitter.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibilityt() {
        FragmentTwitter.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        FragmentTwitter.rl_schdule.setBackgroundResource(R.drawable.blue_left);
        FragmentTwitter.rl_sent.setBackgroundResource(R.drawable.white_right);

        FragmentTwitter.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        FragmentTwitter.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        FragmentTwitter.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }


    /***********************************************
     * Setting and resetting all events
     ***********************************************/
    public void setAlarms(Context context) {

        List<ModelScheduleSms> alarms = dataquery.getschsms();

        if (alarms != null) {
            for (ModelScheduleSms alarm : alarms) {

                if (datecomparision(alarm.schedule_date)) {
                    sendSMSMessage(context, alarm);
                }

            }
        }
    }

    /*****************************************
     * Check week day is Friday
     ***************************************/
    private boolean weekdayscheck(String schedule_date) {

        try {

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(schedule_date);

            Calendar call = Calendar.getInstance();
            call.setTime(newDate);
            SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

            String week = dayFormat.format(call.getTime());
            if (week.equals(Calendar.FRIDAY)) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /*****************************************
     * Generate Notification
     ***************************************/
    private void generatenotifiction(Context ctx, String id) {
        PowerManager pm = (PowerManager)
                ctx.getSystemService(Context.POWER_SERVICE);
        isScreenOn = pm.isScreenOn();
        if (classtatus && isScreenOn) {

            Intent i = new Intent(ctx.getApplicationContext(), MyAlertDialog.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("msg", "Your Delayd SMS has been sent");
            ctx.startActivity(i);

        } else {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(ctx)
                            .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                            .setContentTitle("Confirmation")
                            .setContentText("Your Delayd SMS has been sent")
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            Intent resultIntent = new Intent(ctx, Dashboard.class);
            android.support.v4.app.TaskStackBuilder stackBuilder = android.support.v4.app.TaskStackBuilder.create(ctx);
            stackBuilder.addParentStack(Dashboard.class);

// Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.

            mNotificationManager.notify(Integer.parseInt(id), mBuilder.build());
        }
    }

    /*****************************************
     * Send SMS logic
     ***************************************/
    protected void sendSMSMessage(Context context, ModelScheduleSms alarm) {
        Log.i("Send SMS", "");
        try {
            SmsManager smsManager = SmsManager.getDefault();
            String numbers[] = alarm.recvd_no.split(", *");
            for (String s : numbers) {
                smsManager.sendTextMessage(s, null, alarm.message, null, null);
            }


        } catch (Exception e) {
            // Toast.makeText(context, "SMS failed, please try again.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        if (alarm.recurring_type.equals("0")) {

            generatenotifiction(context, alarm.sms_id);
            dataquery.deletesms(alarm.sms_id);
        } else if (alarm.recurring_type.equals("d")) {

            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "d");
            dataquery.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("w")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 7, "d");
            dataquery.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("fn")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 15, "d");
            dataquery.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("wd")) {
            generatenotifiction(context, alarm.sms_id);
            String update;
            if (weekdayscheck(alarm.schedule_date)) {
                update = AddDaysToDate(alarm.schedule_date, 3, "d");
            } else {
                update = AddDaysToDate(alarm.schedule_date, 1, "d");
            }
            dataquery.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("m")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "m");
            dataquery.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("y")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "y");
            dataquery.updatesmsdate(alarm.sms_id, update);
        }
        updatedata(context);
    }

    private void updatedata(Context con) {
        try {
            if (FragmentSms.adaptertype.equals("0")) {
                FragmentSms.schdulesmslist = dataquery.getsms("sch");
                if (FragmentSms.schdulesmslist.size() != 0) {
                    FragmentSms.view_dummy.setVisibility(View.GONE);
                    FragmentSms.view_scheduled.setVisibility(View.VISIBLE);

                    FragmentSms.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                    FragmentSms.rl_schdule.setBackgroundResource(R.drawable.blue_left);
                    FragmentSms.rl_sent.setBackgroundResource(R.drawable.white_right);

                    FragmentSms.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                    FragmentSms.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
                    FragmentSms.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));

                    FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.schdulesmslist, "sch");
                    FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                } else {
                    FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                    FragmentSms.view_scheduled.setVisibility(View.GONE);
                    FragmentSms.dummy_text.setText("You have no SMS scheduled");
                }

            } else {
                FragmentSms.recurringsmslist = dataquery.getsms("rec");
                if (FragmentSms.recurringsmslist.size() != 0) {
                    FragmentSms.view_dummy.setVisibility(View.GONE);
                    FragmentSms.view_scheduled.setVisibility(View.VISIBLE);

                    FragmentEmail.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
                    FragmentEmail.rl_schdule.setBackgroundResource(R.drawable.white_left);
                    FragmentEmail.rl_sent.setBackgroundResource(R.drawable.white_right);


                    FragmentEmail.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
                    FragmentEmail.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
                    FragmentEmail.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));

                    FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.recurringsmslist, "rec");
                    FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                } else {
                    FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                    FragmentSms.view_scheduled.setVisibility(View.GONE);
                    FragmentSms.dummy_text.setText("You have no SMS recurring");
                }


            }
        } catch (Exception e) {
        }

    }


    /************************************
     * add days to current date
     **********************************/
    private String AddDaysToDate(String dt, int days, String type) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (type.equals("m")) {
            c.add(Calendar.MONTH, days);
        } else if (type.equals("y")) {
            c.add(Calendar.YEAR, days);
        } else {
            c.add(Calendar.DATE, days);
        }
        // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String output = sdf1.format(c.getTime());

        return output;

    }

    /**************************************
     * Compare dates
     ************************************/
    private boolean datecomparision(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = sdf.parse(date);
            if (new Date().equals(newDate) || new Date().after(newDate)) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

   /* public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }*/


    /*************************************************
     * Update group info
     ***********************************************/
    class updateinfo extends AsyncTask<String, Void, String> {
        String threadid, response;

        public updateinfo(String threadids) {
            this.threadid = threadids;
        }

        @Override
        protected String doInBackground(String... params) {

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "profile_detail")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("delayd_id", threadid)
                    .appendQueryParameter("type", "group");
            response = new JSONParser().getJSONFromUrl(Utils.base_url, builder);
            System.out.println("response=" + response);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    JSONObject dobj = obj.optJSONObject("data");
                    ModelMsgThread item = new ModelMsgThread();
                    item.thread_id = dobj.optString("thread_id");
                    item.participant_ids = dobj.optString("participant_ids");
                    item.participant_numbers = dobj.optString("participant_nos");
                    // String name = URLDecoder.decode(dobj.optString("group_name"), "UTF-8");
                    item.group_name = dobj.optString("group_name");
                    item.group_icon = dobj.optString("group_icon");
                    dataquery.updategrpmsgthread(item);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }


    }

    /*************************************
     * Update contacts
     ***********************************/
    class contactsync extends AsyncTask<String, Void, String> {
        ArrayList<ModelContacts> arr_cont;

        public contactsync(ArrayList<ModelContacts> mContacts) {
            this.arr_cont = mContacts;
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if (arr_cont.size() > 0) {
                    String json = new Gson().toJson(arr_cont);
                    //
                    String auth_code = FragmentChat.prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String status = object.getString("status");
                    if (status.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject obj = jsonArray.getJSONObject(i);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = obj.getString("delayd_user_id");
                            model.name = obj.getString("name");
                            model.profile_pic = obj.getString("profile_pic");
                            model.last_updated_date = obj.getString("last_updated_date");
                            model.contact_no = obj.getString("no");
                            model.country_code = obj.getString("country_code");
                            model.delayed_user = "1";

                            //delayedContacts.add(model);
                            dataquery.UpdateContacts2(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    private String ResponseMethod(String auth_code, String data) {
        String res = "";
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }
}