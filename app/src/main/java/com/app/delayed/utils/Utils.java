package com.app.delayed.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijeet on 2/5/2016.
 */
public class Utils {

    /****************Live Url*******************/
    public static String base_url = "https://delaydweb.com/delayd/index_V2.php?";
    /*******************Development Url**************************/
    //public static String base_url = "http://67.227.213.94/~delaydwebcot/delayd/index.php?";

    // public static String base_url = "http://caclients.net/projects/delayd/index.php?";
//    public static String base_url = "http://67.227.213.94/~delaydwebcot/delayd/index.php?";
    public static String scheduled_sms = base_url + "service_type=get_schedule_sms";
    public static String update_facebook_detail = base_url + "service_type=update_facebook_detail";
    public static String update_twitter_detail = base_url + "service_type=update_twitter_detail";
    public static String twitter_post = base_url + "service_type=twitter_post";
    public static Uri selected_image;
    public static Uri pimageUri = null;
    public static Bitmap croppedImage;
    public static String Imgurl = "https://delaydweb.com/delayd/image.php";
    public static boolean change_image = true;
    public static String getAll_users = base_url + "service_type=get_alluser_contact_info";
    public static String BLOCK_USER = base_url + "service_type=change_block_status";
    public static String USER_NOTIFICATION = base_url + "service_type=change_notification_status";
    public static ArrayList<String> daterr = new ArrayList<>();
    public static ArrayList<String> coarr = new ArrayList<>();
    //**********variable to check whcih chat section is visible**********
    public static int chat_section = 0;

    //---------------base 64 for in-app purchase---------------//
    public static String base64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiDrG39M/lX9ITYMUCsrZUTqhi17mEGcyjJcg4f7DTE9d5C0TwjfirySl2DCjk+Pc270So1YC1Y/iHwpVpozRWpSRSq8j1OFNfI57ceGNqCBKyTyhVO9uTy5xdjDHUy3reWmURNVFzxSOiHsMwlEB8tONyrOxFWwwT2L+4mZQyVtM/Y+tmDfpMdzyIuxPBHOsSLT0iM9HYFY6GwhPZZxcSsLoNk7YSyBnQn1x8urezOb3OXY+XAVG8GD8ZjRw+/X6FKRsjDAlOVdKJvNbI6xPwGxFHmK+/dN3QZ1bzZYwVOxVaIsroMVnbmEh7ucX94tX1nPhHupBPzxOJrexaR9jqQIDAQAB";

    /*******************************************
     * Gmail composer
     *****************************************/
    public static void shareToGMail(Activity activity, String[] email, String subject, String content) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
        final PackageManager pm = activity.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        activity.startActivity(emailIntent);
    }
}
