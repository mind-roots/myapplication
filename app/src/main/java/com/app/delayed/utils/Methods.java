package com.app.delayed.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import com.app.delayed.R;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelCountry;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Balvinder on 2/4/2016.
 */
public class Methods {
    public static int curday, curmont, curyear, curhour, curmin;
    //  public static boolean valid = true;
    public static Context context;
    public static EditText edt;
    public static Calendar c;
    public static StringBuilder builder;
    public static int mYear, mMonth, mDay, hour, minute, compareday, comparemonth;
    public static int TIME_PICKER_INTERVAL = 5, style;
    public static TimePicker timePicker;
    public static boolean mIgnoreEvent = false;
    public static int divider = 1;
    public static int i = 0;

    public Methods(Context con, EditText edt_time) {
        this.context = con;
        this.edt = edt_time;
        c = Calendar.getInstance();

        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);

        if (Build.VERSION.SDK_INT < 21) {
            //   style = R.style.DialogTheme2;
            style = Color.TRANSPARENT;
            divider = 1;
            // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } else {
            style = R.style.DialogTheme1;
            divider = 15;
        }

    }

    public static int ReturnSize(Context ctx) {
        int size = 0;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        size = width / 4;

        return size;
    }

    public static void timePicker(Context ctx, TimePickerDialog.OnTimeSetListener timePickerListener, int hour, int minute) {

        TimePickerDialog dialogg = new TimePickerDialog(ctx, R.style.AppTheme_Dialog, timePickerListener, hour, minute, false);
        dialogg.show();

    }

    public static void datePicker(Context ctx, DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(ctx, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }

    // **************************************Connection check**************************************
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    // **************************************Connection Dialog**************************************
    public static void conDialog(final Context c) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                c);

        alert.setTitle("Internet connection unavailable.");
        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
        alert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        c.startActivity(new Intent(
                                Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });

        alert.show();
    }

    // *******************************************Alert Dialog**************************************
    public static void dialog(Context ctx, String title, String msg) {

        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    public static ArrayList<ModelCountry> getNameEmailDetails(Context con) {
        DatabaseQueries query = new DatabaseQueries(context);
        ArrayList<ModelCountry> names = new ArrayList<ModelCountry>();
        try {
            ContentResolver cr = con.getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor cur1 = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (cur1.moveToNext()) {
                        //to get the contact names
                        String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        Log.e("Name :", name);
                        String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        Log.e("Email", email);
                        if (email != null) {
                            ModelCountry model = new ModelCountry();
                            model.name = name;
                            model.email = email;
                            if (!query.emailexist(email)){
                                query.insertemail(name,email);
                            }
                            names.add(model);
                        }
                    }
                    cur1.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return names;
    }

    public static void getdate(Context con, int mYear, int mMonth, int mDay, int hour, int minute) {
        builder = new StringBuilder();
        i = 0;
        datePickerbuild(con, datePickerListener, mYear, mMonth, mDay);

    }

    public static void datePickerbuild(Context ctx, DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(ctx, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mYear = c.get(Calendar.YEAR);
                if (mYear < curyear) {

                    Snackbar.make(edt, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
                } else if (mMonth < curmont && mYear == curyear) {

                    Snackbar.make(edt, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
                } else if (mDay < curday && mYear == curyear && mMonth == curmont) {

                    Snackbar.make(edt, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
                } else {

                    timePicker(context);
                }

            }
        });

        dialog.show();

    }


    public static DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c.set(Calendar.YEAR, mYear);
            c.set(Calendar.MONTH, mMonth);
            c.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;
            SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy");
            String dateSet = format1.format(c.getTime());
            builder.append(dateSet);


        }
    };

    public static void timePicker(Context ctx) {


        new TimePickerDialogs(ctx, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                // TODO Auto-generated method stub
                if (i == 0) {
                    i = i + 1;
                    int selectedMinute = min / divider;
                    if (selectedHour < curhour && compareday <= curday && comparemonth <= curmont) {
                        Snackbar.make(edt, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else if (selectedMinute < curmin && compareday <= curday && selectedHour == curhour && comparemonth <= curmont) {
                        Snackbar.make(edt, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else {
                       /* int set = curmin;
                        if (selectedMinute > 0 && selectedMinute <= 7 || selectedMinute > 53 && selectedMinute <= 59) {
                            set = 0;
                        } else if (selectedMinute > 7 && selectedMinute <= 15 || selectedMinute > 15 && selectedMinute <= 23) {
                            set = 15;
                        } else if (selectedMinute > 23 && selectedMinute <= 30 || selectedMinute > 30 && selectedMinute <= 37) {
                            set = 30;
                        } else if (selectedMinute > 37 && selectedMinute <= 45 || selectedMinute > 45 && selectedMinute <= 53) {
                            set = 45;
                        } else {
                            set = selectedMinute;
                        }*/
                        c.set(Calendar.HOUR_OF_DAY, selectedHour);
                        c.set(Calendar.MINUTE, min);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                        String displayValue = timeFormatter.format(c.getTime());
                        builder.append(", " + displayValue).toString();
                        edt.setText(builder.toString());
                        return;

                    }
                }
            }
        }, curhour, curmin, false).show();


    }

    public static class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;
        int hr, min;
        Calendar ct;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL, is24HourView);
            //   super(arg0, dialogTheme1, callBack, hourOfDay, minute/TIME_PICKER_INTERVAL, is24HourView);
            this.hr = hourOfDay;
            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            ct = Calendar.getInstance();
            //   int tym = (curmin - (curmin / TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);

            minute = minute * 5;

         /*   if ((hourOfDay < curhour && compareday <= curday && comparemonth <= curmont)||(minute < curmin && compareday <= curday && hourOfDay == curhour && comparemonth <= curmont)) {

                updateTime(curhour, curmin);
            }*/
            if ((hourOfDay < curhour && compareday <= curday && comparemonth <= curmont) || (minute < curmin && compareday <= curday && hourOfDay == curhour && comparemonth <= curmont)) {

                timePicker.setCurrentHour(ct.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(ct.get(Calendar.MINUTE));
            }
        }

        @Override
        public void updateTime(int hourOfDay, int minuteOfHour) {
            super.updateTime(hourOfDay, minuteOfHour);

        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (callback != null && timePicker != null) {
                timePicker.clearFocus();
                callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }
                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
  /*  public static class TimePickerDialogs extends TimePickerDialog {
        private final static int TIME_PICKER_INTERVAL = 5;
        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
            // TODO Auto-generated constructor stub
            this.callback = callBack;
        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (callback != null && timePicker != null) {
                timePicker.clearFocus();
                callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }
                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }
*/

    /************************************************
     * Fetch Contacts for chat
     **********************************************/
    public static List<ModelContacts> FetchContacts(Context context) {
        String lastdate="";
        List<ModelContacts> contacts = new ArrayList<>();
        DatabaseQueries query = new DatabaseQueries(context);
        try{
            lastdate = query.getlastdate();
        }catch (Exception e){
            e.printStackTrace();
        }


        Date date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            date = formatter.parse(lastdate);
            System.out.println(date);
            System.out.println(formatter.format(date));

        } catch (Exception e) {
            e.printStackTrace();
            lastdate ="NA";
        }
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
        Cursor cursor = null;
        int photoIdIdx;
        try {
            if (Build.VERSION.SDK_INT < 18) {
               // if (lastdate.equals("NA")) {
                    cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                            null, ContactsContract.CommonDataKinds.Phone.DATA_VERSION + " ASC");

//                } else {
//                    cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.DATA_VERSION + ">?",
//                            new String[]{"" + date.getTime()}, ContactsContract.CommonDataKinds.Phone.DATA_VERSION + " ASC");
//                }
            } else {
                if (lastdate.equals("NA")) {
                    cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP + " ASC");
                } else {
                    cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP + ">?",
                            new String[]{"" + date.getTime()}, ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP + " ASC");
                }
            }
            int contactIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (Build.VERSION.SDK_INT < 18)
                photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA_VERSION);
            else
                photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP);
            cursor.moveToFirst();
            if (cursor.getCount() > 0 ) {

                do {
                    ModelContacts model = new ModelContacts();
                    model.cont_id = cursor.getString(contactIdIdx);
                    model.cont_name = cursor.getString(nameIdx);
                    String number = cursor.getString(phoneNumberIdx);
                    model.cont_date = format.format(new Date(Long.parseLong(cursor.getString(photoIdIdx))));

                    number = number.replaceAll("[^0-9]","");
//                    if (number.contains(" "))
//                        number = number.replaceAll(" ", "");
//                    if (number.contains("-"))
//                        number = number.replaceAll("-", "");
//                    if (number.contains("(") || number.contains(")")) {
//                        number = number.replace("(", "");
//                        number = number.replace(")", "");
//                    }

                    model.cont_number = number;
                    Log.i("ID: ", model.cont_id);
                    Log.i("Name: ", "" + model.cont_name);
                    Log.i("Number: ", "" + model.cont_number);
                    Log.i("Photo: ", "" + model.cont_date);
                    Log.i("**", "*****************************************");
                    int count = query.getcontactcount(number);
                    Log.i("Photo: ", "" + count);
                    if (count > 0) {
                        query.UpdateContactsdb(model);
                    } else {
                        query.InsertContacts(model);
                    }

                    contacts.add(model);
                    //...
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contacts;
    }

    public static String tonormaltime(String getdate, String timeformat) {
       /* try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse("2012-08-15T22:56:02.038Z");

            DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            pstFormat.setTimeZone(TimeZone.getTimeZone("PST"));

            System.out.println(pstFormat.format(date));
        } catch (Exception e) {
        }
*/

        System.out.print("date=" + getdate);
        String value = "NA";
        SimpleDateFormat format1;
        if (Utils.chat_section == 0) {

            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        } else {
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        }
//        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date newDate = format1.parse(getdate);
            if (format2.format(newDate).equals(format2.format(new Date()))) {
                if (timeformat.equals("12")) {
                    SimpleDateFormat tymformat = new SimpleDateFormat("hh:mm a");
                    value = tymformat.format(newDate);
                } else {
                    SimpleDateFormat tymformat = new SimpleDateFormat("HH:mm");
                    value = tymformat.format(newDate);
                }

            } else {
                value = format2.format(newDate);
            }

        } catch (Exception e) {
            value = getdate;
        }


        return value;
    }


    public static String tochatnormaltime(String getdate, String timeformat, int type) {


        System.out.print("date=" + getdate);
        String value = "NA";
        SimpleDateFormat format1;
        if (Utils.chat_section == 0) {

            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        } else {
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        }


        try {
            Date newDate = format1.parse(getdate);

            if (timeformat.equals("12")) {
                if (type == 1) {
                    SimpleDateFormat tymformat = new SimpleDateFormat("hh:mm a");
                    value = tymformat.format(newDate);
                } else {
                    SimpleDateFormat tymformat = new SimpleDateFormat("dd MMM, yyyy");
                    value = tymformat.format(newDate);
                }

            } else {
                if (type == 1) {
                    SimpleDateFormat tymformat = new SimpleDateFormat("HH:mm");
                    value = tymformat.format(newDate);
                } else {
                    SimpleDateFormat tymformat = new SimpleDateFormat("dd MMM, yyyy");
                    value = tymformat.format(newDate);
                }

            }


        } catch (Exception e) {
            value = getdate;
        }


        return value;
    }

    public static String normalviewstime(String getdate, String timeformat) {


        System.out.print("date=" + getdate);
        String value = "NA";
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date newDate = format1.parse(getdate);
            if (format2.format(newDate).equals(format2.format(new Date()))) {
                if (timeformat.equals("12")) {
                    SimpleDateFormat tymformat = new SimpleDateFormat("hh:mm a");
                    value = tymformat.format(newDate);
                } else {
                    SimpleDateFormat tymformat = new SimpleDateFormat("HH:mm");
                    value = tymformat.format(newDate);
                }

            } else {
                value = format2.format(newDate);
            }

        } catch (Exception e) {
            value = getdate;
        }


        return value;
    }

    public static Uri SaveImage(final Bitmap finalBitmap) {
        final Uri[] uri = new Uri[1];
        String root = Environment.getExternalStorageDirectory().toString();

        File myDir = new File(root + "/VidCode/Captured Images/");
        if (!myDir.exists())
            myDir.mkdirs();

        String fname = "/image-" + System.currentTimeMillis() + ".png";
        File file = new File(myDir, fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            // localImagePath = myDir + fname;
            uri[0] = Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uri[0];
    }


}
