package com.app.delayed.utils;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class App extends Application {

    public static String objectId;
    public static boolean IS_APP_RUNNING = false;
    SharedPreferences prefs;
    public static Context context;
    String error;
    public static String additionalMessage = "";
    JSONObject obj;
    public static String type = "home";
    public static boolean isOpen;
    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("font path ", getAssets() + "textfont.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "text_light_font.ttf");



        //****************on signal implementation*****************//

        try {
            OneSignal.startInit(this)
                    .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                    .setAutoPromptLocation(true)
                    .init();
            OneSignal.enableNotificationsWhenActive(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**************************Beta****************************/
       /* new Instabug.Builder(this, "6227cfade5a44d2b151684b1bc6e086d")
                .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                .setShouldShowIntroDialog(false)
                .build();*/
        /**************************Live****************************/
        new Instabug.Builder(this, "aeca0a88daf7b21c4cb6f3243b98c984")
                .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                .setShouldShowIntroDialog(false)
                .build();

    }


    private int Minute5(int min){
        int minute=0;
        if (min%5 != 0){
            minute = (min-(min%5))+5;
            return minute;
        }
        return min;
    }

    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        /**
         * Callback to implement in your app to handle when a notification is opened from the Android status bar or
         * a new one comes in while the app is running.
         * This method is located in this Application class as an example, you may have any class you wish implement NotificationOpenedHandler and define this method.
         *
         * @param message        The message string the user seen/should see in the Android status bar.
         * @param additionalData The additionalData key value pair section you entered in on onesignal.com.
         * @param isActive       Was the app in the foreground when the notification was received.
         */

        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {

            try {
                if (additionalData != null) {
                    if (additionalData.has("actionSelected"))
                        additionalMessage += "Pressed ButtonID: " + additionalData.getString("actionSelected");
                    isOpen = isActive;
                   obj = new JSONObject(additionalData.toString());
                    type = obj.optString("type");
                    System.out.println("message" + message);
                    System.out.println("one signal data" + obj);
                    //SplashScreen.push = true;
                    System.out.println("values in one signal" + type);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }


        }


    }
} 