package com.app.delayed.utils;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.delayed.Dashboard;
import com.app.delayed.R;
import com.app.delayed.adapters.ScheduleSmsAdapter;
import com.app.delayed.chatExtended.MyAlertDialog;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.fragments.FragmentSms;
import com.app.delayed.model.ModelScheduleSms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/********************************************************************
 * Not in use now. Code shifted to OneSignal Receiver
 * ******************************************************************/
public class SMSReciever extends WakefulBroadcastReceiver {
    DatabaseQueries query;
    List<ActivityManager.RunningTaskInfo> tasks;
    boolean classtatus,isScreenOn;

    @Override
    public void onReceive(Context context, Intent intent) {

        query = new DatabaseQueries(context);
        classtatus = isApplicationSentToBackground(context);
       // Toast.makeText(context, "Delayd", Toast.LENGTH_SHORT).show();
        setAlarms(context);
    }

    /***********************************************
     * Setting and resetting all events
     ***********************************************/
    public void setAlarms(Context context) {

        List<ModelScheduleSms> alarms = query.getschsms();

        if (alarms != null) {
            for (ModelScheduleSms alarm : alarms) {

                if (datecomparision(alarm.schedule_date)) {
                    sendSMSMessage(context, alarm);
                }

            }
        }
    }

    /*****************************************
     * Check week day is Friday
     ***************************************/
    private boolean weekdayscheck(String schedule_date) {

        try {

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(schedule_date);

            Calendar call = Calendar.getInstance();
            call.setTime(newDate);
            SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

            String week = dayFormat.format(call.getTime());
            if (week.equals(Calendar.FRIDAY)) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /*****************************************
     * Generate Notification
     ***************************************/
    private void generatenotifiction(Context ctx, String id) {
        PowerManager pm = (PowerManager)
                ctx.getSystemService(Context.POWER_SERVICE);
        isScreenOn = pm.isScreenOn();
        if (classtatus && isScreenOn) {

            Intent i = new Intent(ctx.getApplicationContext(), MyAlertDialog.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("msg", "Your Delayd SMS has been sent");
            ctx.startActivity(i);

        } else {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(ctx)
                            .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                            .setContentTitle("Confirmation")
                            .setContentText("Your Delayd SMS has been sent")
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            Intent resultIntent = new Intent(ctx, Dashboard.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
            stackBuilder.addParentStack(Dashboard.class);

// Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.

            mNotificationManager.notify(Integer.parseInt(id), mBuilder.build());
        }
    }

    /*****************************************
     * Send SMS logic
     ***************************************/
    protected void sendSMSMessage(Context context, ModelScheduleSms alarm) {
        Log.i("Send SMS", "");
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(alarm.recvd_no, null, alarm.message, null, null);

        } catch (Exception e) {
           // Toast.makeText(context, "SMS failed, please try again.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        if (alarm.recurring_type.equals("0")) {

            generatenotifiction(context, alarm.sms_id);
            query.deletesms(alarm.sms_id);
        } else if (alarm.recurring_type.equals("d")) {

            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "d");
            query.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("w")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 7, "d");
            query.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("fn")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 15, "d");
            query.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("wd")) {
            generatenotifiction(context, alarm.sms_id);
            String update;
            if (weekdayscheck(alarm.schedule_date)) {
                update = AddDaysToDate(alarm.schedule_date, 3, "d");
            } else {
                update = AddDaysToDate(alarm.schedule_date, 1, "d");
            }
            query.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("m")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "m");
            query.updatesmsdate(alarm.sms_id, update);
        } else if (alarm.recurring_type.equals("y")) {
            generatenotifiction(context, alarm.sms_id);
            String update = AddDaysToDate(alarm.schedule_date, 1, "y");
            query.updatesmsdate(alarm.sms_id, update);
        }
        updatedata(context);
    }

    private void updatedata(Context con) {
        try {
            if (FragmentSms.adaptertype.equals("0")) {
                FragmentSms.schdulesmslist = query.getsms("sch");
                if (FragmentSms.schdulesmslist.size() != 0) {
                    FragmentSms.view_dummy.setVisibility(View.GONE);
                    FragmentSms.view_scheduled.setVisibility(View.VISIBLE);

                    FragmentSms.rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                    FragmentSms.rl_schdule.setBackgroundResource(R.drawable.blue_left);
                    FragmentSms.rl_sent.setBackgroundResource(R.drawable.white_right);

                    FragmentSms.txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                    FragmentSms.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
                    FragmentSms.txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));

                    FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.schdulesmslist, "sch");
                    FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                } else {
                    FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                    FragmentSms.view_scheduled.setVisibility(View.GONE);
                    FragmentSms.dummy_text.setText("You have no SMS scheduled");
                }

            } else {
                FragmentSms.recurringsmslist = query.getsms("rec");
                if (FragmentSms.recurringsmslist.size() != 0) {
                    FragmentSms.view_dummy.setVisibility(View.GONE);
                    FragmentSms.view_scheduled.setVisibility(View.VISIBLE);

                    FragmentEmail.rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
                    FragmentEmail.rl_schdule.setBackgroundResource(R.drawable.white_left);
                    FragmentEmail.rl_sent.setBackgroundResource(R.drawable.white_right);


                    FragmentEmail.txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
                    FragmentEmail.txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
                    FragmentEmail.txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));

                    FragmentSms.adapter = new ScheduleSmsAdapter(con, FragmentSms.recurringsmslist, "rec");
                    FragmentSms.recycler_list_sms.setAdapter(FragmentSms.adapter);
                } else {
                    FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                    FragmentSms.view_scheduled.setVisibility(View.GONE);
                    FragmentSms.dummy_text.setText("You have no SMS recurring");
                }


            }
        } catch (Exception e) {
        }

    }


    /************************************
     * add days to current date
     **********************************/
    private String AddDaysToDate(String dt, int days, String type) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (type.equals("m")) {
            c.add(Calendar.MONTH, days);
        } else if (type.equals("y")) {
            c.add(Calendar.YEAR, days);
        } else {
            c.add(Calendar.DATE, days);
        }
        // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String output = sdf1.format(c.getTime());

        return output;

    }

    /**************************************
     * Compare dates
     ************************************/
    private boolean datecomparision(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = sdf.parse(date);
            if (new Date().equals(newDate) || new Date().after(newDate)) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }
  /* *//****************************************
     * Set an event
     **************************************//*
    private static void setAlarm(Context context, Calendar calendar, PendingIntent pIntent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
        }
    }

   /*//****************************************
     * Cancel all events
     **************************************//**//*
    public void cancelAlarms(Context context) {


       ArrayList<ModelScheduleSms> alarms = query.getschsms();

        if (alarms != null) {
            int index = 0;
            for (ModelScheduleSms alarm : alarms) {
                // if ((alarm.status).equals("0")) {
                PendingIntent pIntent = createPendingIntent(context, alarm, index);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pIntent);

                // }
                index++;
            }
        }*//**//*

    }


   *//**//* private static PendingIntent createPendingIntent(Context context, SMSModel model, int index) {
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra(ReminderDBHelper.ID, model.id);
        intent.putExtra(ReminderDBHelper.TIME, model.time);
        return PendingIntent.getService(context, index, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }*/
}