package com.app.delayed.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.delayed.Dashboard;
import com.app.delayed.GroupScreen;
import com.app.delayed.NewGroup;
import com.app.delayed.NewSMS;
import com.app.delayed.R;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelGroup;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/4/2016.
 */
public class GroupAdapter extends BaseAdapter {

    Context con;
    List<ModelGroup> data;
    LayoutInflater inflater;
    ViewHolder holder;
    SharedPreferences prefs, prefsd;
    public static List<String> contact_number_value = new ArrayList<>();
    public static List<String> contact_name_value = new ArrayList<>();
    String cnames, cnumbers;
    String typego, auth_code;
    DatabaseQueries dataquery;

    public GroupAdapter(Context ctx, List<ModelGroup> arr, String visible) {
        this.typego = visible;
        this.con = ctx;
        this.data = arr;
        inflater = LayoutInflater.from(ctx);
        dataquery = new DatabaseQueries(con);
        prefs = con.getSharedPreferences("name_phone_value", con.MODE_PRIVATE);
        prefsd = con.getSharedPreferences("delayed", con.MODE_PRIVATE);
        auth_code = prefsd.getString("auth_code", null);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grp_item, parent, false);
            holder = new ViewHolder();
            holder.grp_icon = (CircularImageView) convertView.findViewById(R.id.grp_icon);
            holder.grp_name = (TextView) convertView.findViewById(R.id.grp_name);
            holder.cont_name = (TextView) convertView.findViewById(R.id.cont_name);
            holder.mainlayout = (LinearLayout) convertView.findViewById(R.id.mainlayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        System.out.print("img:" + data.get(position).grp_img);
        holder.grp_name.setText(data.get(position).grp_name);
        holder.cont_name.setText(data.get(position).grp_contact_name);

        Glide.with(con)
                .load(data.get(position).grp_img).placeholder(R.mipmap.ic_launcher)
                .crossFade()
                .into(holder.grp_icon);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typego.equals("no")) {
                    NewSMS.set_groupname = data.get(position).grp_name;
                    NewSMS.set_name = data.get(position).grp_contact_name;
                    NewSMS.set_phone = data.get(position).grp_contact;
                    NewSMS.set = true;
                    ((Activity) con).finish();
                } else {
                    Intent intent = new Intent(con, NewGroup.class);
                    intent.putExtra("IntentType", "update");
                    intent.putExtra("groupname", data.get(position).grp_name);
                    intent.putExtra("cnames", data.get(position).grp_contact_name);
                    intent.putExtra("cnumbers", data.get(position).grp_contact);
                    intent.putExtra("grpimage", data.get(position).grp_img);
                    intent.putExtra("grpid", data.get(position).grp_id);
                    con.startActivity(intent);
                }
            }

        });

        holder.mainlayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (FragmentEmail.inttype == 2) {
                } else {
                    dialog("Alert!", "Are you sure you want to remove this Group?", android.R.drawable.ic_dialog_info, position);

                }
                return false;
            }
        });
        return convertView;
    }

    public void dialog(String title, String msg, int icon, final int pos) {
        new AlertDialog.Builder(con)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dataquery.delete_value(data.get(pos).grp_id);
                        ArrayList<String> ids = dataquery.getsmsid(data.get(pos).grp_name);
                        dataquery.delete_sms(data.get(pos).grp_name);
                        Dashboard.newsms = true;
                        GroupScreen.groupList.remove(pos);
                        new delete(ids).execute();
                        if (GroupScreen.groupList.size() == 0) {

                            GroupScreen.lay_list.setVisibility(View.GONE);
                            GroupScreen.lay_dummy.setVisibility(View.VISIBLE);

                        }
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })

                .setIcon(icon)
                .show();

    }

    public class ViewHolder {
        public CircularImageView grp_icon;
        public TextView grp_name, cont_name;
        public LinearLayout mainlayout;

    }

    class delete extends AsyncTask<String, Void, String> {
        String response;
        ArrayList<String> ids;

        public delete(ArrayList<String> smsid) {
            this.ids = smsid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            for (String SmsId : ids) {
                response = Delete_Method("delete_sms", auth_code, SmsId);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }

    public String Delete_Method(String service_type, String auth_code, String id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sms_id", id);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}
