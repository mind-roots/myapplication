package com.app.delayed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.NewEmail;
import com.app.delayed.R;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleEmail;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by admin on 2/8/2016.
 */
public class ScheduleEmailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private LayoutInflater inflater;
    public ArrayList<ModelScheduleEmail> data = new ArrayList<ModelScheduleEmail>();
    SharedPreferences prefs;
    String argument;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_VALUE = 2;
    String listtype, auth_code, timeformat;
    boolean network;

    public ScheduleEmailAdapter(Context context, ArrayList<ModelScheduleEmail> arrayList, String type) {
        this.context = context;
        this.data = arrayList;
        this.listtype = type;
        prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
        network = Methods.isNetworkConnected(context);
        auth_code = prefs.getString("auth_code", null);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).view_type.equals("header")) {
            return TYPE_HEADER;
        } else if (data.get(position).view_type.equals("values")) {
            return TYPE_VALUE;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VALUE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_email_item, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new HeaderViewHolder(v);
        }
        return null;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        timeformat = prefs.getString("timeformat", null);

        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hold = (HeaderViewHolder) holder;

            hold.txtheader.setText(savedateformatter(data.get(position).header_date));

        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.txt_message.setText(data.get(position).subject);
            if (position == data.size() - 1) {
                viewHolder.divider.setVisibility(View.GONE);
            } else if (data.get(position + 1).view_type.equals("header")) {
                viewHolder.divider.setVisibility(View.GONE);
            } else {
                viewHolder.divider.setVisibility(View.VISIBLE);
            }
            if (data.get(position).recurring_type.equals("0")) {
                //      viewHolder.txt_recurring.setText("No Recurring");
                viewHolder.linear1.setVisibility(View.VISIBLE);
                viewHolder.linear2.setVisibility(View.GONE);

            } else if (data.get(position).recurring_type.equals("wd")) {
                viewHolder.txt_recurring.setText("Recur weekdays");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("fn")) {
                viewHolder.txt_recurring.setText("Recur forthnightly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("d")) {
                viewHolder.txt_recurring.setText("Recur Daily");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("w")) {
                viewHolder.txt_recurring.setText("Recur Weekly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("m")) {
                viewHolder.txt_recurring.setText("Recur Monthly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("y")) {
                viewHolder.txt_recurring.setText("Recur Yearly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else {
                viewHolder.txt_recurring.setText("");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            }

            viewHolder.txt_name.setText(data.get(position).to_id);
            viewHolder.txt_name1.setText(data.get(position).to_id);


            viewHolder.txt_time.setText(savetimeformatter(data.get(position).schedule_date));
            viewHolder.txt_time1.setText(savetimeformatter(data.get(position).schedule_date));
            viewHolder.ll_mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String intenttype, recurtype;
                    if (FragmentEmail.inttype == 2) {
                        intenttype = "nonedit";
                        recurtype = "Sent";
                    } else {
                        intenttype = "update";
                        recurtype = data.get(position).recurring_type;
                    }

                    Intent intent = new Intent(context, NewEmail.class);
                    intent.putExtra("intenttype", intenttype);
                    intent.putExtra("message", data.get(position).message);
                    intent.putExtra("subject", data.get(position).subject);
                    intent.putExtra("to_id", data.get(position).to_id);
                    intent.putExtra("cc_id", data.get(position).cc_id);
                    intent.putExtra("from_id", data.get(position).from_id);
                    intent.putExtra("recurring_type", recurtype);
                    intent.putExtra("schedule_date", data.get(position).schedule_date);
                    intent.putExtra("email_id", data.get(position).email_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            });
            viewHolder.ll_mainlayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (FragmentEmail.inttype == 2) {
                    } else {
                        try {
                            dialog("Alert!", "Are you sure you want to delete this email?", android.R.drawable.ic_dialog_info, position);

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    return false;
                }
            });
        }

    }

    public void dialog(String title, String msg, int icon, final int pos) throws IllegalStateException{
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //   data.remove(pos);

                        String eid = data.get(pos).email_id;
                        if (!network) {
                            Methods.conDialog(context);

                        } else {
                            new delete(eid, pos).execute();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txt_name, txt_time, txt_recurring, txt_message, txt_time1, txt_name1;

        protected LinearLayout ll_mainlayout, linear1, linear2;
        public RelativeLayout divider;

        public ViewHolder(View v) {
            super(v);
            txt_name = (TextView) v.findViewById(R.id.txt_name);
            txt_time = (TextView) v.findViewById(R.id.txt_time);
            txt_recurring = (TextView) v.findViewById(R.id.txt_recurring);
            txt_message = (TextView) v.findViewById(R.id.txt_message);
            ll_mainlayout = (LinearLayout) v.findViewById(R.id.ll_mainlayout);
            linear1 = (LinearLayout) v.findViewById(R.id.linear1);
            linear2 = (LinearLayout) v.findViewById(R.id.linear2);
            txt_name1 = (TextView) v.findViewById(R.id.txt_name1);
            txt_time1 = (TextView) v.findViewById(R.id.txt_time1);
            divider = (RelativeLayout) v.findViewById(R.id.divider);
        }
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtheader;


        public HeaderViewHolder(View v) {
            super(v);
            this.txtheader = (TextView) v.findViewById(R.id.txtheader);

        }
    }

    class delete extends AsyncTask<String, Void, String> {
        String SmsId, response;
        int position;

        public delete(String smsid, int pos) {
            this.SmsId = smsid;
            this.position = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentEmail.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            response = Delete_Method("delete_email", auth_code, SmsId);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentEmail.progressbar.setVisibility(View.GONE);
            try {
                JSONObject jobj = new JSONObject(response);
                String status = jobj.optString("status");
                if (status.equals("true")) {
                    if (listtype.equals("rec")) {
                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                            FragmentEmail.recurringemaillist.remove(position);
                            FragmentEmail.recurringemaillist.remove(position - 1);
                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                            FragmentEmail.recurringemaillist.remove(position);
                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentEmail.recurringemaillist.remove(position);
                            FragmentEmail.recurringemaillist.remove(position - 1);
                        } else {
                            FragmentEmail.recurringemaillist.remove(position);
                        }


                        if (FragmentEmail.recurringemaillist.size() == 0) {
                            FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                            FragmentEmail.view_scheduled.setVisibility(View.GONE);
                            FragmentEmail.dummy_text.setText("You don't have any \n Emails recurring.");
                        }
                    } else if (listtype.equals("sch")) {
                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                            FragmentEmail.schduleemaillist.remove(position);
                            FragmentEmail.schduleemaillist.remove(position - 1);
                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                            FragmentEmail.schduleemaillist.remove(position);
                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentEmail.schduleemaillist.remove(position);
                            FragmentEmail.schduleemaillist.remove(position - 1);
                        } else {
                            FragmentEmail.schduleemaillist.remove(position);
                        }


                        if (FragmentEmail.schduleemaillist.size() == 0) {
                            FragmentEmail.view_dummy.setVisibility(View.VISIBLE);
                            FragmentEmail.view_scheduled.setVisibility(View.GONE);
                            FragmentEmail.dummy_text.setText("You don't have any \n Emails scheduled.");
                        }
                    }
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Response Error", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
            }
        }
    }

    public String Delete_Method(String service_type, String auth_code, String id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("post_id", id);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMMM dd, EEEE");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = senddate;
        }
        return setdate;

    }

    public String savetimeformatter(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (timeformat.equals("12")) {
                format1 = new SimpleDateFormat("hh:mm a");
            } else {
                format1 = new SimpleDateFormat("HH:mm");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }
}

