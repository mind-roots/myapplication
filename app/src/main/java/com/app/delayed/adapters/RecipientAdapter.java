package com.app.delayed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.NewGroup;
import com.app.delayed.R;

import java.util.ArrayList;

/**
 * Created by user on 11/19/2015.
 */
public class RecipientAdapter extends RecyclerView.Adapter<RecipientAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    Context context;
    ArrayList<String> data;

    public RecipientAdapter(Context con, ArrayList<String> list) {
        // TODO Auto-generated constructor stub
        this.context = con;
        inflater = LayoutInflater.from(con);
        this.data = list;
    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.recipients, parent, false);

        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        public TextView name;
        public LinearLayout linearclick;


        public MyViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.txt_name);
            linearclick = (LinearLayout) itemLayoutView.findViewById(R.id.linearclick);
        }

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // TODO Auto-generated method stub
        holder.name.setText(data.get(position));
        holder.linearclick.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                dialog("Alert!", "Are you sure you want to remove this contact?", android.R.drawable.ic_dialog_info, position);
                return false;
            }
        });
    }

    public void dialog(String title, String msg, int icon, final int pos) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                     //   data.remove(pos);
                        NewGroup.arr_name.remove(pos);
                        NewGroup.arr_phone.remove(pos);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

                .setIcon(icon)
                .show();
    }
}
