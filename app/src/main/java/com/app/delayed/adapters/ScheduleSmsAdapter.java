package com.app.delayed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.NewSMS;
import com.app.delayed.R;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentSms;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 2/6/2016.
 */
public class ScheduleSmsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private LayoutInflater inflater;
    public ArrayList<ModelScheduleSms> data = new ArrayList<ModelScheduleSms>();
    SharedPreferences prefs;
    String argument;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_VALUE = 2;
    String listtype, auth_code, timeformat;
    boolean network;
    DatabaseQueries queries;

    public ScheduleSmsAdapter(Context context, ArrayList<ModelScheduleSms> arrayList, String type) {
        this.context = context;
        this.data = arrayList;
        this.listtype = type;
        prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
        network = Methods.isNetworkConnected(context);
        auth_code = prefs.getString("auth_code", null);
        queries = new DatabaseQueries(context);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).view_type.equals("header")) {
            return TYPE_HEADER;
        } else if (data.get(position).view_type.equals("values")) {
            return TYPE_VALUE;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VALUE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_sms_item, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new HeaderViewHolder(v);
        }
        return null;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        timeformat = prefs.getString("timeformat", null);
        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hold = (HeaderViewHolder) holder;

            hold.txtheader.setText(savedateformatter(data.get(position).header_date));

        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            if (position == data.size() - 1) {
                viewHolder.divider.setVisibility(View.GONE);
            } else if (data.get(position + 1).view_type.equals("header")) {
                viewHolder.divider.setVisibility(View.GONE);
            } else {
                viewHolder.divider.setVisibility(View.VISIBLE);
            }
            viewHolder.txt_message.setText(data.get(position).message);
            if (data.get(position).recurring_type.equals("0")) {
                //      viewHolder.txt_recurring.setText("No Recurring");
                viewHolder.linear1.setVisibility(View.VISIBLE);
                viewHolder.linear2.setVisibility(View.GONE);
            } else if (data.get(position).recurring_type.equals("wd")) {
                viewHolder.txt_recurring.setText("Recur weekdays");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("fn")) {
                viewHolder.txt_recurring.setText("Recur forthnightly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("d")) {
                viewHolder.txt_recurring.setText("Recur Daily");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("w")) {
                viewHolder.txt_recurring.setText("Recur Weekly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("m")) {
                viewHolder.txt_recurring.setText("Recur Monthly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else if (data.get(position).recurring_type.equals("y")) {
                viewHolder.txt_recurring.setText("Recur Yearly");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            } else {
                viewHolder.txt_recurring.setText("");
                viewHolder.linear1.setVisibility(View.GONE);
                viewHolder.linear2.setVisibility(View.VISIBLE);
            }

            if (!data.get(position).group_name.equals("") || data.get(position).group_name.length() != 0) {
                viewHolder.txt_name.setText(data.get(position).group_name);
                viewHolder.txt_name1.setText(data.get(position).group_name);
            } else if (!data.get(position).contact_name.equals("") || data.get(position).contact_name.length() != 0) {
                viewHolder.txt_name.setText(data.get(position).contact_name);
                viewHolder.txt_name1.setText(data.get(position).contact_name);
            } else {
                viewHolder.txt_name.setText(data.get(position).recvd_no);
                viewHolder.txt_name1.setText(data.get(position).recvd_no);
            }


            viewHolder.txt_time.setText(savetimeformatter(data.get(position).schedule_date));
            viewHolder.txt_time1.setText(savetimeformatter(data.get(position).schedule_date));
            viewHolder.main_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String intenttype, recurtype;
                    if (FragmentSms.inttype == 2) {
                        intenttype = "nonedit";
                        recurtype = "Sent";
                    } else {
                        intenttype = "update";
                        recurtype = data.get(position).recurring_type;
                    }
                    Intent intent = new Intent(context, NewSMS.class);
                    intent.putExtra("intenttype", intenttype);
                    intent.putExtra("message", data.get(position).message);
                    intent.putExtra("recvd_no", data.get(position).recvd_no);
                    intent.putExtra("group_name", data.get(position).group_name);
                    intent.putExtra("schedule_date", data.get(position).schedule_date);
                    intent.putExtra("recurring_type", recurtype);
                    intent.putExtra("contact_name", data.get(position).contact_name);
                    intent.putExtra("sms_id", data.get(position).sms_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            viewHolder.main_layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (FragmentSms.inttype == 2) {
                    } else {
                        try {
                            dialog("Alert!", "Are you sure you want to delete this SMS? ", android.R.drawable.ic_dialog_info, position);

                        }catch (IllegalStateException e){
                            e.printStackTrace();
                        }

                    }
                    return false;
                }
            });
        }

    }

    public void dialog(String title, String msg, int icon, final int pos) throws IllegalStateException {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //   data.remove(pos);
                        String smsid = data.get(pos).sms_id;
                        if (!network) {
                            Methods.conDialog(context);

                        } else {
//                            queries.deletesms(smsid);
//                            notifyadapter(pos);
                              new delete(smsid, pos).execute();
                        }


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();

    }

    private void notifyadapter(int position) {
        if (listtype.equals("rec")) {
            if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                FragmentSms.recurringsmslist.remove(position);
                FragmentSms.recurringsmslist.remove(position - 1);
            } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                FragmentSms.recurringsmslist.remove(position);
            } else if (data.get(position - 1).view_type.equals("header")) {
                FragmentSms.recurringsmslist.remove(position);
                FragmentSms.recurringsmslist.remove(position - 1);
            } else {
                FragmentSms.recurringsmslist.remove(position);

            }

            if (FragmentSms.recurringsmslist.size() == 0) {
                FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                FragmentSms.view_scheduled.setVisibility(View.GONE);
                FragmentSms.dummy_text.setText("You have no SMS recurring");
            }

        } else if (listtype.equals("sch")) {
            if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {

                FragmentSms.schdulesmslist.remove(position);
                FragmentSms.schdulesmslist.remove(position - 1);

            } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {

                FragmentSms.schdulesmslist.remove(position);

            } else if (data.get(position - 1).view_type.equals("header")) {
                FragmentSms.schdulesmslist.remove(position);
                FragmentSms.schdulesmslist.remove(position - 1);
            } else {
                FragmentSms.schdulesmslist.remove(position);

            }


            if (FragmentSms.schdulesmslist.size() == 0) {
                FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                FragmentSms.view_scheduled.setVisibility(View.GONE);
                FragmentSms.dummy_text.setText("You have no SMS scheduled");
            }
        }
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txt_name, txt_time, txt_recurring, txt_message, txt_time1, txt_name1;
        protected LinearLayout main_layout;
        public RelativeLayout divider;
        protected LinearLayout linear1, linear2;

        public ViewHolder(View v) {
            super(v);
            txt_name = (TextView) v.findViewById(R.id.txt_name);
            txt_time = (TextView) v.findViewById(R.id.txt_time);
            txt_recurring = (TextView) v.findViewById(R.id.txt_recurring);
            txt_message = (TextView) v.findViewById(R.id.txt_message);
            main_layout = (LinearLayout) v.findViewById(R.id.main_layout);
            divider = (RelativeLayout) v.findViewById(R.id.divider);
            linear1 = (LinearLayout) v.findViewById(R.id.linear1);
            linear2 = (LinearLayout) v.findViewById(R.id.linear2);
            txt_name1 = (TextView) v.findViewById(R.id.txt_name1);
            txt_time1 = (TextView) v.findViewById(R.id.txt_time1);
        }
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtheader;


        public HeaderViewHolder(View v) {
            super(v);
            this.txtheader = (TextView) v.findViewById(R.id.txtheader);

        }
    }

    class delete extends AsyncTask<String, Void, String> {
        String SmsId, response;
        int position;

        public delete(String smsid, int pos) {
            this.SmsId = smsid;
            this.position = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentSms.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            response = Delete_Method("delete_androidsms", auth_code, SmsId);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentSms.progressbar.setVisibility(View.GONE);
            try {
                JSONObject jobj = new JSONObject(response);
                String status = jobj.optString("status");
                if (status.equals("true")) {

                    queries.deletesms(SmsId);
                    notifyadapter(position);
                    /*
                    if (listtype.equals("rec")) {
                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                            FragmentSms.recurringsmslist.remove(position);
                            FragmentSms.recurringsmslist.remove(position - 1);
                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                            FragmentSms.recurringsmslist.remove(position);
                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentSms.recurringsmslist.remove(position);
                            FragmentSms.recurringsmslist.remove(position - 1);
                        } else {
                            FragmentSms.recurringsmslist.remove(position);

                        }

                        if (FragmentSms.recurringsmslist.size() == 0) {
                            FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                            FragmentSms.view_scheduled.setVisibility(View.GONE);
                            FragmentSms.dummy_text.setText("You have no SMS recurring");
                        }

                    } else if (listtype.equals("sch")) {
                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {

                            FragmentSms.schdulesmslist.remove(position);
                            FragmentSms.schdulesmslist.remove(position - 1);

                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {

                            FragmentSms.schdulesmslist.remove(position);

                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentSms.schdulesmslist.remove(position);
                            FragmentSms.schdulesmslist.remove(position - 1);
                        } else {
                            FragmentSms.schdulesmslist.remove(position);

                        }


                        if (FragmentSms.schdulesmslist.size() == 0) {
                            FragmentSms.view_dummy.setVisibility(View.VISIBLE);
                            FragmentSms.view_scheduled.setVisibility(View.GONE);
                            FragmentSms.dummy_text.setText("You have no SMS scheduled");
                        }
                    }
                    notifyDataSetChanged();
                */
                } else {
                    Toast.makeText(context, "Response Error", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String Delete_Method(String service_type, String auth_code, String id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sms_id", id);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMMM dd, EEEE");
            setdate = format1.format(newDate);
        } catch (Exception e) {

            setdate = senddate;
        }
        return setdate;

    }

    public String savetimeformatter(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (timeformat.equals("12")) {
                format1 = new SimpleDateFormat("hh:mm a");
            } else {
                format1 = new SimpleDateFormat("HH:mm");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }
}

