package com.app.delayed.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.fragments.FragmentFacebook;
import com.app.delayed.fragments.FragmentSms;
import com.app.delayed.fragments.FragmentTwitter;

/**
 * Created by android on 12/4/2015.
 */
public class PagerAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs;


    public PagerAdapter(FragmentManager fm,int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment chat = new FragmentChat();
                return chat;
            case 1:
                Fragment sms = new FragmentSms();
                return sms;
            case 2:
                Fragment email = new FragmentEmail();
                return email;
            case 3:
                Fragment facebook = new FragmentFacebook();
                return facebook;
            case 4:
                Fragment twitter = new FragmentTwitter();
                return twitter;

            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return mNumOfTabs;
    }
}


