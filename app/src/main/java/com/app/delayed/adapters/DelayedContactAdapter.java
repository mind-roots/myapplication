package com.app.delayed.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.ModelUser;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DelayedContactAdapter extends RecyclerView.Adapter<DelayedContactAdapter.ContactViewHolder> {
    String id;

    SharedPreferences prefs;
    private List<ModelUser> contactList;
    List<ModelUser> filterList = new ArrayList<>();
    Context ctx;
    DatabaseQueries dataquery;

    public DelayedContactAdapter(Context ctx, List<ModelUser> contactList) {
        this.contactList = contactList;
        this.ctx = ctx;
        filterList.addAll(contactList);
        prefs = ctx.getSharedPreferences("delayed", ctx.MODE_PRIVATE);
        id = prefs.getString("id", null);
        dataquery = new DatabaseQueries(ctx);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {
        ModelUser ci = contactList.get(i);

        try {
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(ci.user_name.charAt(0)), ctx.getResources().getColor(R.color.colorPrimary));

            if (ci.user_pic.length() > 5) {
                contactViewHolder.pic2.setImageDrawable(drawable);
                Picasso.with(ctx)
                        .load(ci.user_pic)
//                    .placeholder(R.mipmap.default_user)
//                    .error(R.mipmap.default_user)
                        .into(contactViewHolder.pic, new Callback() {
                            @Override
                            public void onSuccess() {
                                contactViewHolder.pic2.setVisibility(View.INVISIBLE);
                                contactViewHolder.pic.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                contactViewHolder.pic2.setVisibility(View.VISIBLE);
                                contactViewHolder.pic.setVisibility(View.INVISIBLE);
                            }
                        });

            } else {
                contactViewHolder.pic2.setVisibility(View.VISIBLE);
                contactViewHolder.pic.setVisibility(View.INVISIBLE);
                contactViewHolder.pic2.setImageDrawable(drawable);

            }
        } catch (Exception e) {
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound("+", ctx.getResources().getColor(R.color.colorPrimary));
            contactViewHolder.pic2.setVisibility(View.VISIBLE);
            contactViewHolder.pic.setVisibility(View.INVISIBLE);
            contactViewHolder.pic2.setImageDrawable(drawable);

        }
//        Glide.with(ctx)
//                .load(ci.user_pic)
//                        //  .placeholder(R.mipmap.avatar)
//                .error(R.mipmap.avatar)
//                .crossFade()
//                .into(contactViewHolder.pic);
        contactViewHolder.name.setText(ci.user_name);
        String number = "";
        if (ci.user_contact.length() == 10) {
            number = ci.user_contact;
        } else if (ci.user_contact.length() > 10) {
            number = ci.user_contact.substring(ci.user_contact.length() - 10);
        }
        if (ci.user_country_code == null) {
            contactViewHolder.msg.setText(number);
        } else {
            String code = ci.user_country_code.replaceAll("[+]", "");
            contactViewHolder.msg.setText(code + number);
        }

        if (ci.delayedtype.equals("0")) {
            contactViewHolder.time.setText("Invite to Delayd");
            contactViewHolder.time.setBackgroundColor(ctx.getResources().getColor(R.color.colorPrimary));
            contactViewHolder.time.setTextColor(Color.WHITE);
            contactViewHolder.time.setPadding(12, 4, 12, 4);
        } else {

            try {
                contactViewHolder.time.setText("");
                contactViewHolder.time.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));
                contactViewHolder.time.setPadding(0, 0, 0, 0);
                contactViewHolder.time.setBackgroundResource(0);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        contactViewHolder.item_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if (contactList.get(i).delayedtype.equals("3")) {
                    Intent intent = new Intent(ctx, CreateNewGroup.class);
                    intent.putExtra("isUpdate", false);
                    ctx.startActivity(intent);
                    ((Activity) ctx).finish();
                } else*/
                Utils.chat_section=0;
                if (contactList.get(i).delayedtype.equals("2")) {
                    String pids = contactList.get(i).user_id;
                    String prestids = dataquery.getexistingthread(pids, 1);
                    Intent intent = new Intent(ctx, ChattingActivity.class);
                    intent.putExtra("threadids", prestids);
                    intent.putExtra("contenttype", "recent");
                    intent.putExtra("participants_ids", contactList.get(i).user_id);
                    intent.putExtra("name", contactList.get(i).user_name);
                    intent.putExtra("image", contactList.get(i).user_pic);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(intent);
                    ((Activity) ctx).finish();
                } else if (contactList.get(i).delayedtype.equals("1")) {
                    String pids = contactList.get(i).user_id + "," + id + "/" + id + "," + contactList.get(i).user_id;
                    String prestids = dataquery.getexistingthread(pids, 0);
                    Intent intent = new Intent(ctx, ChattingActivity.class);
                    intent.putExtra("threadids", prestids);
                    intent.putExtra("contenttype", "recent");
                    intent.putExtra("participants_ids", contactList.get(i).user_id);
                    intent.putExtra("name", contactList.get(i).user_name);
                    intent.putExtra("image", contactList.get(i).user_pic);
//                    intent.putExtra("notification_status", contactList.get(i).notification_status);
//                    intent.putExtra("blocked_status", contactList.get(i).blocked_status);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(intent);
                    ((Activity) ctx).finish();
                } else if (contactList.get(i).delayedtype.equals("0")) {
                    Uri uri = Uri.parse("smsto:" + contactList.get(i).user_contact);
                    Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
                    smsIntent.putExtra("sms_body", "Install Delayd so we can schedule messages to each other!\nhttp://onelink.to/67tss9");
                    ctx.startActivity(smsIntent);
                }

            }
        });

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.chat_contact_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }


    /*****************************************
     * View holder
     ***************************************/
    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView pic;
        public ImageView pic2;
        public TextView name, time, msg;
        public LinearLayout item_click;

        public ContactViewHolder(View itemView) {
            super(itemView);

            pic = (CircularImageView) itemView.findViewById(R.id.pic);
            pic2 = (ImageView) itemView.findViewById(R.id.pic2);
            name = (TextView) itemView.findViewById(R.id.name);
            msg = (TextView) itemView.findViewById(R.id.msg);
            time = (TextView) itemView.findViewById(R.id.time);
            item_click = (LinearLayout) itemView.findViewById(R.id.item_click);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactList.clear();
        if (charText.length() == 0) {
            contactList.addAll(filterList);
        } else {
            for (ModelUser wp : filterList) {
                if (wp.user_name.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    contactList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}