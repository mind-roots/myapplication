package com.app.delayed.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.NewFacebook;
import com.app.delayed.R;
import com.app.delayed.fragments.FragmentFacebook;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.Model_facebook;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Abhijeet on 2/8/2016.
 */
public class Facebook_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private LayoutInflater inflater;
    public ArrayList<Model_facebook> data = new ArrayList<Model_facebook>();
    SharedPreferences prefs;
    String argument;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_VALUE = 2;
    private static final int TYPE_FB = 3;
    String listtype, auth_code, timeformat;
    boolean network;


    public Facebook_Adapter(Context context, ArrayList<Model_facebook> arrayList, String type) {
        this.context = context;
        this.data = arrayList;
        this.listtype = type;
        prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
        network = Methods.isNetworkConnected(context);

        auth_code = prefs.getString("auth_code", null);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).view_type.equals("header")) {
            return TYPE_HEADER;
        } else if (data.get(position).view_type.equals("values")) {
            return TYPE_VALUE;
        } else if (data.get(position).view_type.equals("facebook")) {
            return TYPE_FB;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VALUE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.facebook_list_item, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_FB) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.facebook_recurring, parent, false);
            return new FbViewHolder(v);
        }
        return null;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        timeformat = prefs.getString("timeformat", null);
        if (holder instanceof FbViewHolder) {
            final FbViewHolder fbhold = (FbViewHolder) holder;
            fbhold.txt_fbemail.setText(data.get(position).fb_email);
            fbhold.txt_fbname.setText(data.get(position).fb_name);
            fbhold.img_icon.setImageResource(R.mipmap.ic_facebook_b);
        } else if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hold = (HeaderViewHolder) holder;

            hold.txtheader.setText(savedateformatter(data.get(position).header_date));

        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;

            if (data.get(position).facebook_post.equals("")) {
                viewHolder.txt_message.setVisibility(View.GONE);
            } else {
                viewHolder.txt_message.setVisibility(View.VISIBLE);
                viewHolder.txt_message.setText(data.get(position).facebook_post);
            }
            if (position == data.size() - 1) {
                viewHolder.divider.setVisibility(View.GONE);
            } else if (data.get(position + 1).view_type.equals("header")) {
                viewHolder.divider.setVisibility(View.GONE);
            } else {
                viewHolder.divider.setVisibility(View.VISIBLE);
            }
            if (data.get(position).recurring_type.equals("0")) {
                //      viewHolder.txt_recurring.setText("No Recurring");
            } else if (data.get(position).recurring_type.equals("wd")) {
                viewHolder.txt_recurring.setText("Recur weekdays");
            } else if (data.get(position).recurring_type.equals("fn")) {
                viewHolder.txt_recurring.setText("Recur forthnightly");
            } else if (data.get(position).recurring_type.equals("d")) {
                viewHolder.txt_recurring.setText("Recur Daily");
            } else if (data.get(position).recurring_type.equals("w")) {
                viewHolder.txt_recurring.setText("Recur Weekly");
            } else if (data.get(position).recurring_type.equals("m")) {
                viewHolder.txt_recurring.setText("Recur Monthly");
            } else if (data.get(position).recurring_type.equals("y")) {
                viewHolder.txt_recurring.setText("Recur Yearly");
            } else {
                viewHolder.txt_recurring.setText("");
            }

            viewHolder.txt_time.setText(savetimeformatter(data.get(position).schedule_date));
            System.out.print("fb=" + data.get(position).facebook_image);


            if (data.get(position).facebook_image.equals("null") || data.get(position).facebook_image.length() == 0 || data.get(position).facebook_image == null) {
                viewHolder.img_post.setVisibility(View.GONE);
            } else {
                viewHolder.img_post.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(data.get(position).facebook_image)
                        .crossFade()
                        .into(viewHolder.img_post);
            }
            viewHolder.lay_click.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            String intenttype, recurtype;
                                                            if (FragmentFacebook.inttype == 2) {
                                                                intenttype = "nonedit";
                                                                recurtype = "Sent";
                                                            } else {
                                                                intenttype = "update";
                                                                recurtype = data.get(position).recurring_type;
                                                            }
                                                            Intent i = new Intent(context, NewFacebook.class);
                                                            i.putExtra("intenttype", intenttype);
                                                            i.putExtra("schedule_date", data.get(position).schedule_date);
                                                            i.putExtra("recurring_type", recurtype);
                                                            i.putExtra("facebook_post", data.get(position).facebook_post);
                                                            i.putExtra("facebook_image", data.get(position).facebook_image);
                                                            i.putExtra("fb_id", data.get(position).fb_id);
                                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            context.startActivity(i);

                                                        }
                                                    }

            );
            viewHolder.lay_click.setOnLongClickListener(new View.OnLongClickListener()

                                                        {
                                                            @Override
                                                            public boolean onLongClick(View v) {
                                                                if (FragmentFacebook.inttype == 2) {
                                                                } else {
                                                                    try {
                                                                        dialog("Alert!", "Are you sure you want to delete this post? ", android.R.drawable.ic_dialog_info, position);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }

                                                                }
                                                                return false;
                                                            }
                                                        }

            );
        }

    }

    public void dialog(String title, String msg, int icon, final int pos) throws IllegalStateException {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //   data.remove(pos);

                        String fbid = data.get(pos).fb_id;
                        if (!network) {
                            Methods.conDialog(context);

                        } else {
                            new delete(fbid, pos).execute();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txt_time, txt_recurring, txt_message;
        ImageView img_post;
        LinearLayout lay_click;
        public RelativeLayout divider;

        public ViewHolder(View v) {
            super(v);
            txt_time = (TextView) v.findViewById(R.id.txt_time);
            txt_recurring = (TextView) v.findViewById(R.id.txt_recurring);
            txt_message = (TextView) v.findViewById(R.id.txt_message);
            img_post = (ImageView) v.findViewById(R.id.img_post);
            lay_click = (LinearLayout) v.findViewById(R.id.lay_click);
            divider = (RelativeLayout) v.findViewById(R.id.divider);
        }
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtheader;


        public HeaderViewHolder(View v) {
            super(v);
            this.txtheader = (TextView) v.findViewById(R.id.txtheader);

        }
    }

    class FbViewHolder extends RecyclerView.ViewHolder {

        protected TextView txt_fbemail, txt_fbname;

        protected ImageView img_icon;

        public FbViewHolder(View v) {
            super(v);
            this.txt_fbemail = (TextView) v.findViewById(R.id.txt_fbemail);
            this.txt_fbname = (TextView) v.findViewById(R.id.txt_fbname);
            this.img_icon = (ImageView) v.findViewById(R.id.img_icon);
        }
    }

    class delete extends AsyncTask<String, Void, String> {
        String SmsId, response;
        int position;

        public delete(String smsid, int pos) {
            this.SmsId = smsid;
            this.position = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FragmentFacebook.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            response = Delete_Method("delete_fbpost", auth_code, SmsId);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentFacebook.progressbar.setVisibility(View.GONE);
            try {
                JSONObject jobj = new JSONObject(response);
                String status = jobj.optString("status");
                if (status.equals("true")) {
                    if (listtype.equals("rec")) {

                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                            FragmentFacebook.recurringfblist.remove(position);
                            FragmentFacebook.recurringfblist.remove(position - 1);
                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                            FragmentFacebook.recurringfblist.remove(position);
                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentFacebook.recurringfblist.remove(position);
                            FragmentFacebook.recurringfblist.remove(position - 1);
                        } else {
                            FragmentFacebook.recurringfblist.remove(position);
                        }

                        if (FragmentFacebook.recurringfblist.size() <= 1) {
                            FragmentFacebook.recurringfblist.clear();
                            FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                            FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                            FragmentFacebook.dummy_text.setText("You don't have any \n post recurring.");
                        }
                    } else if (listtype.equals("sch")) {
                        if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("header")) {
                            FragmentFacebook.schdulefblist.remove(position);
                            FragmentFacebook.schdulefblist.remove(position - 1);
                        } else if (data.get(position - 1).view_type.equals("header") && data.size() != position + 1 && data.get(position + 1).view_type.equals("values")) {
                            FragmentFacebook.schdulefblist.remove(position);
                        } else if (data.get(position - 1).view_type.equals("header")) {
                            FragmentFacebook.schdulefblist.remove(position);
                            FragmentFacebook.schdulefblist.remove(position - 1);
                        } else {
                            FragmentFacebook.schdulefblist.remove(position);
                        }

                        if (FragmentFacebook.schdulefblist.size() <= 1) {
                            FragmentFacebook.schdulefblist.clear();
                            FragmentFacebook.view_dummy.setVisibility(View.VISIBLE);
                            FragmentFacebook.view_scheduled.setVisibility(View.GONE);
                            FragmentFacebook.dummy_text.setText("You don't have any \n Emails scheduled.");
                        }
                    }
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Response Error", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
            }
        }
    }

    public String Delete_Method(String service_type, String auth_code, String id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("post_id", id);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMMM dd, EEEE");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
            setdate = senddate;
        }
        return setdate;

    }

    public String savetimeformatter(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (timeformat.equals("12")) {
                format1 = new SimpleDateFormat("hh:mm a");
            } else {
                format1 = new SimpleDateFormat("HH:mm");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }
}

