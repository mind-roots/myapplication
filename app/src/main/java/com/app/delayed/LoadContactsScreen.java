package com.app.delayed;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ProgressBar;

import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/20/2016.
 */
public class LoadContactsScreen extends Activity {

    public static List<ModelContacts> arr_cont;
    SharedPreferences prefs;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_contacts);

        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        progress = (ProgressBar) findViewById(R.id.progress);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("timeformat", "12");
        editor.commit();
        if (Methods.isNetworkConnected(LoadContactsScreen.this)) {


            if (Build.VERSION.SDK_INT >= 23){
                AllowPermission();
            }else {
                new ContactsAsync().execute();
                //new getemail().execute();
            }

        } else {

            Snackbar.make(progress, "Please check network connection", Snackbar.LENGTH_LONG).show();
        }

    }


    private class ContactsAsync extends AsyncTask<Void, Void, Void> {

        String status;
        DatabaseQueries query;

        @Override
        protected Void doInBackground(Void... params) {
            query = new DatabaseQueries(LoadContactsScreen.this);
            arr_cont = Methods.FetchContacts(LoadContactsScreen.this);
            Methods.getNameEmailDetails(LoadContactsScreen.this);
            String json = new Gson().toJson(arr_cont);
        //    System.out.println("json=" + json);
            Log.i("dgdg",""+json);
            String auth_code = prefs.getString("auth_code", null);

            String response = ResponseMethod(auth_code, json);
            Log.i("JSON: ", "" + response);

            try {
                JSONObject object = new JSONObject(response);

                status = object.getString("status");
                if (status.equals("true")) {

                    JSONArray jsonArray = object.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject obj = jsonArray.getJSONObject(i);
                        ModelDelayedContact model = new ModelDelayedContact();

                        model.delayd_user_id = obj.getString("delayd_user_id");
                        model.profile_pic = obj.getString("profile_pic");
                        model.last_updated_date = obj.getString("last_updated_date");
                        model.contact_no = obj.getString("no");
                        model.country_code = obj.getString("country_code");
                        model.delayed_user = "1";
                        //delayedContacts.add(model);
                        query.UpdateContacts(model);
                    }

                } else {

                    // Do something when status false
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            startActivity(new Intent(LoadContactsScreen.this, Dashboard.class));
            finish();
        }
    }


    private String ResponseMethod(String auth_code, String data) {
        String res = "";

        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }

    /**************************************************
     * Allow Permissions
     * ************************************************/
    private void AllowPermission(){

        int hasSMSPermission = ActivityCompat.checkSelfPermission(LoadContactsScreen.this, Manifest.permission.READ_CONTACTS);
        List<String> permissions = new ArrayList<String>();

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( android.Manifest.permission.READ_CONTACTS );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(LoadContactsScreen.this, permissions.toArray(new String[permissions.size()]), 100);
        }else {
            new ContactsAsync().execute();
            //new getemail().execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        new ContactsAsync().execute();
                      //  new getemail().execute();
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d( "Permissions", "Permission Denied: " + permissions[i] );
                        startActivity(new Intent(LoadContactsScreen.this, Dashboard.class));
                        finish();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }


    /*************************************
     * Load emails
     ***********************************/
    class getemail extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            Methods.getNameEmailDetails(LoadContactsScreen.this);
           /* for (int i = 0; i < emails.size(); i++) {
                item = new Person(emails.get(i).email, emails.get(i).email);
                userlist.add(item);

            }*/

          //  isloademail = true;
            return null;
        }
    }

}
