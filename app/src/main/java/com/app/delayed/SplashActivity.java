package com.app.delayed;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 2/24/2016.
 */
public class SplashActivity extends AppCompatActivity {
    SharedPreferences prefs;
    String auth_code, pin_nos;
    boolean b;
    List<ModelContacts> arr_cont = new ArrayList<>();
    DatabaseQueries dataquery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        b = Methods.isNetworkConnected(SplashActivity.this);
        prefs = getSharedPreferences("delayed", SplashActivity.this.MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        pin_nos = prefs.getString("pin_nos", null);
        dataquery = new DatabaseQueries(SplashActivity.this);
        try {
            ContentValues cv = new ContentValues();
            cv.put("badgecount", 0);
            getContentResolver().update(Uri.parse("content://com.sec.badge/apps"), cv, "package=?", new String[]{getPackageName()});
        } catch (Exception e) {
        }
        loaddata();


    }

    private void loaddata() {

        if (auth_code != null && pin_nos != null) {
//                    startActivity(new Intent(SplashActivity.this, Dashboard.class));
//                    // startActivity(new Intent(SplashActivity.this, LoadContactsScreen.class));
//                    finish();
            new contactsync().execute();
        }else{
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    if (auth_code != null && pin_nos != null) {
//                    startActivity(new Intent(SplashActivity.this, Dashboard.class));
//                    // startActivity(new Intent(SplashActivity.this, LoadContactsScreen.class));
//                    finish();
                        // new contactsync().execute();
                    } else {
                        startActivity(new Intent(SplashActivity.this, FirstScreen.class));
                        finish();
                    }

                }
            }, 3000);
        }
    }

    /*************************************
     * Update contacts
     ***********************************/
    class contactsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Methods.FetchContacts(SplashActivity.this);
                arr_cont = dataquery.getupdateduserlist();

                if (arr_cont.size() > 0) {
                    String json = new Gson().toJson(arr_cont);
                    //
                    String auth_code = prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String status = object.getString("status");
                    if (status.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject obj = jsonArray.getJSONObject(i);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = obj.getString("delayd_user_id");
                            model.profile_pic = obj.getString("profile_pic");
                            model.last_updated_date = obj.getString("last_updated_date");
                            model.contact_no = obj.getString("no");
                            model.country_code = obj.getString("country_code");
                            model.delayed_user = "1";
                            //delayedContacts.add(model);
                            dataquery.UpdateContacts(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            startActivity(new Intent(SplashActivity.this, Dashboard.class));
            // startActivity(new Intent(SplashActivity.this, LoadContactsScreen.class));
            finish();
        }
    }


    private String ResponseMethod(String auth_code, String data) {
        String res = "";

        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }

}
