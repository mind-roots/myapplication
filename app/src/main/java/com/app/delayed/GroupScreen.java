package com.app.delayed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.delayed.adapters.GroupAdapter;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.ModelGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/4/2016.
 */
public class GroupScreen extends AppCompatActivity {

    Toolbar toolbar;
    TextView title;
    RelativeLayout create;
    public static RelativeLayout lay_list, lay_dummy;
    ListView grp_list;
    public static List<ModelGroup> groupList = new ArrayList<>();
    GroupAdapter adapter;
    String menuvisibilty;
    DatabaseQueries dataquery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_screen);

        init();

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupScreen.this, NewGroup.class);
                intent.putExtra("IntentType", "new");
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
        groupList = dataquery.getGroup();
        adapter = new GroupAdapter(GroupScreen.this, groupList, menuvisibilty);
        grp_list.setAdapter(adapter);

        if (groupList.size() >= 1) {

            lay_list.setVisibility(View.VISIBLE);
            lay_dummy.setVisibility(View.GONE);

        } else {
            lay_list.setVisibility(View.GONE);
            lay_dummy.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }

    //************Innitialize UI elements**************
    private void init() {
        Intent intent = getIntent();
        menuvisibilty = intent.getStringExtra("menu");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dataquery = new DatabaseQueries(GroupScreen.this);
         title = (TextView)findViewById(R.id.toolbar_title);
        title.setText("Groups");
        setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        create = (RelativeLayout) findViewById(R.id.btn_create);
        lay_list = (RelativeLayout) findViewById(R.id.lay_list);
        lay_dummy = (RelativeLayout) findViewById(R.id.lay_dummy);
        grp_list = (ListView) findViewById(R.id.list_groups);

//        adapter = new GroupAdapter(GroupScreen.this, groupList, menuvisibilty);
//        grp_list.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        if (menuvisibilty.equals("no")) {
            menu.findItem(R.id.action_add).setVisible(false);
        } else {
            menu.findItem(R.id.action_add).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_add) {
            Intent intent = new Intent(GroupScreen.this, NewGroup.class);
            intent.putExtra("IntentType", "new");
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
