package com.app.delayed;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentFacebook;
import com.app.delayed.fragments.FragmentTwitter;
import com.app.delayed.model.CircleTransform;
import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Set;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Balvinder on 2/2/2016.
 */
public class Settings extends AppCompatActivity {

    Toolbar toolbar;
    TextView title;
    TextView txt_email, txt_credits, txt_tw_email, txt_fb_email, txt_user_name, txt_user_numbr;
    LinearLayout ll_profile, ll_sms, ll_email, ll_fb, ll_tweet;
    TextView version_code;
    RelativeLayout main_layout, rl_delete, rl_feedback, rl_issue,rl_rating;
    SharedPreferences prefs;
    String auth_code, access_token, token_secret;
    CircularImageView img_profile;
    ImageView pic2;
    ProgressBar progressbar;
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;
    private static Twitter twitter;
    private static RequestToken requestToken;
    public static final int WEBVIEW_REQUEST_CODE = 100;
    boolean network;
    CallbackManager callbackManager;
    JSONParser parser;
    Switch sw_time;
    DatabaseQueries dataquery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        init();
        setvalue();
       // new ShowCredits().execute();
        onClicks();
    }


    //************Innitialize UI elements**************
    private void init() {
        dataquery = new DatabaseQueries(Settings.this);
        parser = new JSONParser();
        callbackManager = CallbackManager.Factory.create();
        network = Methods.isNetworkConnected(Settings.this);
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        setTitle("");
        title.setText("Settings");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);

        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        sw_time = (Switch) findViewById(R.id.sw_time);
        txt_credits = (TextView) findViewById(R.id.txt_credits);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_tw_email = (TextView) findViewById(R.id.txt_tw_email);
        txt_fb_email = (TextView) findViewById(R.id.txt_fb_email);
        txt_user_numbr = (TextView) findViewById(R.id.txt_user_num);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        version_code = (TextView) findViewById(R.id.version_code);
        img_profile = (CircularImageView) findViewById(R.id.img_profile);
        pic2 = (ImageView) findViewById(R.id.pic2);

        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_sms = (LinearLayout) findViewById(R.id.ll_sms);
        ll_email = (LinearLayout) findViewById(R.id.ll_email);
        ll_fb = (LinearLayout) findViewById(R.id.ll_fb);
        ll_tweet = (LinearLayout) findViewById(R.id.ll_tweet);

        main_layout = (RelativeLayout) findViewById(R.id.main_layout);
        rl_delete = (RelativeLayout) findViewById(R.id.rl_delete);
        rl_feedback = (RelativeLayout) findViewById(R.id.rl_feedback);
        rl_issue = (RelativeLayout) findViewById(R.id.rl_issue);
        rl_rating = (RelativeLayout) findViewById(R.id.rl_rating);
        version_code.setText(BuildConfig.VERSION_NAME);
    }

    private void onClicks() {
        sw_time.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("timeformat", "24");
                    editor.commit();
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("timeformat", "12");
                    editor.commit();
                }
            }
        });
        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, ProfileScreen.class);
                intent.putExtra("Intenttype", "update");
                startActivity(intent);
            }
        });
        ll_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("setting_email", null) != null && !prefs.getString("setting_email", null).equals("null") && prefs.getString("setting_email", null).length() != 0) {
                    popdialog("Update", "Update mail", "Update", prefs.getString("setting_email", null));
                } else {
                    popdialog("Enter", "Enter mail", "Enter", "");

                }

            }
        });
        ll_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this, BuyCredits.class).putExtra("buy",true));
            }
        });
        ll_tweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length()>0) {
                    dialog("Alert!", "Are you sure you want to disconnect with Twitter? All scheduled and recurring tweets will be deleted.", android.R.drawable.ic_dialog_info, "logout_twitter");

                } else {
                    new loginTwitter().execute();
                }
            }
        });
        ll_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {

                        dialog("Alert!", "Are you sure you want to disconnect with Facebook? All scheduled and recurring posts will be deleted.", android.R.drawable.ic_dialog_info, "logout_fb");
                    } else {
                        loginToFacebook();
                        //  Snackbar.make(ll_fb, "Different hash key", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        rl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog("Alert!", "Are you sure you want to delete this account?", android.R.drawable.ic_dialog_info, "del_account");

            }
        });

        rl_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Settings.this, Feedback.class));
                String[] emails = {"hello@delaydapp.com"};
                Utils.shareToGMail(Settings.this,emails,"Feedback about Delayd", "");
            }
        });

        rl_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.app.delayed&hl=en"));
                // intent.putExtra("link","");
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivity(intent);
                }else{
                    Toast.makeText(Settings.this, "No application found to respond this intent.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rl_issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this, InstaShake.class));
            }
        });
    }

    /*******************************************************
     * Check network before logout
     * *****************************************************/
    public void dialog(String title, String msg, int icon, final String type) {
        new AlertDialog.Builder(Settings.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (!network) {
                            Methods.conDialog(Settings.this);
                        } else {
                            new logout(type).execute();
                        }


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })

                .setIcon(icon)
                .show();

    }

    private void setvalue() {

        try {
            if (prefs.getString("timeformat", null).equals("24")) {
                sw_time.setChecked(true);
            } else {
                sw_time.setChecked(false);
            }
        }catch (Exception e){
            e.printStackTrace();
            sw_time.setChecked(false);
        }


        if (!prefs.getString("fb_token", null).equals("") && prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length() != 0) {
            txt_fb_email.setText("" + (prefs.getString("fb_name", null)));
        } else {
            txt_fb_email.setText("Not Connected");
        }
        if (prefs.getString("setting_email", null) != null && !prefs.getString("setting_email", null).equals("null") && prefs.getString("setting_email", null).length() != 0) {
            txt_email.setText("" + (prefs.getString("setting_email", null)));
        }
        try{
            if (!prefs.getString("twitter_uname", null).equals("") && prefs.getString("twitter_uname", null) != null && !prefs.getString("twitter_uname", null).equals("null") && prefs.getString("twitter_uname", null).length() != 0) {
                txt_tw_email.setText("" + (prefs.getString("twitter_uname", null)));
            } else {
                txt_tw_email.setText("Not Connected");
            }
        }catch (Exception e){

        }

        if (prefs.getString("name", null) != null || !prefs.getString("name", null).equals("null") || prefs.getString("name", null).length() != 0) {
            txt_user_name.setText("" + (prefs.getString("name", null)));
        }
        if (prefs.getString("mobile_no", null) != null || !prefs.getString("mobile_no", null).equals("null") || prefs.getString("mobile_no", null).length() != 0) {
            txt_user_numbr.setText("" + (prefs.getString("mobile_no", null)));
        }
        if (prefs.getString("credits", null) != null || !prefs.getString("credits", null).equals("null") || prefs.getString("credits", null).length() != 0) {
            txt_credits.setText("" + (prefs.getString("credits", null)));
        }
        if (prefs.getString("chat_icon", null) != null || !prefs.getString("chat_icon", null).equals("null") || prefs.getString("chat_icon", null).length() != 0) {

            int size = Methods.ReturnSize(Settings.this);
            System.out.print("size=" + size);
         /*   Glide.with(Settings.this)
                    .load(prefs.getString("chat_icon", null))
                    .crossFade()
                    .into(img_profile);*/
//            Picasso.with(Settings.this)
//                    .load(prefs.getString("chat_icon", null))
//                    .placeholder(R.mipmap.default_user)
//                    .into(img_profile);

            try {

                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(prefs.getString("name", "+").charAt(0)),getResources().getColor(R.color.colorPrimary));

                if (prefs.getString("chat_icon", null).length()>5) {
                    pic2.setImageDrawable(drawable);
                    Picasso.with(Settings.this)
                            .load(prefs.getString("chat_icon", null))
//                        .placeholder(drawable)
//                        .error(drawable)
                            .into(img_profile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    pic2.setVisibility(View.INVISIBLE);
                                    img_profile.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onError() {
                                    pic2.setVisibility(View.VISIBLE);
                                    img_profile.setVisibility(View.INVISIBLE);
                                }
                            });


                }else {
                    pic2.setVisibility(View.VISIBLE);
                    img_profile.setVisibility(View.INVISIBLE);
                    pic2.setImageDrawable(drawable);
                }

            } catch (Exception e) {
                TextDrawable drawable = TextDrawable.builder()
                        .buildRound("+",getResources().getColor(R.color.colorPrimary));
                pic2.setVisibility(View.VISIBLE);
                img_profile.setVisibility(View.INVISIBLE);
                pic2.setImageDrawable(drawable);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // *********************************************Facebook Login**********************************
    public void loginToFacebook() {
        try {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            access_token = com.facebook.AccessToken.getCurrentAccessToken().getToken();
                            Log.i("Fb Access Token:", "" + access_token);
                            Dashboard.isFb = true;
                            new Update_fb_details().execute();
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            // Snackbar.make(btn_compose, "" + exception.toString(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {

            Snackbar.make(toolbar, "Error : "+e, Snackbar.LENGTH_SHORT).show();
            Log.i("Error: ",""+e);
        }

    }


    //*************************************Fb Async Task Class**************************************
    public class Update_fb_details extends AsyncTask<Void, Void, Void> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = FbUpdate_Method(prefs.getString("auth_code", null), access_token);
            Log.i("Response:", "" + response);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            updateprefs(response, "logout_fb");
        }
    }

    //*************************************Fb details method****************************************
    public String FbUpdate_Method(String auth_code, String fb_token) {
        String res = null;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("fb_token", fb_token);
        res = parser.getJSONFromUrl(Utils.update_facebook_detail, builder);
        return res;
    }

    class loginTwitter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            try {
                final Intent intent = new Intent(Settings.this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, 100);
            } catch (Exception e) {
                Snackbar.make(ll_tweet, "Slow Internet Connection", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == 100) {

            try {
                String verifier = data.getExtras().getString(oAuthVerifier);
                Log.i("Twitter verifier:", "" + verifier);
                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                access_token = accessToken.getToken();
                token_secret = accessToken.getTokenSecret();

                Dashboard.isTw = true;

                new Update_tw_details().execute();
            } catch (Exception e) {
              //  Log.e("Twitter Login Failed", e.getMessage());
                Toast.makeText(Settings.this, "Wrong username or password.", Toast.LENGTH_SHORT).show();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
        //     callbackManager.onActivityResult(requestCode, resultCode, data);
        // client.onActivityResult(requestCode, resultCode, data);
    }

    //*************************************Tw Async Task Class**************************************
    public class Update_tw_details extends AsyncTask<Void, Void, Void> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = TwUpdate_Method(prefs.getString("auth_code", null), access_token, token_secret);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            updateprefs(response, "logout_twitter");

        }
    }


    public String TwUpdate_Method(String auth_code, String twitter_token, String twitter_secret) {
        String res = null;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("twitter_token", twitter_token)
                .appendQueryParameter("twitter_secret", twitter_secret);
        res = parser.getJSONFromUrl(Utils.update_twitter_detail, builder);
        return res;
    }

    private void popdialog(final String title, final String message, final String text, final String name) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this, R.style.DialogTheme1);
        builder.setTitle(title);
        builder.setMessage(message);
        LayoutInflater inflater = LayoutInflater.from(Settings.this);
        View view = inflater.inflate(R.layout.edittext, null);
        final EditText input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter email");
        input.setText(name);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        // input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        // input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        input.setSelection(name.length());
        input.setTextColor(Color.BLACK);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(text, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                HideKayboard();
                if (input.length() != 0) {
                    if (!network) {
                        Methods.conDialog(Settings.this);
                    } else {
                        new updateemail(input.getText().toString()).execute();
                    }
                } else {
                    Snackbar.make(ll_fb, "Please enter required feild.", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HideKayboard();
                dialog.dismiss();
            }
        });
        builder.show();

    }

    class updateemail extends AsyncTask<String, Void, String> {
        String email, response;

        public updateemail(String s) {
            this.email = s;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {


            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "update_profile_email")
                    .appendQueryParameter("email", email)
                    .appendQueryParameter("auth_code", auth_code);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);
            updateprefs(response, "email");
        }
    }

    class logout extends AsyncTask<String, Void, String> {
        String servicetype, response;

        public logout(String service) {
            this.servicetype = service;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", servicetype)
                    .appendQueryParameter("auth_code", auth_code);
            response = parser.getJSONFromUrl(Utils.base_url, builder);

            Log.i("Logout res: ",response);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);
            if (servicetype.equals("del_account")) {
                try {
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("true")) {
                        Dashboard.newchat=true;
                        prefs.edit().clear().commit();
                        dataquery.delete("user_contacts");
                        dataquery.delete("grouping");
                        dataquery.delete("message_threads");
                        dataquery.delete("message_thread_content");
                        dataquery.delete("scheduled_content_info");
                        Intent intent = new Intent(Settings.this, FirstScreen.class);
                        startActivity(intent);
                        finish();
                        for (int i = 0; i < ActivityStack.activity.size(); i++) {
                            ActivityStack.activity.get(i).finish();
                        }
                    } else {
                        Dashboard.newchat=true;
                        prefs.edit().clear().commit();
                        dataquery.delete("user_contacts");
                        dataquery.delete("grouping");
                        dataquery.delete("message_threads");
                        dataquery.delete("message_thread_content");
                        dataquery.delete("scheduled_content_info");
                        Intent intent = new Intent(Settings.this, FirstScreen.class);
                        startActivity(intent);
                        finish();
                        for (int i = 0; i < ActivityStack.activity.size(); i++) {
                            ActivityStack.activity.get(i).finish();
                        }
                        Snackbar.make(rl_delete, "Response Error.", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Dashboard.newchat=true;
                    prefs.edit().clear().commit();
                    dataquery.delete("user_contacts");
                    dataquery.delete("grouping");
                    dataquery.delete("message_threads");
                    dataquery.delete("message_thread_content");
                    dataquery.delete("scheduled_content_info");
                    Intent intent = new Intent(Settings.this, FirstScreen.class);
                    startActivity(intent);
                    finish();
                    for (int i = 0; i < ActivityStack.activity.size(); i++) {
                        ActivityStack.activity.get(i).finish();
                    }
                }
            } else {
                updateprefs(response, servicetype);

            }

        }
    }

    public void updateprefs(String response, String type) {
        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.getString("status");
            if (status.equals("true")) {
                JSONObject pin_nos = obj.optJSONObject("pin_nos");
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("id", pin_nos.optString("id"));
                editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                editor.putString("country_id", pin_nos.optString("country_id"));
                editor.putString("country", pin_nos.optString("country"));
                editor.putString("fb_status", pin_nos.optString("status"));
                editor.putString("pin", pin_nos.optString("pin"));
               // String name = URLDecoder.decode(pin_nos.optString("name"),"UTF-8");
                editor.putString("name", pin_nos.optString("name"));
                editor.putString("setting_email", pin_nos.optString("email"));
                editor.putString("fb_name", pin_nos.optString("fb_name"));
                editor.putString("fb_email", pin_nos.optString("fb_email"));
                editor.putString("fb_token", pin_nos.optString("fb_token"));
                editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                editor.putString("credits", pin_nos.optString("credits"));
                editor.putString("flag", pin_nos.optString("flag"));
                editor.putString("auth_code", pin_nos.optString("auth_code"));
                editor.putString("token_id", pin_nos.optString("token_id"));
                editor.putString("token_type", pin_nos.optString("token_type"));
                editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                editor.putString("last_updated", pin_nos.optString("last_updated"));
                editor.commit();
                if (type.equals("logout_twitter")) {
                    android.webkit.CookieManager.getInstance().removeAllCookie();
                    if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length()>0) {
                        FragmentTwitter.txt_btn_compose.setText("Compose Tweet");
                    } else {
                        FragmentTwitter.txt_btn_compose.setText("Connect");
                    }
                    FragmentTwitter.recurringtweetlist.clear();
                    FragmentTwitter.schduletweetlist.clear();
                    FragmentTwitter.posttweetlist.clear();
                    Dashboard.newtweet = true;
                } else if (type.equals("logout_fb")) {
                    LoginManager.getInstance().logOut();
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                        FragmentFacebook.txt_btn_compose.setText("Compose Post");
                    } else {
                        FragmentFacebook.txt_btn_compose.setText("Connect");
                    }
                    FragmentFacebook.recurringfblist.clear();
                    FragmentFacebook.schdulefblist.clear();
                    FragmentFacebook.postfblist.clear();
                    Dashboard.newfb = true;
                }
                setvalue();
            } else {
                Snackbar.make(main_layout, "Response Error", Snackbar.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setvalue();
    }

    /**************************************************
     * Update credits webservice
     * ************************************************/
    private class ShowCredits extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {

            String response = CreditMethod(auth_code);
            Log.i("credits response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                    editor.putString("name", pin_nos.optString("name"));
                    editor.putString("setting_email", pin_nos.optString("email"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                    editor.commit();

                } else {
                    // Snackbar.make(edt_name, "Response Error" + status, Snackbar.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (prefs.getString("credits", null) != null || !prefs.getString("credits", null).equals("null") || prefs.getString("credits", null).length() != 0) {
                txt_credits.setText("" + (prefs.getString("credits", null)));
            }

        }
    }

    //***********Network call for credits update***************
    private String CreditMethod(String auth_code){
        String res="";
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "show_credits")
                .appendQueryParameter("auth_code",auth_code);

        res = new JSONParser().getJSONFromUrl(Utils.base_url,builder);
        return res;
    }

}
