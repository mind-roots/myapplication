package com.app.delayed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

/**
 * Created by Balvinder on 5/9/2016.
 */
public class EmailTextArea extends Activity {

    RelativeLayout mFinish;
    EditText mTextArea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_text_area);

        init();

        mFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent();
                in.putExtra("text", mTextArea.getText().toString());
                setResult(RESULT_OK, in);
                finish();
            }
        });

    }

    private void init(){

        mFinish = (RelativeLayout)findViewById(R.id.btn_finish);
        mTextArea = (EditText)findViewById(R.id.text);

        mTextArea.setText(getIntent().getStringExtra("text"));
        mTextArea.setSelection(mTextArea.getText().length());
    }
}
