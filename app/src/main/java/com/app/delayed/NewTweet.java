package com.app.delayed;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.delayed.adapters.TweeterAdapter;
import com.app.delayed.chatExtended.Entities.Image;
import com.app.delayed.fragments.FragmentTwitter;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelTwitterData;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Balvinder on 2/1/2016.
 */
public class NewTweet extends AppCompatActivity {

    Toolbar toolbar;
    TextView char_count, txt_uname, txt_hname, title;
    EditText edt_message;
    EditText edt_set_time, edt_set_recurring;
    int curday, curmont, curyear, curhour, curmin;

    int rec_id;
    Calendar c, c1;
    ProgressBar progressbar;
    RelativeLayout btn_schedule;
    TextView mSchText;
    boolean network;
    SharedPreferences prefs;
    CharSequence[] items = {"Non recurring", "Recur daily", "Recur weekdays", "Recur weekly", "Recur forthnightly", "Recur monthly", "Recur yearly"};
    String[] sentitems = {"0", "d", "wd", "w", "fn", "m", "y"};
    String recurring_type, intenttype;
    String schedule_date, twitter_post, twitter_id,auth_code;
    boolean valid = true;
    Methods method;
    int TIME_PICKER_INTERVAL = 5;
    boolean hourstype = false;
    boolean setr = false;
    int style, compareday, comparemonth;
    int uphour, upmin, upday, upmonth, upyear;
    ImageView mTimeArrow, mSchArrow;
    private boolean isFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_tweet);

        init();

        clickevents();

    }

    //************Initialize UI elements**************
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        network = Methods.isNetworkConnected(NewTweet.this);
        c = Calendar.getInstance();
        c1 = Calendar.getInstance();
        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);
        char_count = (TextView) findViewById(R.id.char_count);
        edt_message = (EditText) findViewById(R.id.message);
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        if (prefs.getString("timeformat", null).equals("24")) {
            hourstype = true;
        } else {
            hourstype = false;
        }
        edt_set_recurring = (EditText) findViewById(R.id.set_recurring);
        edt_set_time = (EditText) findViewById(R.id.set_time);
        txt_hname = (TextView) findViewById(R.id.txt_hname);
        txt_uname = (TextView) findViewById(R.id.txt_uname);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        btn_schedule = (RelativeLayout) findViewById(R.id.btn_schedule);
        mTimeArrow = (ImageView) findViewById(R.id.arrow_time);
        mSchArrow = (ImageView) findViewById(R.id.arrow_sch);
        mSchText = (TextView) findViewById(R.id.sch_text);
        Intent intent = getIntent();
        intenttype = intent.getStringExtra("intenttype");
        if (intenttype.equals("update") || intenttype.equals("nonedit")) {

            schedule_date = intent.getStringExtra("schedule_date");
            recurring_type = intent.getStringExtra("recurring_type");
            twitter_post = intent.getStringExtra("twitter_post");
            twitter_id = intent.getStringExtra("twitter_id");

            setTitle("");
            title.setText("Update Tweet");
            mSchText.setText("Update");
            edt_set_time.setText(setformater(schedule_date));
            edt_message.setText(twitter_post);
            char_count.setText(twitter_post.length() + "/140");
        /*    if (recurring_type.equals("0")) {
                rec_id = 0;
                edt_set_recurring.setText("Non Recurring");
            } else if (recurring_type.equals("fn")) {
                rec_id = 1;
                edt_set_recurring.setText("Recurring forthnightly");
            } else if (recurring_type.equals("wd")) {
                rec_id = 2;
                edt_set_recurring.setText("Recurring weekdays");
            } else if (recurring_type.equals("d")) {
                rec_id = 3;
                edt_set_recurring.setText("Recurring Daily");
            } else if (recurring_type.equals("w")) {
                rec_id = 4;
                edt_set_recurring.setText("Recurring Weekly");
            } else if (recurring_type.equals("m")) {
                rec_id = 5;
                edt_set_recurring.setText("Recurring Monthly");
            } else if (recurring_type.equals("y")) {
                rec_id = 6;
                edt_set_recurring.setText("Recurring Yearly");
            }*/
            if (recurring_type.equals("0")) {
                rec_id = 0;
                edt_set_recurring.setText("Non Recurring");
            } else if (recurring_type.equals("d")) {
                rec_id = 1;
                edt_set_recurring.setText("Recur Daily");
            } else if (recurring_type.equals("wd")) {
                rec_id = 2;
                edt_set_recurring.setText("Recur weekdays");
            } else if (recurring_type.equals("w")) {
                rec_id = 3;
                edt_set_recurring.setText("Recur weekly");
            } else if (recurring_type.equals("fn")) {
                rec_id = 4;
                edt_set_recurring.setText("Recur forthnightly");
            } else if (recurring_type.equals("m")) {
                rec_id = 5;
                edt_set_recurring.setText("Recur Monthly");
            } else if (recurring_type.equals("y")) {
                rec_id = 6;
                edt_set_recurring.setText("Recur Yearly");
            }
            if (intenttype.equals("nonedit")) {
                edt_set_recurring.setText("Sent");
                btn_schedule.setVisibility(View.INVISIBLE);
                edt_message.setEnabled(false);
                edt_message.setTextColor(Color.BLACK);
                title.setText("Sent Tweet");
                mTimeArrow.setVisibility(View.INVISIBLE);
                mSchArrow.setVisibility(View.INVISIBLE);
                char_count.setVisibility(View.INVISIBLE);

            }
        } else {
            rec_id = 0;
            recurring_type = "0";
            edt_set_recurring.setText("Non recurring");
            setTitle("");
            title.setText("New Tweet");
            mSchText.setText("Schedule");
        }
        if (prefs.getString("twitter_hname", null) != null && !prefs.getString("twitter_hname", null).equals("")) {
            txt_hname.setText("@" + prefs.getString("twitter_hname", null));
        }
        if (prefs.getString("twitter_uname", null) != null && !prefs.getString("twitter_uname", null).equals("")) {
            txt_uname.setText(prefs.getString("twitter_uname", null));
        }
        method = new Methods(NewTweet.this, edt_set_time);
    }

    //************clickevents**************
    private void clickevents() {
        edt_set_recurring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (!intenttype.equals("nonedit")) {
                    RecurringPicker(NewTweet.this, edt_set_recurring);
                }
            }
        });

        edt_set_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
          /*      hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mYear = c.get(Calendar.YEAR);

                if (Build.VERSION.SDK_INT < 21) {
                    method.getdate(NewTweet.this, mYear, mMonth, mDay, hour, minute);
                } else {
                    builder = new StringBuilder();
                    Methods.datePicker(NewTweet.this, datePickerListener, mYear, mMonth, mDay);
                }*/
                int setday, setmonth, setyear;
                if (intenttype.equals("update") || intenttype.equals("nonedit")) {
                    updatevalues(edt_set_time.getText().toString());
                    setday = upday;
                    setmonth = upmonth;
                    setyear = upyear;
                } else {
                    setday = curday;
                    setmonth = curmont;
                    setyear = curyear;
                }
                if (!intenttype.equals("nonedit")) {
                    if (Build.VERSION.SDK_INT < 21) {
                        style = android.graphics.Color.TRANSPARENT;
                        datePickerbuildbelow(datePickerListener2, setyear, setmonth, setday);
                    } else {
                        style = R.style.DialogTheme1;
                        datePicker(datePickerListener1, setyear, setmonth, setday);
                    }
                }
            }
        });

        edt_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >= 140) {
                    Snackbar.make(edt_message, "Limit exceeds.", Snackbar.LENGTH_SHORT).show();
                    return;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                char_count.setText(s.length() + "/140");
            }
        });

        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_set_time.length() == 0) {
                    Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
                } else if (edt_set_recurring.length() == 0) {
                    Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
                } else if (edt_message.length() == 0) {
                    Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
                } else {
                    String sentdate = sentformatter(edt_set_time.getText().toString(), 0);
                    String trigdate = sentformatter(edt_set_time.getText().toString(), 1);
                    if (!network) {
                        Methods.conDialog(NewTweet.this);
                    } else {
                        new Twitter_Post(trigdate, sentdate, edt_message.getText().toString(), recurring_type).execute();
                    }
                }
            }
        });

    }

  /*  private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                int ti = 0;

                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {

                    if (selectedHour < curhour && valid) {
                        edt_set_time.setText("");
                        Snackbar.make(edt_set_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else if (selectedMinute < curmin && valid) {
                        edt_set_time.setText("");
                        Snackbar.make(edt_set_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else {

                        int set = selectedMinute;
                      *//*  if (selectedMinute > 0 && selectedMinute <= 7 || selectedMinute > 53 && selectedMinute <= 59) {
                            set = 0;
                        } else if (selectedMinute > 7 && selectedMinute <= 15 || selectedMinute > 15 && selectedMinute <= 23) {
                            set = 15;
                        } else if (selectedMinute > 23 && selectedMinute <= 30 || selectedMinute > 30 && selectedMinute <= 37) {
                            set = 30;
                        } else if (selectedMinute > 37 && selectedMinute <= 45 || selectedMinute > 45 && selectedMinute <= 53) {
                            set = 45;
                        } *//*
                        if (selectedMinute <= 15) {
                            set = 0;
                        } else if (selectedMinute <= 30) {
                            set = 15;
                        } else if (selectedMinute <= 45) {
                            set = 30;
                        } else if (selectedMinute <= 59) {
                            set = 45;
                        }

                        hour = selectedHour;
                        minute = set;

                        c.set(Calendar.HOUR_OF_DAY, hour);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                        String displayValue = timeFormatter.format(c.getTime());
                        System.out.println("value time : " + displayValue);

                        recurring_type = "0";
                        rec_id = 0;
                        edt_set_recurring.setText("Non recurring");
                        edt_set_time.setText(builder.append(", " + displayValue));
                    }

                }

            };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (year < curyear) {
                valid = false;
                edt_set_time.setText("");
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                edt_set_time.setText("");
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                edt_set_time.setText("");
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                c.set(Calendar.YEAR, mYear);
                c.set(Calendar.MONTH, mMonth);
                c.set(Calendar.DAY_OF_MONTH, mDay);
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy");
                String dateSet = format1.format(c.getTime());
                //dateSet = DateFormat.getDateInstance().format(c.getTime());
                System.out.println("value date : " + dateSet);
//            builder.append((dayOfMonth))
//                    .append(" ").append((monthOfYear)).append(", ").append((year));

                builder.append(dateSet);

                Methods.timePicker(NewTweet.this, timePickerListener, hour, minute);
            }
        }
    };*/

    //*************************************Post tweet Async Task Class******************************
    public class Twitter_Post extends AsyncTask<Void, Void, Void> {
        String trig_date, message, recu_type, response, schdate;

        public Twitter_Post(String trig_date, String schdate, String message, String recu_type) {

            this.trig_date = trig_date;
            this.schdate = schdate;
            this.message = message;
            this.recu_type = recu_type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (intenttype.equals("update")) {
                response = Update_TwPost_Method("update_twitter", prefs.getString("auth_code", null), message, recu_type);
            } else {
                response = TwPost_Method("twitter_post2", prefs.getString("auth_code", null), message, recu_type,
                        prefs.getString("token_id", null), prefs.getString("twitter_secret", null));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    FragmentTwitter.adaptertype = recurring_type;

                    new tweeter_thread().execute();
                } else {
                    progressbar.setVisibility(View.GONE);
                    Snackbar.make(btn_schedule, "Response Error", Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            }
        }
    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(NewTweet.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Dashboard.newtweet = true;
                        finish();

                    }
                })

                .setIcon(icon)
                .show();

    }

    //*************************************Tweet details method*************************************
    public String TwPost_Method(String service_type, String auth_code, String msg,
                                String recu_type, String token_id, String token_secret) {
        String res = null;
        JSONParser parser = new JSONParser();
        String mssg ="";
        try {
            mssg = URLEncoder.encode(msg,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("s_date", sentformatter(edt_set_time.getText().toString(), 0))
                .appendQueryParameter("trigger_date", sentformatter(edt_set_time.getText().toString(), 1))
                .appendQueryParameter("msg", mssg)
                .appendQueryParameter("recu_type", recu_type)
                .appendQueryParameter("token_id", token_id)
                .appendQueryParameter("token_secret", token_secret);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    //*************************************Tweet details method*************************************
    public String Update_TwPost_Method(String service_type, String auth_code, String msg,
                                       String recu_type) {
        String res = null;
        JSONParser parser = new JSONParser();
        String mssg ="";
        try {
            mssg = URLEncoder.encode(msg,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("twt_id", twitter_id)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("schedule_date", sentformatter(edt_set_time.getText().toString(), 0))
                .appendQueryParameter("trigger_date", sentformatter(edt_set_time.getText().toString(), 1))
                .appendQueryParameter("text", mssg)
                .appendQueryParameter("recurring_type", recu_type);

        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();

    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String sentformatter(String sendtime, int i) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1;
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            Date newDate = format1.parse(sendtime);
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (i == 0) {
                settym = format1.format(newDate);
            } else {
                format1.setTimeZone(TimeZone.getTimeZone("UTC"));
                settym = format1.format(newDate);
            }
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    public String setformater(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    //*************RecurringPicker***********
    private void RecurringPicker(Context ctx, final EditText recurring) {

        new AlertDialog.Builder(ctx, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, rec_id, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        rec_id = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button
                        recurring_type = sentitems[rec_id];
                        recurring.setText(items[rec_id]);

                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();


        // return selected_rec;
    }

    //-------------------------time and date dialogss-------------------------//

    /***************************************
     * Date picker
     *************************************/
    public void datePicker(DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(NewTweet.this, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }

    public void datePickerbuildbelow(DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(NewTweet.this, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            /*    if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();


            }
        });

        dialog.show();

    }

    public DatePickerDialog.OnDateSetListener datePickerListener2 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c1.set(Calendar.YEAR, mYear);
            c1.set(Calendar.MONTH, mMonth);
            c1.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;


        }
    };

    public void timePickerbelow() {


        isFirst = true;
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        int setmin, sethour;
        if ((intenttype.equals("update") || intenttype.equals("nonedit")) && compareday != day) {
            setmin = upmin;
            sethour = uphour;

        } else {
            setmin = minute;
            sethour = hour;
            if (setmin >= 55 && setmin <= 59) {
                setmin = 0;
                sethour = sethour + 1;
            } else {
                setmin = setmin + 5;
            }

        }

        if (minute >= 55 && minute <= 59) {
            minute = 0;
            hour = hour + 1;
        } else {
            minute = minute + 5;
        }




        final TimePickerDialogs timeee;

        timeee = new TimePickerDialogs(NewTweet.this, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                c1.set(Calendar.HOUR_OF_DAY, selectedHour);
                if (Build.VERSION.SDK_INT < 21) {
                    if (isFirst){
                        isFirst = false;
                        c1.set(Calendar.MINUTE, min);
                    }
                } else {
                    c1.set(Calendar.MINUTE, min);
                }
                SimpleDateFormat timeFormatter;
                if (hourstype) {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                } else {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                }
                String displayValue = timeFormatter.format(c1.getTime());
                edt_set_time.setText(displayValue);


            }
        }, sethour, setmin, hourstype);
        timeee.setMin(hour, minute);
        timeee.show();


    }

    //********************************
// Date change Listener
//*********************************
    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (year < curyear) {
                valid = false;
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                Snackbar.make(edt_set_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
               /* mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;*/

                c1.set(Calendar.YEAR, year);
                c1.set(Calendar.MONTH, monthOfYear);
                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                compareday = dayOfMonth;
                comparemonth = monthOfYear;
                if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }
                timePickerbelow();
                //   timePicker(ChattingActivity.this, timePickerListener1, curhour, curmin);


            }
        }
    };

    public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        private int minHour = -1;
        private int minMinute = -1;

        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private java.text.DateFormat dateFormat;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
//            this.hr = hourOfDay;
//            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            //    ct = Calendar.getInstance();
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT);

        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);


            boolean validTime = true;
            if (hourOfDay < minHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == minHour && (minute - 1) * 5 < minMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }

            if (hourOfDay > maxHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == maxHour && minute * 5 > maxMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }
            // if (hourOfDay==minHour&&)

            if (validTime) {
                currentHour = hourOfDay;

                currentMinute = minute;
            } else {
                int j = minMinute / 5;
                int r = minMinute % 5;
                if (r > 3) {
                    j = j + 1;
                }
                if (j < minute) {
                    currentMinute = minute;

                } else {
                    currentMinute = j;

                }

            }


            timePicker.setCurrentHour(currentHour);


            timePicker.setCurrentMinute(currentMinute);


        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            //  System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
                timePicker.setOnTimeChangedListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    //------------update values-----------------//
    public void updatevalues(String sendtime) {

        Date newDate;
        try {

            if (hourstype) {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                newDate = format1.parse(sendtime);
            } else {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                newDate = format1.parse(sendtime);
            }


         /*   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);*/
            Calendar call = Calendar.getInstance();
            call.setTime(newDate);

            uphour = call.get(Calendar.HOUR_OF_DAY);
            upmin = call.get(Calendar.MINUTE);
            upmonth = call.get(Calendar.MONTH);
            upday = call.get(Calendar.DAY_OF_MONTH);
            upyear = call.get(Calendar.YEAR);
        } catch (Exception e) {

        }

    }
    class tweeter_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            schedule_response = Gettweet_Method("twitter_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentTwitter.schduletweetlist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.schduletweetlist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.schduletweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                          //  String msg = URLDecoder.decode(iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.schduletweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Gettweet_Method("twitter_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentTwitter.recurringtweetlist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.recurringtweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.recurringtweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                         //   String msg = URLDecoder.decode(iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.recurringtweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Gettweet_Method("get_twitter_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentTwitter.posttweetlist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    FragmentTwitter.posttweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentTwitter.posttweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String msg = URLDecoder.decode(iobj.optString("twitter_post"),"UTF-8");
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            FragmentTwitter.posttweetlist.add(item);
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);

            if (intenttype.equals("update")) {
                dialog("Success!", "Your tweet has been updated", android.R.drawable.ic_dialog_info);
            } else {
                dialog("Success!", "Your tweet has been scheduled", android.R.drawable.ic_dialog_info);
            }

        }
    }

    public String Gettweet_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}
