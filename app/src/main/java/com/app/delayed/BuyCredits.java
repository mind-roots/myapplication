package com.app.delayed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.delayed.model.JSONParser;
import com.app.delayed.util.IabBroadcastReceiver;
import com.app.delayed.util.IabHelper;
import com.app.delayed.util.IabResult;
import com.app.delayed.util.Inventory;
import com.app.delayed.util.Purchase;
import com.app.delayed.utils.Utils;

/**
 * Created by Balvinder on 3/16/2016.
 */
public class BuyCredits extends AppCompatActivity {

    LinearLayout close;
    LinearLayout mCredit1, mCredit2, mCredit3, mCredit4;
    TextView mTitle_txt;
    static final String SKU_30 = "30_credits";
    static final String SKU_60 = "60_credits";
    static final String SKU_120 = "120_credits";
    static final String SKU_250 = "250_credits";
    IabHelper mHelper;
    IabBroadcastReceiver mBroadcastReceiver;
    String ITEM_SKU = "";
    String price = "1.12";
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String auth_code, mCredits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_credits);

        init();

        ClickEvents();

    }


    /***********************************
     * initialize UI elements
     *********************************/
    private void init() {

        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        // mCredits = prefs.getString("credits","0");
        close = (LinearLayout) findViewById(R.id.close);
        mCredit1 = (LinearLayout) findViewById(R.id.credit1);
        mCredit2 = (LinearLayout) findViewById(R.id.credit2);
        mCredit3 = (LinearLayout) findViewById(R.id.credit3);
        mCredit4 = (LinearLayout) findViewById(R.id.credit4);
        mTitle_txt = (TextView) findViewById(R.id.textTitle);

        if (getIntent().hasExtra("buy")) {
            mTitle_txt.setText(R.string.credit_title_buy);
        } else {
            mTitle_txt.setText(R.string.credit_title_out);
        }
        mHelper = new IabHelper(this, Utils.base64);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("Billing", "In-app Billing setup failed: " +
                            result);
                } else {
                    Log.d("Billing", "In-app Billing is set up OK");
                }
            }
        });
    }


    /**********************************
     * Click events
     ********************************/
    private void ClickEvents() {

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mCredit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ITEM_SKU = SKU_30;
                mHelper.launchPurchaseFlow(BuyCredits.this, ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
                mCredits = "30";
            }
        });


        mCredit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ITEM_SKU = SKU_60;
                mHelper.launchPurchaseFlow(BuyCredits.this, ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
                mCredits = "60";
            }
        });

        mCredit3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ITEM_SKU = SKU_120;
                mHelper.launchPurchaseFlow(BuyCredits.this, ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
                mCredits = "120";
            }
        });

        mCredit4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ITEM_SKU = SKU_250;
                mHelper.launchPurchaseFlow(BuyCredits.this, ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
                mCredits = "250";
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase) {
            if (result.isFailure()) {
                // Handle error
                return;
            } else {
                consumeItem();
            }


        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        //    click.setEnabled(true);
                        new UpdateCredits().execute();

                    } else {
                        // handle error
                        Snackbar.make(mCredit1,""+result.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();

        mCredits = null;
        prefs = null;

        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }


    /*************************************************
     * Update credits webservice
     *************************************************/
    private class UpdateCredits extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            String response = CreditsMethod(auth_code, mCredits);
            Log.i("Credits response : ", response);

            return null;
        }
    }

    /*********************************************
     * WEB METHOD TO UPDATE CREDITS
     *********************************************/
    private String CreditsMethod(String auth_code, String credits) {
        String res;
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("service_type", "get_credits")
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("credits", credits);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

}
