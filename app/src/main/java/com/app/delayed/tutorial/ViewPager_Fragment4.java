package com.app.delayed.tutorial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.delayed.Dashboard;
import com.app.delayed.LoadContactsScreen;
import com.app.delayed.R;


/**
 * Created by admin on 11/21/2015.
 */
public class ViewPager_Fragment4 extends Fragment {
    LinearLayout layoutClick;
    SharedPreferences prfs,instal;
    TextView start;
    Boolean value;
    SharedPreferences.Editor edt;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_four, container, false);

        start = (TextView) view.findViewById(R.id.start);

        prfs = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        instal =getActivity().getSharedPreferences("install", Context.MODE_PRIVATE);
        edt = instal.edit();
        edt.putString("instalvalue", "installed");
        edt.commit();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (prfs.getBoolean("logged",false)) {


                Intent i = new Intent(getActivity(), LoadContactsScreen.class);
                startActivity(i);
//                   getActivity().finish();
//                } else {
//                    Intent i = new Intent(getActivity(), SignUp_Activity.class);
//                    startActivity(i);
//                    getActivity().finish();
//                    edt = prfs.edit();
//                    edt.putBoolean("logged" ,true);
//                    edt.commit();
//                }


            }
        });
        return view;
    }
}
