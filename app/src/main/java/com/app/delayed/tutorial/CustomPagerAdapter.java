package com.app.delayed.tutorial;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 11/20/2015.
 */
public class CustomPagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;
    private List<Fragment> fragments;
    private static int NUM_ITEMS = 3;
    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragments = new ArrayList<Fragment>();
        fragments.add(new ViewPager_Fragment1());
        fragments.add(new ViewPager_Fragment2());
       // fragments.add(new ViewPager_Fragment3());
        fragments.add(new ViewPager_Fragment4());
    }
  @Override
    public Fragment getItem(int position) {

                return fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
