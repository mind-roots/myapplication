package com.app.delayed.tutorial;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toolbar;

import com.app.delayed.R;
import com.app.delayed.utils.ActivityStack;


public class ViewpagerActivity extends AppCompatActivity {
    CustomPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_activity);

        ActivityStack.activity.add(ViewpagerActivity.this);
        ViewPager defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
        CircleIndicator defaultIndicator = (CircleIndicator) findViewById(R.id.indicator_default);
        pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        defaultViewpager.setAdapter(pagerAdapter);
        defaultIndicator.setViewPager(defaultViewpager);



    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}