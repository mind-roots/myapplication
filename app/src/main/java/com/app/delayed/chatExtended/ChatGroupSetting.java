package com.app.delayed.chatExtended;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.Dashboard;
import com.app.delayed.R;
import com.app.delayed.chatExtended.CustomAdapters.ContactSuggestionAdapter;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelUser;
import com.app.delayed.model.MultipartUtility;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Balvinder on 3/16/2016.
 */
public class ChatGroupSetting extends AppCompatActivity {

    Toolbar mToolbar;
    TextView mTitle, mUsername;
    CircularImageView mPro_img;
    String mName, mImage="", mThread_id, mUser_id, mAuth, group_icon_value, mUsernos;
    String usernumber;
    SharedPreferences mPrefs;
    SharedPreferences.Editor editor;
    Switch mOn_off;
    DatabaseQueries queries;
    String mBlock_status = "0", mNoti_status = "0";
    String[] participent_ids;
    String my_id;
    List<ModelUser> participents = new ArrayList<>();
    LinearLayout mUser_list, mSharedImages, mProfile, mAdd;
    ContactSuggestionAdapter adapter;
    LayoutInflater inflater;
    View view;
    ViewHolder holder;
   // DatabaseQueries query;
    boolean isGroup = false;
    File path;
    StringBuilder number, ids;
    LinearLayout leave_grp;
    String updateid = "null";
    ImageView pic2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_settings);

        init();

        mSharedImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChatGroupSetting.this, SharedImages.class).putExtra("id", mThread_id));
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ChatGroupSetting.this, CreateNewGroup.class);
                in.putExtra("isUpdate", true);
                in.putExtra("g_name", mName);
                in.putExtra("g_img", mImage);
                startActivityForResult(in, 1);
            }
        });

        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(ChatGroupSetting.this, AddContactsToChatGroup.class);
                // in.putExtra("ids",mUser_id);
                startActivityForResult(in, 2);
            }
        });
        leave_grp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                Intent intent = new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT, Uri.parse("tel:" + "435345343")); //currentNum is my TextView, you can replace it with the number directly such as Uri.parse("tel:1293827")
                intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true); //skips the dialog box that asks the user to confirm creation of contacts
                startActivity(intent);*/

                dialog();
               // new leavegroup(mThread_id).execute();
            }
        });

        mOn_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    mNoti_status = "0";

                } else {
                    mNoti_status = "1";
                }
            }
        });
    }

    /*************************************
     * UI elements initialization
     ************************************/
    private void init() {
        inflater = LayoutInflater.from(ChatGroupSetting.this);
        queries = new DatabaseQueries(ChatGroupSetting.this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        setTitle("");
        mTitle.setText("Group setting");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent in = getIntent();
        mName = in.getStringExtra("name");
        mImage = in.getStringExtra("image");
        mThread_id = in.getStringExtra("threadids");
       // mUser_id = in.getStringExtra("chat_user_id");
        isGroup = in.getBooleanExtra("isGroup", false);
        mBlock_status = in.getStringExtra("block");
        mNoti_status = in.getStringExtra("noti");

        if (mImage == null){
            mImage = "";
        }
        mPrefs = getSharedPreferences("delayed", MODE_PRIVATE);
        mAuth = mPrefs.getString("auth_code", null);
        usernumber = mPrefs.getString("mobile_no", null);
        // mBlock = (LinearLayout) findViewById(R.id.block);
        mPro_img = (CircularImageView) findViewById(R.id.img_profile);
        pic2 = (ImageView) findViewById(R.id.pic2);
        mUsername = (TextView) findViewById(R.id.txt_user_name);
        mOn_off = (Switch) findViewById(R.id.on_off);
        mUser_list = (LinearLayout) findViewById(R.id.list);
        mSharedImages = (LinearLayout) findViewById(R.id.ll_shared);
        leave_grp = (LinearLayout) findViewById(R.id.leave_grp);
        mProfile = (LinearLayout) findViewById(R.id.ll_profile);
        mAdd = (LinearLayout) findViewById(R.id.add);

        mUsername.setText(mName);

        mUser_id = queries.getParticipents(mThread_id);
   /*     Glide.with(ChatGroupSetting.this)
                .load(mImage)
                .crossFade()
                .placeholder(R.mipmap.default_user)
                .into(mPro_img);*/
//        try {
//
//            Picasso.with(ChatGroupSetting.this)
//                    .load(mImage).placeholder(R.mipmap.default_user).into(mPro_img);
//
//        } catch (Exception e) {
//
//        }

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(mName.charAt(0)),getResources().getColor(R.color.colorPrimary));

            if (mImage.length()>5) {
                pic2.setImageDrawable(drawable);
                Picasso.with(ChatGroupSetting.this)
                        .load(mImage)
//                        .placeholder(R.mipmap.default_user)
//                        .error(R.mipmap.default_user)
                        .into(mPro_img, new Callback() {
                            @Override
                            public void onSuccess() {
                                pic2.setVisibility(View.INVISIBLE);
                                mPro_img.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                pic2.setVisibility(View.VISIBLE);
                                mPro_img.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                pic2.setVisibility(View.VISIBLE);
                mPro_img.setVisibility(View.INVISIBLE);
                pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {
            pic2.setVisibility(View.INVISIBLE);
            mPro_img.setVisibility(View.VISIBLE);
            mPro_img.setImageResource(R.mipmap.default_user);

        }


        // mBlock_status = query.getBlock(mThread_id);
        mNoti_status = queries.getNotification(mThread_id);

        if (mNoti_status.equals("0")) {
            mOn_off.setChecked(true);
        } else {
            mOn_off.setChecked(false);
        }

        //***********split group participents********

        gpparticipants();


//        adapter = new ContactSuggestionAdapter(ChatGroupSetting.this, participents);
//        mUser_list.setAdapter(adapter);
        //*******************************************
    }

    private void gpparticipants() {

        participent_ids = mUser_id.split(",");
        my_id = mPrefs.getString("id", "");
        number = new StringBuilder();
        for (String id : participent_ids) {

            if (!my_id.equals(id)) {

                ModelUser user = new ModelUser();
                user.user_name = queries.getname(id, DatabaseQueries.USER_NAME);
                user.user_contact = queries.getname(id, DatabaseQueries.USER_CONT_NO);
                user.user_pic = queries.getname(id, DatabaseQueries.USER_PRO_PIC);
                user.user_id = id;

                number.append(user.user_contact).append(",");
                // participents.add(user);
                inflateParticipents(user, mUser_list);

            } else {
                number.append(usernumber).append(",");
            }
        }
        mUsernos = number.toString();
        if (mUsernos.indexOf(",") != -1) {
            mUsernos = mUsernos.substring(0, mUsernos.length() - 1);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        new EnableDisableNotification().execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                mImage = data.getStringExtra("pic");
                mName = data.getStringExtra("name");
                editor = mPrefs.edit();
                editor.putString("title_name", mName).commit();

                mUsername.setText(mName);
                if (mImage.length() != 0&&!mImage.startsWith("http")) {
                    group_icon_value = "0";
                    path = new File(mImage);
                    setImage(path);
                } else {
                    group_icon_value = "1";
                }
                new new_thread("img").execute();
            } else if (requestCode == 2) {

                //***********split group participents********
                mUser_list.removeAllViews();
                mUser_id = mUser_id.concat(",") + data.getStringExtra("ids");
                participent_ids = mUser_id.split(",");
                //***********remove duplicacy***********
                participent_ids = removeDuplicates(participent_ids);
                //************************************
                number = new StringBuilder();
                ids = new StringBuilder();
                my_id = mPrefs.getString("id", "");
                for (String id : participent_ids) {

                    ids.append(id).append(",");
                    if (!my_id.equals(id)) {

                        ModelUser user = new ModelUser();
                        user.user_name = queries.getname(id, DatabaseQueries.USER_NAME);
                        user.user_contact = queries.Subset(queries.getname(id, DatabaseQueries.USER_CONT_NO));
                        user.user_pic = queries.getname(id, DatabaseQueries.USER_PRO_PIC);
                        user.user_id = id;
                        number.append(user.user_contact).append(",");

                        // participents.add(user);
                        inflateParticipents(user, mUser_list);

                    } else {
                        number.append(usernumber).append(",");
                    }
                }
                mUsernos = number.toString();
                if (mUsernos.indexOf(",") != -1) {
                    mUsernos = mUsernos.substring(0, mUsernos.length() - 1);
                }

                mUser_id = ids.toString();
                if (mUser_id.indexOf(",") != -1) {
                    mUser_id = mUser_id.substring(0, mUser_id.length() - 1);
                }
                //**************************
                group_icon_value = "1";
                new new_thread("txt").execute();
            } else if (requestCode == 3) {
                Uri contactUri = data.getData();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(column);
                if (!updateid.equals("null")) {
                    try {
                        mUser_list.removeAllViews();
                        queries.delayedupdatename(name, updateid);
                        gpparticipants();
                    } catch (Exception e) {
                    }

                }
                Toast.makeText(ChatGroupSetting.this, number, Toast.LENGTH_SHORT).show();
            }

        }

    }

    /*****************************************
     * Inflate participents Layout
     ***************************************/
    private void inflateParticipents(final ModelUser model, LinearLayout parent) {

        view = inflater.inflate(R.layout.suggestion_item, parent, false);
        parent.addView(view);

        holder = new ViewHolder();

        holder.icon = (CircularImageView) view.findViewById(R.id.pic);
        holder.pic2 = (ImageView) view.findViewById(R.id.pic2);
        holder.name = (TextView) view.findViewById(R.id.name);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.user_contact.equals(model.user_name)) {
                    updateid = model.user_id;
                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    contactIntent.putExtra(ContactsContract.Intents.Insert.PHONE, model.user_contact);
                    contactIntent.putExtra("finishActivityOnSaveCompleted", true);
                    startActivityForResult(contactIntent, 3);
                }
            }
        });
        holder.name.setText(model.user_name);
//        Glide.with(ChatGroupSetting.this).load(model.user_pic)
//                // .placeholder(R.mipmap.avatar)
//                .crossFade()
//                .error(R.mipmap.default_user)
//                .into(holder.icon);

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(model.user_name.charAt(0)),getResources().getColor(R.color.colorPrimary));

            if (model.user_pic.length()>5) {
                holder.pic2.setImageDrawable(drawable);
                Picasso.with(ChatGroupSetting.this)
                        .load(model.user_pic)
//                        .placeholder(R.mipmap.default_user)
//                        .error(R.mipmap.default_user)
                        .into(holder.icon, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.pic2.setVisibility(View.INVISIBLE);
                                holder.icon.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.pic2.setVisibility(View.VISIBLE);
                                holder.icon.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                holder.pic2.setVisibility(View.VISIBLE);
                holder.icon.setVisibility(View.INVISIBLE);
                holder.pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {

            holder.pic2.setVisibility(View.INVISIBLE);
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(R.mipmap.default_user);
        }
    }

    public class ViewHolder {

        public CircularImageView icon;
        public ImageView pic2;
        public TextView name;
    }

    /***************************************
     * Set image
     ***************************************/
    private void setImage(File selectedImagePath) {
        System.out.println("phselectedImagePath:: " + selectedImagePath);
        //imogi.setVisibility(View.VISIBLE);
        pic2.setVisibility(View.INVISIBLE);
        mPro_img.setVisibility(View.VISIBLE);
        Glide.with(ChatGroupSetting.this)
                .load(selectedImagePath)
                .crossFade()
                .into(mPro_img);

    }

    public static String[] removeDuplicates(String[] arr) {
        return new HashSet<String>(Arrays.asList(arr)).toArray(new String[0]);
    }

    /**************************************************
     * Enable/Disable Notification webservice
     ************************************************/
    private class EnableDisableNotification extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(ChatGroupSetting.this);
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dia.setMessage("Updating status, Please wait...");
//            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                String res = NotificationMethod(mAuth, mThread_id, mNoti_status);

                Log.i("Notification : ", res);
                JSONObject obj = new JSONObject(res);
                status = obj.getString("status");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  dia.dismiss();
            try {

                if (status.equals("true")) {

                    queries.updateNotification(mThread_id, mNoti_status);

                }

            } catch (Exception e) {

            }
        }
    }

    /**********************************
     * Network call
     ********************************/
    private String NotificationMethod(String auth_code, String thread_id, String status) {

        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_id", thread_id)
                .appendQueryParameter("notification_status", status);
        return parser.getJSONFromUrl(Utils.USER_NOTIFICATION, builder);
    }

    /**********************************************
     * Leave group webservice
     ********************************************/
    public class leavegroup extends AsyncTask<String, Void, String> {
        String thread_id, response;
        ProgressDialog dia = new ProgressDialog(ChatGroupSetting.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating status, Please wait...");
            dia.setCancelable(false);
            dia.show();
        }

        public leavegroup(String cids) {
            this.thread_id = cids;

        }

        @Override
        protected String doInBackground(String... params) {
            JSONParser parser = new JSONParser();
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "leave_group")
                    .appendQueryParameter("auth_code", mAuth)
                    .appendQueryParameter("thread_id", thread_id);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            System.out.println("read=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dia.dismiss();
            try {
                JSONObject obj = new JSONObject(response);
                if (obj.getString("status").equals("true")) {
                    queries.deleteAllContent(thread_id, "", true);
                    queries.deleteThread(thread_id);
                    Dashboard.leavegrp = true;
                    finish();
                }
            } catch (Exception e) {
            }
        }
    }

    class new_thread extends AsyncTask<String, Void, String> {
        String response;
        String gname, gvalue, mType;
        File fpath;

        public new_thread(String type) {

            this.mType = type;
        }

        @Override
        protected String doInBackground(String... params) {


            response = Groupimg(mName, path, group_icon_value, mType);


            System.out.print("new thrd=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Toast.makeText(ChattingActivity.this, "in", Toast.LENGTH_SHORT).show();
            try {

                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    ModelMsgThread item = new ModelMsgThread();
                    JSONObject dobj = obj.optJSONObject("data");
                    item.thread_id = dobj.optString("thread_id");
                    item.participant_ids = dobj.optString("participant_ids");
                    item.participant_numbers = dobj.optString("participant_nos");
                    item.group_icon = dobj.optString("group_icon");
                    item.thread_sync_id = dobj.optString("thread_sync_id");
                    item.last_updated = dobj.optString("last_updated");
                    item.group_name=dobj.optString("group_name");
                 /*   try {
                        String gname = URLDecoder.decode(dobj.optString("group_name"));
                        item.group_name = gname;
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    queries.updatemsgthread(item, mType);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            editor = mPrefs.edit();
            editor.putString("current_thread", mThread_id);
            editor.commit();
        }

    }

    private String Groupimg(String mName, File path, String group_icon_value, String type) {
        String charset = "UTF-8";
        String name="";
      /*  try {
           name = URLEncoder.encode(mName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        String requestURL = Utils.base_url + "service_type=update_group_info";
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("auth_code", mAuth);
            multipart.addFormField("thread_id", mThread_id);
            multipart.addFormField("group_name", mName);
            if (group_icon_value.equals("1")) {
                multipart.addFormField("group_icon_value", group_icon_value);
            }
            multipart.addFormField("participant_ids", mUser_id);
            multipart.addFormField("participant_nos", mUsernos);
            multipart.addFormField("test", "");

            if (path != null && path.length() != 0 && !group_icon_value.equals("1")) {
                multipart.addFilePart("group_icon", path);
            }
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }


    /**********************************************
     * Leave Group dialog
     ********************************************/
    public void dialog() {
        new AlertDialog.Builder(ChatGroupSetting.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to leave this group? Any scheduled or recurring messages will not be sent!")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new leavegroup(mThread_id).execute();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })

                .show();

    }

}
