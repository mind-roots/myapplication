package com.app.delayed.chatExtended.Entities;

import java.util.ArrayList;

/**
 * Created by user on 2/25/2016.
 */
public class ModelMsgThread {
    public String thread_id;
    public String participant_ids;
    public String participant_numbers;
    public String group_name;
    public String group_icon;
    public String thread_sync_id;
    public String last_updated;
    public String notification_status;
    public String deleted_status;
    public String blocked_status;
    public String chat_user_id;
    public String last_msg;
    public String last_read_status;
    public boolean isGroup;
    public ArrayList<String> readcontentids = new ArrayList<>();

}
