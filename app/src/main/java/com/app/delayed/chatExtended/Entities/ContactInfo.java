package com.app.delayed.chatExtended.Entities;

/**
 * Created by Balvinder on 2/11/2016.
 */
public class ContactInfo {

    public String name;
    public String image;
    public String msg;
    public String time;
    public boolean isRead;
}
