package com.app.delayed.chatExtended.CustomAdapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private List<ModelMsgThread> contactList;
    Context ctx;
    String timeformat, auth_code;
    SharedPreferences prefs;
    DatabaseQueries query;
    JSONParser parser;
    ArrayList<String> ids = new ArrayList<>();

    public ContactAdapter(Context ctx, List<ModelMsgThread> contactList) {
        this.contactList = contactList;
        this.ctx = ctx;
        try {
            prefs = ctx.getSharedPreferences("delayed", ctx.MODE_PRIVATE);
            timeformat = prefs.getString("timeformat", null);
            auth_code = prefs.getString("auth_code", null);
            query = new DatabaseQueries(ctx);
            parser = new JSONParser();

        } catch (Exception e) {

        }

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {
        final ModelMsgThread ci = contactList.get(i);
        // contactViewHolder.pic.setText(ci.name);
        String gname = "";
    /*    try {
            gname = URLDecoder.decode(ci.group_name, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            gname = ci.group_name;
        }*/
        contactViewHolder.name.setText(ci.group_name);

        if (Utils.chat_section == 0) {
            contactViewHolder.time.setText(Methods.tonormaltime(ci.last_updated, timeformat));
        } else {
            contactViewHolder.time.setText("");
        }
        //         contactViewHolder.time.setText(comparedate(ci.last_updated, timeformat));
//        if (Utils.chat_section == 0) {
//            if (ci.readcontentids.size() > 0) {
//                contactViewHolder.txt_unreadcount.setVisibility(View.VISIBLE);
//                contactViewHolder.txt_unreadcount.setText("");
//            } else {
//                contactViewHolder.txt_unreadcount.setVisibility(View.INVISIBLE);
//            }
//        } else {
//            contactViewHolder.txt_unreadcount.setVisibility(View.INVISIBLE);
//        }
        contactViewHolder.msg.setText(ci.last_msg);
        if (ci.last_read_status.equals("1")) {

            try {
                contactViewHolder.txt_unreadcount.setVisibility(View.INVISIBLE);
                contactViewHolder.msg.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));
                contactViewHolder.name.setTextColor(ctx.getResources().getColor(R.color.colorBlack));
                contactViewHolder.time.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));

            } catch (Exception e) {

            }

        } else {

            if (ci.last_msg.equals("No Message")) {
                try {
                    contactViewHolder.txt_unreadcount.setVisibility(View.INVISIBLE);
                    contactViewHolder.msg.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));
                    contactViewHolder.name.setTextColor(ctx.getResources().getColor(R.color.colorBlack));
                    contactViewHolder.time.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    if (Utils.chat_section == 0) {
                        contactViewHolder.txt_unreadcount.setVisibility(View.VISIBLE);
                        contactViewHolder.txt_unreadcount.setText("");
                        contactViewHolder.msg.setTextColor(ctx.getResources().getColor(R.color.colorPrimaryLight));
                        contactViewHolder.name.setTextColor(ctx.getResources().getColor(R.color.colorPrimary));
                        contactViewHolder.name.setTypeface(null, Typeface.BOLD);
                        contactViewHolder.time.setTextColor(ctx.getResources().getColor(R.color.colorPrimaryLight));
                    } else {
                        contactViewHolder.txt_unreadcount.setVisibility(View.INVISIBLE);
                        contactViewHolder.msg.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));
                        contactViewHolder.name.setTextColor(ctx.getResources().getColor(R.color.colorBlack));
                        contactViewHolder.time.setTextColor(ctx.getResources().getColor(R.color.colorBlackLight));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(ci.group_name.charAt(0)), ctx.getResources().getColor(R.color.colorPrimary));

            if (ci.group_icon.length() > 5) {
                contactViewHolder.pic2.setImageDrawable(drawable);
                Picasso.with(ctx)
                        .load(ci.group_icon)
//                        .placeholder(d
//                        .error(drawable)
                        .into(contactViewHolder.pic, new Callback() {
                            @Override
                            public void onSuccess() {
                                contactViewHolder.pic2.setVisibility(View.INVISIBLE);
                                contactViewHolder.pic.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                contactViewHolder.pic2.setVisibility(View.VISIBLE);
                                contactViewHolder.pic.setVisibility(View.INVISIBLE);
                            }
                        });


            } else {
                contactViewHolder.pic2.setVisibility(View.VISIBLE);
                contactViewHolder.pic.setVisibility(View.INVISIBLE);
                contactViewHolder.pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {
            e.printStackTrace();

            contactViewHolder.pic2.setVisibility(View.INVISIBLE);
            contactViewHolder.pic.setVisibility(View.VISIBLE);
            contactViewHolder.pic.setImageResource(R.mipmap.default_user);
        }

//        Glide.with(ctx)
//                .load(ci.group_icon)
//                .crossFade()
//                .error(R.mipmap.default_user)
//                        // .placeholder(R.mipmap.avatar)
//                .into(contactViewHolder.pic);


        contactViewHolder.item_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactList.get(i).readcontentids.size() != 0) {
                    StringBuilder ids = new StringBuilder();

                    for (String str : contactList.get(i).readcontentids) {
                        ids.append(str).append(",");
                    }
                    String cids = ids.toString();
                    if (cids.indexOf(",") != -1) {
                        cids = cids.substring(0, cids.length() - 1);
                    }
                    for (String str : contactList.get(i).readcontentids) {
                        query.updatestatus(str);
                    }
                    new contentids(cids, i).execute();
                }
                Intent intent = new Intent(ctx, ChattingActivity.class);
                intent.putExtra("contenttype", FragmentChat.contenttype);
                intent.putExtra("threadids", contactList.get(i).thread_id);
                intent.putExtra("participants_ids", contactList.get(i).chat_user_id);
                intent.putExtra("isGroup", contactList.get(i).isGroup);
                intent.putExtra("participants_nos", contactList.get(i).participant_numbers);
                intent.putExtra("name", contactList.get(i).group_name);
                intent.putExtra("image", contactList.get(i).group_icon);
                intent.putExtra("notification_status", contactList.get(i).notification_status);
                intent.putExtra("blocked_status", contactList.get(i).blocked_status);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
        contactViewHolder.item_click.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (Utils.chat_section == 0) {
                    String msg="";
                    if (ci.isGroup) {
                        msg = "Are you sure you want to leave this group? Any scheduled or recurring messages will not be sent!";
                    } else {
                        msg="Are you sure you want to delete this chat? Any scheduled or recurring messages will still be sent!";
                    }
                    dialog("Alert!",msg , android.R.drawable.ic_dialog_info, ci.thread_id, ci.participant_ids, i);
                }
                return false;
            }
        });
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.chat_contact, viewGroup, false);

        return new ContactViewHolder(itemView);
    }


    /**********************************************
     * View holder
     ********************************************/
    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public ImageView pic, pic2;
        public TextView name, msg, time, txt_unreadcount;
        public LinearLayout item_click;

        public ContactViewHolder(View itemView) {
            super(itemView);

            pic = (ImageView) itemView.findViewById(R.id.pic);
            pic2 = (ImageView) itemView.findViewById(R.id.pic2);
            name = (TextView) itemView.findViewById(R.id.name);
            msg = (TextView) itemView.findViewById(R.id.msg);
            time = (TextView) itemView.findViewById(R.id.time);
            txt_unreadcount = (TextView) itemView.findViewById(R.id.txt_unreadcount);
            item_click = (LinearLayout) itemView.findViewById(R.id.item_click);

        }
    }


    /**********************************************
     * Delete thread dialog
     ********************************************/
    public void dialog(String title, String msg, int icon, final String thread_id, final String participents, final int pos) {
        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (contactList.get(pos).readcontentids.size() > 0) {
                            StringBuilder ids = new StringBuilder();

                            for (String str : contactList.get(pos).readcontentids) {
                                ids.append(str).append(",");
                            }
                            String cids = ids.toString();
                            if (cids.indexOf(",") != -1) {
                                cids = cids.substring(0, cids.length() - 1);
                            }
                            /*for (String str : contactList.get(pos).readcontentids) {
                                query.updatestatus(str);
                            }*/
                            new contentids(cids, pos).execute();
                        }
                        if (contactList.get(pos).isGroup) {
                            //its a group
                            new leavegroup(thread_id).execute();
                            query.deleteAllContent(thread_id, "", true);
                            query.deleteThread(thread_id);

                        } else {
                            //its a single user

                            if (Utils.chat_section != 0) {
                            } else {
                                query.deleteAllContent(thread_id, String.valueOf(Utils.chat_section), false);
                                FragmentChat.mDummyText.setText("You have no Delayd chats");
                                FragmentChat.mBtnCompose.setVisibility(View.VISIBLE);
                            }
                        }
                        contactList.remove(pos);
                        notifyDataSetChanged();
                        if (contactList.size() <= 0) {
                            FragmentChat.mDummyView.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();

    }

    /************************************************
     * Compare date time
     **********************************************/
    private String comparedate(String getdate, String timeformat) {
        System.out.print("date=" + getdate);
        String value = "NA";
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date newDate = format1.parse(getdate);
            if (format2.format(newDate).equals(format2.format(new Date()))) {
                if (timeformat.equals("12")) {
                    SimpleDateFormat tymformat = new SimpleDateFormat("hh:mm a");
                    value = tymformat.format(newDate);
                } else {
                    SimpleDateFormat tymformat = new SimpleDateFormat("HH:mm");
                    value = tymformat.format(newDate);
                }

            } else {
                value = format2.format(newDate);
            }

        } catch (Exception e) {
            value = getdate;
        }


        return value;
    }


    /**********************************************
     * Content read webservice
     ********************************************/
    public class contentids extends AsyncTask<String, Void, String> {
        String ids, response;
        int pos;

        public contentids(String cids, int i) {
            this.ids = cids;
            this.pos = i;
        }

        @Override
        protected String doInBackground(String... params) {
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "content_read")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("content_id", ids);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            System.out.println("read=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                JSONObject jobj = new JSONObject(response);
                String status = jobj.optString("status");
                if (status.equals("true")) {
                    /*for (String str : contactList.get(pos).readcontentids) {
                        query.updatestatus(str);
                    }*/
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**********************************************
     * Leave group webservice
     ********************************************/
    public class leavegroup extends AsyncTask<String, Void, String> {
        String thread_id, response;


        public leavegroup(String cids) {
            this.thread_id = cids;

        }

        @Override
        protected String doInBackground(String... params) {
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "leave_group")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("thread_id", thread_id);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            System.out.println("read=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }
}