package com.app.delayed.chatExtended;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.delayed.R;
import com.app.delayed.chatExtended.CustomAdapters.ImageAdapter;
import com.app.delayed.databaseUtils.DatabaseQueries;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 3/17/2016.
 */
public class SharedImages extends AppCompatActivity {

    Toolbar mToolbar;
    TextView mTitle;
    RecyclerView mList_images;
    String mThreadId;
    LinearLayout mLayDummy;
    List<String> mArrayImage = new ArrayList<>();
    DatabaseQueries queries;
    ImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shared_images);

        init();

        try {
            mArrayImage = queries.GetImages(mThreadId);

            Log.i("Urls: ", ""+mArrayImage);
            adapter = new ImageAdapter(SharedImages.this, mArrayImage);
            mList_images.setAdapter(adapter);

            //*************Check has Images or not************
            if (mArrayImage.size() > 0) {
                mList_images.setVisibility(View.VISIBLE);
                mLayDummy.setVisibility(View.GONE);
                // mList_images.setAdapter();

            } else {
                mLayDummy.setVisibility(View.VISIBLE);
                mList_images.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);

    }

    /*****************************************
     * Initialize UI elements
     ***************************************/
    private void init() {

        queries = new DatabaseQueries(SharedImages.this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        setTitle("");
        mTitle.setText("Shared images");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mThreadId = getIntent().getStringExtra("id");
        mList_images = (RecyclerView) findViewById(R.id.images_list);
        mList_images.setLayoutManager(new GridLayoutManager(SharedImages.this, 3));
        mList_images.setHasFixedSize(true);
        mLayDummy = (LinearLayout) findViewById(R.id.lay_dummy);

    }

    /***************************************
     * Get Images
     ***************************************/
    private List<String> GetImages(String thread_id) {

        List<String> urls = new ArrayList<>();
        File filepath = Environment.getExternalStorageDirectory();
        File imageArr = new File(filepath.getAbsolutePath() + "/Android/data/com.app.delayed/cache/SharedImages");
        if (imageArr.exists() && imageArr.isDirectory()) {

            File[] arr = imageArr.listFiles();
            for (File f : arr) {

                if (f.getName().startsWith(thread_id)) {
                    urls.add(f.getAbsolutePath());
                }
            }
        }

        return urls;
    }

}
