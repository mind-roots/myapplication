package com.app.delayed.chatExtended;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Balvinder on 2/29/2016.
 */
public class ViewContact extends AppCompatActivity {

    LinearLayout mBlock, mSharedImages;
    CircularImageView mPro_img;
    TextView mTitle, mUsername, mTextBlock;
    Toolbar mToolbar;
    String mName, mImage;
    SharedPreferences mPrefs;
    Switch mOn_off;
    String mBlock_status = "0", mNoti_status = "0";
    String mAuth, mThread_id, mUser_id,userid;
    DatabaseQueries query;
    ImageView pic2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_contact);

        init();

        mBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Snackbar.make(v, "User blocked", Snackbar.LENGTH_SHORT).show();
                if (mBlock_status.equals("1")) {
                    mBlock_status = "0";
                }else {
                    mBlock_status = "1";
                }
                new BlockUser().execute();

            }
        });

        mOn_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    mNoti_status = "0";

                } else {
                    mNoti_status = "1";
                }
            }
        });

        mSharedImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewContact.this, SharedImages.class).putExtra("id", mThread_id));
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        // new EnableDisableNotification().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        new EnableDisableNotification().execute();
    }

    /*************************************
     * Initialiaze UI elements
     ***********************************/
    private void init() {

        query = new DatabaseQueries(ViewContact.this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        setTitle("");
        mTitle.setText("View Contact");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPrefs = getSharedPreferences("delayed", MODE_PRIVATE);
        mAuth = mPrefs.getString("auth_code", null);
        userid = mPrefs.getString("id", null);
        mBlock = (LinearLayout) findViewById(R.id.block);
        mSharedImages = (LinearLayout) findViewById(R.id.ll_shared);
        mPro_img = (CircularImageView) findViewById(R.id.img_profile);
        pic2 = (ImageView) findViewById(R.id.pic2);
        mUsername = (TextView) findViewById(R.id.txt_user_name);
        mTextBlock = (TextView) findViewById(R.id.block_txt);
        mOn_off = (Switch) findViewById(R.id.on_off);

        Intent in = getIntent();
        mName = in.getStringExtra("name");
        mImage = in.getStringExtra("image");
        mThread_id = in.getStringExtra("threadids");
        mUser_id = in.getStringExtra("chat_user_id");
        mBlock_status = in.getStringExtra("block");
        mNoti_status = in.getStringExtra("noti");

        mUsername.setText(mName);
//        Glide.with(ViewContact.this)
//                .load(mImage)
//                .crossFade()
//                .error(R.mipmap.avatar)
//                .into(mPro_img);

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(mName.charAt(0)),getResources().getColor(R.color.colorPrimary));

            if (mImage.length()>5) {
                pic2.setImageDrawable(drawable);
                Picasso.with(ViewContact.this)
                        .load(mImage)
//                        .placeholder(R.mipmap.default_user)
//                        .error(R.mipmap.default_user)
                        .into(mPro_img, new Callback() {
                            @Override
                            public void onSuccess() {
                                pic2.setVisibility(View.INVISIBLE);
                                mPro_img.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                pic2.setVisibility(View.VISIBLE);
                                mPro_img.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                pic2.setVisibility(View.VISIBLE);
                mPro_img.setVisibility(View.INVISIBLE);
                pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound("+",getResources().getColor(R.color.colorPrimary));
            pic2.setVisibility(View.VISIBLE);
            mPro_img.setVisibility(View.INVISIBLE);
            pic2.setImageDrawable(drawable);
        }

if (userid.equals(mUser_id)){
    mBlock.setVisibility(View.INVISIBLE);
}
        mBlock_status = query.getBlock(mThread_id);
        mNoti_status = query.getNotification(mThread_id);

        try {

            if (mNoti_status.equals("0")) {
                mOn_off.setChecked(true);
            } else {
                mOn_off.setChecked(false);
            }

            if (mBlock_status.equals("0")) {

                mTextBlock.setText("Block");
            } else {
                mTextBlock.setText("Unblock");
            }

        }catch (Exception e){

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**********************************************
     * Block user webservice
     **********************************************/
    private class BlockUser extends AsyncTask<Void, Void, Void> {

        ProgressDialog dia = new ProgressDialog(ViewContact.this);
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Updating status, Please wait...");
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String response = BlockMethod(mAuth, mThread_id, mUser_id, mBlock_status);
                Log.i("Response: ", response);

                JSONObject obj = new JSONObject(response);
                status = obj.getString("status");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            try {

                if (status.equals("true")) {

                    query.updateBlock(mThread_id, mBlock_status);

                    if (mBlock_status.equals("1")) {
                        mTextBlock.setText("Unblock");
                    }else {
                        mTextBlock.setText("Block");

                    }
                    Snackbar.make(mBlock, "User blocked", Snackbar.LENGTH_SHORT).show();

                } else {
                    Snackbar.make(mBlock, "User blocking problem", Snackbar.LENGTH_SHORT).show();
                }

            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }

    /**********************************
     * Network call
     ********************************/
    private String BlockMethod(String auth_code, String thread_id, String block_uid, String block_status) {

        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_id", thread_id)
                .appendQueryParameter("blocked_user_id", block_uid)
                .appendQueryParameter("blocked_status", block_status);
        return parser.getJSONFromUrl(Utils.BLOCK_USER, builder);
    }

    /**************************************************
     * Enable/Disable Notification webservice
     ************************************************/
    private class EnableDisableNotification extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(ViewContact.this);
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dia.setMessage("Updating status, Please wait...");
//            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                String res = NotificationMethod(mAuth, mThread_id, mNoti_status);

                Log.i("Notification : ", res);
                JSONObject obj = new JSONObject(res);
                status = obj.getString("status");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  dia.dismiss();
            try {

                if (status.equals("true")) {

                    query.updateNotification(mThread_id, mNoti_status);

                }

            } catch (Exception e) {

            }
        }
    }

    /**********************************
     * Network call
     ********************************/
    private String NotificationMethod(String auth_code, String thread_id, String status) {

        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_id", thread_id)
                .appendQueryParameter("notification_status", status);
        return parser.getJSONFromUrl(Utils.USER_NOTIFICATION, builder);
    }
}
