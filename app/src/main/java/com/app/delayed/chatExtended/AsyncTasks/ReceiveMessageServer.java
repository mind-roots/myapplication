package com.app.delayed.chatExtended.AsyncTasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.app.delayed.chatExtended.Entities.Message;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

public class ReceiveMessageServer extends AbstractReceiver {

    private Context mContext;
    private static final int SERVER_PORT = 4445;

    String response, auth_code;
    JSONParser parser;
    SharedPreferences prefs;
    DatabaseQueries dataquery;

    public ReceiveMessageServer(Context context) {
        mContext = context;

        parser = new JSONParser();
        prefs = mContext.getSharedPreferences("delayed", mContext.MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        dataquery = new DatabaseQueries(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {

        response = schdule_method("get_new_content");
        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.optString("status");
            if (status.equals("true")) {
                ModelMsgThrdContent item = new ModelMsgThrdContent();
                JSONArray jarr = obj.optJSONArray("data");
                for (int i = 0; i < jarr.length(); i++) {
                    JSONObject dobj = jarr.optJSONObject(i);
                    item.thread_id = dobj.optString("thread_id");
                    item.content_id = dobj.optString("content_id");
                    item.user_id = dobj.optString("user_id");
                    item.type = dobj.optString("type");
                    item.content = dobj.optString("content");
                    item.thumb_url = dobj.optString("thumb_url");
                  //  new downloadimage(item.thumb_url, item.thread_id, item.content_id).execute();
                    item.status = dobj.optString("status");
                    item.content_sync_id = dobj.optString("content_sync_id");
                    item.last_updated = dobj.optString("last_updated");
                    if (item.type.equals("0")) {
                        //   publishProgress(item.content);
                    } else {
                    }
                    dataquery.insertmsgthreadcontent(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

	/*	try {
            serverSocket = new ServerSocket(SERVER_PORT);
			while(true){
				Socket clientSocket = serverSocket.accept();
				
				InputStream inputStream = clientSocket.getInputStream();
				ObjectInputStream objectIS = new ObjectInputStream(inputStream);
				Message message = (Message) objectIS.readObject();
				
				//Add the InetAdress of the sender to the message
				InetAddress senderAddr = clientSocket.getInetAddress();
				message.setSenderAddress(senderAddr);
				
				clientSocket.close();
				publishProgress(message);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}*/

        return null;
    }

    public class downloadimage extends AsyncTask<String, Void, String> {
        String img_url;
        String threadid;
        String contentid;

        public downloadimage(String thumb_url, String thread_id, String content_id) {
            this.img_url = thumb_url;
            this.threadid = thread_id;
            this.contentid = content_id;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(img_url);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                urlConnection.connect();

                File SDCardRoot = Environment.getExternalStorageDirectory();
                //create a new file, specifying the path, and the filename
                //which we want to save the file as.
                File file = new File(SDCardRoot,"somefile.jpg");

                //this will be used to write the downloaded data into the file we created
                FileOutputStream fileOutput = new FileOutputStream(file);

                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                int downloadedSize = 0;

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer

                //now, read through the input buffer and write the contents to the file
                while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    //this is where you would do something to report the prgress, like this maybe
                    //updateProgress(downloadedSize, totalSize);

                }
                //close the output stream when done
                fileOutput.close();

                //catch some possible errors...
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void downloadimage(String thumb_url, String threadid, String contentid) {


    }

    @Override
    protected void onCancelled() {
      /*  try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        super.onCancelled();
    }


  /*  @Override
    protected void onProgressUpdate(String values) {
        super.onProgressUpdate(values);
        playNotification(mContext, values);

        //If the message contains a video or an audio, we saved this file to the external storage
      *//*  int type = values[0].getmType();
        if (type == Message.AUDIO_MESSAGE || type == Message.VIDEO_MESSAGE || type == Message.FILE_MESSAGE || type == Message.DRAWING_MESSAGE) {
            values[0].saveByteArrayToFile(mContext);
        }*//*
//Message msg=new Message(type, text,sender, name,  threadids);
        InetAddress sender = null;
        Message msg = new Message(0, "text", sender, "name", "33");
        new SendMessageServer(mContext, false).executeOnExecutor(THREAD_POOL_EXECUTOR, msg);
    }*/

    private String schdule_method(String service_type) {
        String res = null;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("content_sync_id", "0");


        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;

    }
}
