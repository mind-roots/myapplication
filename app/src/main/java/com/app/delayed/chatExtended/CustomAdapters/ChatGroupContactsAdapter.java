package com.app.delayed.chatExtended.CustomAdapters;

/**
 * Created by Balvinder on 3/9/2016.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.ModelUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChatGroupContactsAdapter extends RecyclerView.Adapter<ChatGroupContactsAdapter.ContactViewHolder> {
    String id;

    SharedPreferences prefs;
    private List<ModelUser> contactList;
    Context ctx;
    DatabaseQueries dataquery;

    public ChatGroupContactsAdapter(Context ctx, List<ModelUser> contactList) {
        this.contactList = contactList;
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences("delayed", ctx.MODE_PRIVATE);
        id = prefs.getString("id", null);
        dataquery = new DatabaseQueries(ctx);

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder contactViewHolder, final int i) {
        ModelUser ci = contactList.get(i);

        try {

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(String.valueOf(ci.user_name.charAt(0)), ctx.getResources().getColor(R.color.colorPrimary));

        if (ci.user_pic.length()>5) {
            contactViewHolder.pic2.setImageDrawable(drawable);
            Picasso.with(ctx)
                    .load(ci.user_pic)
//                        .placeholder(drawable)
//                        .error(drawable)
                    .into(contactViewHolder.pic, new Callback() {
                        @Override
                        public void onSuccess() {
                            contactViewHolder.pic2.setVisibility(View.INVISIBLE);
                            contactViewHolder.pic.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            contactViewHolder.pic2.setVisibility(View.VISIBLE);
                            contactViewHolder.pic.setVisibility(View.INVISIBLE);
                        }
                    });


        }else {
            contactViewHolder.pic2.setVisibility(View.VISIBLE);
            contactViewHolder.pic.setVisibility(View.INVISIBLE);
            contactViewHolder.pic2.setImageDrawable(drawable);
        }

        } catch (Exception e) {
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound("+", ctx.getResources().getColor(R.color.colorPrimary));
            contactViewHolder.pic2.setVisibility(View.VISIBLE);
            contactViewHolder.pic.setVisibility(View.INVISIBLE);
            contactViewHolder.pic2.setImageDrawable(drawable);
        }
//        Glide.with(ctx)
//                .load(ci.user_pic)
//               // .placeholder(R.mipmap.avatar)
//                .error(R.mipmap.avatar)
//                .crossFade()
//                .into(contactViewHolder.pic);
        contactViewHolder.name.setText(ci.user_name);
        // contactViewHolder.time.setText(ci.cont_date);
        contactViewHolder.msg.setText(ci.user_contact);
        contactViewHolder.item_click.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Delete contact
                new AlertDialog.Builder(ctx)
                        .setMessage("Are you sure you want to remove this contact?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                contactList.remove(i);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                return false;
            }
        });

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.chat_contact_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public ImageView pic, pic2;
        public TextView name, time, msg;
        public LinearLayout item_click;

        public ContactViewHolder(View itemView) {
            super(itemView);

            pic = (ImageView) itemView.findViewById(R.id.pic);
            pic2 = (ImageView) itemView.findViewById(R.id.pic2);
            name = (TextView) itemView.findViewById(R.id.name);
            msg = (TextView) itemView.findViewById(R.id.msg);
            time = (TextView) itemView.findViewById(R.id.time);
            item_click = (LinearLayout) itemView.findViewById(R.id.item_click);

        }
    }
}
