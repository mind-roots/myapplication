package com.app.delayed.chatExtended.util;

import com.app.delayed.chatExtended.Entities.Message;

import java.util.Comparator;

/**
 * Created by Balvinder on 4/20/2016.
 */
public class ChatComparator implements Comparator<Message> {
    @Override
    public int compare(Message lhs, Message rhs) {
        return lhs.time_text.compareTo(rhs.time_text);
    }
}
