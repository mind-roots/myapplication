package com.app.delayed.chatExtended.CustomAdapters;

/**
 * Created by Balvinder on 3/18/2016.
 */


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.delayed.R;
import com.app.delayed.chatExtended.ViewImageActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ContactViewHolder> {

    private List<String> contactList;
    Context ctx;

    public ImageAdapter(Context ctx, List<String> contactList) {
        this.contactList = contactList;
        this.ctx = ctx;

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, final int i) {
        Glide.with(ctx)
                .load(contactList.get(i))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(contactViewHolder.pic);

        contactViewHolder.pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, ViewImageActivity.class).putExtra("fileName", contactList.get(i)).putExtra("userName",""));
            }
        });

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.image_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }


    /****************************
     * View Holder
     **************************/
    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public ImageView pic;

        public ContactViewHolder(View itemView) {
            super(itemView);

            pic = (ImageView) itemView.findViewById(R.id.pic);

        }
    }

}
