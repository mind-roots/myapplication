package com.app.delayed.chatExtended.Entities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.app.delayed.chatExtended.ChattingActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.net.InetAddress;

@SuppressWarnings("serial")
public class Message implements Serializable {
    private static final String TAG = "Message";
    public static final int TEXT_MESSAGE = 0;
    public static final int IMAGE_MESSAGE = 1;
    public static final int AUDIO_MESSAGE = 2;
    public static final int VIDEO_MESSAGE = 3;
    public static final int FILE_MESSAGE = 4;
    public static final int DRAWING_MESSAGE = 5;
    public static final int PHOTO_MESSAGE = 6;

    private int mType;
    private String mText;
    private byte[] byteArray;
    public String imgurl;
    public String originalurl;
    private String fileName;
    private String audioLength;
    public  String headerdate;
public boolean displaydate;
    public String getSchstatus() {
        return schstatus;
    }

    public void setSchstatus(String schstatus) {
        this.schstatus = schstatus;
    }

    public String getSchdate() {
        return schdate;
    }

    public void setSchdate(String schdate) {
        this.schdate = schdate;
    }

    public String getTriggerdate() {
        return triggerdate;
    }

    public void setTriggerdate(String triggerdate) {
        this.triggerdate = triggerdate;
    }

    public String getSchtype() {
        return schtype;
    }

    public void setSchtype(String schtype) {
        this.schtype = schtype;
    }

    private long fileSize;
    private String filePath, thread_id;
    private boolean isMine;
    public  String time_text;
    public String username, userpic;
    public String schstatus, schdate, triggerdate,triggerdate2, schtype;
    // public String delayed_time;'pu
    public String mContentid;
    public boolean isDelayed;

    public String getTriggerdate2() {
        return triggerdate2;
    }

    public void setTriggerdate2(String triggerdate2) {
        this.triggerdate2 = triggerdate2;
    }

    public String getmContentid() {
        return mContentid;
    }

    public void setmContentid(String mContentid) {
        this.mContentid = mContentid;
    }

    Context con;

    public Message(Context chattingActivity) {
        this.con = chattingActivity;
    }

    //Getters and Setters
    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }


    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAudioLength() {
        return audioLength;
    }

    public void setAudioLength(String audioLength) {
        this.audioLength = audioLength;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean isMine) {
        this.isMine = isMine;
    }

    public String getThread_id() {
        return thread_id;
    }

    public void setThread_id(String thread_id) {
        this.thread_id = thread_id;
    }


    public Bitmap byteArrayToBitmap(byte[] b) {
        Log.v(TAG, "Convert byte array to image (bitmap)");
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }

    public void saveByteArrayToFile(Context context) {
        Log.v(TAG, "Save byte array to file");
        switch (mType) {
            case Message.AUDIO_MESSAGE:
                filePath = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC).getAbsolutePath() + "/" + fileName;
                System.out.print("fil=" + filePath);
                break;
            case Message.VIDEO_MESSAGE:
                filePath = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES).getAbsolutePath() + "/" + fileName;
                break;
            case Message.FILE_MESSAGE:
                filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" + fileName;
                break;
            case Message.DRAWING_MESSAGE:
                filePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + fileName;
                break;
        }

        File file = new File(filePath);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream fos = new FileOutputStream(file.getPath());

            fos.write(byteArray);
            fos.close();
            Log.v(TAG, "Write byte array to file DONE !");
        } catch (java.io.IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Write byte array to file FAILED !");
        }
    }
}
