package com.app.delayed.chatExtended;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.app.delayed.R;
import com.app.delayed.cropimage.CropActivityP;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 3/8/2016.
 */
public class CreateNewGroup extends AppCompatActivity {

    TextView mLength_count,txt_text;
    EditText mGroup_name;
    CircularImageView mGroup_icon;
    Toolbar mToolbar;
    TextView mTitle;
    String mName, mPic;
    boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_chat_group);

        init();

        mGroup_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidekeyboard();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    imagedialog();
                    //startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

        mGroup_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


                count = s.length();
                mLength_count.setText(String.valueOf(s.length()));
//                if (IntentType.equals("new")) {
//                    newmenu.findItem(R.id.action_done).setVisible(true);
//                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hidekeyboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        if (isUpdate) {

            menu.findItem(R.id.action_next).setTitle("Update");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            finish();

        } else if (item.getItemId() == R.id.action_next) {

            mName = mGroup_name.getText().toString();
            //start intent to add contacts activity
            hidekeyboard();
            if (mName.length() > 0) {

                if (!isUpdate) {

                    startActivity(new Intent(CreateNewGroup.this, AddContactsToChatGroup.class)
                            .putExtra("g_name", mName)
                            .putExtra("group_icon_value", "")
                            .putExtra("g_pic", mPic));

                } else {

                    Intent in = new Intent();
                    in.putExtra("name", mName);
                    in.putExtra("pic", mPic);
                    setResult(RESULT_OK, in);
                }
                finish();
            } else {
                Snackbar.make(mGroup_name, "Enter group name.", Snackbar.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************************
     * Initialize UI elements
     ****************************************/
    private void init() {
        isUpdate = getIntent().getBooleanExtra("isUpdate", false);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        mGroup_icon = (CircularImageView) findViewById(R.id.img_photo);
        mGroup_name = (EditText) findViewById(R.id.edt_name);
        mLength_count = (TextView) findViewById(R.id.txt_count);
        txt_text=(TextView)findViewById(R.id.txt_text);

        if (isUpdate) {

            mTitle.setText("Update Chat Group");
            mName = getIntent().getStringExtra("g_name");
            mPic = getIntent().getStringExtra("g_img");

            mGroup_name.setText(mName);
            if (mName.length()<20) {
                mGroup_name.setSelection(mName.length());
                mLength_count.setText(String.valueOf(mName.length()));
            }else {
                mGroup_name.setSelection(mName.length()-1);
                mLength_count.setText(String.valueOf(mName.length()));
            }

            Glide.with(CreateNewGroup.this)
                    .load(mPic)
                    .crossFade()
                    .error(R.mipmap.default_user)
                    .into(mGroup_icon);
            if (mPic!=null&&mPic.length()!=0&&!mPic.equals("")&&mPic!=""){
                txt_text.setVisibility(View.GONE);
            }else{
                txt_text.setVisibility(View.VISIBLE);
            }

        } else {
            mTitle.setText("New Chat Group");
        }
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");


    }


    /*************************************
     * Hide keyboard
     ***********************************/
    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = CreateNewGroup.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) CreateNewGroup.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //*************Crop Image Methods*******************
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
          //  Bitmap bit = (Bitmap) data.getExtras().get("data");
           // Utils.selected_image = Methods.SaveImage(bit);
            Intent in = new Intent(CreateNewGroup.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Utils.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Utils.selected_image);
            Intent in = new Intent(CreateNewGroup.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            //   set_img.setImageURI(Utils.pimageUri);
              setImage(Utils.pimageUri);
 //           mGroup_icon.setImageURI(Utils.pimageUri);
            mPic = getRealPathFromURI(CreateNewGroup.this, Utils.pimageUri);
        } else {
            Utils.pimageUri = null;
        }
    }

    //**********************************Webservice parameter RegisterMethod*************************
    private void setImage(Uri selectedImagePath) {
        System.out.println("phselectedImagePath:: " + selectedImagePath);
        //imogi.setVisibility(View.VISIBLE);
        txt_text.setVisibility(View.GONE);
        Glide.with(CreateNewGroup.this)
                .load(selectedImagePath)
                .crossFade()
                .error(R.mipmap.default_user)
                .into(mGroup_icon);

    }


    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(CreateNewGroup.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(CreateNewGroup.this, Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(CreateNewGroup.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            imagedialog();
           // startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                       // startActivityForResult(getPickImageChooserIntent(), 200);
                        imagedialog();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(mGroup_name, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void imagedialog() {

        final CharSequence[] items = {"Pick from gallery", "Take from camera"};
        new AlertDialog.Builder(CreateNewGroup.this, R.style.AppTheme_Dialog)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == 0) {

                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, 200);

                        } else {


                            String root = Environment.getExternalStorageDirectory().toString();

                            File myDir = new File(root + "/VidCode/Captured Images/");
                            if (!myDir.exists())
                                myDir.mkdirs();

                            String fname = "/image-" + System.currentTimeMillis() + ".png";
                            File file = new File(myDir, fname);
                            Utils.selected_image=Uri.fromFile(file);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Utils.selected_image);
                            startActivityForResult(cameraIntent, 100);
                        }
                    }
                })
                .show();
    }
}
