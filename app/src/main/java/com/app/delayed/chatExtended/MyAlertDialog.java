package com.app.delayed.chatExtended;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Window;

import com.app.delayed.R;

public class MyAlertDialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide activity title
        //setContentView(R.layout.activity_my_alert_dialog);

        AlertDialog.Builder Builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setMessage(getIntent().getStringExtra("msg"))
                .setTitle("Confirmation")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyAlertDialog.this.finish();
                    }
                });
        Builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
                MyAlertDialog.this.finish();
            }
        });
        Builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MyAlertDialog.this.finish();
            }
        });
        AlertDialog alertDialog = Builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

}