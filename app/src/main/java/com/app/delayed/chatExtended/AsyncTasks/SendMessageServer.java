package com.app.delayed.chatExtended.AsyncTasks;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.chatExtended.Entities.Message;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.chatExtended.util.ImageCaching;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.Utils;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class SendMessageServer extends AsyncTask<Message, Message, Message> {
    private static final String TAG = "SendMessageServer";
    private Context mContext;
    private static final int SERVER_PORT = 4446;
    private boolean isMine;
    String response, auth_code;
    JSONParser parser;
    SharedPreferences prefs;
    DatabaseQueries dataquery;
    boolean schduled = false;
    String status;
    String error_msg;
    String mName = "";

    public SendMessageServer(Context context, boolean mine) {
        mContext = context;
        isMine = mine;
        parser = new JSONParser();
        prefs = mContext.getSharedPreferences("delayed", mContext.MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        dataquery = new DatabaseQueries(mContext);
        mName = ChattingActivity.mTitle.getText().toString();
    }

    @Override
    protected Message doInBackground(Message... msg) {
        Log.v(TAG, "doInBackground");

        //Display le message on the sender before sending it
        if (msg[0].getSchstatus().equals("0")) {
            schduled = false;
            publishProgress(msg);
        } else {
            schduled = true;
        }
        //Send the message to clients
        System.out.print("type1=" + msg[0].getmType());
        if (msg[0].getmType() == 0) {
            String message = "";
            try {
                message = URLEncoder.encode(msg[0].getmText(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            response = schdule_method("create_new_content", msg[0].getmType(), message, msg[0].getThread_id(), msg[0].getSchtype(), msg[0].getTriggerdate(), msg[0].getSchdate(), msg[0].getSchstatus());
            System.out.print("type1====" + response);
        } else if (msg[0].getmType() == 1 || msg[0].getmType() == 2) {

            System.out.print("type1=" + msg[0].getFilePath());
            System.out.print("type1=" + msg[0].getmType());
            System.out.print("type1=" + msg[0].getThread_id());
            System.out.print("Audio length = " + msg[0].getAudioLength());

            response = parser.uploadimgchat(auth_code, "create_new_content", msg[0].getmType(), msg[0].getFilePath(), msg[0].getThread_id(), msg[0].getSchtype(), msg[0].getTriggerdate(), msg[0].getSchdate(), msg[0].getSchstatus(), msg[0].getAudioLength());
            System.out.print("type2=" + response);

        }
        try {
            JSONObject obj = new JSONObject(response);
            status = obj.optString("status");
            if (status.equals("true")) {

                ModelMsgThrdContent item = new ModelMsgThrdContent();
                JSONObject dobj = obj.optJSONObject("data");
                item.thread_id = dobj.optString("thread_id");
                String content_id = dobj.optString("content_id");
                item.content_id = content_id;
                item.user_id = dobj.optString("user_id");
                item.type = dobj.optString("type");
                item.content_time = dobj.optString("content_time");
                if (msg[0].getmType() == 1) {
                    Bitmap bit = ImageCaching.loadThumb(dobj.optString("content"));
                    String path = ImageCaching.saveImageInSdCard(bit, Integer.parseInt(msg[0].getThread_id()));
                    item.content = path;
                } else {
                    item.content = dobj.optString("content");
                }

                item.thumb_url = dobj.optString("thumb_url");

                item.read_status = "1";
               /* if (mName.equals("You")) {
                    if (msg[0].getSchstatus().equals("0")) {
                        item.read_status = "1";
                    } else {
                        item.read_status = "0";
                    }

                } else {
                    item.read_status = "1";
                }*/

                item.status = dobj.optString("status");
                item.scheduled_type = dobj.optString("scheduled_type");
                // item.schduled_date = msg[0].getSchdate();
                item.schduled_date = msg[0].getTriggerdate();
                item.content_sync_id = "0";
                item.last_updated = dobj.optString("last_updated");
                item.deleted_time = msg[0].time_text;
                //item.last_updated=msg[0].time_text;
                dataquery.insertmsgthreadcontent(item);
                if (msg[0].getSchstatus().equals("1")) {
                    String schty = "0";
                    if (msg[0].getSchtype().equals("0")) {
                        schty = "sch";
                    } else {
                        schty = "rec";
                    }
                    dataquery.insertschcontent(content_id, schty, msg[0].getSchdate());
                }
            } else {
                error_msg = obj.getString("error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return msg[0];
    }

    private String schdule_method(String service_type, int type, String content, String threadid, String schtype, String trigerdate, String schdate, String schstatus) {
        String res = null;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_id", threadid)
                .appendQueryParameter("type", String.valueOf(type))
                .appendQueryParameter("content", content)
                .appendQueryParameter("status", schstatus)
                .appendQueryParameter("scheduled_type", schtype)
                .appendQueryParameter("scheduled_date_time", schdate)
                .appendQueryParameter("trigger_date_time", trigerdate);
//String u="http://67.227.213.94/~delaydwebcot/delayd/index.php?service_type=create_new_content&auth_code=gkpsdw&thread_id=18&type=0&content='&status=0&scheduled_type=null&scheduled_date_time=&trigger_date_time=";
        res = parser.getJSONFromUrl(Utils.base_url, builder);

        return res;

    }

    @Override
    protected void onProgressUpdate(Message... values) {
        super.onProgressUpdate(values);
        int y = values[0].getmType();
        System.out.print("type=" + y);

        //if(isActivityRunning(WifiConnect.class)){
        ChattingActivity.refreshList(values[0], isMine);
        //}
    }

    @Override
    protected void onPostExecute(Message result) {
        Log.v(TAG, "onPostExecute");
        super.onPostExecute(result);

        try {
            if (status.equals("false")) {
                if (error_msg.length() > 0) {
                    Toast.makeText(mContext, "Unable to send : " + error_msg, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Unable to send ", Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

      /*  if (schduled ) {
            Toast.makeText(mContext, "Your message has been Schduled.", Toast.LENGTH_SHORT).show();
        }*/
    }

    @SuppressWarnings("rawtypes")
    public Boolean isActivityRunning(Class activityClass) {
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }
}