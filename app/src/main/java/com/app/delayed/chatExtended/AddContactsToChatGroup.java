package com.app.delayed.chatExtended;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.delayed.R;
import com.app.delayed.chatExtended.CustomAdapters.ChatGroupContactsAdapter;
import com.app.delayed.chatExtended.CustomAdapters.ContactSuggestionAdapter;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.ModelUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 3/8/2016.
 */
public class AddContactsToChatGroup extends AppCompatActivity {

    RecyclerView mContactsList;
    LinearLayoutManager mLayoutManager;
    public static ImageView mAdd;
    public static AutoCompleteTextView mSuggestion_box;
    Toolbar mToolbar;
    TextView mTitle;
    ContactSuggestionAdapter adapter;
    public static List<ModelUser> mContacts = new ArrayList<>();
    public static List<ModelUser> mContactsGroup = new ArrayList<>();
    DatabaseQueries query;
    public static ModelUser model;
    String mParicipents, mParicipent_nos;
    String mGname, mGpic;
    RelativeLayout rl_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact_to_chat_group);

        init();

//        rl_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mSuggestion_box.length()>0) {
//                    hidekeyboard();
//                    mSuggestion_box.setText("");
//                    if (!isDuplicate(mContactsGroup, model)) {
//                        mContactsGroup.add(model);
//                    } else {
//                        Snackbar.make(v, "Already added.", Snackbar.LENGTH_SHORT).show();
//                    }
//                    adapter.notifyDataSetChanged();
//                }else{
//                    Snackbar.make(v, "Select participant.", Snackbar.LENGTH_SHORT).show();
//                }
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        hidekeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hidekeyboard();
        mContactsGroup.clear();
    }

    /*******************************************
     * Initialize UI elements
     *******************************************/
    private void init() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setText("Add contacts to group");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mAdd = (ImageView) findViewById(R.id.img_add);
        mSuggestion_box = (AutoCompleteTextView) findViewById(R.id.suggestion);
        mContactsList = (RecyclerView) findViewById(R.id.contacts_list);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mContactsList.setLayoutManager(mLayoutManager);
        rl_add = (RelativeLayout) findViewById(R.id.rl_add);

        if (getIntent().hasExtra("g_name")) {
            mGname = getIntent().getStringExtra("g_name");
            mGpic = getIntent().getStringExtra("g_pic");
        }

        mContactsList.setAdapter(new ChatGroupContactsAdapter(AddContactsToChatGroup.this, mContactsGroup));
        query = new DatabaseQueries(AddContactsToChatGroup.this);

        try {
            mContacts = query.GetUserList("1");
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter = new ContactSuggestionAdapter(AddContactsToChatGroup.this, mContacts);
        mSuggestion_box.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_finish, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            finish();

        } else if (item.getItemId() == R.id.action_finish) {

            //create thread service
            if (mContactsGroup.size() > 1) {
                mParicipents = GetParicipents(mContactsGroup);
                mParicipent_nos=GetNumber(mContactsGroup);
                Log.i("participents: ", mParicipents);
                if (getIntent().hasExtra("g_name")) {
                    Intent intent = new Intent(AddContactsToChatGroup.this, ChattingActivity.class);
                    intent.putExtra("threadids", "no");
                    intent.putExtra("contenttype", "recent");
                    intent.putExtra("g_name", mGname);
                    intent.putExtra("g_pic", mGpic);
                    intent.putExtra("participants_ids", mParicipents);
                    intent.putExtra("isGroup", true);
                    intent.putExtra("notification_status", "0");
                    intent.putExtra("blocked_status", "0");
                    intent.putExtra("participants_nos", mParicipent_nos);
                    startActivity(intent);
                } else {
                    Intent in = new Intent();
                    in.putExtra("ids", mParicipents);
                    setResult(RESULT_OK, in);
                }
                finish();

            } else {
                Snackbar.make(rl_add, "Minimum 2 participants required.", Snackbar.LENGTH_SHORT).show();
            }

            if (mContactsGroup.size() == 1) {
                if (!getIntent().hasExtra("g_name")) {

                    mParicipents = GetParicipents(mContactsGroup);
                    Intent in = new Intent();
                    in.putExtra("ids", mParicipents);
                    setResult(RESULT_OK, in);
                    finish();
                } else {
                    Snackbar.make(rl_add, "Minimum 2 participants required.", Snackbar.LENGTH_SHORT).show();
                }

            } else {
                Snackbar.make(rl_add, "Minimum 1 participant required.", Snackbar.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }


    /*************************************
     * Hide keyboard
     ***********************************/
    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = AddContactsToChatGroup.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) AddContactsToChatGroup.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /***************************************
     * Get participent ids
     *************************************/
    private String GetParicipents(List<ModelUser> arr) {
        StringBuffer sb = new StringBuffer();
        for (ModelUser model : arr) {
            sb.append(model.user_id).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    /***************************************
     * Get participent number
     *************************************/
    private String GetNumber(List<ModelUser> arr) {
        StringBuffer sb = new StringBuffer();
        for (ModelUser model : arr) {
            sb.append(model.user_contact).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }
    /***************************************
     * Check Duplicate entries
     *************************************/
    private boolean isDuplicate(List<ModelUser> arr, ModelUser user) {
        for (ModelUser model : arr) {

            if (model.user_id.equals(user.user_id)) {
                return true;
            }
        }

        return false;
    }

}
