package com.app.delayed.chatExtended.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.chatExtended.AddContactsToChatGroup;
import com.app.delayed.model.ModelUser;
import com.app.delayed.utils.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Balvinder on 3/8/2016.
 */
public class ContactSuggestionAdapter extends BaseAdapter implements Filterable {

    Context context;
    List<ModelUser> mData, suggestions, tempItems;
    LayoutInflater inflater;
    ViewHolder holder;

    public ContactSuggestionAdapter(Context ctx, List<ModelUser> data) {

        this.context = ctx;
        this.mData = data;
        inflater = LayoutInflater.from(ctx);
        tempItems = new ArrayList<ModelUser>(mData); // this makes the difference.
        suggestions = new ArrayList<ModelUser>();
        suggestions.addAll(data);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.suggestion_item, parent, false);
            holder = new ViewHolder();

            holder.icon = (CircularImageView) convertView.findViewById(R.id.pic);
            holder.pic2 = (ImageView) convertView.findViewById(R.id.pic2);
            holder.name = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(mData.get(position).user_name);

      /*  try {
            Picasso.with(context)
                    .load(mData.get(position).user_pic)
                    .placeholder(R.mipmap.default_user)
                    .error(R.mipmap.default_user)
                    .into(holder.icon);
        } catch (Exception e) {

        }
*/

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(mData.get(position).user_name.charAt(0)), context.getResources().getColor(R.color.colorPrimary));

            if (mData.get(position).user_pic.length()>5) {
                holder.pic2.setImageDrawable(drawable);
                Picasso.with(context)
                        .load(mData.get(position).user_pic)
//                        .placeholder(drawable)
//                        .error(drawable)
                        .into(holder.icon, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.pic2.setVisibility(View.INVISIBLE);
                                holder.icon.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.pic2.setVisibility(View.VISIBLE);
                                holder.icon.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                holder.pic2.setVisibility(View.VISIBLE);
                holder.icon.setVisibility(View.INVISIBLE);
                holder.pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound("+", context.getResources().getColor(R.color.colorPrimary));
            holder.pic2.setVisibility(View.VISIBLE);
            holder.icon.setVisibility(View.INVISIBLE);
            holder.pic2.setImageDrawable(drawable);
        }

//        Glide.with(context).load(mData.get(position).user_pic)
//               // .placeholder(R.mipmap.avatar)
//                .crossFade()
//                .error(R.mipmap.avatar)
//                .into(holder.icon);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModelUser model = new ModelUser();
                model.user_name = mData.get(position).user_name;
                model.user_id = mData.get(position).user_id;
                model.user_pic = mData.get(position).user_pic;
                model.user_contact = mData.get(position).user_contact;

                if (AddContactsToChatGroup.mSuggestion_box.length() > 0) {
                    hidekeyboard();
                    AddContactsToChatGroup.mSuggestion_box.setText("");
                    if (!isDuplicate(AddContactsToChatGroup.mContactsGroup, model)) {
                        AddContactsToChatGroup.mContactsGroup.add(model);
                    } else {
                        Snackbar.make(AddContactsToChatGroup.mAdd, "Already added.", Snackbar.LENGTH_SHORT).show();
                    }
                    notifyDataSetChanged();
                } else {
                    Snackbar.make(AddContactsToChatGroup.mAdd, "Select participant.", Snackbar.LENGTH_SHORT).show();
                }

            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public class ViewHolder {

        public CircularImageView icon;
        public ImageView pic2;
        public TextView name;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            AddContactsToChatGroup.model = new ModelUser();
            String str = ((ModelUser) resultValue).user_name;
            AddContactsToChatGroup.model.user_name = str;
            AddContactsToChatGroup.model.user_id = ((ModelUser) resultValue).user_id;
            AddContactsToChatGroup.model.user_pic = ((ModelUser) resultValue).user_pic;
            AddContactsToChatGroup.model.user_contact = ((ModelUser) resultValue).user_contact;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            if (constraint != null) {
                suggestions.clear();
                for (ModelUser people : tempItems) {
                    //   if (people.user_name.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                    suggestions.add(people);
                    //   }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //   List<ModelUser> filterList = (ArrayList<ModelUser>) results.values;
            System.out.println("new=" + constraint);
            String charText = String.valueOf(constraint);
            charText = charText.toLowerCase(Locale.getDefault());
            mData.clear();
            if (charText.length() == 0) {
                mData.addAll(suggestions);
            } else {
                for (ModelUser wp : suggestions) {
                    if (wp.user_name.toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        mData.add(wp);
                    }
                }
            }
            notifyDataSetChanged();


        }
    };

    /***************************************
     * Check Duplicate entries
     *************************************/
    private boolean isDuplicate(List<ModelUser> arr, ModelUser user) {
        for (ModelUser model : arr) {

            if (model.user_id.equals(user.user_id)) {
                return true;
            }
        }

        return false;
    }

    /*************************************
     * Hide keyboard
     ***********************************/
    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ((Activity) context)
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
