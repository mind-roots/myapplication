package com.app.delayed.chatExtended;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.delayed.Dashboard;
import com.app.delayed.R;
import com.app.delayed.chatExtended.AsyncTasks.SendMessageServer;
import com.app.delayed.chatExtended.CustomAdapters.ChatAdapter;
import com.app.delayed.chatExtended.Entities.Image;
import com.app.delayed.chatExtended.Entities.MediaFile;
import com.app.delayed.chatExtended.Entities.Message;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.chatExtended.Receivers.WifiDirectBroadcastReceiver;
import com.app.delayed.chatExtended.ui.ViewProxy;
import com.app.delayed.chatExtended.util.ActivityUtilities;
import com.app.delayed.chatExtended.util.FileUtilities;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.model.MultipartUtility;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ChattingActivity extends AppCompatActivity {
    private static final String TAG = "ChattingActivity";
    private static final int PICK_IMAGE = 1;
    private static final int TAKE_PHOTO = 2;
    private static final int RECORD_AUDIO = 3;
    private static final int RECORD_VIDEO = 4;
    private static final int CHOOSE_FILE = 5;
    private static final int DRAWING = 6;
    private static final int DOWNLOAD_IMAGE = 100;
    private static final int DELETE_MESSAGE = 101;
    private static final int DOWNLOAD_FILE = 102;
    private static final int COPY_TEXT = 103;
    private static final int SHARE_TEXT = 104;
    private WifiP2pManager mManager;
    private Channel mChannel;
    private WifiDirectBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private EditText edit;
    public static ListView listView;
    public static List<Message> listMessage = new ArrayList<>();
    public static List<Message> childMessage = new ArrayList<>();
    public static List<Message> nextmsgchk = new ArrayList<>();
    public static ChatAdapter chatAdapter;
    private Uri fileUri;
    int rotateImage;
    Uri fileUrii;
    private String fileURL;
    private ArrayList<Uri> tmpFilesUri;
    private MediaRecorder mRecorder;
    private String mFileName;
    Toolbar toolbar;
    public static TextView mTitle;
    public static String mName, mPic, mContenttype;
    private static String audio_length = "00:00";
    public static int position = -1;

    //*******Variables fro Audio Record*****
    private TextView recordTimeText;
    private ImageButton audioSendButton;
    private View recordPanel;
    private View slideText;
    private float startedDraggingX = -1;
    private float distCanMove = dp(80);
    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private Timer timer;
    boolean isVisible = false;
    boolean isDelayed = false;
    boolean isCancelAudio = false;
    int mYear, mMonth, mDay, hour, minute;
    Calendar c;
    // StringBuilder builder;
    int rec_id;
    TextView textViewSlide;
    ImageView mslideArrow;
    File destination;
    String participants_ids, threadids, auth_code, notification_status, blocked_status, participants_nos;
    JSONParser parser;
    SharedPreferences prefs;
    DatabaseQueries dataquery;
    boolean valid = false;
    TextView delayed, button;
    int curday, curmont, curyear, curhour, curmin;
    String userid, username, userpic;
    String triggerdate, triggerdate2, schdate, schtype;
    CharSequence[] items = {"Non recurring", "Recur daily", "Recur weekdays", "Recur weekly", "Recur forthnightly", "Recur monthly", "Recur yearly"};
    String[] sentitems = {"0", "d", "wd", "w", "fn", "m", "y"};
    SharedPreferences.Editor editor;
    int divider = 1, style, compareday, comparemonth;

    LinearLayout mWriteZone;
    //ArrayList<String> readcontentids = new ArrayList<>();
    int typemessage;
    String[] participent_ids_arr;
    int TIME_PICKER_INTERVAL = 5;
    public static int offset = 0;
    String typeset = "0";
    public static boolean update = false;
    public static int lastpos = 0;
    boolean isGroup = false;
    boolean hourstype = false;
    boolean setr = false;
    boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initcall();

        Dashboard.newchat = true;
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = WifiDirectBroadcastReceiver.createInstance();
        mReceiver.setmActivity(this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        //Start the service to receive message
       /* if (Dashboard.newchat) {
            Dashboard.newchat = false;
            //   startService(new Intent(this, MessageService.class));
        }*/
        //Initialize the adapter for the chat


        mTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("", "");
                if (isGroup) {
                    startActivity(new Intent(ChattingActivity.this, ChatGroupSetting.class)
                            .putExtra("name", mName)
                            .putExtra("chat_user_id", participants_ids)
                            .putExtra("threadids", threadids)
                            .putExtra("noti", notification_status)
                            .putExtra("block", blocked_status)
                            .putExtra("isGroup", isGroup)
                            .putExtra("image", mPic));
                } else {
                    startActivity(new Intent(ChattingActivity.this, ViewContact.class)
                            .putExtra("name", mName)
                            .putExtra("chat_user_id", participants_ids)
                            .putExtra("threadids", threadids)
                            .putExtra("noti", notification_status)
                            .putExtra("block", blocked_status)
                            .putExtra("isGroup", isGroup)
                            .putExtra("image", mPic));
                }
            }
        });

        //Initialize the list of temporary files URI
        tmpFilesUri = new ArrayList<>();

        Record();

        //Send a message

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //    HideKeyboard();
                if (!edit.getText().toString().equals("")) {
                    Log.v(TAG, "Send message");
                    if (Methods.isNetworkConnected(ChattingActivity.this)) {

                        isDelayed = false;
                        if (!threadids.equals("no")) {
                            sendMessage(Message.TEXT_MESSAGE, isDelayed);
                        } else {
                            //send with some delay
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 1/2s = 500ms
                                    sendMessage(Message.TEXT_MESSAGE, isDelayed);
                                }
                            }, 700);
                        }

                    } else {
                        Snackbar.make(delayed, "No internet connection.", Snackbar.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ChattingActivity.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                }
            }
        });


        delayed.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                HideKeyboard();
                if (!edit.getText().toString().equals("")) {
                    Log.v(TAG, "Send message");
                    if (Methods.isNetworkConnected(ChattingActivity.this)) {

                        isDelayed = true;
                        typemessage = Message.TEXT_MESSAGE;

                        if (Build.VERSION.SDK_INT < 21) {

                            datePickerbuildbelow(datePickerListener2, curyear, curmont, curday);
                        } else {

                            datePicker(datePickerListener1, curyear, curmont, curday);
                        }
                    } else {
                        Snackbar.make(delayed, "No internet connection.", Snackbar.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(ChattingActivity.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Register the context menu to the list view (for pop up menu)
        //  registerForContextMenu(listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                int k = i;
                long g = l;
                if (i==listMessage.size()){
                    i=i-1;
                }
                Message model = listMessage.get(i);
                if (model.schstatus.equals("4")) {
                    return false;
                }
                String message = "";
                if (Utils.chat_section == 0) {

                    message = "Are you sure you want to delete this message? This will not remove the message from the recipients chat.";

                } else if (Utils.chat_section == 1) {
                    message = "Are you sure you want to delete your message? It will not be sent once deleted.";
                } else if (Utils.chat_section == 2) {
                    message = "Are you sure you want to delete your message? It will not be sent once deleted.";
                }
                dialog("Alert!", message, android.R.drawable.ic_dialog_info, i);

                return true;
            }
        });

        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isVisible = false;
                findViewById(R.id.record_view).setVisibility(View.GONE);
                return false;
            }
        });

    }


    private void initcall() {
        if (Build.VERSION.SDK_INT < 21) {
            //   style = R.style.DialogTheme2;
            style = android.graphics.Color.TRANSPARENT;
            divider = 1;
            // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } else {
            style = R.style.DialogTheme1;
            divider = 15;
        }
        parser = new JSONParser();
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        Intent intent = getIntent();
        threadids = intent.getStringExtra("threadids");
        mName = intent.getStringExtra("name");
        mPic = intent.getStringExtra("image");
        if (getIntent().hasExtra("g_name")) {
            mName = getIntent().getStringExtra("g_name");
            mPic = intent.getStringExtra("g_pic");
        }
        editor = prefs.edit();
        editor.putString("title_name", mName).commit();
        mContenttype = intent.getStringExtra("contenttype");
        participants_ids = intent.getStringExtra("participants_ids");
        participants_nos = intent.getStringExtra("participants_nos");
        notification_status = intent.getStringExtra("notification_status");
        blocked_status = intent.getStringExtra("blocked_status");
        isGroup = getIntent().getBooleanExtra("isGroup", false);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        setTitle("");
        // mTitle.setText(mName);
        mTitle.setText(prefs.getString("title_name", null));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        auth_code = prefs.getString("auth_code", null);
        userid = prefs.getString("id", null);
        username = prefs.getString("name", null);
        userpic = prefs.getString("chat_icon", null);

        try {
            if (prefs.getString("timeformat", null).equals("24")) {
                hourstype = true;
            } else {
                hourstype = false;
            }
        } catch (Exception e) {

        }

        dataquery = new DatabaseQueries(ChattingActivity.this);

        delayed = (TextView) findViewById(R.id.sendDelayed);
        button = (TextView) findViewById(R.id.sendMessage);
        mWriteZone = (LinearLayout) findViewById(R.id.writeZone);
        edit = (EditText) findViewById(R.id.editMessage);
        listView = (ListView) findViewById(R.id.messageList);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        //listView.setStackFromBottom(true);
        if (threadids.equals("no")) {
            new new_thread().execute();
        } else {
            editor = prefs.edit();
            editor.putString("current_thread", threadids);
            editor.commit();
            notification_status = intent.getStringExtra("notification_status");
            blocked_status = intent.getStringExtra("blocked_status");
        }
        participent_ids_arr = participants_ids.split(",");

        //commented on 25-05-2016

      /*  if (isGroup) {
            /*//***********split group participents********
         ArrayList<ModelContacts> mContacts = new ArrayList<>();
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
         String date = format.format(new Date());
         String[] nos = participants_nos.split(",");

         for (int i = 0; i < participent_ids_arr.length; i++) {
         String id = participent_ids_arr[i];
         System.out.println("ids=" + id);

         if (!userid.equals(id)) {
         if (dataquery.contactexist(id)) {
         } else {
         ModelContacts user = new ModelContacts();
         user.cont_name = nos[i];
         user.cont_number = nos[i];
         user.cont_id = id;
         user.cont_date = date;
         dataquery.InsertnewContacts(user);
         mContacts.add(user);

         }


         }

         }

         if (mContacts.size() > 0) {
         new contactsync(mContacts).execute();
         }
         }
         */

        for (String id : participent_ids_arr) {
            if (!userid.equals(id)) {
                new userupdate(id).execute();
            }
        }

        c = Calendar.getInstance();
        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);
        if (!threadids.equals("no")) {

            if (isGroup) {
                new updateinfo(threadids).execute();
            }

           /* readcontentids = dataquery.getreadstatus(threadids);
            StringBuilder ids = new StringBuilder();

            for (String str : readcontentids) {
                ids.append(str).append(",");
            }
            String cids = ids.toString();
            if (cids.indexOf(",") != -1) {
                cids = cids.substring(0, cids.length() - 1);
            }
            if (readcontentids.size() != 0) {
                new contentids(cids).execute();
            }*/
            if (mContenttype.equals("recent")) {
                typeset = "0";
                offset = 0;
                listMessage = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "0", offset, 20);

                // listMessage = dataquery.getthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "0");
            } else if (mContenttype.equals("sch")) {
                typeset = "1";
                offset = 0;
                listMessage = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "1", offset, 20);

                //  listMessage = dataquery.getthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "1");
            } else if (mContenttype.equals("rec")) {
                typeset = "2";
                offset = 0;
                listMessage = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "2", offset, 20);

                //   listMessage = dataquery.getthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, "2");
            }
            /*else {
                listMessage = dataquery.gettfilterhreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, mContenttype);
            }*/
        } else {
            listMessage.clear();
        }

        //   Collections.sort(listMessage, new ChatComparator());
        //Collections.reverse(listMessage);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View convertViewHeader = inflater.inflate(R.layout.loadmore, null);
        nextmsgchk = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, typeset, offset + 20, 20);

        if (nextmsgchk.size() > 0) {
            listView.addHeaderView(convertViewHeader, null, false);
        }
        chatAdapter = new ChatAdapter(this, listMessage);
        listView.setAdapter(chatAdapter);
        listView.setSelection(listMessage.size() - 1);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastVisibleRow = listView.getLastVisiblePosition();
                System.out.println("last=" + lastVisibleRow);
                System.out.println("lastsss=" + listMessage.size());
                System.out.println("lastsss11=" + firstVisibleItem);
                //  System.out.println("lastsss22=" + lastVisibleRow);
                if (listMessage.size() - 1 == lastVisibleRow) {
                    update = true;
                } else {
                    update = false;
                    //  lastpos = lastVisibleRow - visibleItemCount;
                    lastpos = firstVisibleItem;
                }

                try {
                    if (position >= 0 && (position < firstVisibleItem || position >= lastVisibleRow)) {
                        //chatAdapter.notifyDataSetChanged();
                        ChatAdapter.isPlaying = false;
                        if (ChatAdapter.mPlayer != null) {
                            ChatAdapter.mPlayer.stop();

                        }
                        ChatAdapter.mPlayer.release();
                        ChatAdapter.mPlayer = null;
                        position = -1;
                    }
                } catch (Exception e) {
                    chatAdapter.notifyDataSetChanged();
                    e.printStackTrace();
                    position = -1;
                }


            }
        });
        convertViewHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                offset = offset + 20;
                System.out.println("limit=" + offset);
                childMessage = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, typeset, offset, 20);
                nextmsgchk = dataquery.getlimitthreadchat(threadids, ChattingActivity.this, userid, mPic, mName, userpic, username, typeset, offset + 20, 20);
                listMessage.addAll(0, childMessage);
                ChatAdapter.counter = 0;
                chatAdapter.notifyDataSetChanged();
                if (nextmsgchk.size() == 0) {
                    listView.removeHeaderView(convertViewHeader);
                    listView.setSelection(childMessage.size());
                } else {
                    //  listView.smoothScrollToPosition(19);

                    listView.setSelection(childMessage.size() + 1);

                }
            }
        });
        if (mContenttype.equals("rec") || mContenttype.equals("sch")) {
            mWriteZone.setVisibility(View.GONE);
        } else {
            mWriteZone.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        ActivityUtilities.customiseActionBar(this);
    }

    class new_thread extends AsyncTask<String, Void, String> {
        String response;

        @Override
        protected String doInBackground(String... params) {

            if (getIntent().hasExtra("g_name")) {
                response = GroupThread(getIntent().getStringExtra("g_name"), participants_ids, getIntent().getStringExtra("g_pic"));
            } else {
                response = schdule_method("create_new_thread");
            }

            System.out.print("new thrd=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Toast.makeText(ChattingActivity.this, "in", Toast.LENGTH_SHORT).show();
            try {

                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    ModelMsgThread item = new ModelMsgThread();
                    JSONObject dobj = obj.optJSONObject("data");
                    threadids = dobj.optString("thread_id");
                    item.thread_id = dobj.optString("thread_id");
                    item.participant_ids = dobj.optString("participant_ids");
                    item.participant_numbers = dobj.optString("participant_nos");
                    // String gname = URLDecoder.decode(dobj.optString("group_name"), "UTF-8");
                    item.group_name = dobj.optString("group_name");
                    item.group_icon = dobj.optString("group_icon");
                    item.thread_sync_id = dobj.optString("thread_sync_id");
                    item.last_updated = dobj.optString("last_updated");
                    item.notification_status = dobj.optString("notification_status");
                    item.blocked_status = dobj.optString("blocked_status");
                    dataquery.insertmsgthread(item);

                } else if (status.equals("false1")) {

                    threadids = obj.optString("thread_id");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            editor = prefs.edit();
            editor.putString("current_thread", threadids);
            editor.commit();
        }

    }

    private String schdule_method(String service_type) {
        String res = null;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("participant", participants_ids)
                .appendQueryParameter("blocked_status", "0")
                .appendQueryParameter("notification_status", "0");

        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;

    }

    /********************************************
     * Create group thread
     ********************************************/
    private String GroupThread(String g_name, String participants, String image) {
        String charset = "UTF-8";
        File sourceFile = null;
        if (image != null) {
            sourceFile = new File(image);
        }
        String requestURL = Utils.base_url + "service_type=create_new_thread";
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String gname = URLEncoder.encode(g_name, "UTF-8");

            multipart.addFormField("auth_code", auth_code);
            multipart.addFormField("group_name", gname);
            multipart.addFormField("participant", participants);
            multipart.addFormField("blocked_status", "0");
            multipart.addFormField("notification_status", "0");
            if (image != null) {
                multipart.addFilePart("group_icon", sourceFile);
            }
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }


    @Override
    public void onResume() {
        super.onResume();

        registerReceiver(mReceiver, mIntentFilter);

        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.v(TAG, "Discovery process succeeded");
            }

            @Override
            public void onFailure(int reason) {
                Log.v(TAG, "Discovery process failed");
            }
        });
        saveStateForeground(true);
        if (Dashboard.leavegrp) {
            Dashboard.leavegrp = false;
            finish();
        }
        mTitle.setText(prefs.getString("title_name", null));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
        saveStateForeground(false);
    }

   /* @Override
    public void onBackPressed() {
        AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
        newDialog.setTitle("Close chatroom");
        newDialog.setMessage("Are you sure you want to close this chatroom?\n"
                + "You will no longer be able to receive messages, and "
                + "all unsaved media files will be deleted.\n"
                + "If you are the server, all other users will be disconnected as well.");

        newDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearTmpFiles(getExternalFilesDir(null));
//				if(WifiConnect.server!=null){
//					WifiConnect.server.interrupt();
//				}
                android.os.Process.killProcess(android.os.Process.myPid());
            }

        });

        newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        newDialog.show();
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearTmpFiles(getExternalFilesDir(null));
    }

    // Handle the data sent back by the 'for result' activities (pick/take image, record audio/video)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK && data.getData() != null) {
                    fileUri = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getApplicationContext().getContentResolver().query(fileUri, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    rotateImage = getCameraPhotoOrientation(ChattingActivity.this, fileUri, filePath);
                    Log.i("gal ####: ",""+rotateImage);
                    Image image = new Image(this, fileUri);

                    /******save image after solving rotation problem with new Uri*******/

                    fileUri = SaveImage(RotateBitmap(image.getBitmapFromUri(fileUri), rotateImage));
                    // sendMessage(Message.IMAGE_MESSAGE, isDelayed);
                    simpledialog(Message.IMAGE_MESSAGE, "Do you want to schedule your image or send it now? ", "Image scheduling");
                }
                break;
            case TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
//                    Bitmap bit = (Bitmap) data.getExtras().get("data");
//                    fileUri = SaveImage(bit);
//                    Bitmap bit = MediaStore.Images.Media.getBitmap(
//                            getContentResolver(), fileUri);
                    //  sendMessage(Message.PHOTO_MESSAGE, isDelayed);
//                    ExifInterface exif = null;     //Since API Level 5
//                    try {
//                        exif = new ExifInterface(fileUri.getPath());
//                        String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//                        Log.i("camera ##### : ",""+exifOrientation);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//
//                    //*******************added 8/8/16**********************
                    rotateImage = getCameraPhotoOrientation(ChattingActivity.this, fileUri, fileUri.getPath());
                    Log.i("gal ####: ",""+rotateImage);
                    Image image = new Image(this, fileUri);

                    /******save image after solving rotation problem with new Uri*******/

                    fileUri = SaveImage(RotateBitmap(image.getBitmapFromUri(fileUri), rotateImage));
                    //**********************************************************************************
                    simpledialog(Message.PHOTO_MESSAGE, "Do you want to schedule your image or send it now?", "Image scheduling");
                    tmpFilesUri.add(fileUri);
                    // fileUri = null;
                }
                break;
            case RECORD_AUDIO:
                if (resultCode == RESULT_OK) {
                    fileURL = (String) data.getStringExtra("audioPath");
                    // sendMessage(Message.AUDIO_MESSAGE, isDelayed);
                    simpledialog(Message.AUDIO_MESSAGE, "Do you want to schedule your voice message or send it now?", "Audio scheduling");
                }
                break;
            case RECORD_VIDEO:
                if (resultCode == RESULT_OK) {
                    fileUri = data.getData();
                    fileURL = MediaFile.getRealPathFromURI(this, fileUri);
                    //   sendMessage(Message.VIDEO_MESSAGE, isDelayed);
                    // simpledialog(Message.VIDEO_MESSAGE);
                }
                break;
            case CHOOSE_FILE:
                if (resultCode == RESULT_OK) {
                    fileURL = (String) data.getStringExtra("filePath");
                    //  sendMessage(Message.FILE_MESSAGE, isDelayed);
                    //simpledialog(Message.FILE_MESSAGE);
                }
                break;
            case DRAWING:
                if (resultCode == RESULT_OK) {
                    fileURL = (String) data.getStringExtra("drawingPath");
                    //     sendMessage(Message.DRAWING_MESSAGE, isDelayed);
                    //simpledialog(Message.DRAWING_MESSAGE);
                }
                break;
        }
    }


    //***************Record audio Method*************
    private void Record() {

        recordPanel = findViewById(R.id.record_panel);
        recordTimeText = (TextView) findViewById(R.id.recording_time_text);
        slideText = findViewById(R.id.slideText);
        audioSendButton = (ImageButton) findViewById(R.id.chat_audio_send_button);
        textViewSlide = (TextView) findViewById(R.id.slideToCancelTextView);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) slideText
                .getLayoutParams();
        params.leftMargin = dp(30);
        slideText.setLayoutParams(params);
        ViewProxy.setAlpha(slideText, 1);
        textViewSlide.setText("Tap and hold to record");
        mslideArrow = (ImageView) findViewById(R.id.slideArrow);

        audioSendButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) slideText
                            .getLayoutParams();
                    params.leftMargin = dp(30);
                    slideText.setLayoutParams(params);
                    ViewProxy.setAlpha(slideText, 1);
                    startedDraggingX = -1;
                    mFileName = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC).getAbsolutePath();
                    mFileName += "/" + FileUtilities.fileName() + ".mp3";
                    startRecording();
                    //startrecord();
                    textViewSlide.setText("Slide To Cancel");
                    mslideArrow.setVisibility(View.VISIBLE);
                    audioSendButton.getParent()
                            .requestDisallowInterceptTouchEvent(true);
                    recordPanel.setVisibility(View.VISIBLE);
                    isCancelAudio = false;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP
                        || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    startedDraggingX = -1;
                    //stoprecord();
                    stopRecording();
                    if (!isCancelAudio) {

                        fileURL = mFileName;
                        simpledialog(Message.AUDIO_MESSAGE, "Do you want to schedule your voice message or send it now?", "Audio scheduling");
                        //  sendMessage(Message.AUDIO_MESSAGE, isDelayed);

                    }
                    textViewSlide.setText("Tap and hold to record");
                    findViewById(R.id.record_view).setVisibility(View.GONE);
                    isVisible = false;

                    mslideArrow.setVisibility(View.INVISIBLE);

                } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    float x = motionEvent.getX();
                    if (x < -distCanMove) {
                        //stoprecord();
                        stopRecording();
                        isCancelAudio = true;
                    }
                    x = x + ViewProxy.getX(audioSendButton);
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) slideText
                            .getLayoutParams();
                    if (startedDraggingX != -1) {
                        float dist = (x - startedDraggingX);
                        params.leftMargin = dp(30) + (int) dist;
                        slideText.setLayoutParams(params);
                        float alpha = 1.0f + dist / distCanMove;
                        if (alpha > 1) {
                            alpha = 1;
                        } else if (alpha < 0) {
                            alpha = 0;
                        }
                        ViewProxy.setAlpha(slideText, alpha);
                    }
                    if (x <= ViewProxy.getX(slideText) + slideText.getWidth()
                            + dp(30)) {
                        if (startedDraggingX == -1) {
                            startedDraggingX = x;
                            distCanMove = (recordPanel.getMeasuredWidth()
                                    - slideText.getMeasuredWidth() - dp(48)) / 2.0f;
                            if (distCanMove <= 0) {
                                distCanMove = dp(80);
                            } else if (distCanMove > dp(80)) {
                                distCanMove = dp(80);
                            }
                        }
                    }
                    if (params.leftMargin > dp(30)) {
                        params.leftMargin = dp(30);
                        slideText.setLayoutParams(params);
                        ViewProxy.setAlpha(slideText, 1);
                        startedDraggingX = -1;
                    }
                } else {
                    textViewSlide.setText("Tap and hold to record");
                }
                view.onTouchEvent(motionEvent);
                return true;
            }
        });

    }


    /**************************************
     * Audio Recrding method
     ************************************/
    public void startRecording() {
        startTime = SystemClock.uptimeMillis();
        timer = new Timer();
        MyTimerTask myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 1000, 1000);
        vibrate();
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setMaxDuration(60000);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);


        try {
            mRecorder.prepare();
            mRecorder.start();
            mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                @Override
                public void onInfo(MediaRecorder mr, int what, int extra) {
                    if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                        Log.v("VIDEOCAPTURE", "Maximum Duration Reached");
                        mr.stop();
                        if (timer != null) {
                            timer.cancel();
                            audio_length = recordTimeText.getText().toString();
                        }
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    /**************************************
     * Stop Recrding method
     ************************************/
    public void stopRecording() {
        if (timer != null) {
            timer.cancel();
            audio_length = recordTimeText.getText().toString();
        }
        if (recordTimeText.getText().toString().equals("00:00")) {
            return;
        }
        recordTimeText.setText("00:00");
        vibrate();
        try {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        } catch (Exception e) {

        }

    }


    /**************************************
     * Vibrate device
     ************************************/
    private void vibrate() {
        // TODO Auto-generated method stub
        try {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static int dp(float value) {
        return (int) Math.ceil(1 * value);
    }


    /**************************************
     * Audio Timer
     ************************************/
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            final String hms = String.format(
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(updatedTime)
                            - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
                            .toHours(updatedTime)),
                    TimeUnit.MILLISECONDS.toSeconds(updatedTime)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes(updatedTime)));
            long lastsec = TimeUnit.MILLISECONDS.toSeconds(updatedTime)
                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                    .toMinutes(updatedTime));
            System.out.println(lastsec + " hms " + hms);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        if (recordTimeText != null)
                            recordTimeText.setText(hms);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }
            });
        }

    }

    /**************************************
     * Date formatter
     ************************************/
    public String sentformatter(String sendtime, int i) {

        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            Date newDate = format1.parse(sendtime);
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (i == 0) {
                settym = format1.format(newDate);
            } else {
                format1.setTimeZone(TimeZone.getTimeZone("UTC"));
                settym = format1.format(newDate);
            }
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    /**************************************
     * Send message method
     ************************************/
    public void sendMessage(int type, boolean isDelayed) {
        Log.v(TAG, "Send message starts");
        // Message written in EditText is always sent
        //	Message mes = new Message(type, edit.getText().toString(), null, WifiConnect.chatName);

        Message mes = new Message(ChattingActivity.this);


        mes.time_text = GetFormattedTime();

        mes.userpic = userpic;
        mes.username = username;
        mes.headerdate = "-1";
        if (listMessage.size() > 0) {
            String datee = listMessage.get(listMessage.size() - 1).time_text;
            boolean value = match(datee);
            if (value) {
                mes.headerdate = "0";
            } else {
                mes.headerdate = "1";
            }
            mes.displaydate = false;
        } else {
            mes.headerdate = "1";
            mes.displaydate = true;
        }
        //  mes.delayed_time = "Delayed from: " + GetFormattedTime();

        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            schdate = timeFormatter.format(c.getTime());
            // mes.time_text=schdate;
            timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            triggerdate = timeFormatter.format(c.getTime());
            triggerdate = triggerdate + ":00";

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isDelayed) {
            Toast.makeText(ChattingActivity.this, "Your message has been scheduled.", Toast.LENGTH_SHORT).show();
            if (schtype.equals("0")) {
                mes.setSchstatus("1");
            } else {
                mes.setSchstatus("2");
            }
            mes.setSchdate(schdate);
            mes.setTriggerdate(triggerdate);
            mes.setSchtype(schtype);

            mes.isDelayed = true;
        } else {
            mes.setSchstatus("0");
            mes.setSchdate("");
            mes.setTriggerdate("");
            mes.setSchtype("null");

            mes.isDelayed = false;
        }
        if (type == 0) {
            mes.setmType(type);
        } else if (type == 1 || type == 6) {
            mes.setmType(1);
        } else if (type == 2) {
            mes.setmType(2);
        }

        mes.setmText(edit.getText().toString());
        mes.setThread_id(threadids);
        switch (type) {


            case Message.IMAGE_MESSAGE:
                System.out.println("uri image=" + fileUri);
                Image image = new Image(this, fileUri);
                //    String image_path = getRealPathFromURI(getApplicationContext(), fileUri);
                // String path = RealPathUtil.getRealPathFromURI(this, fileUri);
                String path = fileUri.getPath();
                mes.setByteArray(image.bitmapToByteArray(image.getBitmapFromUri(fileUri)));
                mes.setFilePath(path);
                mes.imgurl = "";
                mes.setAudioLength("");
                mes.setFileName(image.getFileName());
                mes.setFileSize(image.getFileSize());
                Log.v(TAG, "Set byte array to image ok");
                break;
            case Message.PHOTO_MESSAGE:
                System.out.println("uri photo=" + fileUri);
                Image image2 = new Image(this);
                mes.setmType(1);
                mes.imgurl = "";
                //     String image_path1 = getRealPathFromURI(getApplicationContext(), fileUri);
                String realPath = fileUri.getPath();
                // String realPath = RealPathUtil.getRealPathFromURI(this, fileUri);
                mes.setByteArray(image2.bitmapToByteArray(image2.getBitmapFromUri(fileUri)));
                mes.setFileName(new File(fileUri.getPath()).getName());
                mes.setFilePath(realPath);
                mes.setAudioLength("");
                fileUri = null;
                // mes.setFileSize(image.getFileSize());
                Log.v(TAG, "Set byte array to image ok");
                break;
            case Message.AUDIO_MESSAGE:
                System.out.print("uri=" + fileURL);
                MediaFile audioFile = new MediaFile(this, fileURL, Message.AUDIO_MESSAGE);
                System.out.println("audiolenght=" + audio_length);
                mes.setByteArray(audioFile.fileToByteArray());
                mes.setAudioLength(audio_length);
                mes.setFileName(audioFile.getFileName());
                mes.setFilePath(audioFile.getFilePath());

                break;
            case Message.VIDEO_MESSAGE:
                MediaFile videoFile = new MediaFile(this, fileURL, Message.AUDIO_MESSAGE);

                mes.setByteArray(videoFile.fileToByteArray());
                mes.setFileName(videoFile.getFileName());
                mes.setFilePath(videoFile.getFilePath());
                tmpFilesUri.add(fileUri);
                break;
            case Message.FILE_MESSAGE:
                MediaFile file = new MediaFile(this, fileURL, Message.FILE_MESSAGE);

                mes.setByteArray(file.fileToByteArray());
                mes.setFileName(file.getFileName());
                break;
            case Message.DRAWING_MESSAGE:
                MediaFile drawingFile = new MediaFile(this, fileURL, Message.DRAWING_MESSAGE);

                mes.setByteArray(drawingFile.fileToByteArray());
                mes.setFileName(drawingFile.getFileName());
                mes.setFilePath(drawingFile.getFilePath());
                break;
        }
        Log.v(TAG, "Message object hydrated");

        Log.v(TAG, "Start AsyncTasks to send the message");
//        if (mReceiver.isGroupeOwner() == WifiDirectBroadcastReceiver.IS_OWNER) {
//            Log.v(TAG, "Message hydrated, start SendMessageServer AsyncTask");
        new SendMessageServer(ChattingActivity.this, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mes);
//        } else if (mReceiver.isGroupeOwner() == WifiDirectBroadcastReceiver.IS_CLIENT) {
//            Log.v(TAG, "Message hydrated, start SendMessageClient AsyncTask");
//            new SendMessageClient(ChattingActivity.this, mReceiver.getOwnerAddr()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mes);
//        }

        if (destination != null && destination.exists()) {
            destination.delete();
        }

        edit.setText("");
    }

    private boolean match(String datee) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date2 = dateFormat.parse(datee);

            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            String d1 = dateFormat1.format(date2);


            Date date = new Date();
            String d2 = dateFormat1.format(date);

            if (d1.equals(d2)) {
                return true;
            }

        } catch (Exception e) {
        }
        return false;
    }


    /**************************************
     * Refresh the message list
     ************************************/
    public static void refreshList(Message message, boolean isMine) {
        Log.v(TAG, "Refresh message list starts");

        message.setMine(isMine);
        listMessage.add(message);
        if (chatAdapter != null) {
            chatAdapter.notifyDataSetChanged();
        }


        Log.v(TAG, "Chat Adapter notified of the changes");

        //Scroll to the last element of the list
        listView.setSelection(listMessage.size() - 1);
    }


    // Save the app's state (foreground or background) to a SharedPrefereces
    public void saveStateForeground(boolean isForeground) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Editor edit = prefs.edit();
        edit.putBoolean("isForeground", isForeground);
        edit.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mContenttype.equals("recent")) {
            getMenuInflater().inflate(R.menu.menu_chat, menu);

        }

        return true;
    }

    // Handle click on the menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int idItem = item.getItemId();
        switch (idItem) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.send_image:
                //     showPopup(toolbar);
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    // showPopup(toolbar);
                    selectPicker();
                }
                return true;

            case R.id.send_audio:
                Log.v(TAG, "Start activity to record audio");
                //  startActivityForResult(new Intent(this, RecordAudioActivity.class), RECORD_AUDIO);
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermissionAudio();
                } else {
                    Record();
                    if (isVisible) {
                        isVisible = false;
                        findViewById(R.id.record_view).setVisibility(View.GONE);
                    } else {
                        isVisible = true;

                        findViewById(R.id.record_view).setVisibility(View.VISIBLE);
                    }

                }

                return true;

       /*     case R.id.send_video:
                Log.v(TAG, "Start activity to record video");
//	        	Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//	        	takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
//	        	if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
//	                startActivityForResult(takeVideoIntent, RECORD_VIDEO);
//	            }
                return true;*/

         /*   case R.id.send_file:
                Log.v(TAG, "Start activity to choose file");
//	        	Intent chooseFileIntent = new Intent(this, FilePickerActivity.class);
//	        	startActivityForResult(chooseFileIntent, CHOOSE_FILE);
                return true;*/

          /*  case R.id.send_drawing:
                Log.v(TAG, "Start activity to draw");
//	        	Intent drawIntent = new Intent(this, DrawingActivity.class);
//	        	startActivityForResult(drawIntent, DRAWING);
                return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //*************RecurringPicker***********
    private void selectPicker() {

        // int selected_rec=0;
        final CharSequence[] items = {"Pick from gallery", "Take from camera"};
        new AlertDialog.Builder(ChattingActivity.this, R.style.AppTheme_Dialog)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == 0) {
//                            if (Build.VERSION.SDK_INT >= 23) {
//
//                                AllowPermission();
//                            } else {
//                                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                                startActivityForResult(intent, 1);
//                            }
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, PICK_IMAGE);

                        } else {
//                            Intent in = new Intent(ChattingActivity.this, GroupScreen.class);
//                            in.putExtra("menu", "no");
//                            startActivity(in);
                            fileUri = CameraImage();
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            startActivityForResult(cameraIntent, TAKE_PHOTO);
                        }
                    }
                })
                .show();


        // return selected_rec;
    }

    //Show the popup menu
    public void showPopup(View v) {

        final PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.pick_image:
                        Log.v(TAG, "Pick an image");
                        //    Intent intent = new Intent(Intent.ACTION_PICK);
                    /*    Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image*//**//*");
                      //  intent.setAction(Intent.ACTION_GET_CONTENT);

                        // Prevent crash if no app can handle the intent
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(intent, PICK_IMAGE);
                        }*/
                        popup.dismiss();
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, PICK_IMAGE);
/*
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, 2);*/
                        break;

                    case R.id.take_photo:

                        Log.v(TAG, "Take a photo");

                    /*    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileUrii = getOutputMediaFileUri(1);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                        // start the image capture Intent
                        startActivityForResult(cameraIntent, TAKE_PHOTO);*/
                        popup.dismiss();
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, TAKE_PHOTO);

                        break;
                }
                return true;
            }
        });
        popup.inflate(R.menu.send_image);
        popup.show();
    }

    //Create pop up menu for image download, delete message, etc...
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");

        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        Message mes = listMessage.get((int) info.position);
        //Option to delete message independently of its type
        menu.add(0, DELETE_MESSAGE, Menu.NONE, "Delete message");

        if (!mes.getmText().equals("")) {
            //Option to copy message's text to clipboard
            menu.add(0, COPY_TEXT, Menu.NONE, "Copy message text");
            //Option to share message's text
            // menu.add(0, SHARE_TEXT, Menu.NONE, "Share message text");
        }

        int type = mes.getmType();
        switch (type) {
            case Message.IMAGE_MESSAGE:
                //  menu.add(0, DOWNLOAD_IMAGE, Menu.NONE, "Download image");
                break;
            case Message.FILE_MESSAGE:
                menu.add(0, DOWNLOAD_FILE, Menu.NONE, "Download file");
                break;
            case Message.AUDIO_MESSAGE:
                //  menu.add(0, DOWNLOAD_FILE, Menu.NONE, "Download audio file");
                break;
            case Message.VIDEO_MESSAGE:
                menu.add(0, DOWNLOAD_FILE, Menu.NONE, "Download video file");
                break;
            case Message.DRAWING_MESSAGE:
                menu.add(0, DOWNLOAD_FILE, Menu.NONE, "Download drawing");
                break;
        }
    }

    //Handle click event on the pop up menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case DOWNLOAD_IMAGE:
                downloadImage(info.id);
                return true;

            case DELETE_MESSAGE:
                deleteMessage(info.id);
                return true;

            case DOWNLOAD_FILE:
                downloadFile(info.id);
                return true;

            case COPY_TEXT:
                copyTextToClipboard(info.id);
                return true;

            case SHARE_TEXT:
                shareMedia(info.id, Message.TEXT_MESSAGE);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    //**************Download image and save it to Downloads*****************
    public void downloadImage(long id) {
        Message mes = listMessage.get((int) id);
        Bitmap bm = mes.byteArrayToBitmap(mes.getByteArray());
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

        FileUtilities.saveImageFromBitmap(this, bm, path, mes.getFileName());
        FileUtilities.refreshMediaLibrary(this);
    }

    //************Download file and save it to Downloads*************
    public void downloadFile(long id) {
        Message mes = listMessage.get((int) id);
        String sourcePath = mes.getFilePath();
        String destinationPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

        FileUtilities.copyFile(this, sourcePath, destinationPath, mes.getFileName());
        FileUtilities.refreshMediaLibrary(this);
    }

    //************************************************************************
    //Delete a message from the message list (doesn't delete on other phones)
    //************************************************************************
    public void deleteMessage(long id) {

        Message mes = listMessage.get((int) id);

        dataquery.deleteContent(threadids, mes.getmContentid(), mes.time_text);
        listMessage.remove((int) id);
        chatAdapter.notifyDataSetChanged();
        if (Utils.chat_section != 0) {
            new deleteschmsg(mes.getmContentid()).execute();
        }
    }


    //************************************************************************
    //Delete a temporary files
    //************************************************************************
    private void clearTmpFiles(File dir) {
        try {
            File[] childDirs = dir.listFiles();
            for (File child : childDirs) {
                if (child.isDirectory()) {
                    clearTmpFiles(child);
                } else {
                    child.delete();
                }
            }
            for (Uri uri : tmpFilesUri) {
                getContentResolver().delete(uri, null, null);
            }
            FileUtilities.refreshMediaLibrary(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public void talkTo(String destination) {
        edit.setText("@" + destination + " : ");
        edit.setSelection(edit.getText().length());
    }*/

    //**************************************************
    // copy text
    //**************************************************
    private void copyTextToClipboard(long id) {
        Message mes = listMessage.get((int) id);
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("message", mes.getmText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Message copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    //**************************************************
    // Share message
    //**************************************************
    private void shareMedia(long id, int type) {
        Message mes = listMessage.get((int) id);

        switch (type) {
            case Message.TEXT_MESSAGE:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mes.getmText());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
        }
    }


    //************Get formatted Time***************
    private String GetFormattedTime() {
        String time = "", compreheadertime;
        Calendar cal = Calendar.getInstance();
        // SimpleDateFormat format = new SimpleDateFormat("dd MMM, hh:mm a");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        time = format.format(cal.getTime());
        return time;
    }

    //********************************
// Time change Listener
//*********************************
    private TimePickerDialog.OnTimeSetListener timePickerListener1 =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {

                    if (selectedHour < curhour && valid) {
                        Snackbar.make(delayed, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else if (selectedMinute < curmin && valid) {
                        Snackbar.make(delayed, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else {
                        int set = selectedMinute;
                        if (selectedMinute <= 15) {
                            set = 0;
                        } else if (selectedMinute <= 30) {
                            set = 15;
                        } else if (selectedMinute <= 45) {
                            set = 30;
                        } else if (selectedMinute <= 59) {
                            set = 45;
                        }

                        hour = selectedHour;
                        minute = set;
                        c.set(Calendar.HOUR_OF_DAY, hour);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                        String dateSet = format1.format(c.getTime());
                        // builder.append(dateSet);

                    }

                    RecurringPicker(ChattingActivity.this);
                }

            };

    //********************************
// Date change Listener
//*********************************
    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (year < curyear) {
                valid = false;
                Snackbar.make(delayed, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                Snackbar.make(delayed, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                Snackbar.make(delayed, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                c.set(Calendar.YEAR, mYear);
                c.set(Calendar.MONTH, mMonth);
                c.set(Calendar.DAY_OF_MONTH, mDay);
                compareday = mDay;
                comparemonth = mMonth;
                if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }

                timePickerbelow(ChattingActivity.this);
                //   timePicker(ChattingActivity.this, timePickerListener1, curhour, curmin);


            }
        }
    };


    //*************RecurringPicker***********
    private void RecurringPicker(Context ctx) {

        // int selected_rec=0;
        new android.support.v7.app.AlertDialog.Builder(ctx, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, rec_id, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        rec_id = ((android.support.v7.app.AlertDialog) dialog).getListView().getCheckedItemPosition();
                        schtype = sentitems[rec_id];
                        dialog.dismiss();
                        sendMessage(typemessage, isDelayed);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

    private Uri SaveImage(final Bitmap finalBitmap) {
        final Uri[] uri = new Uri[1];
        String root = Environment.getExternalStorageDirectory().toString();

        File myDir = new File(root + "/VidCode/Captured Images/");
        if (!myDir.exists())
            myDir.mkdirs();

        String fname = "/image-" + System.currentTimeMillis() + ".png";
        File file = new File(myDir, fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            // localImagePath = myDir + fname;
            uri[0] = Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uri[0];
    }


    private Uri CameraImage() {
        final Uri[] uri = new Uri[1];
        String root = Environment.getExternalStorageDirectory().toString();

        File myDir = new File(root + "/VidCode/Captured Images/");
        if (!myDir.exists())
            myDir.mkdirs();

        String fname = "/image-" + System.currentTimeMillis() + ".png";
        File file = new File(myDir, fname);
        uri[0] = Uri.fromFile(file);
        return uri[0];
    }

    /***************************************
     * Time picker
     *************************************/
    public void timePicker(Context ctx, TimePickerDialog.OnTimeSetListener timePickerListener, int hour, int minute) {

        TimePickerDialog dialogg = new TimePickerDialog(ctx, R.style.AppTheme_Dialog, timePickerListener, hour, minute, false);
        dialogg.show();

    }

    /***************************************
     * Date picker
     *************************************/
    public void datePicker(DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(ChattingActivity.this, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }


    public void datePickerbuildbelow(DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(ChattingActivity.this, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }
                timePickerbelow(ChattingActivity.this);


            }
        });

        dialog.show();

    }


    public DatePickerDialog.OnDateSetListener datePickerListener2 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c.set(Calendar.YEAR, mYear);
            c.set(Calendar.MONTH, mMonth);
            c.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;


        }
    };

    public void timePickerbelow(Context ctx) {

        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        isFirst = true;
        if (minute >= 55 && minute <= 59) {
            minute = 0;
            hour = hour + 1;
        } else {
            minute = minute + 5;
        }

        final TimePickerDialogs timeee;

        timeee = new TimePickerDialogs(ctx, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {

                c.set(Calendar.HOUR_OF_DAY, selectedHour);
                if (isFirst) {
                    isFirst = false;

                    if (Build.VERSION.SDK_INT < 21) {
                        c.set(Calendar.MINUTE, min);
                    } else {
                        c.set(Calendar.MINUTE, min);
                    }
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                    String displayValue = timeFormatter.format(c.getTime());


                    RecurringPicker(ChattingActivity.this);

                    // int selectedMinute = min / divider;
               /* Toast.makeText(getApplicationContext(), String.valueOf(selectedHour)+":"+String.valueOf(min), Toast.LENGTH_SHORT).show();
                int selectedMinute = min;
                if (selectedHour < curhour) {
                    Toast.makeText(getApplicationContext(), "invalid hour", Toast.LENGTH_SHORT).show();
                } else if (selectedMinute < curmin && selectedHour == curhour) {
                    Toast.makeText(getApplicationContext(), "invalid minute", Toast.LENGTH_SHORT).show();

                } else {


                    return;

                }*/

                }
            }
        }, hour, minute, hourstype);
        timeee.setMin(hour, minute);
        timeee.show();




      /*  new TimePickerDialogs(ctx, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                System.out.println("view11==" + view);

                // int selectedMinute = min / divider;
                int selectedMinute = min;
                if (selectedHour < curhour && compareday <= curday && comparemonth <= curmont) {
                    Snackbar.make(delayed, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                } else if (selectedMinute < curmin && compareday <= curday && selectedHour == curhour && comparemonth <= curmont) {
                    Snackbar.make(delayed, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                } else {

                    c.set(Calendar.HOUR_OF_DAY, selectedHour);
                    c.set(Calendar.MINUTE, selectedMinute);
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                    String displayValue = timeFormatter.format(c.getTime());
                    // builder.append(", " + displayValue).toString();
                    RecurringPicker(ChattingActivity.this);
                    return;

                }

            }
        }, curhour, curmin, hourstype).show();*/


    }


   /* public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;
        int hr, min;
        Calendar ct;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, (minute + 5) / TIME_PICKER_INTERVAL, is24HourView);
            //   super(arg0, dialogTheme1, callBack, hourOfDay, minute/TIME_PICKER_INTERVAL, is24HourView);
            this.hr = hourOfDay;
            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            ct = Calendar.getInstance();
            //   int tym = (curmin - (curmin / TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            if (setr) {
                ct.set(Calendar.HOUR_OF_DAY, hourOfDay);
                ct.set(Calendar.MINUTE, 0);
            }
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);
            String time = String.valueOf(hourOfDay) + "::" + String.valueOf(minute);
            System.out.println("tym=" + time);
            minute = minute * 5;
            System.out.println("cal111=" + minute);
            System.out.println("cal222=" + curmin);
            if ((hourOfDay < curhour && compareday <= curday && comparemonth <= curmont)) {
                //    updateTime(curhour, curmin);
//                timePicker.setCurrentHour(ct.get(Calendar.HOUR_OF_DAY));
//                timePicker.setCurrentMinute(ct.get(Calendar.MINUTE));
//                System.out.println("cal111="+ct.get(Calendar.HOUR_OF_DAY)+":"+ct.get(Calendar.MINUTE));
//                System.out.println("cal222=" + timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute());

                timePicker.setCurrentHour(ct.get(Calendar.HOUR_OF_DAY));


            } else if (minute <= curmin && compareday <= curday && hourOfDay == curhour && comparemonth <= curmont) {
                int i = ct.get(Calendar.MINUTE);
                i = ((i - (i % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL) / 5;
                if (setr) {
                    i = i - 1;
                }
                timePicker.setCurrentMinute(i);
            } else {
                timePicker.setCurrentMinute(timePicker.getCurrentMinute());
            }

          *//*  try {
                if (Build.VERSION.SDK_INT > 21) {
                    if (hourOfDay < curhour || (hourOfDay == curhour
                            && minute < curmin)) {
                        updateTime(curhour, curmin);
                    }
                } else {
                    if ((hourOfDay < curhour && compareday <= curday && comparemonth <= curmont) || (minute < curmin && compareday <= curday && hourOfDay == curhour && comparemonth <= curmont)) {
                        updateTime(curhour, curmin);
                        //   updateTime(curhour, (curmin - (curmin % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }*//*

        }

        @Override
        public void updateTime(int hourOfDay, int minuteOfHour) {
            super.updateTime(hourOfDay, minuteOfHour);

        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/

    public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        private int minHour = -1;
        private int minMinute = -1;

        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private java.text.DateFormat dateFormat;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
//            this.hr = hourOfDay;
//            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            //    ct = Calendar.getInstance();
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT);

        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);
            System.out.println("minHour=" + minHour);
            System.out.println("minMinute=" + minMinute);

            System.out.println("currentHour=" + currentHour);
            System.out.println("currentMinute=" + currentMinute);

            System.out.println("hourOfDay=" + hourOfDay);
            System.out.println("minute=" + minute);

            boolean validTime = true;
            if (hourOfDay < minHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == minHour && (minute - 1) * 5 < minMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;
                System.out.println("1111");
            }

            if (hourOfDay > maxHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == maxHour && minute * 5 > maxMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;
                System.out.println("2222");
            }
            // if (hourOfDay==minHour&&)

            if (validTime) {
                currentHour = hourOfDay;
                System.out.println("3333");
                currentMinute = minute;
            } else {
                int j = minMinute / 5;
                int r = minMinute % 5;
                if (r > 3) {
                    j = j + 1;
                }
                if (j < minute) {
                    currentMinute = minute;
                    System.out.println("4444");
                } else {
                    currentMinute = j;
                    System.out.println("5555");
                }

            }
            //   setMin(currentHour, currentMinute);
            //   updateTime(currentHour, currentMinute);
            System.out.println("sethour=" + currentHour);
            System.out.println("setminute=" + currentMinute);


            timePicker.setCurrentHour(currentHour);
            int i = minute;
            i = ((i - (i % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL) / 5;

            timePicker.setCurrentMinute(currentMinute);


          /*  else if (minute == 11&&hourOfDay==minHour) {
                currentMinute = 0;
                currentHour = currentHour + 1;
                minHour = currentHour;
                minMinute = currentMinute;
            }*/

          /*  if ((hourOfDay < curhour )) {


                timePicker.setCurrentHour(ct.get(Calendar.HOUR_OF_DAY));


            } else if (minute <= curmin && hourOfDay == curhour ) {
                int i = ct.get(Calendar.MINUTE);
                i = ((i - (i % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL) / 5;

                timePicker.setCurrentMinute(i);
            } else {
                timePicker.setCurrentMinute(timePicker.getCurrentMinute());
            }*/


        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            //  System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
                timePicker.setOnTimeChangedListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    /***************************************
     * Hide keyboard
     *************************************/
    private void HideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(ChattingActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(ChattingActivity.this, android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(ChattingActivity.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            //  showPopup(toolbar);
            selectPicker();
        }
    }

    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermissionAudio() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(ChattingActivity.this, Manifest.permission.RECORD_AUDIO);

        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.RECORD_AUDIO);
        }


        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(ChattingActivity.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            if (isVisible) {
                isVisible = false;
                findViewById(R.id.record_view).setVisibility(View.GONE);
            } else {
                isVisible = true;
                findViewById(R.id.record_view).setVisibility(View.VISIBLE);
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        // showPopup(toolbar);
                        selectPicker();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(listView, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;

            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        if (isVisible) {
                            isVisible = false;
                            findViewById(R.id.record_view).setVisibility(View.GONE);
                        } else {
                            isVisible = true;
                            findViewById(R.id.record_view).setVisibility(View.VISIBLE);
                        }

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(listView, "You are not allowed to Record audio", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    //-------------------image picker-----------------------//
    public Bitmap decodeFile(String path) {

        // you can provide file path here
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;

            ExifInterface exif = new ExifInterface(path);

            orientation = exif
                    .getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Log.e("ExifInteface .........", "rotation =" + orientation);

            // exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);

            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                // m.postScale((float) bm.getWidth(), (float) bm.getHeight());
                // if(m.preRotate(90)){
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }


    }


    /*************************************************
     * Solve image rotation method
     ***********************************************/
    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }


    /***************************************
     * Rotate Bitmap
     *************************************/
    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Bitmap bit = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        if (source.getWidth() > 1000) {
            //  Bitmap b = CompressBitmap(source);
            Bitmap b = scaleImageKeepAspectRatio(source);
            bit = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        } else {
            bit = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        }

        return bit;
    }

    /***************************************
     * Scale Image by keeping aspect ratio
     *************************************/
    public static Bitmap scaleImageKeepAspectRatio(Bitmap scaledGalleryBitmap) {
        int imageWidth = scaledGalleryBitmap.getWidth();
        int imageHeight = scaledGalleryBitmap.getHeight();
        int newHeight = (imageHeight * 1000) / imageWidth;
        Bitmap bb = Bitmap.createScaledBitmap(scaledGalleryBitmap, 1000, newHeight, false);

        return bb;
    }


    /***************************************************
     * Dialog to make message delayed or send now
     *************************************************/
    private void simpledialog(final int typemsg, String msg, String title) {

        typemessage = typemsg;
        new android.support.v7.app.AlertDialog.Builder(ChattingActivity.this, R.style.AppTheme_Dialog)
                .setTitle(title)
                .setMessage(msg)

                .setPositiveButton("Delayd", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isDelayed = true;
                        // builder = new StringBuilder();
                        if (Build.VERSION.SDK_INT < 21) {

                            datePickerbuildbelow(datePickerListener2, curyear, curmont, curday);
                        } else {

                            datePicker(datePickerListener1, curyear, curmont, curday);
                        }
                    }
                })
                .setNegativeButton("Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isDelayed = false;
                        sendMessage(typemessage, isDelayed);
                    }
                })

                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    class userupdate extends AsyncTask<String, Void, String> {

        String ids, response;

        public userupdate(String no) {
            this.ids = no;
        }

        @Override
        protected String doInBackground(String... params) {
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "get_delayd_user_info")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("delayed_user_id", ids)
                    .appendQueryParameter("last_updated_date", "");
            response = parser.getJSONFromUrl(Utils.base_url, builder);

            try {

                JSONObject obj = new JSONObject(response);
                if (obj.optString("status").equals("true")) {
                    JSONObject jobj = obj.getJSONObject("data");
                    ModelDelayedContact model = new ModelDelayedContact();
                    model.delayd_user_id = jobj.optString("delayd_user_id");
                    model.last_updated_date = jobj.optString("last_updated_date");
                    model.profile_pic = jobj.optString("profile_pic");
                    dataquery.delayedupdate(model);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    class updateinfo extends AsyncTask<String, Void, String> {
        String threadid, response;

        public updateinfo(String threadids) {
            this.threadid = threadids;
        }

        @Override
        protected String doInBackground(String... params) {
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "profile_detail")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("delayd_id", threadid)
                    .appendQueryParameter("type", "group");
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            System.out.println("response=" + response);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    JSONObject dobj = obj.optJSONObject("data");
                    ModelMsgThread item = new ModelMsgThread();
                    item.thread_id = dobj.optString("thread_id");
                    item.participant_ids = dobj.optString("participant_ids");
                    item.participant_numbers = dobj.optString("participant_nos");
                    // String name = URLDecoder.decode(dobj.optString("group_name"), "UTF-8");
                    item.group_name = dobj.optString("group_name");
                    item.group_icon = dobj.optString("group_icon");
                    dataquery.updategrpmsgthread(item);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }


    }

    //------------------delete schduled messages=-----------------------//
    class deleteschmsg extends AsyncTask<String, Void, String> {
        String contentid, response;

        public deleteschmsg(String s) {
            this.contentid = s;
        }

        @Override
        protected String doInBackground(String... params) {
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "delete_content_schedule")
                    .appendQueryParameter("auth_code", auth_code)
                    .appendQueryParameter("content_id", contentid);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            System.out.println("sch delet=" + response);
            return null;
        }
    }

    /*************************************
     * Update contacts
     ***********************************/
    class contactsync extends AsyncTask<String, Void, String> {
        ArrayList<ModelContacts> arr_cont;

        public contactsync(ArrayList<ModelContacts> mContacts) {
            this.arr_cont = mContacts;
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if (arr_cont.size() > 0) {
                    String json = new Gson().toJson(arr_cont);
                    //
                    String auth_code = prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String status = object.getString("status");
                    if (status.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject obj = jsonArray.getJSONObject(i);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = obj.getString("delayd_user_id");
                            model.name = obj.getString("name");
                            model.profile_pic = obj.getString("profile_pic");
                            model.last_updated_date = obj.getString("last_updated_date");
                            model.contact_no = obj.getString("no");
                            model.country_code = obj.getString("country_code");
                            model.delayed_user = "1";

                            //delayedContacts.add(model);
                            dataquery.UpdateContacts2(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    private String ResponseMethod(String auth_code, String data) {
        String res = "";

        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }

    /**********************************************
     * Delete thread dialog
     ********************************************/
    public void dialog(String title, String msg, int icon, final int pos) {
        new AlertDialog.Builder(ChattingActivity.this, R.style.AppTheme_Dialog)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteMessage(pos);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();

    }
}