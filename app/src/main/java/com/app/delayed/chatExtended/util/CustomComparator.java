package com.app.delayed.chatExtended.util;

import com.app.delayed.chatExtended.Entities.ModelMsgThread;

import java.util.Comparator;

public class CustomComparator implements Comparator<ModelMsgThread> {
    @Override
    public int compare(ModelMsgThread o1, ModelMsgThread o2) {
        return o1.last_updated.compareTo(o2.last_updated);
    }
}