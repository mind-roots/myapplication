package com.app.delayed.chatExtended.CustomAdapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.provider.MediaStore.Images.Thumbnails;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.app.delayed.R;
import com.app.delayed.chatExtended.ChattingActivity;
import com.app.delayed.chatExtended.Entities.Message;
import com.app.delayed.chatExtended.PlayVideoActivity;
import com.app.delayed.chatExtended.ViewImageActivity;
import com.app.delayed.chatExtended.util.FileUtilities;
import com.app.delayed.chatExtended.util.ImageCaching;
import com.app.delayed.chatExtended.util.ImageHelper;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.utils.CircularImageView;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ChatAdapter extends BaseAdapter {
    private List<Message> listMessage;
    private LayoutInflater inflater;
    public static Bitmap bitmap;
    Context mContext;
    private HashMap<String, Bitmap> mapThumb;
    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private Timer timer;
    public static boolean isPlaying = false;
    CountDownTimer count_timer;
    DatabaseQueries query;
    String timeformat, auth_code, prev;
    SharedPreferences prefs;
    public static int counter = 0, setpos;
    ArrayList<String> datearr = new ArrayList<>();
    ArrayList<Integer> posarr = new ArrayList<>();
    public static MediaPlayer mPlayer, mPlayer1,play;

    public ChatAdapter(Context context, List<Message> listMessage) {
        this.listMessage = listMessage;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        mapThumb = new HashMap<String, Bitmap>();
        query = new DatabaseQueries(context);
        prefs = context.getSharedPreferences("delayed", context.MODE_PRIVATE);
        timeformat = prefs.getString("timeformat", null);
    }

    @Override
    public int getCount() {
        return listMessage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        CacheView cache;
        CacheView2 cache2;
        Message mes = listMessage.get(position);
        int type = mes.getmType();

        if ((Boolean) mes.isMine()) {

            if (view == null) {
                view = inflater.inflate(R.layout.chatto, null);
                cache = new CacheView();
                cache.chatName = (TextView) view.findViewById(R.id.chatName);
                cache.text = (TextView) view.findViewById(R.id.text);
                cache.time = (TextView) view.findViewById(R.id.time);
                cache.time_text = (TextView) view.findViewById(R.id.time_text);
                cache.delayed_time = (TextView) view.findViewById(R.id.delayed_time);
                cache.lay_time = (LinearLayout) view.findViewById(R.id.lay_time);
                cache.image = (ImageView) view.findViewById(R.id.image);
                cache.progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
                cache.relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
                cache.audioPlayer = (LinearLayout) view.findViewById(R.id.playAudio);
                cache.play_pause = (ImageView) view.findViewById(R.id.play_pause);
                cache.main = (RelativeLayout) view.findViewById(R.id.main);
                cache.videoPlayer = (ImageView) view.findViewById(R.id.playVideo);
                cache.pic2 = (ImageView) view.findViewById(R.id.pic2);
                cache.fileSaved = (TextView) view.findViewById(R.id.fileSaved);
                cache.videoPlayerButton = (ImageView) view.findViewById(R.id.buttonPlayVideo);
                cache.fileSavedIcon = (ImageView) view.findViewById(R.id.file_attached_icon);
                cache.userAvatarImageView = (CircularImageView) view.findViewById(R.id.userAvatarImageView);
                view.setTag(cache);
            } else {
                //Retrive the items from cache
                view = convertView;
                cache = (CacheView) view.getTag();
            }
            MethodCallsCache(cache, position, view, type, mes);

        } else {

            if (view == null) {
                view = inflater.inflate(R.layout.chat_row, null);
                cache2 = new CacheView2();
                cache2.chatName = (TextView) view.findViewById(R.id.chatName);
                cache2.text = (TextView) view.findViewById(R.id.text);
                cache2.time = (TextView) view.findViewById(R.id.time);
                cache2.time_text = (TextView) view.findViewById(R.id.time_text);
                cache2.image = (ImageView) view.findViewById(R.id.image);
                cache2.progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
                cache2.relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
                cache2.lay_content = (RelativeLayout) view.findViewById(R.id.lay_content);
                cache2.audioPlayer = (LinearLayout) view.findViewById(R.id.playAudio);
                cache2.delayed_time = (TextView) view.findViewById(R.id.delayed_time);
                cache2.left_msg = (TextView) view.findViewById(R.id.left_msg);
                cache2.lay_time = (LinearLayout) view.findViewById(R.id.lay_time);
                cache2.left_layout = (LinearLayout) view.findViewById(R.id.left_layout);
                cache2.main = (RelativeLayout) view.findViewById(R.id.main);
                cache2.play_pause = (ImageView) view.findViewById(R.id.play_pause);
                cache2.videoPlayer = (ImageView) view.findViewById(R.id.playVideo);
                cache2.pic2 = (ImageView) view.findViewById(R.id.pic2);
                cache2.fileSaved = (TextView) view.findViewById(R.id.fileSaved);
                cache2.videoPlayerButton = (ImageView) view.findViewById(R.id.buttonPlayVideo);
                cache2.fileSavedIcon = (ImageView) view.findViewById(R.id.file_attached_icon);
                cache2.userAvatarImageView = (CircularImageView) view.findViewById(R.id.userAvatarImageView);
                view.setTag(cache2);
            } else {

                //Retrive the items from cache
                view = convertView;
                cache2 = (CacheView2) view.getTag();
            }

            MethodCallsCache2(cache2, position, view, type, mes);
        }


        return view;
    }

    private void disableAllMediaViews(CacheView cache) {
        cache.text.setVisibility(View.GONE);
        cache.image.setVisibility(View.GONE);
        cache.progressbar.setVisibility(View.GONE);
        cache.audioPlayer.setVisibility(View.GONE);
        cache.videoPlayer.setVisibility(View.GONE);
        cache.fileSaved.setVisibility(View.GONE);
        cache.videoPlayerButton.setVisibility(View.GONE);
        cache.fileSavedIcon.setVisibility(View.GONE);
    }

    private void enableTextView(CacheView cache, String text) {
        if (!text.equals("") && text != null && !text.equals(null)) {
            cache.text.setVisibility(View.VISIBLE);
            cache.text.setText(text);
            Linkify.addLinks(cache.text, Linkify.PHONE_NUMBERS);
            Linkify.addLinks(cache.text, Patterns.WEB_URL, "myweburl:");
        }
    }

    private void disableAllMediaViews2(CacheView2 cache) {
        cache.text.setVisibility(View.GONE);
        cache.image.setVisibility(View.GONE);
        cache.progressbar.setVisibility(View.GONE);
        cache.audioPlayer.setVisibility(View.GONE);
        cache.videoPlayer.setVisibility(View.GONE);
        cache.fileSaved.setVisibility(View.GONE);
        cache.videoPlayerButton.setVisibility(View.GONE);
        cache.fileSavedIcon.setVisibility(View.GONE);
    }

    private void enableTextView2(CacheView2 cache, String text) {
        if (!text.equals("")) {
            cache.text.setVisibility(View.VISIBLE);
            cache.text.setText(text);
            Linkify.addLinks(cache.text, Linkify.PHONE_NUMBERS);
            Linkify.addLinks(cache.text, Patterns.WEB_URL, "myweburl:");
        }
    }


    private void MethodCallsCache(final CacheView cache, final int position, View view, int type, final Message mes) {


        if (mes.headerdate.equals("0")) {
            cache.lay_time.setVisibility(View.INVISIBLE);
        } else {
            cache.lay_time.setVisibility(View.VISIBLE);
            if (Utils.coarr.contains(mes.getmContentid())) {
                String tt = Methods.tochatnormaltime(mes.time_text, timeformat, 0);
                cache.time_text.setText(tt);
            } else {
                if (mes.displaydate) {
                    String tt = Methods.tochatnormaltime(mes.time_text, timeformat, 0);
                    cache.time_text.setText(tt);
                } else {
                    if (mes.displaydate) {
                        String tt = Methods.tochatnormaltime(mes.time_text, timeformat, 0);
                        cache.time_text.setText(tt);
                    } else {
                        cache.lay_time.setVisibility(View.INVISIBLE);
                    }
                }

            }


        }

        String setmessage = "";
        if (mes.schtype.equals("0")) {
            setmessage = Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("d")) {
            setmessage = "Recurring daily at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("wd")) {
            setmessage = "Recurring weekdays at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("w")) {
            setmessage = "Recurring weekly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("fn")) {
            setmessage = "Recurring forthnightly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("m")) {
            setmessage = "Recurring monthly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("y")) {
            setmessage = "Recurring yearly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else {
            setmessage = Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        }
        if (Utils.chat_section == 0) {
            cache.delayed_time.setText(Methods.tochatnormaltime(mes.time_text, timeformat, 1));
        } else {
            cache.delayed_time.setText(setmessage);
        }
        //  cache.chatName.setText(listMessage.get(position).getChatName());
        cache.chatName.setTag(cache);
        cache.chatName.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                CacheView cache = (CacheView) v.getTag();
                // ((ChattingActivity) mContext).talkTo((String) cache.chatName.getText());
                return true;
            }
        });

        //Colourise differently own message
        if ((Boolean) listMessage.get(position).isMine()) {

            cache.relativeLayout.setBackground(view.getResources().getDrawable(R.drawable.rectangle_102));

        } else {
            cache.relativeLayout.setBackground(view.getResources().getDrawable(R.drawable.rectangle_1102));

        }

        //We disable all the views and enable certain views depending on the message's type
        disableAllMediaViews(cache);
//        try {
//            Picasso.with(mContext)
//                    .load(mes.userpic)
//                    .placeholder(R.mipmap.default_user)
//                    .error(R.mipmap.default_user)
//                    .into(cache.userAvatarImageView);
//        } catch (Exception e) {
//
//        }

        try {

            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(prefs.getString("name", null).charAt(0)),mContext.getResources().getColor(R.color.colorPrimary));

            if (prefs.getString("chat_icon", null).length()>5) {
                cache.pic2.setImageDrawable(drawable);
                Picasso.with(mContext)
                        .load(mes.userpic)
//                        .placeholder(R.mipmap.default_user)
//                        .error(R.mipmap.default_user)
                        .into(cache.userAvatarImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                cache.pic2.setVisibility(View.INVISIBLE);
                                cache.userAvatarImageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                cache.pic2.setVisibility(View.VISIBLE);
                                cache.userAvatarImageView.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                cache.pic2.setVisibility(View.VISIBLE);
                cache.userAvatarImageView.setVisibility(View.INVISIBLE);
                cache.pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {
            cache.pic2.setVisibility(View.INVISIBLE);
            cache.userAvatarImageView.setVisibility(View.VISIBLE);
            cache.userAvatarImageView.setImageResource(R.mipmap.default_user);
        }

//        Glide.with(mContext)
//                .load(mes.userpic)
////                .placeholder(R.mipmap.avatar)
//                .crossFade()
//                .error(R.mipmap.default_user)
//                .into(cache.userAvatarImageView);
        /***********************************************
         Text Message
         ***********************************************/
        if (type == Message.TEXT_MESSAGE) {
            enableTextView(cache, mes.getmText());
        }

        /***********************************************
         Image Message
         ***********************************************/
        else if (type == Message.PHOTO_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFileName())) {
                Bitmap thumb = mes.byteArrayToBitmap(mes.getByteArray());
                mapThumb.put(mes.getFileName(), thumb);
            }
            cache.image.setImageBitmap(ImageHelper.getRoundedCornerBitmap(mapThumb.get(mes.getFileName()), 4));
            cache.image.setTag(position);

            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    bitmap = mes.byteArrayToBitmap(mes.getByteArray());

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    String fileName = mes.getFileName();
                    intent.putExtra("fileName", fileName);
                    intent.putExtra("userName", listMessage.get(position).username);
                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         Photo Message
         ***********************************************/
        else if (type == Message.IMAGE_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);


            if (URLUtil.isValidUrl(mes.imgurl)) {


                if (mes.originalurl.contains("http")) {
                    cache.image.setClickable(false);
                    cache.progressbar.setVisibility(View.VISIBLE);
                    new CachingImages(mes.originalurl, Integer.parseInt(mes.getThread_id()), cache.image, mes.getmContentid(), cache.progressbar, mes).execute();

//                    Glide.with(mContext)
//                            .load(mes.imgurl)
//                            .override(300, 300)
//                            .centerCrop()
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    cache.progressbar.setVisibility(View.GONE);
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    cache.progressbar.setVisibility(View.GONE);
//
//                                    new CachingImages(mes.originalurl, Integer.parseInt(mes.getThread_id()), cache.image, mes.getmContentid(), cache.progressbar, mes).execute();
//                                    return false;
//                                }
//                            })
//                            .crossFade()
//                            .into(cache.image);
                } else {
                    cache.progressbar.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(mes.originalurl)

                            .override(300, 300)
                            .centerCrop()
                            .into(cache.image);
                }
            } else {
                try {
                    Bitmap thumb = mes.byteArrayToBitmap(mes.getByteArray());
                    mapThumb.put(mes.getFileName(), thumb);                // }
                    cache.image.setImageBitmap(ImageHelper.getRoundedCornerBitmap(thumb, 4));
                    cache.image.setTag(position);
                } catch (Exception e) {
                }

            }
            //if (!mapThumb.containsKey(mes.getFileName())) {


            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    String fileName = "";
                    Message mes = listMessage.get(position);
                    if (URLUtil.isValidUrl(mes.imgurl)) {
                        fileName = mes.originalurl;
                    } else {
                        //  Message mes = listMessage.get((Integer) v.getTag());
                        bitmap = mes.byteArrayToBitmap(mes.getByteArray());
                        fileName = mes.getFileName();
                    }

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    intent.putExtra("fileName", fileName);
                    intent.putExtra("userName", listMessage.get(position).username);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         Audio Message
         ***********************************************/
        else if (type == Message.AUDIO_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.audioPlayer.setVisibility(View.VISIBLE);
            cache.time.setText(mes.getAudioLength());
            cache.play_pause.setTag(position);
            // cache.audioPlayer.setTag(position);
            //  cache.time.setText("00:00");

/*

              play = new MediaPlayer();
            play.setAudioStreamType(AudioManager.STREAM_MUSIC);

                try {
                    if (URLUtil.isValidUrl(listMessage.get(position).originalurl)) {


                        play.setDataSource(listMessage.get(position).originalurl);
                        play.prepareAsync();
                    } else {
                        final Message mess = listMessage.get(position);
                        File file = new File(mes.getFilePath());
                        Uri uri = Uri.fromFile(file);
                        play.setDataSource(mContext, uri);
                        play.prepare();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

            }
            cache.play_pause.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (play.isPlaying()) {
                        cache.time.setText(mes.getAudioLength());
                        play.pause();
                        cache.play_pause.setImageResource(android.R.drawable.ic_media_play);
                        if (count_timer != null) {
                            count_timer.cancel();
                        }
                    } else {
                        play.start();
                        cache.play_pause.setImageResource(android.R.drawable.ic_media_pause);
                        startTime = SystemClock.uptimeMillis();
                        timer = new Timer();


                        final long duration = play.getDuration();


                        int minutes = (int) (duration / (60 * 1000));
                        int seconds = (int) ((duration / 1000) % 60);
                        final String str = String.format("%02d:%02d", minutes, seconds);


                        //cache.time.setText(str);

                        play.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                            @Override
                            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                DownTimer(cache.time, duration, str);
                            }
                        });
                        play.setOnCompletionListener(new OnCompletionListener() {

                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                //Re-enable the button when the audio has finished playing
                                // v.setEnabled(true);
                                cache.play_pause.setImageResource(android.R.drawable.ic_media_play);
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }

                            }
                        });
                    }
                }
            });
*/
            cache.play_pause.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    //  notifyDataSetChanged();
                    ChattingActivity.position = position;
                    if (URLUtil.isValidUrl(listMessage.get(position).originalurl)) {
                        if (!isPlaying) {
                            mPlayer = new MediaPlayer();
                            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                            try {
                                mPlayer.setDataSource(listMessage.get(position).originalurl);
                                mPlayer.prepare();
                            } catch (IllegalArgumentException e) {
                                //   Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (SecurityException e) {
                                //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (IllegalStateException e) {
                                //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        try {
                            if (isPlaying) {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_b));
                                isPlaying = false;
                                if (mPlayer != null) {
                                    mPlayer.stop();

                                }
                                mPlayer.release();
                                mPlayer = null;
                                notifyDataSetChanged();
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }

                            } else {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.stop_w));

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();


                                final long duration = mPlayer.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer.start();
                                isPlaying = true;
                                mPlayer.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_w));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (Exception e) {
                            e.printStackTrace();
                            notifyDataSetChanged();
                        }
                    } else {
                        mPlayer = new MediaPlayer();
                        final Message mes = listMessage.get((Integer) v.getTag());
                        try {
                            if (isPlaying) {
                                isPlaying = false;
                                if (mPlayer != null) {
                                    mPlayer.stop();

                                }
                                mPlayer.release();
                                notifyDataSetChanged();
                                mPlayer = null;
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_w));
                            } else {

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();

                                File file = new File(mes.getFilePath());
                                Uri uri = Uri.fromFile(file);
                                mPlayer.setDataSource(mContext, uri);
                                mPlayer.prepare();
                                final long duration = mPlayer.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer.start();
                                isPlaying = true;
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.stop_w));
                                mPlayer.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_w));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });


          /*  cache.play_pause.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    //  notifyDataSetChanged();
                    if (URLUtil.isValidUrl(listMessage.get(position).originalurl)) {
                        mPlayer1 = new MediaPlayer();
                        mPlayer1.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        try {
                            mPlayer1.setDataSource(listMessage.get(position).originalurl);
                            mPlayer1.prepare();
                        } catch (IllegalArgumentException e) {
                            //   Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                        } catch (SecurityException e) {
                            //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                        } catch (IllegalStateException e) {
                            //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mPlayer1.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                            @Override
                            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                System.out.println("buffer22");
                            }
                        });
                        mPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                System.out.println("buffer22stop");
                            }
                        });
                        try {
                            if (isPlaying) {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));

                                if (mPlayer1 != null) {
                                    mPlayer1.stop();

                                }
                                mPlayer1.release();
                                mPlayer1 = null;
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }
                            } else {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();


                                final long duration = mPlayer1.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer1.start();
                                isPlaying = true;
                                mPlayer1.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mPlayer1 = new MediaPlayer();
                        final Message mes = listMessage.get((Integer) v.getTag());
                        try {
                            if (isPlaying) {

                                if (mPlayer1 != null) {
                                    mPlayer1.stop();

                                }
                                mPlayer1.release();
                                mPlayer1 = null;
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));
                            } else {

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();

                                File file = new File(mes.getFilePath());
                                Uri uri = Uri.fromFile(file);
                                mPlayer1.setDataSource(mContext, uri);
                                mPlayer1.prepare();
                                final long duration = mPlayer1.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer1.start();
                                isPlaying = true;
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));
                                mPlayer1.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_play));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });*/
        }

        /***********************************************
         Video Message
         ***********************************************/
        else if (type == Message.VIDEO_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.videoPlayer.setVisibility(View.VISIBLE);
            cache.videoPlayerButton.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFilePath())) {
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mes.getFilePath(), Thumbnails.MINI_KIND);
                mapThumb.put(mes.getFilePath(), thumb);
            }
            cache.videoPlayer.setImageBitmap(mapThumb.get(mes.getFilePath()));

            cache.videoPlayerButton.setTag(position);
            cache.videoPlayerButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    Intent intent = new Intent(mContext, PlayVideoActivity.class);
                    intent.putExtra("filePath", mes.getFilePath());
                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         File Message
         ***********************************************/
        else if (type == Message.FILE_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.fileSavedIcon.setVisibility(View.VISIBLE);
            cache.fileSaved.setVisibility(View.VISIBLE);
            cache.fileSaved.setText(mes.getFileName());
        }

        /***********************************************
         Drawing Message
         ***********************************************/
        else if (type == Message.DRAWING_MESSAGE) {
            enableTextView(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFileName())) {
                Bitmap thumb = FileUtilities.getBitmapFromFile(mes.getFilePath());
                mapThumb.put(mes.getFileName(), thumb);
            }
            cache.image.setImageBitmap(mapThumb.get(mes.getFileName()));
            cache.image.setTag(position);

            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    bitmap = mes.byteArrayToBitmap(mes.getByteArray());

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    String fileName = mes.getFileName();
                    intent.putExtra("fileName", fileName);

                    mContext.startActivity(intent);
                }
            });
        }
    }

    private void MethodCallsCache2(final CacheView2 cache, final int position, View view, int type, final Message mes) {
        //  cache.chatName.setText(listMessage.get(position).getChatName());
        cache.chatName.setTag(cache);
        cache.chatName.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                CacheView cache = (CacheView) v.getTag();
                //  ((ChattingActivity) mContext).talkTo((String) cache.chatName.getText());
                return true;
            }
        });
        String timeget = Methods.tochatnormaltime(mes.time_text, timeformat, 0);
        // cache.time_text.setText(Methods.tochatnormaltime(mes.time_text, timeformat, 0));

        if (mes.headerdate.equals("0")) {
            cache.lay_time.setVisibility(View.INVISIBLE);
        } else {

            if (Utils.coarr.contains(mes.getmContentid())) {
                cache.lay_time.setVisibility(View.VISIBLE);
                String tt = Methods.tochatnormaltime(mes.time_text, timeformat, 0);
                cache.time_text.setText(tt);
            } else {

                cache.lay_time.setVisibility(View.INVISIBLE);


            }


        }


        String setmessage = "";
        if (mes.schtype.equals("0")) {
            setmessage = "Non recurring at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("d")) {
            setmessage = "Recurring daily at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("wd")) {
            setmessage = "Recurring weekdays at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("w")) {
            setmessage = "Recurring weekly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("fn")) {
            setmessage = "Recurring forthnightly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("m")) {
            setmessage = "Recurring monthly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else if (mes.schtype.equals("y")) {
            setmessage = "Recurring yearly at " + Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        } else {
            setmessage = Methods.tochatnormaltime(mes.time_text, timeformat, 1);
        }
        if (Utils.chat_section == 0) {
            cache.delayed_time.setText(Methods.tochatnormaltime(mes.time_text, timeformat, 1));
        } else {
            cache.delayed_time.setText(setmessage);
        }

        //Colourise differently own message
        if ((Boolean) listMessage.get(position).isMine()) {

            cache.relativeLayout.setBackground(view.getResources().getDrawable(R.drawable.rectangle_102));

        } else {
            //   chatto_bg_gy
            cache.relativeLayout.setBackground(view.getResources().getDrawable(R.drawable.rectangle_1102));

        }

        //We disable all the views and enable certain views depending on the message's type
        disableAllMediaViews2(cache);
//        try {
//            Picasso.with(mContext)
//                    .load(mes.userpic)
//                    .placeholder(R.mipmap.default_user)
//                    .error(R.mipmap.default_user)
//                    .into(cache.userAvatarImageView);
//
//        } catch (Exception e) {
//
//        }
        try {

            String name = mes.username;
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(mes.username.charAt(0)),mContext.getResources().getColor(R.color.colorPrimary));

            if (mes.userpic.length()>5) {
                cache.pic2.setImageDrawable(drawable);
                Picasso.with(mContext)
                        .load(mes.userpic)
//                        .placeholder(R.mipmap.default_user)
//                        .error(R.mipmap.default_user)
                        .into(cache.userAvatarImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                cache.pic2.setVisibility(View.INVISIBLE);
                                cache.userAvatarImageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                cache.pic2.setVisibility(View.VISIBLE);
                                cache.userAvatarImageView.setVisibility(View.INVISIBLE);
                            }
                        });


            }else {
                cache.pic2.setVisibility(View.VISIBLE);
                cache.userAvatarImageView.setVisibility(View.INVISIBLE);
                cache.pic2.setImageDrawable(drawable);
            }

        } catch (Exception e) {

            e.printStackTrace();
//            TextDrawable drawable = TextDrawable.builder()
//                    .buildRound("+",mContext.getResources().getColor(R.color.colorPrimary));

            cache.pic2.setVisibility(View.INVISIBLE);
            cache.userAvatarImageView.setVisibility(View.VISIBLE);
            cache.userAvatarImageView.setImageResource(R.mipmap.default_user);
           // cache.pic2.setImageDrawable(drawable);
        }

//        Glide.with(mContext)
//                .load(mes.userpic)
////                .placeholder(R.mipmap.avatar)
//                .crossFade()
//                .error(R.mipmap.default_user)
//                .into(cache.userAvatarImageView);
        /***********************************************
         Text Message
         ***********************************************/
        if (type == Message.TEXT_MESSAGE) {
            enableTextView2(cache, mes.getmText());

            //****************added on 25-05-16 for left group indication**************
            if (mes.schstatus.equals("4")){
                cache.lay_content.setVisibility(View.GONE);
                cache.left_layout.setVisibility(View.VISIBLE);
                cache.left_msg.setText(mes.getmText());


            }
            //**************************************************************************
        }

        /***********************************************
         Image Message
         ***********************************************/
        else if (type == Message.PHOTO_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFileName())) {
                Bitmap thumb = mes.byteArrayToBitmap(mes.getByteArray());
                mapThumb.put(mes.getFileName(), thumb);
            }
            cache.image.setImageBitmap(ImageHelper.getRoundedCornerBitmap(mapThumb.get(mes.getFileName()), 4));
            cache.image.setTag(position);

            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    bitmap = mes.byteArrayToBitmap(mes.getByteArray());

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    String fileName = mes.getFileName();
                    intent.putExtra("fileName", fileName);

                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         Photo Message
         ***********************************************/
        else if (type == Message.IMAGE_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);


            if (URLUtil.isValidUrl(mes.imgurl)) {

                if (mes.originalurl.contains("http")) {
                    cache.image.setClickable(false);
                    cache.progressbar.setVisibility(View.VISIBLE);
                    new CachingImages(mes.originalurl, Integer.parseInt(mes.getThread_id()), cache.image, mes.getmContentid(), cache.progressbar, mes).execute();

//                    Glide.with(mContext)
//                            .load(mes.imgurl)
//                            .override(300, 300)
//                            .centerCrop()
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    cache.progressbar.setVisibility(View.GONE);
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    cache.progressbar.setVisibility(View.GONE);
//
//                                    new CachingImages(mes.originalurl, Integer.parseInt(mes.getThread_id()), cache.image, mes.getmContentid(), cache.progressbar, mes).execute();
//                                    return false;
//                                }
//                            })
//                            .crossFade()
//                            .into(cache.image);
                } else {
                    cache.progressbar.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(mes.originalurl)
                            .override(300, 300)
                            .centerCrop()
                            .into(cache.image);

                }
            } else {
                try {

                    Bitmap thumb = mes.byteArrayToBitmap(mes.getByteArray());
                    mapThumb.put(mes.getFileName(), thumb);                // }
                    cache.image.setImageBitmap(ImageHelper.getRoundedCornerBitmap(thumb, 4));
                    cache.image.setTag(position);
                } catch (Exception e) {
                }

            }
            //if (!mapThumb.containsKey(mes.getFileName())) {

            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    String fileName = "";
                    Message mes = listMessage.get(position);
                    if (URLUtil.isValidUrl(mes.imgurl)) {
                        fileName = mes.originalurl;
                    } else {
                        //  Message mes = listMessage.get((Integer) v.getTag());
                        bitmap = mes.byteArrayToBitmap(mes.getByteArray());
                        fileName = mes.getFileName();
                    }

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    intent.putExtra("fileName", fileName);
                    intent.putExtra("userName", listMessage.get(position).username);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         Audio Message  Left
         ***********************************************/
        else if (type == Message.AUDIO_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.audioPlayer.setVisibility(View.VISIBLE);
            cache.time.setText(mes.getAudioLength());
            cache.play_pause.setTag(position);
            //  cache.audioPlayer.setTag(position);
            //  cache.time.setText("00:00");

            cache.play_pause.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    //  notifyDataSetChanged();
                    ChattingActivity.position = position;
                    if (URLUtil.isValidUrl(listMessage.get(position).originalurl)) {
                        if (!isPlaying) {
                            mPlayer = new MediaPlayer();
                            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                            try {
                                mPlayer.setDataSource(listMessage.get(position).originalurl);
                                mPlayer.prepare();
                            } catch (IllegalArgumentException e) {
                                //   Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (SecurityException e) {
                                //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (IllegalStateException e) {
                                //     Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        try {
                            if (isPlaying) {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_b));
                                isPlaying = false;
                                if (mPlayer != null) {
                                    mPlayer.stop();

                                }
                                mPlayer.release();
                                mPlayer = null;
                                notifyDataSetChanged();
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }

                            } else {
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.stop_b));

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();


                                final long duration = mPlayer.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer.start();
                                isPlaying = true;
                                mPlayer.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_b));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (Exception e) {
                            e.printStackTrace();
                            notifyDataSetChanged();
                        }
                    } else {
                        mPlayer = new MediaPlayer();
                        final Message mes = listMessage.get((Integer) v.getTag());
                        try {
                            if (isPlaying) {
                                isPlaying = false;
                                if (mPlayer != null) {
                                    mPlayer.stop();

                                }
                                mPlayer.release();
                                notifyDataSetChanged();
                                mPlayer = null;
                                if (count_timer != null) {
                                    count_timer.cancel();
                                }
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_b));
                            } else {

                                startTime = SystemClock.uptimeMillis();
                                timer = new Timer();

                                File file = new File(mes.getFilePath());
                                Uri uri = Uri.fromFile(file);
                                mPlayer.setDataSource(mContext, uri);
                                mPlayer.prepare();
                                final long duration = mPlayer.getDuration();


                                int minutes = (int) (duration / (60 * 1000));
                                int seconds = (int) ((duration / 1000) % 60);
                                final String str = String.format("%02d:%02d", minutes, seconds);


                                cache.time.setText(str);
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        DownTimer(cache.time, duration, str);
                                    }
                                });
                                mPlayer.start();
                                isPlaying = true;
                                ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.play_b));
                                mPlayer.setOnCompletionListener(new OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        //Re-enable the button when the audio has finished playing
                                        // v.setEnabled(true);
                                        ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(R.mipmap.stop_b));
                                        if (count_timer != null) {
                                            count_timer.cancel();
                                        }
                                        isPlaying = false;
                                    }
                                });
                            }


                            //Disable the button when the audio is playing
                            // v.setEnabled(false);
                            // ((ImageView) v).setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_media_pause));


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });


        }
        /***********************************************
         Video Message
         ***********************************************/
        else if (type == Message.VIDEO_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.videoPlayer.setVisibility(View.VISIBLE);
            cache.videoPlayerButton.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFilePath())) {
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mes.getFilePath(), Thumbnails.MINI_KIND);
                mapThumb.put(mes.getFilePath(), thumb);
            }
            cache.videoPlayer.setImageBitmap(mapThumb.get(mes.getFilePath()));

            cache.videoPlayerButton.setTag(position);
            cache.videoPlayerButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    Intent intent = new Intent(mContext, PlayVideoActivity.class);
                    intent.putExtra("filePath", mes.getFilePath());
                    mContext.startActivity(intent);
                }
            });
        }

        /***********************************************
         File Message
         ***********************************************/
        else if (type == Message.FILE_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.fileSavedIcon.setVisibility(View.VISIBLE);
            cache.fileSaved.setVisibility(View.VISIBLE);
            cache.fileSaved.setText(mes.getFileName());
        }

        /***********************************************
         Drawing Message
         ***********************************************/
        else if (type == Message.DRAWING_MESSAGE) {
            enableTextView2(cache, mes.getmText());
            cache.image.setVisibility(View.VISIBLE);

            if (!mapThumb.containsKey(mes.getFileName())) {
                Bitmap thumb = FileUtilities.getBitmapFromFile(mes.getFilePath());
                mapThumb.put(mes.getFileName(), thumb);
            }
            cache.image.setImageBitmap(mapThumb.get(mes.getFileName()));
            cache.image.setTag(position);

            cache.image.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Message mes = listMessage.get((Integer) v.getTag());
                    bitmap = mes.byteArrayToBitmap(mes.getByteArray());

                    Intent intent = new Intent(mContext, ViewImageActivity.class);
                    String fileName = mes.getFileName();
                    intent.putExtra("fileName", fileName);

                    mContext.startActivity(intent);
                }
            });
        }
    }

    //Cache
    private static class CacheView {
        public TextView chatName;
        public TextView text;
        public TextView time;
        public TextView time_text;
        public TextView delayed_time;
        public ImageView image;
        public ProgressBar progressbar;
        public RelativeLayout relativeLayout, main;
        public ImageView play_pause;
        public LinearLayout audioPlayer, lay_time;
        public ImageView videoPlayer;
        public ImageView videoPlayerButton;
        public ImageView fileSavedIcon,pic2;
        public TextView fileSaved;
        public CircularImageView userAvatarImageView;
    }

    //Cache
    private static class CacheView2 {
        public TextView chatName;
        public TextView text;
        public TextView time;
        public TextView time_text;
        public ImageView image;
        public TextView delayed_time, left_msg;
        public ProgressBar progressbar;
        public RelativeLayout relativeLayout, main, lay_content;
        public ImageView play_pause;
        public LinearLayout audioPlayer, lay_time, left_layout;
        public ImageView videoPlayer;
        public ImageView videoPlayerButton,pic2;
        public ImageView fileSavedIcon;
        public TextView fileSaved;
        public CircularImageView userAvatarImageView;
    }


    /********************************************************
     * Count down timer
     ******************************************************/
    private void DownTimer(final TextView textView, final long time, final String set) {

        count_timer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {

                int minutes = (int) (millisUntilFinished / (60 * 1000));
                int seconds = (int) ((millisUntilFinished / 1000) % 60);
                String str = String.format("%02d:%02d", minutes, seconds);
                textView.setText(str);
            }

            public void onFinish() {
                textView.setText(set);
            }
        }.start();

    }

    //*************** Download for Caching Images***********************
    private class CachingImages extends AsyncTask<Void, Void, Void> {

        String url, final_url, content_id;
        int id;
        ImageView image;
        ProgressBar bar;
        Message mes;

        public CachingImages(String url, int thread_id, ImageView img, String content_id, ProgressBar progress, Message mes) {

            this.url = url;
            this.id = thread_id;
            this.image = img;
            this.content_id = content_id;
            this.bar = progress;
            this.mes = mes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            final_url = ImageCaching.saveImageInSdCard(ImageCaching.loadThumb(url), id);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //save it to database and update Imageview
            bar.setVisibility(View.GONE);

            query.updateImagePath(content_id, final_url);

            try {
                mes.originalurl = final_url;
                Glide.with(mContext).load(final_url)
                        .centerCrop()
                        .override(300, 300)
                        .into(image);
                image.setClickable(true);
            } catch (Exception e) {

                e.printStackTrace();
            }

        }
    }


}
