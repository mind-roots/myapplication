package com.app.delayed.chatExtended.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Balvinder on 3/30/2016.
 */
public class ImageCaching {


    //**********Connect to server to load images**************
    public static Bitmap loadThumb(String url) {

        Bitmap thumb = null;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        // opts.inSampleSize = 4;
        opts.inScaled = false;

        try {

            URL u = new URL(url);
            URLConnection c = u.openConnection();
            c.connect();

            BufferedInputStream stream = new BufferedInputStream(
                    c.getInputStream());

            thumb = BitmapFactory.decodeStream(stream, null, opts);

            stream.close();

        } catch (MalformedURLException e) {
            Log.e("ERROR", "malformed url: " + url);
        } catch (IOException e) {
            Log.e("ERROR", "An error has occurred downloading the image: "
                    + url);
        }

        return thumb;
    }

    //************Save downloaded images to SD card***************
    public static String saveImageInSdCard(Bitmap bt, int thread_id) {
        OutputStream fOut = null;
        Uri outputFileUri = null;
        try {
            File root = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Android" + File.separator + "data" + File.separator + "com.app.delayed" + File.separator + "cache" + File.separator + "SharedImages" + File.separator);
            root.mkdirs();
            root = new File(root, thread_id + "_"+System.currentTimeMillis()+".jpg");
            outputFileUri = Uri.fromFile(root);
            fOut = new FileOutputStream(root);
        } catch (Exception e) {

        }

        Bitmap bm = bt;
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }

        return outputFileUri.toString();
    }



}
