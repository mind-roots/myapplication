package com.app.delayed.chatExtended.Entities;

/**
 * Created by user on 2/26/2016.
 */
public class ModelMsgThrdContent {
    public String thread_id;
    public String content_id;
    public String user_id;
    public String type;
    public String content;
    public String thumb_url;
    public String status;
    public String content_sync_id;
    public String last_updated;
    public String read_status;
    public String content_time;
    public String schduled_date;
    public String scheduled_type;
    public String deleted_time;
}
