package com.app.delayed.chatExtended;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.delayed.R;
import com.app.delayed.chatExtended.CustomAdapters.ChatAdapter;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;


public class ViewImageActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView contentView;
    String fileName, userName;
    ProgressBar progressbar;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_image);
        init();
        setvalue();


    }

    private void setvalue() {
        if (URLUtil.isValidUrl(fileName)) {
            progressbar.setVisibility(View.VISIBLE);
            Glide.with(ViewImageActivity.this)
                    .load(fileName)
                   /* .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressbar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressbar.setVisibility(View.GONE);
                            return false;
                        }
                    })*/
                    .into(contentView);

          //  setTitle(new File(fileName).getName());
        } else {
           // setTitle(fileName);
            try {
                contentView.setImageBitmap(ChatAdapter.bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private void init() {
        dialog = new ProgressDialog(ViewImageActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Downloading...");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        Intent intent = getIntent();
        fileName = intent.getStringExtra("fileName");
        userName = intent.getStringExtra("userName");

        contentView = (ImageView) findViewById(R.id.fullscreen_content);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // setTitle("Image");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_download, menu);
        menu.findItem(R.id.action_download).setVisible(true);
       /* if (menuvisibilty.equals("no")) {
            menu.findItem(R.id.action_download).setVisible(false);
        } else {
            menu.findItem(R.id.action_download).setVisible(true);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_download) {
            if (URLUtil.isValidUrl(fileName)) {
                new downloadimage(fileName).execute();
            } else {
                SaveImage(ChatAdapter.bitmap);
                Toast.makeText(ViewImageActivity.this, "Image has been saved.", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class downloadimage extends AsyncTask<String, Void, String> {
        String name;

        public downloadimage(String fileName) {
            this.name = fileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(name).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
                SaveImage(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //  dialog.dismiss();
            Toast.makeText(ViewImageActivity.this, "Image has been saved.", Toast.LENGTH_SHORT).show();
        }
    }

    private Uri SaveImage(Bitmap finalBitmap) {

        final Uri[] uri = new Uri[1];

        String root = Environment.getExternalStorageDirectory().toString();

        File myDir = new File(root + "/Delayd/Downloaded Images/");
        if (!myDir.exists())
            myDir.mkdirs();

        String fname="";
        if (fileName !=null) {
             fname = new File(fileName).getName();
        } else {
            fname=System.currentTimeMillis()+".jpg";
        }

        File file = new File(myDir, fname);

        try {
     /*       if(front==1){
                finalBitmap = rotate(finalBitmap, 270);
            }else  {
                finalBitmap = rotate(finalBitmap, 90);
                front=0;
            }
            img.setImageBitmap(finalBitmap);*/
            //img.setRotation(90);
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            uri[0] = Uri.fromFile(file);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

        MediaScannerConnection.scanFile(ViewImageActivity.this, new String[] { file.getPath() }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("TAG", "Scanned " + path);
                    }
                });

        }catch (Exception e){
            e.printStackTrace();
        }

        return uri[0];
    }

}
