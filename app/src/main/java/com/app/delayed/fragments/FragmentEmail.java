package com.app.delayed.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.delayed.Dashboard;
import com.app.delayed.NewEmail;
import com.app.delayed.R;
import com.app.delayed.adapters.ScheduleEmailAdapter;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleEmail;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class FragmentEmail extends Fragment {
    public static RecyclerView recycler_list_email;
    RecyclerView.LayoutManager mLayoutManager;
    public static ScheduleEmailAdapter adapter;
    View view;
    public static RelativeLayout rl_schdule, rl_recuring, rl_sent;
    public static TextView txt_tab_schdule, txt_tab_recuring, txt_tab_sent;
    public static RelativeLayout view_scheduled, view_dummy;
    public static RelativeLayout compose;
    public static TextView dummy_text;
    public static ProgressBar progressbar;
    boolean network;
    String auth_code;
    SharedPreferences prefs;
    public static ArrayList<ModelScheduleEmail> schduleemaillist = new ArrayList<>();
    public static ArrayList<ModelScheduleEmail> recurringemaillist = new ArrayList<>();
    public static ArrayList<ModelScheduleEmail> sentemaillist = new ArrayList<>();
    public static String adaptertype = "0";
    public static int inttype = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_email, container, false);

        init(view);
        getdata();
        click_events();
        return view;
    }

    private void getdata() {

        if (!network) {
            Methods.conDialog(getActivity());

        } else {
            new getEmail().execute();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Dashboard.newemail) {
            Dashboard.newemail = false;
        //    getdata();
            setdata();
        }else {
            if (adapter!=null){
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setdata() {

        try {
            if (adaptertype.equals("0")) {

                    if (schduleemaillist.size() != 0) {
                        view_dummy.setVisibility(View.GONE);
                        view_scheduled.setVisibility(View.VISIBLE);
                        schdulevisibility();
                        adapter = new ScheduleEmailAdapter(getActivity(), schduleemaillist, "sch");
                        recycler_list_email.setAdapter(adapter);
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Emails scheduled");
                    }

            } else {


                    if (recurringemaillist.size() != 0) {
                        view_dummy.setVisibility(View.GONE);
                        view_scheduled.setVisibility(View.VISIBLE);
                        recurrvisibility();
                        adapter = new ScheduleEmailAdapter(getActivity(), recurringemaillist, "rec");
                        recycler_list_email.setAdapter(adapter);
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Emails recurring");
                    }


            }
        } catch (Exception e) {

        }

    }

    private void init(View rootView) {
        prefs = getActivity().getSharedPreferences("delayed", getActivity().MODE_PRIVATE);
        network = Methods.isNetworkConnected(getActivity());
        auth_code = prefs.getString("auth_code", null);
        System.out.print("auth_code" + auth_code);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_list_email = (RecyclerView) rootView.findViewById(R.id.list_scheduled);
        recycler_list_email.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        rl_schdule = (RelativeLayout) rootView.findViewById(R.id.rl_schdule);
        rl_recuring = (RelativeLayout) rootView.findViewById(R.id.rl_recur);
        rl_sent = (RelativeLayout) rootView.findViewById(R.id.rl_sent);
        txt_tab_schdule = (TextView) rootView.findViewById(R.id.txt_schdule);
        txt_tab_recuring = (TextView) rootView.findViewById(R.id.txt_recur);
        txt_tab_sent = (TextView) rootView.findViewById(R.id.txt_sent);

        view_dummy = (RelativeLayout) rootView.findViewById(R.id.lay_dummy);
        view_scheduled = (RelativeLayout) rootView.findViewById(R.id.lay_scheduled);


        compose = (RelativeLayout) rootView.findViewById(R.id.btn_compose);
        dummy_text = (TextView) rootView.findViewById(R.id.dummy_text);
    }

    private void click_events() {
        rl_schdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schdulevisibility();
                inttype = 0;
                if (schduleemaillist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleEmailAdapter(getActivity(), schduleemaillist, "sch");
                    recycler_list_email.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    compose.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Emails scheduled");
                }
            }
        });

        rl_recuring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (FragmentChat.valueTest == true) {

                recurrvisibility();
                inttype = 1;

                if (recurringemaillist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleEmailAdapter(getActivity(), recurringemaillist, "rec");
                    recycler_list_email.setAdapter(adapter);

                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    compose.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Emails recurring");
                }
            }
        });

        rl_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                rl_schdule.setBackgroundResource(R.drawable.white_left);
                rl_sent.setBackgroundResource(R.drawable.blue_right);
                inttype = 2;

                txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                txt_tab_sent.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
                if (sentemaillist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleEmailAdapter(getActivity(), sentemaillist, "sent");
                    recycler_list_email.setAdapter(adapter);
                } else {
                    compose.setVisibility(View.INVISIBLE);
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Emails sent");

                }

            }
        });

        compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), NewEmail.class);
                intent.putExtra("intenttype", "new");
                startActivity(intent);
            }
        });
    }

    class getEmail extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, email_response, status_schedule, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {


            //--------------------get_schedule_email----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Getsms_Method("get_schedule_email", auth_code);
            System.out.println("schedule response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(schedule_response);
                status_schedule = jobj.optString("status");
                schduleemaillist.clear();
                if (status_schedule.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iterator = obj.keys();
                    while (iterator.hasNext()) {

                        String key = iterator.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        schduleemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String msg = URLDecoder.decode(iobj.optString("message"));
                            item.message = iobj.optString("message");
                           // String sub = URLDecoder.decode(iobj.optString("subject"));
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            schduleemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            //   }
            //--------------------get_recurring_email----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_email", auth_code);
            System.out.println("recurring response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                recurringemaillist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        recurringemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                          //  String msg = URLDecoder.decode(iobj.optString("message"));
                            item.message = iobj.optString("message");
                           // String sub = URLDecoder.decode(iobj.optString("subject"));
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            recurringemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_email-------------------------------//
            email_response = Getsms_Method("get_email", auth_code);
            System.out.println("email response:: " + schedule_response);

            try {
                JSONObject jobj = new JSONObject(email_response);
                statust = jobj.optString("status");
                sentemaillist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        sentemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                          //  String msg = URLDecoder.decode(iobj.optString("message"));
                            item.message = iobj.optString("message");
                          //  String sub = URLDecoder.decode(iobj.optString("subject"));
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            sentemaillist.add(item);

                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);
            try {
                if (adaptertype.equals("0")) {
                    if (status_schedule.equals("true")) {
                        if (schduleemaillist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibility();
                            adapter = new ScheduleEmailAdapter(getActivity(), schduleemaillist, "sch");
                            recycler_list_email.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no Emails scheduled");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Emails scheduled");
                    }
                } else {

                    if (statusr.equals("true")) {
                        if (recurringemaillist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            recurrvisibility();
                            adapter = new ScheduleEmailAdapter(getActivity(), recurringemaillist, "rec");
                            recycler_list_email.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no Emails recurring");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Emails recurring");
                    }

                }
            } catch (Exception e) {

            }


        }
    }

    private void recurrvisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        rl_schdule.setBackgroundResource(R.drawable.white_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);


        txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        rl_schdule.setBackgroundResource(R.drawable.blue_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);

        txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

    public String Getsms_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}
