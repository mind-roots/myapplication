package com.app.delayed.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.delayed.Dashboard;
import com.app.delayed.FirstSchduled;
import com.app.delayed.NewSMS;
import com.app.delayed.R;
import com.app.delayed.adapters.ScheduleSmsAdapter;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class FragmentSms extends Fragment {


    public static RecyclerView recycler_list_sms;

    RecyclerView.LayoutManager mLayoutManager;
    public static ScheduleSmsAdapter adapter;

    View view;
    public static RelativeLayout rl_schdule, rl_recuring, rl_sent;
    //  RelativeLayout view_scheduled, view_recurring, view_sent, view_dummy;
    public static RelativeLayout view_dummy, view_scheduled;
    public static TextView txt_tab_schdule, txt_tab_recuring, txt_tab_sent;
    public static RelativeLayout compose;
    public static TextView dummy_text;
    public static ProgressBar progressbar;
    boolean network;
    String auth_code;
    SharedPreferences prefs;
    public static ArrayList<ModelScheduleSms> schdulesmslist = new ArrayList<>();
    public static ArrayList<ModelScheduleSms> recurringsmslist = new ArrayList<>();
    public static ArrayList<ModelScheduleSms> sentsmslist = new ArrayList<>();
    public static int inttype = 0;
    public static String adaptertype = "0";
    DatabaseQueries queries;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.frag_sms, container, false);

        init(view);

        //   getdata();
        getdbdata();
        click_events();
        return view;
    }

    private void getdata() {

        if (!network) {
            Methods.conDialog(getActivity());

        } else {
            new getsms().execute();
        }
    }


    private void init(View rootView) {
        queries = new DatabaseQueries(getActivity());
        prefs = getActivity().getSharedPreferences("delayed", getActivity().MODE_PRIVATE);
        network = Methods.isNetworkConnected(getActivity());
        auth_code = prefs.getString("auth_code", null);
        System.out.print("auth_code" + auth_code);
        mLayoutManager = new LinearLayoutManager(getActivity());

        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        rl_schdule = (RelativeLayout) rootView.findViewById(R.id.rl_schdule);
        rl_recuring = (RelativeLayout) rootView.findViewById(R.id.rl_recur);
        rl_sent = (RelativeLayout) rootView.findViewById(R.id.rl_sent);
        txt_tab_schdule = (TextView) rootView.findViewById(R.id.txt_schdule);
        txt_tab_recuring = (TextView) rootView.findViewById(R.id.txt_recur);
        txt_tab_sent = (TextView) rootView.findViewById(R.id.txt_sent);

        recycler_list_sms = (RecyclerView) rootView.findViewById(R.id.list_scheduled);


        recycler_list_sms.setLayoutManager(mLayoutManager);


        view_dummy = (RelativeLayout) rootView.findViewById(R.id.lay_dummy);
        view_scheduled = (RelativeLayout) rootView.findViewById(R.id.lay_scheduled);
//        view_recurring = (RelativeLayout) rootView.findViewById(R.id.lay_recurring);
//        view_sent = (RelativeLayout) rootView.findViewById(R.id.lay_sent);

        compose = (RelativeLayout) rootView.findViewById(R.id.btn_compose);
        dummy_text = (TextView) rootView.findViewById(R.id.dummy_text);


    }

    private void click_events() {
        rl_schdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schdulevisibility();
                inttype = 0;
                adaptertype="0";
                getdbdata();
              /*  if (schdulesmslist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleSmsAdapter(getActivity(), schdulesmslist, "sch");
                    recycler_list_sms.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    compose.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no SMS scheduled");
                }*/

            }
        });

        rl_recuring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recurrvisibility();
                inttype = 1;
                adaptertype="1";
                getdbdata();
                /*if (recurringsmslist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleSmsAdapter(getActivity(), recurringsmslist, "rec");
                    recycler_list_sms.setAdapter(adapter);

                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    compose.setVisibility(View.VISIBLE);
                    dummy_text.setText("You have no SMS recurring");
                }*/
            }
        });

        rl_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                rl_schdule.setBackgroundResource(R.drawable.white_left);
                rl_sent.setBackgroundResource(R.drawable.blue_right);

                txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                txt_tab_sent.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));

                inttype = 2;
                if (sentsmslist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new ScheduleSmsAdapter(getActivity(), sentsmslist, "sent");
                    recycler_list_sms.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    compose.setVisibility(View.INVISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no SMS sent");
                }
            }
        });

        compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewSMS.class);
                intent.putExtra("intenttype", "new");
                startActivity(intent);
            }
        });
    }


    class getsms extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Getsms_Method("get_schedule_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                schdulesmslist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        schdulesmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            schdulesmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                recurringsmslist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        recurringsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            recurringsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getsms_Method("get_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                sentsmslist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        sentsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            sentsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);
            try {
                if (adaptertype.equals("0")) {
                    if (statuss.equals("true")) {
                        if (schdulesmslist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibility();
                            /*if (Dashboard.viewshow) {
                                Dashboard.viewshow = false;
                                if (schdulesmslist.size() == 1) {
                                    startActivity(new Intent(getActivity(), FirstSchduled.class));

                                }
                            }*/
                            adapter = new ScheduleSmsAdapter(getActivity(), schdulesmslist, "sch");
                            recycler_list_sms.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no SMS scheduled");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no SMS scheduled");
                    }
                } else {

                    if (statusr.equals("true")) {
                        if (recurringsmslist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            recurrvisibility();
                            /*if (Dashboard.viewshow) {
                                Dashboard.viewshow = false;
                                if (recurringsmslist.size() == 1) {
                                    startActivity(new Intent(getActivity(), FirstSchduled.class));

                                }
                            }*/
                            adapter = new ScheduleSmsAdapter(getActivity(), recurringsmslist, "rec");
                            recycler_list_sms.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no SMS recurring");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no SMS recurring");
                    }

                }
            } catch (Exception e) {
            }
        }
    }

    class getcredits extends AsyncTask<String, Void, String> {
        String authcode, response;

        public getcredits(String auth_code) {
            this.authcode = auth_code;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONParser parser = new JSONParser();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "get_profile")
                    .appendQueryParameter("auth_code", authcode);
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                    editor.putString("name", pin_nos.optString("name"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                    editor.commit();
                }
            } catch (Exception e) {
            }
        }
    }

    public String Getsms_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void recurrvisibility() {
        rl_recuring.setBackgroundResource(R.drawable.blue_right);
        rl_schdule.setBackgroundResource(R.drawable.white_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);

        txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibility() {
        rl_recuring.setBackgroundResource(R.drawable.white_right);
        rl_schdule.setBackgroundResource(R.drawable.blue_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);

        txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Dashboard.newsms) {
            Dashboard.newsms = false;
            //----caling asyc--//
            //   getdata();
            //---method to reset adapter when loader in new sms------------//
            //   setdata();

            //---------getting sms list from database-----------------//
            getdbdata();
      //      new getcredits(auth_code).execute();
        } else {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void getdbdata() {

        if (adaptertype.equals("0")) {
            schdulesmslist = queries.getsms("sch");
            if (schdulesmslist.size() != 0) {
                view_dummy.setVisibility(View.GONE);
                view_scheduled.setVisibility(View.VISIBLE);
                schdulevisibility();
                adapter = new ScheduleSmsAdapter(getActivity(), schdulesmslist, "sch");
                recycler_list_sms.setAdapter(adapter);
            } else {
                view_dummy.setVisibility(View.VISIBLE);
                view_scheduled.setVisibility(View.GONE);
                dummy_text.setText("You have no SMS scheduled");
            }
        } else {
            recurringsmslist = queries.getsms("rec");
            if (recurringsmslist.size() != 0) {
                view_dummy.setVisibility(View.GONE);
                view_scheduled.setVisibility(View.VISIBLE);
                recurrvisibility();
                adapter = new ScheduleSmsAdapter(getActivity(), recurringsmslist, "rec");
                recycler_list_sms.setAdapter(adapter);
            } else {
                view_dummy.setVisibility(View.VISIBLE);
                view_scheduled.setVisibility(View.GONE);
                dummy_text.setText("You have no SMS recurring");
            }


        }
    }

    private void setdata() {
        try {
            if (adaptertype.equals("0")) {
                if (schdulesmslist.size() != 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    schdulevisibility();

                    adapter = new ScheduleSmsAdapter(getActivity(), schdulesmslist, "sch");
                    recycler_list_sms.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no SMS scheduled");
                }

            } else {

                if (recurringsmslist.size() != 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    recurrvisibility();

                    adapter = new ScheduleSmsAdapter(getActivity(), recurringsmslist, "rec");
                    recycler_list_sms.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no SMS recurring");
                }


            }
        } catch (Exception e) {
        }

    }
}
