package com.app.delayed.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.app.delayed.Dashboard;
import com.app.delayed.LogoutAlert;
import com.app.delayed.NewFacebook;
import com.app.delayed.R;
import com.app.delayed.adapters.Facebook_Adapter;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.Model_facebook;
import com.app.delayed.tutorial.ViewpagerActivity;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class FragmentFacebook extends Fragment {

    View view;
    public static RelativeLayout rl_schdule, rl_recuring, rl_sent;
    public static TextView txt_tab_schdule, txt_tab_recuring, txt_tab_sent;
    public static RelativeLayout view_scheduled, view_dummy, lay_scheduled;
    public static RelativeLayout btn_compose;
    public static TextView txt_btn_compose;
    public static TextView dummy_text;
    CallbackManager callbackManager;
    String access_token, status, id, mobile_no, country_id, country, fb_status,
            pin, name, fb_name, fb_email, fb_token, fb_token_date, twitter_token, twitter_secret, twitter_token_date, twitter_hname,
            twitter_uname, credits, flag, auth_code, token_id, token_type, chat_icon, last_updated;
    SharedPreferences prefs;
    public static ArrayList<Model_facebook> schdulefblist = new ArrayList<>();
    public static ArrayList<Model_facebook> recurringfblist = new ArrayList<>();
    public static ArrayList<Model_facebook> postfblist = new ArrayList<>();
    String servicetype = "get_schedule_sms";
    public static ProgressBar progressbar;
    public static RecyclerView list_scheduled;
    RecyclerView.LayoutManager mLayoutManager;
    public static Facebook_Adapter adapter;
    boolean network;
    public static String adaptertype = "0";
    public static int inttype = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_facebook, container, false);
        FacebookSdk.sdkInitialize(getActivity());

        showHashKey();
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        init(view);

        click_events();
        getdata();

        return view;
    }

    private void getdata() {
        if (!network) {
            Methods.conDialog(getActivity());

        } else {
            if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null")&& prefs.getString("fb_token", null).length()>0) {
                view_dummy.setVisibility(View.GONE);
                view_scheduled.setVisibility(View.VISIBLE);


                new facebook_thread(auth_code).execute();

            } else {
                view_dummy.setVisibility(View.VISIBLE);
                view_scheduled.setVisibility(View.GONE);
                schdulefblist.clear();
                recurringfblist.clear();
                postfblist.clear();
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Dashboard.newfb) {
            Dashboard.newfb = false;
          //  getdata();
            setdata();
        }else {
            if (adapter!=null){
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setdata() {
        if (adaptertype.equals("0")) {
                    if (schdulefblist.size() != 0) {
                        view_dummy.setVisibility(View.GONE);
                        view_scheduled.setVisibility(View.VISIBLE);
                        schdulevisibility();
                        adapter = new Facebook_Adapter(getActivity(), schdulefblist, "sch");
                        list_scheduled.setAdapter(adapter);
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Facebook posts scheduled");
                    }


        } else {

                if (recurringfblist.size() != 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    recurrvisibility();
                    adapter = new Facebook_Adapter(getActivity(), recurringfblist, "rec");
                    list_scheduled.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no recurring posts");
                }


        }

    }

    private void init(View rootView) {
        network = Methods.isNetworkConnected(getActivity());
        rl_schdule = (RelativeLayout) rootView.findViewById(R.id.rl_schdule);
        rl_recuring = (RelativeLayout) rootView.findViewById(R.id.rl_recur);
        rl_sent = (RelativeLayout) rootView.findViewById(R.id.rl_sent);
        txt_tab_schdule = (TextView) rootView.findViewById(R.id.txt_schdule);
        txt_tab_recuring = (TextView) rootView.findViewById(R.id.txt_recur);
        txt_tab_sent = (TextView) rootView.findViewById(R.id.txt_sent);

        view_dummy = (RelativeLayout) rootView.findViewById(R.id.lay_dummy);
        view_scheduled = (RelativeLayout) rootView.findViewById(R.id.lay_scheduled);


        btn_compose = (RelativeLayout) rootView.findViewById(R.id.btn_compose);
        dummy_text = (TextView) rootView.findViewById(R.id.dummy_text);
        txt_btn_compose = (TextView) rootView.findViewById(R.id.txt_btn_compose);

        prefs = getActivity().getSharedPreferences("delayed", getActivity().MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        list_scheduled = (RecyclerView) rootView.findViewById(R.id.list_scheduled);
        mLayoutManager = new LinearLayoutManager(getActivity());
        list_scheduled.setLayoutManager(mLayoutManager);
        String val = prefs.getString("fb_token", null);
        if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
            txt_btn_compose.setText("Compose Post");
        } else {
            txt_btn_compose.setText("Connect");
        }
        callbackManager = CallbackManager.Factory.create();
    }

    private void click_events() {
        rl_schdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schdulevisibility();
                inttype = 0;
                if (schdulefblist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new Facebook_Adapter(getActivity(), schdulefblist, "sch");
                    list_scheduled.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    btn_compose.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Facebook posts scheduled");
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                        txt_btn_compose.setText("Compose Post");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }
                }

            }
        });

        rl_recuring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (FragmentChat.valueTest == true) {
                inttype = 1;
                recurrvisibility();
                if (recurringfblist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new Facebook_Adapter(getActivity(), recurringfblist, "rec");
                    list_scheduled.setAdapter(adapter);

                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    btn_compose.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no recurring posts");
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                        txt_btn_compose.setText("Compose Post");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }

                }
            }
        });

        rl_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                rl_schdule.setBackgroundResource(R.drawable.white_left);
                rl_sent.setBackgroundResource(R.drawable.blue_right);
                inttype = 2;
                txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                txt_tab_sent.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
                if (postfblist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new Facebook_Adapter(getActivity(), postfblist, "sent");
                    list_scheduled.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    btn_compose.setVisibility(View.INVISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Facebook posts sent");
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                        txt_btn_compose.setText("Compose Post");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }
                }
            }
        });

        btn_compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (prefs.getString("fb_token", null) != null && !prefs.getString("fb_token", null).equals("null") && prefs.getString("fb_token", null).length()>0) {
                        new facebook_thread(auth_code).execute();
                        Intent intent = new Intent(getActivity(), NewFacebook.class);
                        intent.putExtra("intenttype", "new");
                        startActivity(intent);
                    } else {
                        loginToFacebook();
                        //Snackbar.make(btn_compose, "Different hash key", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // *********************************************Facebook Login**********************************
    public void loginToFacebook() {
        try {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            access_token = AccessToken.getCurrentAccessToken().getToken();
                            Log.i("Fb Access Token:", "" + access_token);
                            Dashboard.isFb = true;
                            new Update_fb_details().execute();
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Snackbar.make(btn_compose, "" + exception.toString(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {

            Snackbar.make(btn_compose, "Error : "+e, Snackbar.LENGTH_SHORT).show();
            Log.i("Error: ",""+e);
        }

    }


    //*************************************onActivityResult*****************************************
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    //*************************************Fb Async Task Class**************************************
    public class Update_fb_details extends AsyncTask<Void, Void, Void> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = FbUpdate_Method(prefs.getString("auth_code", null), access_token);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    txt_btn_compose.setText("Compose Post");
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                   // String name = URLDecoder.decode(pin_nos.optString("name"),"UTF-8");
                    editor.putString("name", pin_nos.optString("name"));
                    editor.putString("setting_email", pin_nos.optString("email"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                    editor.commit();
                    getdata();
                } else if(status.equals("false1")){
                    // Snackbar.make(btn_compose, "Response Error" + status, Snackbar.LENGTH_SHORT).show();
                    Intent i=new Intent(getActivity(),LogoutAlert.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(i);
                }
                else {
                    Toast.makeText(getActivity(),"Unable to fetch data, Please try again.", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
            }

        }
    }

    //*************************************Fb details method****************************************
    public String FbUpdate_Method(String auth_code, String fb_token) {
        String res = null;
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("fb_token", fb_token);
        res = parser.getJSONFromUrl(Utils.update_facebook_detail, builder);
        return res;
    }

    // **********************************************Show Hashkey***********************************
    private void showHashKey() {
        // TODO Auto-generated method stub
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo("com.app.delayed", PackageManager.GET_SIGNATURES); // Your
            // here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                System.out.println("KeyHash11:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    //****************************facebook_thread***************************************************

    class facebook_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust, auth_code;

        public facebook_thread(String auth_code) {
            this.auth_code = auth_code;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            System.out.println("Authcode::::::: " + auth_code);
            schedule_response = Getfb_Method("fb_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                schdulefblist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    schdulefblist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        schdulefblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                          //  String post = URLDecoder.decode(iobj.optString("facebook_post"));
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            schdulefblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getfb_Method("facebook_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                recurringfblist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    recurringfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        recurringfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String post = URLDecoder.decode(iobj.optString("facebook_post"));
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            recurringfblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getfb_Method("get_facebook_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                postfblist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    postfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        postfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String post = URLDecoder.decode(iobj.optString("facebook_post"));
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            postfblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);

            if (adaptertype.equals("0")) {
                try {


                    if (statuss.equals("true")) {
                        if (schdulefblist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibility();
                            adapter = new Facebook_Adapter(getActivity(), schdulefblist, "sch");
                            list_scheduled.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no Facebook posts scheduled");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Facebook posts scheduled");
                    }
                } catch (Exception e) {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Facebook posts scheduled");
                }
            } else {

                if (statusr.equals("true")) {
                    if (recurringfblist.size() != 0) {
                        view_dummy.setVisibility(View.GONE);
                        view_scheduled.setVisibility(View.VISIBLE);
                        recurrvisibility();
                        adapter = new Facebook_Adapter(getActivity(), recurringfblist, "rec");
                        list_scheduled.setAdapter(adapter);
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no recurring posts");
                    }
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no recurring posts");
                }

            }


        }
    }


    private void recurrvisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        rl_schdule.setBackgroundResource(R.drawable.white_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);


        txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        rl_schdule.setBackgroundResource(R.drawable.blue_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);

        txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }

    public String Getfb_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}


