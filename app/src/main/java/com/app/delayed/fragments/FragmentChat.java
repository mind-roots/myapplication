package com.app.delayed.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.delayed.ChatContactsScreen;
import com.app.delayed.Dashboard;
import com.app.delayed.LogoutAlert;
import com.app.delayed.R;
import com.app.delayed.chatExtended.CustomAdapters.ContactAdapter;
import com.app.delayed.chatExtended.Entities.ContactInfo;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.chatExtended.util.CustomComparator;
import com.app.delayed.chatExtended.util.TabUtils;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelCountry;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Person;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class FragmentChat extends Fragment {

    RelativeLayout rl_tab1, rl_tab2, rl_tab3;
    TextView txt_tab1, txt_tab2, txt_tab3;
    View view;
    public static RelativeLayout mDummyView;
    public static RelativeLayout mBtnCompose;
    public static TextView mDummyText;
    public static RecyclerView contact_list;
    List<ContactInfo> data_list = new ArrayList<>();
    public static ArrayList<ModelMsgThread> thread_list = new ArrayList<>();
    boolean[] isRead = {true, true, false, true, true, true, true, true};
    ProgressBar progressbar;
    public static ContactAdapter adapter;
    String auth_code, timeformat;
    public static SharedPreferences prefs;
    boolean network;
    DatabaseQueries dataquery;
    boolean show = false;
    String userid, user_name, chat_icon;
    String content_sync_id, thread_sync_id;
    public static String contenttype = "recent";
    SharedPreferences.Editor editor;

    boolean schdule = false;

    ArrayList<ModelCountry> emails = new ArrayList<>();
    Person item;
    public static ArrayList<Person> userlist = new ArrayList<>();

    List<ModelContacts> arr_cont = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.frag_chat, container, false);

        init(view);
        click_events();
        // getdatafrmdb("0");
        //     getdata();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        editor = prefs.edit();
        editor.putString("current_thread", "all");
        editor.commit();
        if (Utils.chat_section == 0) {
            getdatafrmdb("0");
        } else if (Utils.chat_section == 1) {
            schdule = true;
            getdatafrmdb("1");
        } else {
            schdule = true;
            getdatafrmdb("2");
        }

    }

    /****************************************
     * Setting adapters
     **************************************/
    private void getdatafrmdb(String status) {
        chat_icon = prefs.getString("chat_icon", null);
        user_name = prefs.getString("name", null);
        thread_list = dataquery.getallmsgs(userid, status, user_name, chat_icon);

        TabUtils.updateTabBadge(Dashboard.tabChat, DatabaseQueries.unreadcount);
        Collections.sort(FragmentChat.thread_list, new CustomComparator());
        Collections.reverse(thread_list);
        adapter = new ContactAdapter(getActivity(), thread_list);
        contact_list.setAdapter(adapter);


        if (thread_list.size() > 0) {
            show = false;

            mDummyView.setVisibility(View.GONE);
        } else {
            show = true;

        }
        if (schdule) {
            schdule = false;
            if (thread_list.size() == 0) {
                mDummyView.setVisibility(View.VISIBLE);
                if (status.equals("1")) {
                    mDummyText.setText("You have no scheduled chats");
                } else {
                    mDummyText.setText("You have no recurring chats");
                }
                mBtnCompose.setVisibility(View.INVISIBLE);
            }
        }
        if (Dashboard.newchat) {
            Dashboard.newchat = false;
            getdata();
        }

    }

    /********************************************
     * Initialize UI elements
     ******************************************/
    private void init(View rootView) {
        prefs = getActivity().getSharedPreferences("delayed", getActivity().MODE_PRIVATE);

        auth_code = prefs.getString("auth_code", null);
        timeformat = prefs.getString("timeformat", null);
        userid = prefs.getString("id", null);

        dataquery = new DatabaseQueries(getActivity());
        rl_tab1 = (RelativeLayout) rootView.findViewById(R.id.rl_tab1);
        rl_tab2 = (RelativeLayout) rootView.findViewById(R.id.rl_tab2);
        rl_tab3 = (RelativeLayout) rootView.findViewById(R.id.rl_tab3);
        txt_tab1 = (TextView) rootView.findViewById(R.id.txt_tab1);
        txt_tab2 = (TextView) rootView.findViewById(R.id.txt_tab2);
        txt_tab3 = (TextView) rootView.findViewById(R.id.txt_tab3);

        contact_list = (RecyclerView) rootView.findViewById(R.id.contact_list);
        mDummyView = (RelativeLayout) rootView.findViewById(R.id.lay_dummy);
        mDummyText = (TextView) rootView.findViewById(R.id.dummy_text);
        mBtnCompose = (RelativeLayout) rootView.findViewById(R.id.btn_compose);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        contact_list.setLayoutManager(llm);


    }

    /**********************************************
     * Webservice and database methos calling
     ********************************************/
    private void getdata() {
        network = Methods.isNetworkConnected(getActivity());
        content_sync_id = dataquery.getsynchid("message_thread_content", "content_sync_id");
        thread_sync_id = dataquery.getsynchid("message_threads", "thread_sync_id");
        if (!network) {
            Methods.conDialog(getActivity());

        } else {

            new getthreadcontent(content_sync_id).execute();

        }
    }


    /********************************************
     * Event handlers
     ******************************************/
    private void click_events() {
        rl_tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contenttype = "recent";
                Utils.chat_section = 0;
                rl_tab2.setBackgroundColor(Color.parseColor("#FFFFFF"));
                rl_tab3.setBackgroundResource(R.drawable.white_right);
                rl_tab1.setBackgroundResource(R.drawable.blue_left);
                txt_tab2.setTextColor(Color.parseColor("#376a9d"));
                txt_tab3.setTextColor(Color.parseColor("#376a9d"));
                txt_tab1.setTextColor(Color.parseColor("#FFFFFF"));
                thread_list.clear();
                getdatafrmdb("0");
                adapter.notifyDataSetChanged();
                if (thread_list.size() == 0) {
                    contact_list.setVisibility(View.GONE);
                    mDummyView.setVisibility(View.VISIBLE);
                    mDummyText.setText("You have no recent chats");
                    mBtnCompose.setVisibility(View.VISIBLE);
                } else {
                    contact_list.setVisibility(View.VISIBLE);
                    mDummyView.setVisibility(View.GONE);
                }
            }
        });

        rl_tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contenttype = "sch";
                Utils.chat_section = 1;
                rl_tab2.setBackgroundColor(Color.parseColor("#376a9d"));
                rl_tab3.setBackgroundResource(R.drawable.white_right);
                rl_tab1.setBackgroundResource(R.drawable.white_left);
                txt_tab2.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab3.setTextColor(Color.parseColor("#376a9d"));
                txt_tab1.setTextColor(Color.parseColor("#376a9d"));
                thread_list.clear();
                getdatafrmdb("1");

                adapter.notifyDataSetChanged();
                /*if (Dashboard.viewshow) {
                    Dashboard.viewshow = false;
                    if (thread_list.size() == 1) {
                        int contentcount = dataquery.getcontent(thread_list.get(0).thread_id);
                        if (contentcount == 1) {
                            startActivity(new Intent(getActivity(), FirstSchduled.class));
                        }
                    }
                }*/
                if (thread_list.size() == 0) {
                    contact_list.setVisibility(View.GONE);
                    mDummyView.setVisibility(View.VISIBLE);
                    mDummyText.setText("You have no scheduled chats");
                    mBtnCompose.setVisibility(View.INVISIBLE);
                } else {
                    contact_list.setVisibility(View.VISIBLE);
                    mDummyView.setVisibility(View.GONE);
                }
            }
        });

        rl_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contenttype = "rec";
                Utils.chat_section = 2;
                rl_tab2.setBackgroundColor(Color.parseColor("#FFFFFF"));
                rl_tab3.setBackgroundResource(R.drawable.blue_right);
                rl_tab1.setBackgroundResource(R.drawable.white_left);
                txt_tab2.setTextColor(Color.parseColor("#376a9d"));
                txt_tab3.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab1.setTextColor(Color.parseColor("#376a9d"));
                thread_list.clear();
                getdatafrmdb("2");
                adapter.notifyDataSetChanged();
               /* if (Dashboard.viewshow) {
                    Dashboard.viewshow = false;
                    if (thread_list.size() == 1) {
                        int contentcount = dataquery.getcontent(thread_list.get(0).thread_id);
                        if (contentcount == 1) {
                            startActivity(new Intent(getActivity(), FirstSchduled.class));
                        }
                    }
                }*/
                if (thread_list.size() == 0) {
                    contact_list.setVisibility(View.GONE);
                    mDummyView.setVisibility(View.VISIBLE);
                    mDummyText.setText("You have no recurring chats");
                    mBtnCompose.setVisibility(View.INVISIBLE);
                } else {
                    contact_list.setVisibility(View.VISIBLE);
                    mDummyView.setVisibility(View.GONE);
                }

            }
        });

        mBtnCompose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChatContactsScreen.class));
            }
        });

    }


    /****************************************
     * Get Latest Messages from server
     **************************************/
    class getthreadcontent extends AsyncTask<String, Void, String> {
        String response, syncid;

        public getthreadcontent(String content_sync_id) {
            this.syncid = content_sync_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (show) {
                progressbar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            response = schdule_method("get_new_content", syncid);
            System.out.print("schedule rec=" + response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    ModelMsgThrdContent item = new ModelMsgThrdContent();
                    JSONArray jarr = obj.optJSONArray("data");
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        String content_id = dobj.optString("content_id");
                        item.content_id = content_id;
                        item.user_id = dobj.optString("user_id");
                        item.type = dobj.optString("type");
                       // String content = URLDecoder.decode(dobj.optString("content"), "UTF-8");
                        item.content = dobj.optString("content");
                        item.thumb_url = dobj.optString("thumb_url");
                        item.content_time = dobj.optString("content_time");
                        item.scheduled_type = dobj.optString("scheduled_type");
                        //  new downloadimage(item.thumb_url, item.thread_id, item
                        //   item.scheduled_type=dobj.optString("scheduled_type");.content_id).execute();
                        String cstatus = dobj.optString("status");
                        item.status = dobj.optString("status");
                        String participants=dataquery.getParticipents(item.thread_id);
                        if (item.user_id.equals(userid)) {
                            item.read_status = "1";
                        } else {
                            item.read_status = "0";
                        }


                        String content_sync_id = dobj.optString("content_sync_id");
                        item.content_sync_id = content_sync_id;
                        item.last_updated = dobj.optString("last_updated");
                        item.schduled_date = "";
                        if (item.type.equals("0")) {
                            //   publishProgress(item.content);
                        } else {
                        }
                        if (dataquery.contentexist(content_id)) {
                            if (participants.equals(userid)&&!participants.equals("")){
                                dataquery.updatemyread(content_id, content_sync_id, cstatus);
                            }
                            dataquery.updatemsgcontent(content_id, content_sync_id, cstatus);
                        } else {
                            dataquery.insertmsgthreadcontent(item);
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new getthread(thread_sync_id).execute();
        }
    }


    /****************************************
     * Get messages web link method
     **************************************/
    private String schdule_method(String service_type, String id) {
        String res = null;
        JSONParser parser = new JSONParser();
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("content_sync_id", id);


        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;

    }


    /***************************************
     * Get All message threads from server
     *************************************/
    class getthread extends AsyncTask<String, Void, String> {
        String schedule_response, sync_ids;
        String status;

        public getthread(String ids) {
            this.sync_ids = ids;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {


            //--------------------get_schedule_email----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Get_thread_Method("get_new_threads", auth_code, sync_ids);
            System.out.println("schedule response:111:  " + schedule_response);
            try {
                JSONObject obj = new JSONObject(schedule_response);
                status = obj.optString("status");
                if (status.equals("true")) {

                    JSONArray jarr = obj.optJSONArray("data");
                    for (int i = 0; i < jarr.length(); i++) {
                        ModelMsgThread item = new ModelMsgThread();
                        JSONObject dobj = jarr.optJSONObject(i);
                        item.thread_id = dobj.optString("thread_id");
                        item.participant_ids = dobj.optString("participant_ids");
                        item.participant_numbers = dobj.optString("participant_nos");
                        item.group_icon = dobj.optString("group_icon");
                        item.thread_sync_id = dobj.optString("thread_sync_id");
                        item.last_updated = dobj.optString("last_updated");
                        item.notification_status = dobj.optString("notification_status");
                        item.blocked_status = dobj.optString("blocked_status");
                        // thread_list.add(item);
//                        try {
//                            String name = URLDecoder.decode(dobj.optString("group_name"), "UTF-8");
//                            item.group_name = name;
//                        }catch (Exception e){
                            item.group_name = dobj.optString("group_name");
                       // }
                        if (dataquery.isthreadexist(item.thread_id)) {
                            // do something
                        } else {
                            dataquery.insertmsgthread(item);
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Utils.chat_section == 0)
                thread_list = dataquery.getallmsgs(userid, "0", user_name, chat_icon);
            else if (Utils.chat_section == 1)
                thread_list = dataquery.getallmsgs(userid, "1", user_name, chat_icon);
            else thread_list = dataquery.getallmsgs(userid, "2", user_name, chat_icon);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                if (show) {
                    show = false;
                    progressbar.setVisibility(View.GONE);
                }

                if (thread_list.size() > 0) {
                    mDummyView.setVisibility(View.GONE);
                    contact_list.setVisibility(View.VISIBLE);
                    Collections.sort(FragmentChat.thread_list, new CustomComparator());
                    Collections.reverse(thread_list);
                    adapter = new ContactAdapter(getActivity(), thread_list);
                    contact_list.setAdapter(adapter);
                    //adapter.notifyDataSetChanged();
                    TabUtils.updateTabBadge(Dashboard.tabChat, DatabaseQueries.unreadcount);
                } else {
                    //    mDummyView.setVisibility(View.VISIBLE);
                    contact_list.setVisibility(View.GONE);
                    mDummyView.setVisibility(View.VISIBLE);
                    if (Utils.chat_section == 2) {
                        mDummyText.setText("You have no recurring chats");
                        mBtnCompose.setVisibility(View.INVISIBLE);
                    } else if (Utils.chat_section == 1) {
                        mDummyText.setText("You have no scheduled chats");
                        mBtnCompose.setVisibility(View.INVISIBLE);
                    } else {
                        mDummyText.setText("You have no recent chats");
                        mBtnCompose.setVisibility(View.VISIBLE);
                    }
                }


                if (status.equals("false1")) {

                    Intent i = new Intent(getActivity(), LogoutAlert.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(i);

                }

            } catch (Exception e) {
                e.printStackTrace();

            }finally {
                progressbar.setVisibility(View.GONE);
            }
        }
    }


    /****************************************
     * Message thread web link method
     **************************************/
    public String Get_thread_Method(String service_type, String auth_code, String syncid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("thread_sync_id", syncid);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }


}
