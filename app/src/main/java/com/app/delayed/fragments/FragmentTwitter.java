package com.app.delayed.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.Dashboard;
import com.app.delayed.LogoutAlert;
import com.app.delayed.NewTweet;
import com.app.delayed.R;
import com.app.delayed.WebViewActivity;
import com.app.delayed.adapters.TweeterAdapter;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelTwitterData;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class FragmentTwitter extends Fragment {
    public static RecyclerView list_tw;
    RecyclerView.LayoutManager mLayoutManager;
    View view;
    public static RelativeLayout rl_schdule, rl_recuring, rl_sent;
    public static TextView txt_tab_schdule, txt_tab_recuring, txt_tab_sent;
    public static RelativeLayout view_scheduled, view_dummy;
    public static RelativeLayout btn_compose;
    public static TextView txt_btn_compose;
    public static TextView dummy_text;
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;
    private static Twitter twitter;
    private static RequestToken requestToken;
    public static final int WEBVIEW_REQUEST_CODE = 100;
    // TwitterAuthClient client;
    public static ProgressBar progressbar;
    SharedPreferences prefs;
    String access_token, token_secret, status, id, mobile_no, country_id, country, fb_status,
            pin, name, fb_name, fb_email, fb_token, fb_token_date, twitter_token, twitter_secret, twitter_token_date, twitter_hname,
            twitter_uname, credits, flag, auth_code, token_id, token_type, chat_icon, last_updated;
    boolean network;
    public static ArrayList<ModelTwitterData> schduletweetlist = new ArrayList<>();
    public static ArrayList<ModelTwitterData> recurringtweetlist = new ArrayList<>();
    public static ArrayList<ModelTwitterData> posttweetlist = new ArrayList<>();
    public static TweeterAdapter adapter;
    public static String adaptertype = "0";
    public static int inttype = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_twitter, container, false);

//        TwitterAuthConfig authConfig =  new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
//        Fabric.with(getActivity(), new Twitter(authConfig));
//        client = new TwitterAuthClient();

        init(view);
        click_events();
        getdata();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Dashboard.newtweet) {
            Dashboard.newtweet = false;
            //    getdata();
            setdata();
        } else {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }

    }

    private void setdata() {

        if (adaptertype.equals("0")) {
            try {
                if (schduletweetlist.size() != 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    schdulevisibility();
                    adapter = new TweeterAdapter(getActivity(), schduletweetlist, "sch");
                    list_tw.setAdapter(adapter);
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Tweets scheduled");
                }
            } catch (Exception e) {
            }
        } else {

            if (recurringtweetlist.size() != 0) {
                view_dummy.setVisibility(View.GONE);
                view_scheduled.setVisibility(View.VISIBLE);
                recurrvisibility();
                adapter = new TweeterAdapter(getActivity(), recurringtweetlist, "rec");
                list_tw.setAdapter(adapter);
            } else {
                view_dummy.setVisibility(View.VISIBLE);
                view_scheduled.setVisibility(View.GONE);
                dummy_text.setText("You have no recurring Tweets");
            }


        }
    }

    private void getdata() {
        if (!network) {
            Methods.conDialog(getActivity());

        } else {
            if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                view_dummy.setVisibility(View.GONE);
                view_scheduled.setVisibility(View.VISIBLE);


                new tweeter_thread(auth_code).execute();

            } else {
                view_dummy.setVisibility(View.VISIBLE);
                view_scheduled.setVisibility(View.GONE);
                schduletweetlist.clear();
                recurringtweetlist.clear();
                posttweetlist.clear();
            }

        }

    }

    private void init(View rootView) {

        network = Methods.isNetworkConnected(getActivity());
        rl_schdule = (RelativeLayout) rootView.findViewById(R.id.rl_schdule);
        rl_recuring = (RelativeLayout) rootView.findViewById(R.id.rl_recur);
        rl_sent = (RelativeLayout) rootView.findViewById(R.id.rl_sent);
        txt_tab_schdule = (TextView) rootView.findViewById(R.id.txt_schdule);
        txt_tab_recuring = (TextView) rootView.findViewById(R.id.txt_recur);
        txt_tab_sent = (TextView) rootView.findViewById(R.id.txt_sent);

        view_dummy = (RelativeLayout) rootView.findViewById(R.id.lay_dummy);
        view_scheduled = (RelativeLayout) rootView.findViewById(R.id.lay_scheduled);

        btn_compose = (RelativeLayout) rootView.findViewById(R.id.btn_compose);
        txt_btn_compose = (TextView) rootView.findViewById(R.id.txt_btn_compose);
        dummy_text = (TextView) rootView.findViewById(R.id.dummy_text);
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        prefs = getActivity().getSharedPreferences("delayed", getActivity().MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        mLayoutManager = new LinearLayoutManager(getActivity());
        list_tw = (RecyclerView) rootView.findViewById(R.id.list_scheduled);
        list_tw.setLayoutManager(mLayoutManager);
        String twitter_secret = prefs.getString("token_id", null);
        System.out.print("tweet=" + twitter_secret);
        if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
            Dashboard.isTw = true;
            txt_btn_compose.setText("Compose Tweet");
        }

    }

    private void click_events() {
        rl_schdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schdulevisibility();
                inttype = 0;
                if (schduletweetlist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new TweeterAdapter(getActivity(), schduletweetlist, "sch");
                    list_tw.setAdapter(adapter);
                } else {
                    btn_compose.setVisibility(View.VISIBLE);
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Tweets scheduled");
                    if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                        txt_btn_compose.setText("Compose Tweet");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }
                }

            }
        });

        rl_recuring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (FragmentChat.valueTest == true) {
                inttype = 1;
                recurrvisibility();
                if (recurringtweetlist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new TweeterAdapter(getActivity(), recurringtweetlist, "rec");
                    list_tw.setAdapter(adapter);

                } else {
                    btn_compose.setVisibility(View.VISIBLE);
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no recurring Tweets");
                    if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                        txt_btn_compose.setText("Compose Tweet");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }

                }
            }
        });

        rl_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
                rl_schdule.setBackgroundResource(R.drawable.white_left);
                rl_sent.setBackgroundResource(R.drawable.blue_right);
                inttype = 2;
                txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
                txt_tab_sent.setTextColor(Color.parseColor("#FFFFFF"));
                txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
                if (posttweetlist.size() > 0) {
                    view_dummy.setVisibility(View.GONE);
                    view_scheduled.setVisibility(View.VISIBLE);
                    adapter = new TweeterAdapter(getActivity(), posttweetlist, "sent");
                    list_tw.setAdapter(adapter);
                } else {
                    btn_compose.setVisibility(View.INVISIBLE);
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no Tweets sent");
                    if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                        txt_btn_compose.setText("Compose Tweet");
                    } else {
                        txt_btn_compose.setText("Connect");
                    }
                }

            }
        });


        btn_compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getString("twitter_token", null) != null && !prefs.getString("twitter_token", null).equals("null") && prefs.getString("twitter_token", null).length() > 0) {
                    Intent intent = new Intent(getActivity(), NewTweet.class);
                    intent.putExtra("intenttype", "new");
                    startActivity(intent);
                } else {
                    new loginTwitter().execute();
                }
            }
        });
    }

    private void loginToTwitter() {

        final ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(consumerKey);
        builder.setOAuthConsumerSecret(consumerSecret);

        final Configuration configuration = builder.build();
        final TwitterFactory factory = new TwitterFactory(configuration);
        twitter = factory.getInstance();

        try {
            requestToken = twitter.getOAuthRequestToken(callbackUrl);
            final Intent intent = new Intent(getActivity(), WebViewActivity.class);
            intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
            startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            try {
                String verifier = data.getExtras().getString(oAuthVerifier);
                Log.i("Twitter verifier:", "" + verifier);
                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                access_token = accessToken.getToken();
                token_secret = accessToken.getTokenSecret();
                Log.i("Twitter Access Token:", "" + token_secret);
                Dashboard.isTw = true;
                txt_btn_compose.setText("Compose Tweet");
                new Update_tw_details().execute();
            } catch (Exception e) {
                Log.e("Twitter Login Failed", e.getMessage());
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
        // client.onActivityResult(requestCode, resultCode, data);
    }

    //*************************************Tw Async Task Class**************************************
    public class Update_tw_details extends AsyncTask<Void, Void, Void> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = TwUpdate_Method(prefs.getString("auth_code", null), access_token, token_secret);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    txt_btn_compose.setText("Compose Tweet");
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                   // String name = URLDecoder.decode(pin_nos.optString("name"),"UTF-8");
                    editor.putString("name", pin_nos.optString("name"));
                    editor.putString("setting_email", pin_nos.optString("email"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                    editor.commit();
                    getdata();
                } else if(status.equals("false1")){
                    Intent i=new Intent(getActivity(),LogoutAlert.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(i);
                }else {
                    Toast.makeText(getActivity(),"Unable to fetch data, Please try again.", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
            }
        }
    }

    //*************************************Fb details method****************************************
    public String TwUpdate_Method(String auth_code, String twitter_token, String twitter_secret) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("twitter_token", twitter_token)
                .appendQueryParameter("twitter_secret", twitter_secret);
        res = parser.getJSONFromUrl(Utils.update_twitter_detail, builder);
        return res;
    }

    class loginTwitter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            try {
                final Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
            } catch (Exception e) {
                Snackbar.make(btn_compose, "Slow Internet Connection", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    class tweeter_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust, auth_code;

        public tweeter_thread(String auth_code) {
            this.auth_code = auth_code;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            schedule_response = Gettweet_Method("twitter_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                schduletweetlist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    schduletweetlist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        schduletweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //String post = URLDecoder.decode(iobj.optString("twitter_post"));
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            schduletweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Gettweet_Method("twitter_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                recurringtweetlist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    recurringtweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        recurringtweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String post = URLDecoder.decode(iobj.optString("twitter_post"));
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            recurringtweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Gettweet_Method("get_twitter_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                posttweetlist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    ModelTwitterData fbvalue = new ModelTwitterData();
                    fbvalue.tw_name = prefs.getString("twitter_hname", null);
                    fbvalue.tw_email = prefs.getString("twitter_uname", null);
                    fbvalue.view_type = "tweet";
                    posttweetlist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelTwitterData value = new ModelTwitterData();
                        value.header_date = key;
                        value.view_type = "header";
                        posttweetlist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelTwitterData item = new ModelTwitterData();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String post = URLDecoder.decode(iobj.optString("twitter_post"));
                            item.twitter_post = iobj.optString("twitter_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.twitter_id = iobj.optString("twitter_id");

                            item.view_type = "values";
                            posttweetlist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);

            if (adaptertype.equals("0")) {
                try {
                    if (statuss.equals("true")) {
                        if (schduletweetlist.size() != 0) {
                            view_dummy.setVisibility(View.GONE);
                            view_scheduled.setVisibility(View.VISIBLE);
                            schdulevisibility();
                            adapter = new TweeterAdapter(getActivity(), schduletweetlist, "sch");
                            list_tw.setAdapter(adapter);
                        } else {
                            view_dummy.setVisibility(View.VISIBLE);
                            view_scheduled.setVisibility(View.GONE);
                            dummy_text.setText("You have no Tweets scheduled");
                        }
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no Tweets scheduled");
                    }
                } catch (Exception e) {
                }
            } else {

                if (statusr.equals("true")) {
                    if (recurringtweetlist.size() != 0) {
                        view_dummy.setVisibility(View.GONE);
                        view_scheduled.setVisibility(View.VISIBLE);
                        recurrvisibility();
                        adapter = new TweeterAdapter(getActivity(), recurringtweetlist, "rec");
                        list_tw.setAdapter(adapter);
                    } else {
                        view_dummy.setVisibility(View.VISIBLE);
                        view_scheduled.setVisibility(View.GONE);
                        dummy_text.setText("You have no recurring Tweets");
                    }
                } else {
                    view_dummy.setVisibility(View.VISIBLE);
                    view_scheduled.setVisibility(View.GONE);
                    dummy_text.setText("You have no recurring Tweets");
                }

            }


        }
    }

    public String Gettweet_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    private void recurrvisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#376a9d"));
        rl_schdule.setBackgroundResource(R.drawable.white_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);


        txt_tab_recuring.setTextColor(Color.parseColor("#FFFFFF"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#376a9d"));
    }

    private void schdulevisibility() {
        rl_recuring.setBackgroundColor(Color.parseColor("#ffffff"));
        rl_schdule.setBackgroundResource(R.drawable.blue_left);
        rl_sent.setBackgroundResource(R.drawable.white_right);

        txt_tab_recuring.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_sent.setTextColor(Color.parseColor("#376a9d"));
        txt_tab_schdule.setTextColor(Color.parseColor("#FFFFFF"));
    }
}
