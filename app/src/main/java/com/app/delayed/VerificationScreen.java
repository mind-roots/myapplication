package com.app.delayed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.delayed.model.JSONParser;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;

import me.philio.pinentry.PinEntryView;

/**
 * Created by Balvinder on 1/29/2016.
 */
public class VerificationScreen extends AppCompatActivity {

    Toolbar toolbar;
    TextView title, here;

    ProgressBar progressBar;
    SharedPreferences prefs, instal;
    String instalvalue;
    SharedPreferences.Editor editor;
    String pin_nos;
    PinEntryView pinentry;
    boolean b;
    String auth_code;
    String token_id;
    String number, country_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_screen);

        init();
        ActivityStack.activity.add(VerificationScreen.this);
        onCliks();
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                token_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + token_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });
    }

    private void onCliks() {
        here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new resend_verification().execute();
            }
        });
        pinentry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();

                if (length == 4) {
                    HideKayboard();

                    if (!b) {
                        Methods.conDialog(VerificationScreen.this);

                    } else {
                        new getpin().execute();

                    }

                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next, menu);
        menu.findItem(R.id.action_next).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

            finish();
            return true;
        }
        if (id == R.id.action_next) {
            HideKayboard();

            if (!b) {
                Methods.conDialog(VerificationScreen.this);

            } else {
                new getpin().execute();
                //  Toast.makeText(getApplicationContext(),pinentry.getText().toString(),Toast.LENGTH_SHORT).show();
            }


            return true;
        } else {

        }

        return super.onOptionsItemSelected(item);
    }


    private void init() {

        prefs = getSharedPreferences("delayed", MODE_PRIVATE);

        instal = getSharedPreferences("install", VerificationScreen.this.MODE_PRIVATE);
        instalvalue = instal.getString("instalvalue", null);
        b = Methods.isNetworkConnected(VerificationScreen.this);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        pinentry = (PinEntryView) findViewById(R.id.pin_view);
        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("Verification");
        setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        number = getIntent().getStringExtra("number");
        country_id = getIntent().getStringExtra("country_id");
        here = (TextView) findViewById(R.id.here);
    }


    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //********************************************resend_verification parsing***********************

    public class resend_verification extends AsyncTask {
        String status = "false", result;

        String service_type = "resend_verification_v2";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = R_Verify_Method(service_type);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");

                if (status.equals("true")) {

                    pin_nos = obj.getString("pin_nos");
                    editor = prefs.edit();
                    editor.putString("pin_nos", pin_nos);
                    editor.commit();

                } else {

                    result = obj.getString("result");

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);

            if (status.equals("true")) {

//                startActivity(new Intent(VerificationScreen.this, VerificationScreen.class));
            } else {
                Snackbar.make(pinentry, "Responce Error", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
    //************************Webservice parameter RegisterMethod***********************************

    public String R_Verify_Method(String service_type) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("country_id", country_id)
                .appendQueryParameter("number", number);
               // .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }


    //********************************************verifyno parsing**********************************
    public class verifyno extends AsyncTask {
        String status = "false";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = Verifyno_Method(prefs.getString("pin_nos", null));
            Log.i("Verification res: ", response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");
                editor = prefs.edit();
                editor.putString("auth_code", obj.optString("auth_code"));
                editor.commit();

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);
//
            if (status.equals("true")) {

                if (prefs.getString("old_user", null).equals("yes")) {
                    new getprofile().execute();
                    Intent i = new Intent(VerificationScreen.this, LoadContactsScreen.class);
                    startActivity(i);
                   /* if (instalvalue != null) {
                        Intent i = new Intent(VerificationScreen.this, LoadContactsScreen.class);
                        startActivity(i);

                    } else {
                        Intent i = new Intent(VerificationScreen.this, ViewpagerActivity.class);
                        startActivity(i);

                    }*/
                } else {
                    Intent intent = new Intent(VerificationScreen.this, ProfileScreen.class);
                    intent.putExtra("Intenttype", "new");
                    startActivity(intent);
                }

            } else {
                Snackbar.make(pinentry, "Response Error " + status, Snackbar.LENGTH_SHORT).show();

            }
        }
    }

    //************************Webservice parameter RegisterMethod***********************************

    public String Verifyno_Method( String pin_nos) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "verify_no_v2")
             //   .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("country_id", country_id)
                .appendQueryParameter("number", number)
                .appendQueryParameter("pin", pin_nos)
                .appendQueryParameter("token_id", token_id)
                .appendQueryParameter("token_type", "onesignal");
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    //********************************************getpin parsing*************************************
    public class getpin extends AsyncTask {
        String status = "false", result, pin_nos;

        String service_type = "get_pin_v2";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = Getpin_Method(service_type);
            Log.i("PIN res: ", response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");
                if (status.equals("true")) {
                    pin_nos = obj.getString("pin_nos");
                    editor = prefs.edit();
                    editor.putString("pin_nos", pin_nos);
                    editor.commit();
                }else {
                    result = obj.getString("error");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);

            if (status.equals("true")) {
                if (prefs.getString("pin_nos", null).equals(pinentry.getText().toString())) {
                    editor = prefs.edit();
                    editor.putString("pin_match", pin_nos);
                    editor.commit();
                   // new getprofile().execute();
                    new verifyno().execute();

                } else {
                    Snackbar.make(pinentry, "The PIN entered is incorrect. \nPlease retry or generate another PIN", Snackbar.LENGTH_SHORT).show();
                }
            } else {
                Snackbar.make(pinentry, "" + result, Snackbar.LENGTH_SHORT).show();

            }
        }
    }


    //************************Webservice parameter RegisterMethod***********************************

    public String Getpin_Method(String service_type) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("country_id", country_id)
                .appendQueryParameter("number", number);
                //.appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    class getprofile extends AsyncTask<String, Void, String> {
        String response;

        @Override
        protected String doInBackground(String... params) {
            JSONParser parser = new JSONParser();
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "get_profile")
                    .appendQueryParameter("auth_code", prefs.getString("auth_code", null));
            response = parser.getJSONFromUrl(Utils.base_url, builder);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            updateprefs(response);
        }
    }

    public void updateprefs(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            String status = obj.getString("status");
            if (status.equals("true")) {
                JSONObject pin_nos = obj.optJSONObject("pin_nos");
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("id", pin_nos.optString("id"));
                editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                editor.putString("country_id", pin_nos.optString("country_id"));
                editor.putString("country", pin_nos.optString("country"));
                editor.putString("fb_status", pin_nos.optString("status"));
                editor.putString("pin", pin_nos.optString("pin"));
                String name = URLDecoder.decode(pin_nos.optString("name"), "UTF-8");
                editor.putString("name",name);
                editor.putString("setting_email", pin_nos.optString("email"));
                editor.putString("fb_name", pin_nos.optString("fb_name"));
                editor.putString("fb_email", pin_nos.optString("fb_email"));
                editor.putString("fb_token", pin_nos.optString("fb_token"));
                editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                editor.putString("credits", pin_nos.optString("credits"));
                editor.putString("flag", pin_nos.optString("flag"));
                editor.putString("auth_code", pin_nos.optString("auth_code"));
                editor.putString("token_id", pin_nos.optString("token_id"));
                editor.putString("token_type", pin_nos.optString("token_type"));
                editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                editor.putString("last_updated", pin_nos.optString("last_updated"));
                editor.putString("timeformat", "12");
                editor.commit();

            } else {
                //   Snackbar.make(pinentry, "Response Error", Snackbar.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }
}
