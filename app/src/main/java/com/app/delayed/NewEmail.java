package com.app.delayed;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.delayed.adapters.ContactPickerAdapter;
import com.app.delayed.customedittext.ContactsCompletionView;
import com.app.delayed.customedittext.TokenCompleteTextView;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentEmail;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleEmail;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Person;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Balvinder on 1/30/2016.
 */
public class NewEmail extends AppCompatActivity implements TokenCompleteTextView.TokenListener {

    Toolbar toolbar;
    LinearLayout lay_bottom;
    EditText edt_time, edt_recurring, edt_subject, edt_compose_email, edt_from;
    TextView mTitle;
    int curday, curmont, curyear, curhour, curmin;
    int rec_id;
    Calendar c, c1;
    String recu_type = null, auth_code, setting_email;
    RelativeLayout btn_schedule;
    TextView mSchText;
    boolean network;
    public static ContactsCompletionView completionView_to, completionView_cc;
    StringBuilder sb_cc, sb_to;
    String intenttype, message, subject, to_id, cc_id, from_id, schedule_date, email_id;
    CharSequence[] items = {"Non recurring", "Recur daily", "Recur weekdays", "Recur weekly", "Recur forthnightly", "Recur monthly", "Recur yearly"};
    String[] sentitems = {"0", "d", "wd", "w", "fn", "m", "y"};
    // ContactPickerAdapter adapter;
    boolean valid = false;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ContactPickerAdapter contactadapter, contactadapter2;
    Methods method;
    JSONParser parser;
    int TIME_PICKER_INTERVAL = 5;
    boolean hourstype = false;
    int style, compareday, comparemonth;
    boolean setr = false;
    int uphour, upmin, upday, upmonth, upyear;
    ImageView mTimeArrow, mSchArrow;
    ProgressDialog dialog;
    String cc_email, to_email;
    DatabaseQueries queries;
    ArrayList<Person> userlist = new ArrayList<>();
    private boolean isFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_email);
        queries = new DatabaseQueries(NewEmail.this);
        getemails();
        init();

        onClicks();
    }

    private void getemails() {

        userlist = queries.getemail();
    }


    //************Innitialize UI elements**************
    private void init() {
        parser = new JSONParser();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        btn_schedule = (RelativeLayout) findViewById(R.id.btn_schedule);
        setTitle("");
        mTitle.setText("New Email");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        c = Calendar.getInstance();
        c1 = Calendar.getInstance();
        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);

        setting_email = prefs.getString("setting_email", null);
        if (prefs.getString("timeformat", null).equals("24")) {
            hourstype = true;
        } else {
            hourstype = false;
        }
        network = Methods.isNetworkConnected(NewEmail.this);

        edt_time = (EditText) findViewById(R.id.edt_set_time);
        edt_recurring = (EditText) findViewById(R.id.edt_set_recurring);
        edt_subject = (EditText) findViewById(R.id.edt_subject);
        edt_compose_email = (EditText) findViewById(R.id.edt_compose_email);
        mTimeArrow = (ImageView) findViewById(R.id.arrow_time);
        mSchArrow = (ImageView) findViewById(R.id.arrow_sch);
        mSchText = (TextView) findViewById(R.id.sch_text);


        completionView_cc = (ContactsCompletionView) findViewById(R.id.edt_cc);
        completionView_cc.setTokenListener(this);
        completionView_cc
                .setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);

        edt_from = (EditText) findViewById(R.id.edt_from);


        completionView_to = (ContactsCompletionView) findViewById(R.id.edt_to);
        completionView_to.setTokenListener(this);
        completionView_to
                .setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);


        lay_bottom = (LinearLayout) findViewById(R.id.lay_bottom);

        method = new Methods(NewEmail.this, edt_time);

        getset();


    }

    private void getset() {


        if (setting_email != null || setting_email.length() != 0) {
            edt_from.setText(setting_email);
            // edt_from.setEnabled(false);
        }
        Intent intent = getIntent();


        intenttype = intent.getStringExtra("intenttype");
        if (intenttype.equals("update") || intenttype.equals("nonedit")) {
            setTitle("");
            mTitle.setText("Update Email");
            mSchText.setText("Update");
            message = intent.getStringExtra("message");
            subject = intent.getStringExtra("subject");
            to_id = intent.getStringExtra("to_id");
            cc_id = intent.getStringExtra("cc_id");
            from_id = intent.getStringExtra("from_id");
            recu_type = intent.getStringExtra("recurring_type");
            schedule_date = intent.getStringExtra("schedule_date");
            email_id = intent.getStringExtra("email_id");
            edt_subject.setText(subject);
            edt_time.setText(setformater(schedule_date));
            edt_compose_email.setText(message);
            edt_from.setText(from_id);

            if (cc_id.length() > 1) {
                String[] ccstr = cc_id.split(",");

                for (String strn : ccstr) {
                    Person item = new Person(strn, strn);
                    completionView_cc.addObject(item, "");

                }

            }
            String[] tostr = to_id.split(",");
            for (String str : tostr) {
                Person item = new Person(str, str);
                completionView_to.addObject(item, "");

            }
           /* if (!set_groupname.equals("") || set_groupname.length() != 0) {
                edt_name.setText(set_groupname);
            } else {
                edt_name.setText(set_name);
            }
*/
//            if (recu_type.equals("0")) {
//                rec_id = 0;
//                edt_recurring.setText("Non Recurring");
//            } else if (recu_type.equals("fn")) {
//                rec_id = 1;
//                edt_recurring.setText("Recurring forthnightly");
//            } else if (recu_type.equals("wd")) {
//                rec_id = 2;
//                edt_recurring.setText("Recurring weekdays");
//            } else if (recu_type.equals("d")) {
//                rec_id = 3;
//                edt_recurring.setText("Recurring Daily");
//            } else if (recu_type.equals("w")) {
//                rec_id = 4;
//                edt_recurring.setText("Recurring Weekly");
//            } else if (recu_type.equals("m")) {
//                rec_id = 5;
//                edt_recurring.setText("Recurring Monthly");
//            } else if (recu_type.equals("y")) {
//                rec_id = 6;
//                edt_recurring.setText("Recurring Yearly");
//            }
            if (recu_type.equals("0")) {
                rec_id = 0;
                edt_recurring.setText("Non Recurring");
            } else if (recu_type.equals("d")) {
                rec_id = 1;
                edt_recurring.setText("Recur Daily");
            } else if (recu_type.equals("wd")) {
                rec_id = 2;
                edt_recurring.setText("Recur weekdays");
            } else if (recu_type.equals("w")) {
                rec_id = 3;
                edt_recurring.setText("Recur weekly");
            } else if (recu_type.equals("fn")) {
                rec_id = 4;
                edt_recurring.setText("Recur forthnightly");
            } else if (recu_type.equals("m")) {
                rec_id = 5;
                edt_recurring.setText("Recur Monthly");
            } else if (recu_type.equals("y")) {
                rec_id = 6;
                edt_recurring.setText("Recur Yearly");
            }

            if (intenttype.equals("nonedit")) {
                edt_recurring.setText("Sent");
                btn_schedule.setVisibility(View.INVISIBLE);
                completionView_cc.setEnabled(false);
                completionView_to.setEnabled(false);
                edt_subject.setTextColor(Color.BLACK);
                edt_subject.setEnabled(false);
                edt_from.setEnabled(false);
                edt_compose_email.setEnabled(false);
                edt_compose_email.setText(Html.fromHtml(message));
                edt_compose_email.setTextColor(Color.BLACK);
                mTitle.setText("Sent Email");

                mTimeArrow.setVisibility(View.INVISIBLE);
                mSchArrow.setVisibility(View.INVISIBLE);

            }
        } else {
            //  edt_name.setText("");
            recu_type = "0";
            edt_recurring.setText("Non recurring");
            setTitle("");
            mTitle.setText("New Email");
            mSchText.setText("Schedule");
        }

        if (userlist.size() != 0) {

            contactadapter = new ContactPickerAdapter(getApplicationContext(),
                    android.R.layout.simple_list_item_1, userlist);
            contactadapter2 = new ContactPickerAdapter(getApplicationContext(),
                    android.R.layout.simple_list_item_1, userlist);
            completionView_to.setAdapter(contactadapter);
            completionView_cc.setAdapter(contactadapter2);


        } else {

        }
    }

    private void onClicks() {

        edt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
              /*  hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mYear = c.get(Calendar.YEAR);

                if (Build.VERSION.SDK_INT < 21) {
                    method.getdate(NewEmail.this, mYear, mMonth, mDay, hour, minute);
                } else {
                    builder = new StringBuilder();
                    Methods.datePicker(NewEmail.this, datePickerListener, curyear, curmont, curday);
                }*/
                int setday, setmonth, setyear;
                if (intenttype.equals("update") || intenttype.equals("nonedit")) {
                    updatevalues(edt_time.getText().toString());
                    setday = upday;
                    setmonth = upmonth;
                    setyear = upyear;
                } else {
                    setday = curday;
                    setmonth = curmont;
                    setyear = curyear;
                }
                if (!intenttype.equals("nonedit")) {
                    if (Build.VERSION.SDK_INT < 21) {
                        style = android.graphics.Color.TRANSPARENT;
                        datePickerbuildbelow(datePickerListener2, setyear, setmonth, setday);
                    } else {
                        style = R.style.DialogTheme1;
                        datePicker(datePickerListener1, setyear, setmonth, setday);
                    }
                }
            }
        });


        edt_recurring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                String to = completionView_to.getText().toString();
                String cc = completionView_cc.getText().toString();
                if (!intenttype.equals("nonedit")) {
                    RecurringPicker(NewEmail.this, edt_recurring);
                }
            }
        });


        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // String sentdate = sentformatter(edt_time.getText().toString());
                //    Toast.makeText(NewEmail.this, sb_to.toString(), Toast.LENGTH_SHORT).show();
                checks();

            }
        });


        edt_compose_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NewEmail.this, EmailTextArea.class);
                in.putExtra("text", edt_compose_email.getText().toString());
                startActivityForResult(in, 1);
            }
        });

    }


    private void checks() {
        hidekeyboard();
        updateTokenConfirmation();
      /*  if (!edt_from.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            //  Snackbar.make(edt_compose_email, "Please enter valid Email-Id", Snackbar.LENGTH_SHORT).show();
            edt_from.setError("Invalid Email Address");
        }*/
        if (completionView_to.getText().toString().length() == 0) {
            Snackbar.make(completionView_to, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_from.getText().toString().length() == 0) {
            Snackbar.make(edt_from, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_time.getText().toString().length() == 0) {
            Snackbar.make(edt_time, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_recurring.getText().toString().length() == 0) {
            Snackbar.make(edt_recurring, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_subject.getText().toString().length() == 0) {
            Snackbar.make(edt_subject, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_compose_email.getText().toString().length() == 0) {
            Snackbar.make(edt_compose_email, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (!edt_from.getText().toString().contains("@")) {
            //  Snackbar.make(edt_compose_email, "Please enter valid Email-Id", Snackbar.LENGTH_SHORT).show();
            edt_from.setError("Invalid Email Address");
        } else {
            if (setting_email == null || setting_email.length() == 0) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("setting_email", edt_from.getText().toString());
                editor.commit();
            }

            if (!network) {
                Methods.conDialog(NewEmail.this);

            } else {
                String sentdate = sentformatter(edt_time.getText().toString(), 0);
                String trigdate = sentformatter(edt_time.getText().toString(), 1);
                new Schedule_email(sentdate, trigdate).execute();

            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /*  private TimePickerDialog.OnTimeSetListener timePickerListener =
              new TimePickerDialog.OnTimeSetListener() {
                  int ti = 0;

                  public void onTimeSet(TimePicker view, int selectedHour,
                                        int selectedMinute) {

                      if (selectedHour < curhour && valid) {
                          edt_time.setText("");
                          Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                      } else if (selectedMinute < curmin && valid) {
                          edt_time.setText("");
                          Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                      } else {

                          int set = selectedMinute;
                        *//*  if (selectedMinute > 0 && selectedMinute <= 7 || selectedMinute > 53 && selectedMinute <= 59) {
                            set = 0;
                        } else if (selectedMinute > 7 && selectedMinute <= 15 || selectedMinute > 15 && selectedMinute <= 23) {
                            set = 15;
                        } else if (selectedMinute > 23 && selectedMinute <= 30 || selectedMinute > 30 && selectedMinute <= 37) {
                            set = 30;
                        } else if (selectedMinute > 37 && selectedMinute <= 45 || selectedMinute > 45 && selectedMinute <= 53) {
                            set = 45;
                        } *//*
                        if (selectedMinute <= 15) {
                            set = 0;
                        } else if (selectedMinute <= 30) {
                            set = 15;
                        } else if (selectedMinute <= 45) {
                            set = 30;
                        } else if (selectedMinute <= 59) {
                            set = 45;
                        }

                        hour = selectedHour;
                        minute = set;

                        c.set(Calendar.HOUR_OF_DAY, hour);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                        String displayValue = timeFormatter.format(c.getTime());
                        System.out.println("value time : " + displayValue);

                        recu_type = "0";
                        rec_id = 0;
                        edt_recurring.setText("Non recurring");
                        edt_time.setText(builder.append(", " + displayValue));
                    }

                }

            };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (year < curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                c.set(Calendar.YEAR, mYear);
                c.set(Calendar.MONTH, mMonth);
                c.set(Calendar.DAY_OF_MONTH, mDay);
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy");
                String dateSet = format1.format(c.getTime());
                //dateSet = DateFormat.getDateInstance().format(c.getTime());
                System.out.println("value date : " + dateSet);
//            builder.append((dayOfMonth))
//                    .append(" ").append((monthOfYear)).append(", ").append((year));

                builder.append(dateSet);

                Methods.timePicker(NewEmail.this, timePickerListener, curhour, curmin);
            }
        }
    };
*/
    //*************RecurringPicker***********
    private void RecurringPicker(Context ctx, final EditText recurring) {

        // int selected_rec=0;

        new AlertDialog.Builder(ctx, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, rec_id, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        rec_id = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button
                        recu_type = sentitems[rec_id];
                        recurring.setText(items[rec_id]);
                        //  edt_recurring.setText(recurring.getText().toString());
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();


        // return selected_rec;
    }

    public String sentformatter(String sendtime, int i) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1;
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            Date newDate = format1.parse(sendtime);
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (i == 0) {
                settym = format2.format(newDate);
            } else {
                format2.setTimeZone(TimeZone.getTimeZone("UTC"));
                settym = format2.format(newDate);
            }
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }


    @Override
    public void onTokenAdded(Object token) {
        updateTokenConfirmation();
    }

    @Override
    public void onTokenRemoved(Object token) {
        updateTokenConfirmation();
    }

    private void updateTokenConfirmation() {

        sb_cc = new StringBuilder();
        for (Object token : completionView_cc.getObjects()) {
            if (!queries.emailexist(token.toString())) {
                queries.insertemail(token.toString(), token.toString());
            }
            sb_cc.append(token.toString());
            sb_cc.append(",");
        }
        cc_email = sb_cc.toString();
        if (cc_email.indexOf(",") != -1) {
            cc_email = cc_email.substring(0, cc_email.length() - 1);
        }

        sb_to = new StringBuilder();
        for (Object token : completionView_to.getObjects()) {
            if (!queries.emailexist(token.toString())) {
                queries.insertemail(token.toString(), token.toString());
            }
            sb_to.append(token.toString());
            sb_to.append(",");
        }
        to_email = sb_to.toString();
        if (to_email.indexOf(",") != -1) {
            to_email = to_email.substring(0, to_email.length() - 1);
        }
        if (cc_email.length() == 0) {
            cc_email = completionView_cc.getText().toString();
            if (!queries.emailexist(cc_email)) {
                queries.insertemail(cc_email, cc_email);
            }
        }
        if (to_email.length() == 0) {
            to_email = completionView_to.getText().toString();
            if (!queries.emailexist(to_email)) {
                queries.insertemail(to_email, to_email);
            }
        }
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = NewEmail.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) NewEmail.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public class Schedule_email extends AsyncTask<Void, Void, Void> {

        String response, sentdate, trigdate, email_text, email_subject_text, reccuring_text;
        String semail;

        public Schedule_email(String sentdate, String trigdate) {
            this.sentdate = sentdate;
            this.trigdate = trigdate;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(NewEmail.this);
            dialog.setMessage("Scheduling email....");
            dialog.show();
            semail = prefs.getString("setting_email", "");

        }

        @Override
        protected Void doInBackground(Void... params) {
            if (intenttype.equals("update")) {
                response = update_schdule_method("update_email", sentdate, trigdate, semail);
            } else {
                response = schdule_method("create_email", sentdate, trigdate, semail);
            }

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("service_type", "update_profile_email")
                    .appendQueryParameter("email", semail)
                    .appendQueryParameter("auth_code", auth_code);
            String res = parser.getJSONFromUrl(Utils.base_url, builder);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                 /*   if (setting_email == null) {
                        editor = prefs.edit();
                        editor.putString("setting_email", edt_from.getText().toString());
                        editor.commit();
                    }*/
                    FragmentEmail.adaptertype = recu_type;

                    new getEmail().execute();
                } else {
                    dialog.dismiss();
                    Snackbar.make(btn_schedule, "Network Error", Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            }

        }
    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(NewEmail.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Dashboard.newemail = true;
                        finish();

                    }
                })

                .setIcon(icon)
                .show();

    }

    public String setformater(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }


    //------------update values-----------------//
    public void updatevalues(String sendtime) {

        Date newDate;
        try {


            if (hourstype) {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                newDate = format1.parse(sendtime);
            } else {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                newDate = format1.parse(sendtime);
            }


         /*   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);*/
            Calendar call = Calendar.getInstance();
            call.setTime(newDate);

            uphour = call.get(Calendar.HOUR_OF_DAY);
            upmin = call.get(Calendar.MINUTE);
            upmonth = call.get(Calendar.MONTH);
            upday = call.get(Calendar.DAY_OF_MONTH);
            upyear = call.get(Calendar.YEAR);
        } catch (Exception e) {

        }


    }

    public String schdule_method(String service_type, String e_date, String t_date, String from_id) {
        String res = null;
        String s = "", sub = "";
        try {
            s = URLEncoder.encode(edt_compose_email.getText().toString(), "UTF-8");
            sub = URLEncoder.encode(edt_subject.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("e_date", e_date)
                .appendQueryParameter("trigger_date", t_date)
                .appendQueryParameter("to_id", to_email)
                .appendQueryParameter("cc_id", cc_email)
                .appendQueryParameter("from_id", edt_from.getText().toString())
                .appendQueryParameter("email_text", s)
                .appendQueryParameter("email_subject", sub)
                .appendQueryParameter("recu_type", recu_type);

        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String update_schdule_method(String service_type, String e_date, String t_date, String from_id) {
        String res = null;
        String s = "", sub = "";
        try {
            s = URLEncoder.encode(edt_compose_email.getText().toString(), "UTF-8");
            sub = URLEncoder.encode(edt_subject.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("email_id", email_id)
                .appendQueryParameter("e_date", e_date)
                .appendQueryParameter("trigger_date", t_date)
                .appendQueryParameter("schedule_date", e_date)
                .appendQueryParameter("to_id", to_email)
                .appendQueryParameter("cc_id", cc_email)
                .appendQueryParameter("from_id", edt_from.getText().toString())
                .appendQueryParameter("text", s)
                .appendQueryParameter("subject", sub)
                .appendQueryParameter("recurring_type", recu_type);

        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    //-------------------------time and date dialogss-------------------------//

    /***************************************
     * Date picker
     *************************************/
    public void datePicker(DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(NewEmail.this, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }

    public void datePickerbuildbelow(DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(NewEmail.this, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
              /*  if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();


            }
        });

        dialog.show();

    }

    public DatePickerDialog.OnDateSetListener datePickerListener2 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c1.set(Calendar.YEAR, mYear);
            c1.set(Calendar.MONTH, mMonth);
            c1.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;


        }
    };

    public void timePickerbelow() {


        isFirst = true;
        int setmin, sethour;
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        if ((intenttype.equals("update") || intenttype.equals("nonedit")) && compareday != day) {
            setmin = upmin;
            sethour = uphour;

        } else {
            setmin = minute;
            sethour = hour;
            if (setmin >= 55 && setmin <= 59) {
                setmin = 0;
                sethour = sethour + 1;
            } else {
                setmin = setmin + 5;
            }

        }

        if (minute >= 55 && minute <= 59) {
            minute = 0;
            hour = hour + 1;
        } else {
            minute = minute + 5;
        }


        final TimePickerDialogs timeee;

        timeee = new TimePickerDialogs(NewEmail.this, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                c1.set(Calendar.HOUR_OF_DAY, selectedHour);
                if (Build.VERSION.SDK_INT < 21) {
                    if (isFirst){
                        isFirst = false;
                        c1.set(Calendar.MINUTE, min);
                    }

                } else {
                    c1.set(Calendar.MINUTE, min);
                }
                SimpleDateFormat timeFormatter;
                if (hourstype) {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                } else {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                }
                String displayValue = timeFormatter.format(c1.getTime());
                edt_time.setText(displayValue);


            }
        }, sethour, setmin, hourstype);
        timeee.setMin(hour, minute);
        timeee.show();

    }

    //********************************
// Date change Listener
//*********************************
    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (year < curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
               /* mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;*/

                c1.set(Calendar.YEAR, year);
                c1.set(Calendar.MONTH, monthOfYear);
                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                compareday = dayOfMonth;
                comparemonth = monthOfYear;
                /*if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();
                //   timePicker(ChattingActivity.this, timePickerListener1, curhour, curmin);


            }
        }
    };

    public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        private int minHour = -1;
        private int minMinute = -1;

        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private java.text.DateFormat dateFormat;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
//            this.hr = hourOfDay;
//            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            //    ct = Calendar.getInstance();
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT);

        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);


            boolean validTime = true;
            if (hourOfDay < minHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == minHour && (minute - 1) * 5 < minMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }

            if (hourOfDay > maxHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == maxHour && minute * 5 > maxMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }
            // if (hourOfDay==minHour&&)

            if (validTime) {
                currentHour = hourOfDay;

                currentMinute = minute;
            } else {
                int j = minMinute / 5;
                int r = minMinute % 5;
                if (r > 3) {
                    j = j + 1;
                }
                if (j < minute) {
                    currentMinute = minute;

                } else {
                    currentMinute = j;

                }

            }


            timePicker.setCurrentHour(currentHour);


            timePicker.setCurrentMinute(currentMinute);


        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            //  System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
                timePicker.setOnTimeChangedListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            edt_compose_email.setText(data.getStringExtra("text"));
        }

    }

    class getEmail extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, email_response, status_schedule, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_email----------------------//

            schedule_response = Getsms_Method("get_schedule_email", auth_code);
            System.out.println("schedule response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(schedule_response);
                status_schedule = jobj.optString("status");
                FragmentEmail.schduleemaillist.clear();
                if (status_schedule.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iterator = obj.keys();
                    while (iterator.hasNext()) {

                        String key = iterator.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.schduleemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //  String msg = URLDecoder.decode(iobj.optString("message"), "UTF-8");
                            item.message = iobj.optString("message");
                            //  String sub = URLDecoder.decode(iobj.optString("subject"), "UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.schduleemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            //   }
            //--------------------get_recurring_email----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_email", auth_code);
            System.out.println("recurring response:: " + schedule_response);
            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentEmail.recurringemaillist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.recurringemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //  String msg = URLDecoder.decode(iobj.optString("message"), "UTF-8");
                            item.message = iobj.optString("message");
                            // String sub = URLDecoder.decode(iobj.optString("subject"), "UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.recurringemaillist.add(item);

                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_email-------------------------------//
            email_response = Getsms_Method("get_email", auth_code);
            System.out.println("email response:: " + schedule_response);

            try {
                JSONObject jobj = new JSONObject(email_response);
                statust = jobj.optString("status");
                FragmentEmail.sentemaillist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleEmail value = new ModelScheduleEmail();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentEmail.sentemaillist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleEmail item = new ModelScheduleEmail();
                            JSONObject iobj = jarr.optJSONObject(i);
                            //  String msg = URLDecoder.decode(iobj.optString("message"), "UTF-8");
                            item.message = iobj.optString("message");
                            //String sub = URLDecoder.decode(iobj.optString("subject"), "UTF-8");
                            item.subject = iobj.optString("subject");
                            item.to_id = iobj.optString("to_id");
                            item.cc_id = iobj.optString("cc_id");
                            item.from_id = iobj.optString("from_id");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.email_id = iobj.optString("email_id");
                            item.view_type = "values";
                            FragmentEmail.sentemaillist.add(item);

                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (intenttype.equals("update")) {
                dialog("Success!", "Your email has been updated", android.R.drawable.ic_dialog_info);
            } else {
                dialog("Success!", "Your email has been scheduled", android.R.drawable.ic_dialog_info);
            }
            dialog.dismiss();


        }
    }

    public String Getsms_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}
