package com.app.delayed;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.delayed.adapters.Facebook_Adapter;
import com.app.delayed.cropimage.CropActivityP;
import com.app.delayed.fragments.FragmentFacebook;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.Model_facebook;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Balvinder on 2/1/2016.
 */
public class NewFacebook extends AppCompatActivity {

    Toolbar toolbar;
    TextView title;
    RelativeLayout btn_schedule;
    TextView mSchText;
    EditText edt_message, edt_time, edt_recurring, name;

    int curday, curmont, curyear, curhour, curmin;
    ImageView add_contact;
    int rec_id;
    Calendar c, c1;
    String image_path;
    LinearLayout get_img;
    TextView fb_name, fb_email;
    public static ImageView set_img;
    SharedPreferences prefs;
    boolean setr = false;
    String intenttype, auth_code;
    String schedule_date, recurring_type, facebook_post, facebook_image, fb_id;
    ProgressBar progressbar, img_progress;
    CharSequence[] items = {"Non recurring", "Recur daily", "Recur weekdays", "Recur weekly", "Recur forthnightly", "Recur monthly", "Recur yearly"};
    String[] sentitems = {"0", "d", "wd", "w", "fn", "m", "y"};
    boolean network;
    boolean valid = true;
    Methods method;
    int TIME_PICKER_INTERVAL = 5;
    boolean hourstype = false;
    int style, compareday, comparemonth;
    int uphour, upmin, upday, upmonth, upyear;
    ImageView mTimeArrow, mSchArrow;
    private boolean isFirst;
    String access_token;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_facebook);
        Utils.change_image = false;
        Utils.pimageUri = null;
        init();
        onclicks();


    }

    //************Innitialize UI elements**************
    private void init() {
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        network = Methods.isNetworkConnected(NewFacebook.this);
        btn_schedule = (RelativeLayout) findViewById(R.id.btn_schedule);
        set_img = (ImageView) findViewById(R.id.set_img);
        mTimeArrow = (ImageView) findViewById(R.id.arrow_time);
        mSchArrow = (ImageView) findViewById(R.id.arrow_sch);
        get_img = (LinearLayout) findViewById(R.id.get_img);
        fb_name = (TextView) findViewById(R.id.fb_name);
        mSchText = (TextView) findViewById(R.id.sch_text);
        fb_email = (TextView) findViewById(R.id.fb_email);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        img_progress = (ProgressBar) findViewById(R.id.img_progress);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        if (prefs.getString("timeformat", null).equals("24")) {
            hourstype = true;
        } else {
            hourstype = false;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        c = Calendar.getInstance();
        c1 = Calendar.getInstance();
        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);

        edt_message = (EditText) findViewById(R.id.message);
        edt_time = (EditText) findViewById(R.id.set_time);
        edt_recurring = (EditText) findViewById(R.id.set_recurring);

        fb_name.setText("" + (prefs.getString("fb_name", null)));
        fb_email.setText("" + (prefs.getString("fb_email", null)));
        Intent intent = getIntent();
        intenttype = intent.getStringExtra("intenttype");

        callbackManager = CallbackManager.Factory.create();

        if (intenttype.equals("update") || intenttype.equals("nonedit")) {
            setTitle("");
            title.setText("Update Status");
            mSchText.setText("Update");
            schedule_date = intent.getStringExtra("schedule_date");
            recurring_type = intent.getStringExtra("recurring_type");
            facebook_post = intent.getStringExtra("facebook_post");
            facebook_image = intent.getStringExtra("facebook_image");
            fb_id = intent.getStringExtra("fb_id");
            img_progress.setVisibility(View.VISIBLE);
            Glide.with(NewFacebook.this)
                    .load(facebook_image)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            img_progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            img_progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .crossFade()
                    .into(set_img);
            edt_time.setText(setformater(schedule_date));
            edt_message.setText(facebook_post);
            edt_message.setSelection(facebook_post.length());

            if (recurring_type.equals("0")) {
                rec_id = 0;
                edt_recurring.setText("Non Recurring");
            } else if (recurring_type.equals("d")) {
                rec_id = 1;
                edt_recurring.setText("Recur Daily");
            } else if (recurring_type.equals("wd")) {
                rec_id = 2;
                edt_recurring.setText("Recur weekdays");
            } else if (recurring_type.equals("w")) {
                rec_id = 3;
                edt_recurring.setText("Recur weekly");
            } else if (recurring_type.equals("fn")) {
                rec_id = 4;
                edt_recurring.setText("Recur forthnightly");
            } else if (recurring_type.equals("m")) {
                rec_id = 5;
                edt_recurring.setText("Recur Monthly");
            } else if (recurring_type.equals("y")) {
                rec_id = 6;
                edt_recurring.setText("Recur Yearly");
            }

            if (intenttype.equals("nonedit")) {
                edt_recurring.setText("Sent");
                btn_schedule.setVisibility(View.INVISIBLE);
                edt_message.setEnabled(false);
                edt_message.setTextColor(Color.BLACK);
                get_img.setVisibility(View.INVISIBLE);
                title.setText("Sent Status");

                mTimeArrow.setVisibility(View.INVISIBLE);
                mSchArrow.setVisibility(View.INVISIBLE);


            }
        } else {
            rec_id = 0;
            recurring_type = "0";
            edt_recurring.setText("Non recurring");
            mSchText.setText("Schedule");
            setTitle("");
            title.setText("New Status");
        }
        method = new Methods(NewFacebook.this, edt_time);
    }

    private void onclicks() {
        get_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {

                    imagedialog();

                    // startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });


        edt_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >= 140) {
                    Snackbar.make(edt_message, "Limit exeeds.", Snackbar.LENGTH_SHORT).show();
                    return;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                // char_count.setText(s.length()+"/160");
            }
        });

        edt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                int setday, setmonth, setyear;
                if (intenttype.equals("update") || intenttype.equals("nonedit")) {
                    updatevalues(edt_time.getText().toString());
                    setday = upday;
                    setmonth = upmonth;
                    setyear = upyear;
                } else {
                    setday = curday;
                    setmonth = curmont;
                    setyear = curyear;
                }
                if (!intenttype.equals("nonedit")) {
                    if (Build.VERSION.SDK_INT < 21) {
                        style = android.graphics.Color.TRANSPARENT;
                        datePickerbuildbelow(datePickerListener2, setyear, setmonth, setday);
                    } else {
                        style = R.style.DialogTheme1;
                        datePicker(datePickerListener1, setyear, setmonth, setday);
                    }
                }
             /*   hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mYear = c.get(Calendar.YEAR);


                if (Build.VERSION.SDK_INT < 21) {
                    method.getdate(NewFacebook.this, mYear, mMonth, mDay, hour, minute);
                } else {
                    builder = new StringBuilder();
                    Methods.datePicker(NewFacebook.this, datePickerListener, mYear, mMonth, mDay);
                }*/
            }
        });


        edt_recurring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (!intenttype.equals("nonedit")) {
                    RecurringPicker(NewFacebook.this, edt_recurring);
                }
            }
        });


        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_time.length() == 0) {
                    Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
                } else if (edt_recurring.length() == 0) {
                    Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (!network) {
                        Methods.conDialog(NewFacebook.this);

                    } else {

                        access_token = AccessToken.getCurrentAccessToken().getToken();
                        if (!AccessToken.getCurrentAccessToken().getPermissions().contains("publish_actions")){
                            Log.i("Fb Access Token:", "" + access_token);
                           // Toast.makeText(NewFacebook.this, "No publish permission", Toast.LENGTH_SHORT).show();
                            UpdateFacebookPermissions();
                        }else {

                            if (Utils.pimageUri != null) {

                                String path = image_path;
                                new fb_image(image_path).execute();
                            } else {
                                new fb_post(facebook_image).execute();
                            }

                        }

                    }
                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
/*
    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                int ti = 0;

                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {

                    if (selectedHour < curhour && valid) {
                        edt_time.setText("");
                        Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else if (selectedMinute < curmin && valid) {
                        edt_time.setText("");
                        Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else {

                        int set = selectedMinute;
                      *//*  if (selectedMinute > 0 && selectedMinute <= 7 || selectedMinute > 53 && selectedMinute <= 59) {
                            set = 0;
                        } else if (selectedMinute > 7 && selectedMinute <= 15 || selectedMinute > 15 && selectedMinute <= 23) {
                            set = 15;
                        } else if (selectedMinute > 23 && selectedMinute <= 30 || selectedMinute > 30 && selectedMinute <= 37) {
                            set = 30;
                        } else if (selectedMinute > 37 && selectedMinute <= 45 || selectedMinute > 45 && selectedMinute <= 53) {
                            set = 45;
                        } *//*
                        if (selectedMinute <= 15) {
                            set = 0;
                        } else if (selectedMinute <= 30) {
                            set = 15;
                        } else if (selectedMinute <= 45) {
                            set = 30;
                        } else if (selectedMinute <= 59) {
                            set = 45;
                        }

                        hour = selectedHour;
                        minute = set;

                        c.set(Calendar.HOUR_OF_DAY, hour);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                        String displayValue = timeFormatter.format(c.getTime());
                        System.out.println("value time : " + displayValue);

                        recurring_type = "0";
                        rec_id = 0;
                        edt_recurring.setText("Non recurring");
                        edt_time.setText(builder.append(", " + displayValue));
                    }

                }

            };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (year < curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                c.set(Calendar.YEAR, mYear);
                c.set(Calendar.MONTH, mMonth);
                c.set(Calendar.DAY_OF_MONTH, mDay);
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy");
                String dateSet = format1.format(c.getTime());
                //dateSet = DateFormat.getDateInstance().format(c.getTime());
                System.out.println("value date : " + dateSet);
//            builder.append((dayOfMonth))
//                    .append(" ").append((monthOfYear)).append(", ").append((year));

                builder.append(dateSet);

                Methods.timePicker(NewFacebook.this, timePickerListener, hour, minute);
            }
        }
    };*/

    //*************RecurringPicker***********
    private void RecurringPicker(Context ctx, final EditText recurring) {

        new AlertDialog.Builder(ctx, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, rec_id, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        rec_id = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button

                        recurring_type = sentitems[rec_id];
                        recurring.setText(items[rec_id]);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();


        // return selected_rec;
    }


    // *******************************onActivityResult**********************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
//            Bitmap bit = (Bitmap) data.getExtras().get("data");
//            Utils.selected_image = Methods.SaveImage(bit);
            Intent in = new Intent(NewFacebook.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Utils.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Utils.selected_image);
            Intent in = new Intent(NewFacebook.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
            set_img.setImageURI(Utils.pimageUri);
        } else {
            //    Utils.pimageUri = null;
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //**************************************Date formater function**********************************


    public String sentformatter(String sendtime, int i) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1;
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            Date newDate = format1.parse(sendtime);
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (i == 0) {
                settym = format1.format(newDate);
            } else {
                format1.setTimeZone(TimeZone.getTimeZone("UTC"));
                settym = format1.format(newDate);
            }
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    //********************************************fb_image parsing*************************************
    public class fb_image extends AsyncTask {
        String response, image_path;

        public fb_image(String image_path) {
            this.image_path = image_path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {

            JSONParser parser = new JSONParser();
            response = parser.uploadFile(image_path);

            System.out.print("fff=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressbar.setVisibility(View.GONE);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("Status");
                if (status.equals("true")) {
                    facebook_image = obj.optString("image");
                    new fb_post(facebook_image).execute();
                } else {
                    Snackbar.make(btn_schedule, "Response Error", Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            }

        }
    }


    //********************************************fb_post parsing*************************************
    public class fb_post extends AsyncTask {
        String response, imgurl;

        public fb_post(String url) {
            this.imgurl = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            if (intenttype.equals("update")) {
                response = update_schdule_method("update_facebook", imgurl);
            } else {
                response = Fb_PostMethod("facebook_post", imgurl);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    FragmentFacebook.adaptertype = recurring_type;

                    new facebook_thread().execute();
                } else {
                    progressbar.setVisibility(View.GONE);
                    Snackbar.make(btn_schedule, "Network Error", Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            }

        }
    }
    //************************Webservice parameter RegisterMethod*************************

    public String update_schdule_method(String service_type, String img) {
        String res = null;
        JSONParser parser = new JSONParser();
        String msg ="";
        try {
            msg = URLEncoder.encode(edt_message.getText().toString(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (img==null){
            img="";
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("s_date", sentformatter(edt_time.getText().toString(), 0))
                .appendQueryParameter("trigger_date", sentformatter(edt_time.getText().toString(), 1))
                .appendQueryParameter("msg", msg)
                .appendQueryParameter("recu_type", recurring_type)
                .appendQueryParameter("fb_id", fb_id)
                .appendQueryParameter("user_pic", img);

        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String Fb_PostMethod(String service_type, String img) {
        String res = null;
        JSONParser parser = new JSONParser();
        String msg ="";
        try {
            msg = URLEncoder.encode(edt_message.getText().toString(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (img==null){
            img="";
        }
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("s_date", sentformatter(edt_time.getText().toString(), 0))
                .appendQueryParameter("trigger_date", sentformatter(edt_time.getText().toString(), 1))
                .appendQueryParameter("msg", msg)
                .appendQueryParameter("recu_type", recurring_type)
                .appendQueryParameter("user_pic", img);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String setformater(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(NewFacebook.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Dashboard.newfb = true;
                        finish();
                    }
                })
                .setIcon(icon)
                .show();

    }

    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(NewFacebook.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(NewFacebook.this, android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(NewFacebook.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            imagedialog();
            // startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        //  startActivityForResult(getPickImageChooserIntent(), 200);
                        imagedialog();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(add_contact, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    //-------------------------time and date dialogss-------------------------//

    /***************************************
     * Date picker
     *************************************/
    public void datePicker(DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(NewFacebook.this, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }

    public void datePickerbuildbelow(DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(NewFacebook.this, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
              /*  if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();


            }
        });

        dialog.show();

    }

    public DatePickerDialog.OnDateSetListener datePickerListener2 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c1.set(Calendar.YEAR, mYear);
            c1.set(Calendar.MONTH, mMonth);
            c1.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;


        }
    };

    public void timePickerbelow() {

        isFirst = true;
        int setmin, sethour;
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        if ((intenttype.equals("update") || intenttype.equals("nonedit")) && compareday != day) {
            setmin = upmin;
            sethour = uphour;

        } else {
            setmin = minute;
            sethour = hour;
            if (setmin >= 55 && setmin <= 59) {
                setmin = 0;
                sethour = sethour + 1;
            } else {
                setmin = setmin + 5;
            }

        }

        if (minute >= 55 && minute <= 59) {
            minute = 0;
            hour = hour + 1;
        } else {
            minute = minute + 5;
        }


        final TimePickerDialogs timeee;

        timeee = new TimePickerDialogs(NewFacebook.this, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                c1.set(Calendar.HOUR_OF_DAY, selectedHour);
                if (Build.VERSION.SDK_INT < 21) {
                    if (isFirst){
                        isFirst = false;
                        c1.set(Calendar.MINUTE, min);
                    }
                } else {
                    c1.set(Calendar.MINUTE, min);
                }
                SimpleDateFormat timeFormatter;
                if (hourstype) {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                } else {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                }
                String displayValue = timeFormatter.format(c1.getTime());
                edt_time.setText(displayValue);


            }
        }, sethour, setmin, hourstype);
        timeee.setMin(hour, minute);
        timeee.show();

    }

    //********************************
// Date change Listener
//*********************************
    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (year < curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
               /* mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;*/

                c1.set(Calendar.YEAR, year);
                c1.set(Calendar.MONTH, monthOfYear);
                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                compareday = dayOfMonth;
                comparemonth = monthOfYear;

                timePickerbelow();

            }
        }
    };

    public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        private int minHour = -1;
        private int minMinute = -1;

        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private java.text.DateFormat dateFormat;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
//            this.hr = hourOfDay;
//            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            //    ct = Calendar.getInstance();
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT);

        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);


            boolean validTime = true;
            if (hourOfDay < minHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == minHour && (minute - 1) * 5 < minMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }

            if (hourOfDay > maxHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == maxHour && minute * 5 > maxMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }
            // if (hourOfDay==minHour&&)

            if (validTime) {
                currentHour = hourOfDay;

                currentMinute = minute;
            } else {
                int j = minMinute / 5;
                int r = minMinute % 5;
                if (r > 3) {
                    j = j + 1;
                }
                if (j < minute) {
                    currentMinute = minute;

                } else {
                    currentMinute = j;

                }

            }


            timePicker.setCurrentHour(currentHour);


            timePicker.setCurrentMinute(currentMinute);


        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            //  System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
                timePicker.setOnTimeChangedListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void imagedialog() {

        final CharSequence[] items = {"Pick from gallery", "Take from camera"};
        new AlertDialog.Builder(NewFacebook.this, R.style.AppTheme_Dialog)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == 0) {

                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, 200);

                        } else {

                            String root = Environment.getExternalStorageDirectory().toString();

                            File myDir = new File(root + "/VidCode/Captured Images/");
                            if (!myDir.exists())
                                myDir.mkdirs();

                            String fname = "/image-" + System.currentTimeMillis() + ".png";
                            File file = new File(myDir, fname);
                            Utils.selected_image = Uri.fromFile(file);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Utils.selected_image);
                            startActivityForResult(cameraIntent, 100);

                        }
                    }
                })
                .show();
    }

    //------------update values-----------------//
    public void updatevalues(String sendtime) {

        Date newDate;
        try {


            if (hourstype) {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                newDate = format1.parse(sendtime);
            } else {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                newDate = format1.parse(sendtime);
            }


         /*   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);*/
            Calendar call = Calendar.getInstance();
            call.setTime(newDate);

            uphour = call.get(Calendar.HOUR_OF_DAY);
            upmin = call.get(Calendar.MINUTE);
            upmonth = call.get(Calendar.MONTH);
            upday = call.get(Calendar.DAY_OF_MONTH);
            upyear = call.get(Calendar.YEAR);
        } catch (Exception e) {

        }


    }

    //****************************facebook_thread***************************************************

    class facebook_thread extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;



        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            System.out.println("Authcode::::::: " + auth_code);
            schedule_response = Getfb_Method("fb_post_schedule", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentFacebook.schdulefblist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.schdulefblist.add(fbvalue);

                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook.schdulefblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                          //  String message = URLDecoder.decode(iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            FragmentFacebook.schdulefblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getfb_Method("facebook_post_recurring", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentFacebook.recurringfblist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.recurringfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook.recurringfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);
                           // String message = URLDecoder.decode(iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                            FragmentFacebook.recurringfblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getfb_Method("get_facebook_post", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentFacebook.postfblist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Model_facebook fbvalue = new Model_facebook();
                    fbvalue.fb_name = prefs.getString("fb_name", null);
                    fbvalue.fb_email = prefs.getString("fb_email", null);
                    fbvalue.view_type = "facebook";
                    FragmentFacebook.postfblist.add(fbvalue);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        Model_facebook value = new Model_facebook();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentFacebook. postfblist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            Model_facebook item = new Model_facebook();
                            JSONObject iobj = jarr.optJSONObject(i);

                           // String message = URLDecoder.decode(iobj.optString("facebook_post"),"UTF-8");
                            item.facebook_post = iobj.optString("facebook_post");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.facebook_image = iobj.optString("facebook_image");
                            item.fb_id = iobj.optString("fb_id");
                            item.view_type = "values";
                           FragmentFacebook.postfblist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);

            if (intenttype.equals("update")) {
                dialog("Success!", "Your post has been updated", android.R.drawable.ic_dialog_info);
            } else {
                dialog("Success!", "Your post has been scheduled", android.R.drawable.ic_dialog_info);
            }

        }
    }

    public String Getfb_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    /************************************************************
     * Facebook publish permission
     * **********************************************************/
    public void UpdateFacebookPermissions() {
        try {

            LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {

                            access_token = AccessToken.getCurrentAccessToken().getToken();
                            if (AccessToken.getCurrentAccessToken().getPermissions().contains("publish_actions")){

                               // Toast.makeText(NewFacebook.this, "No publish permission", Toast.LENGTH_SHORT).show();
                                if (Utils.pimageUri != null) {

                                    String path = image_path;
                                    new fb_image(image_path).execute();
                                } else {
                                    new fb_post(facebook_image).execute();
                                }
                            }
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException exception) {
                           // Snackbar.make(btn_compose, "" + exception.toString(), Snackbar.LENGTH_SHORT).show();
                            Log.i("Error: ",""+exception);
                        }
                    });
        } catch (Exception e) {

            //Snackbar.make(btn_compose, "Error : "+e, Snackbar.LENGTH_SHORT).show();
            Log.i("Error: ",""+e);
        }

    }

}


