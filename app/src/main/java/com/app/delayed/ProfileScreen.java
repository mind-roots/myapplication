package com.app.delayed;

import android.*;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.delayed.cropimage.CropActivityP;
import com.app.delayed.model.CircleTransform;
import com.app.delayed.model.JSONParser;
import com.app.delayed.tutorial.ViewpagerActivity;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;

import com.app.delayed.utils.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Permission;
import java.security.Permissions;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 1/28/2016.
 */
public class ProfileScreen extends AppCompatActivity {

    EditText edt_name;
    Toolbar toolbar;
    TextView title, txt_count, txt_terms;
    ProgressBar progressBar;
    SharedPreferences prefs, instal;
    ImageView img_photo;
    CircularImageView img_photo_set;
    LinearLayout dummy_view, set_view;
    Uri fileUri;
    String selectedImagePath, instalvalue;
    String access_token, status, id, mobile_no, country_id, country, fb_status,
            pin, nname, fb_name, fb_email, ffb_token, fb_token_date, twitter_token, twitter_secret, twitter_token_date, twitter_hname,
            twitter_uname, credits, flag, auth_code, token_id, token_type, chat_icon, last_updated;
    String Intenttype;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_screen);
        init();
        ActivityStack.activity.add(ProfileScreen.this);
        clicks();
    }

    private void clicks() {
        edt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                // TODO Auto-generated method stub
                txt_count.setText(String.valueOf(s.length()));

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });
        img_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    imagedialog();
                    //   startActivityForResult(getPickImageChooserIntent(), 200);
                }

                //    addPhoto();
            }
        });
        img_photo_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermission();
                } else {
                    imagedialog();
                    //  startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });
        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileScreen.this, TermsActivity.class));
            }
        });
    }

    /*******************************************
     * image picker dialog
     * *****************************************/
    private void imagedialog() {

        final CharSequence[] items = {"Pick from gallery", "Take from camera"};
        new AlertDialog.Builder(ProfileScreen.this, R.style.AppTheme_Dialog)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == 0) {

                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, 200);

                        } else {

                            String root = Environment.getExternalStorageDirectory().toString();

                            File myDir = new File(root + "/VidCode/Captured Images/");
                            if (!myDir.exists())
                                myDir.mkdirs();

                            String fname = "/image-" + System.currentTimeMillis() + ".png";
                            File file = new File(myDir, fname);
                            Utils.selected_image=Uri.fromFile(file);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Utils.selected_image);
                            startActivityForResult(cameraIntent, 100);

                        }
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
//            Bitmap bit = (Bitmap) data.getExtras().get("data");
//            Utils.selected_image = Methods.SaveImage(bit);
            Intent in = new Intent(ProfileScreen.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Utils.selected_image = getPickImageResultUri(data);
            Intent in = new Intent(ProfileScreen.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            //   set_img.setImageURI(Utils.pimageUri);
            dummy_view.setVisibility(View.GONE);
            set_view.setVisibility(View.VISIBLE);
            imageUri = Utils.pimageUri;
            img_photo_set.setImageURI(imageUri);
            Utils.pimageUri = null;
//            String image_path = getRealPathFromURI(getApplicationContext(), Utils.pimageUri);
//            setImage(image_path);
        } else {
            Utils.pimageUri = null;
        }


        super.onActivityResult(requestCode, resultCode, data);

    }
    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_finish, menu);
        if (Intenttype.equals("update")) {
            menu.findItem(R.id.action_finish).setTitle("Update");
        } else {
            menu.findItem(R.id.action_finish).setTitle("Finish");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_finish) {
            HideKayboard();
            boolean b = Methods.isNetworkConnected(ProfileScreen.this);
            if (!b) {
                Methods.conDialog(ProfileScreen.this);
            } else {
                if (edt_name.length() != 0) {
                    if (imageUri != null) {
                        String image_path = getRealPathFromURI(getApplicationContext(), imageUri);

                        new update_user(image_path).execute();
                    } else {
                        new update_user("").execute();
                    }
                } else {
                    Snackbar.make(edt_name, "Name can't be empty", Snackbar.LENGTH_SHORT).show();
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
    }

    @Override
    protected void onPause() {
        super.onPause();

        HideKayboard();
    }

    //************Innitialize UI elements**************
    private void init() {

        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        auth_code = prefs.getString("auth_code", null);
        instal = getSharedPreferences("install", ProfileScreen.this.MODE_PRIVATE);
        instalvalue = instal.getString("instalvalue", null);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        dummy_view = (LinearLayout) findViewById(R.id.dummy_view);
        set_view = (LinearLayout) findViewById(R.id.set_view);
        edt_name = (EditText) findViewById(R.id.edt_name);
        img_photo = (ImageView) findViewById(R.id.img_photo);
        txt_count = (TextView) findViewById(R.id.txt_count);
        txt_terms = (TextView) findViewById(R.id.txt_terms);
        img_photo_set = (CircularImageView) findViewById(R.id.img_photo_set);
        setTitle("");
        title.setText("Profile");
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Intenttype = intent.getStringExtra("Intenttype");
        if (Intenttype.equals("update")) {
            txt_terms.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (prefs.getString("name", null) != null && !prefs.getString("name", null).equals("null") && prefs.getString("name", null).length() != 0) {

                edt_name.setText("" + (prefs.getString("name", null)));
                edt_name.setSelection(edt_name.length());
                txt_count.setText("" + edt_name.length());
            }

            if (prefs.getString("chat_icon", null) != null && !prefs.getString("chat_icon", null).equals("null") && prefs.getString("chat_icon", null).length() != 0) {

                setImage(prefs.getString("chat_icon", null));

            }
        }
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    //********************************************update_user parsing*******************************

    public class update_user extends AsyncTask {
        String token, fb_token, token_secret;
        String response, img;
        String name;
        public update_user(String image_path) {
            this.img = image_path;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            try {
                name = URLEncoder.encode(edt_name.getText().toString(),"UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }

            token = prefs.getString("twitter_token","");
            fb_token = prefs.getString("fb_token","");
            token_secret = prefs.getString("twitter_secret","");
            token_secret = token_secret.replace("\r","").replace("\n","");
        }

        @Override
        protected Object doInBackground(Object[] params) {
//             response = UserUpdate_Method("update_users",
//                    name, fb_token, token, token_secret);

            JSONParser parser = new JSONParser();
            response = parser.updateprofile(img, auth_code, name, fb_token, token, token_secret);
            System.out.print("response11=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                    editor.putString("setting_email", pin_nos.optString("email"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                  //  String name = URLDecoder.decode(pin_nos.optString("name"),"UTF-8");

                    editor.putString("name", pin_nos.optString("name"));
                    editor.commit();
                    if (Intenttype.equals("update")) {
                        finish();
                    } else {
                        if (instalvalue != null) {
                            Intent i = new Intent(ProfileScreen.this, LoadContactsScreen.class);
                            startActivity(i);
                        } else {
                            startActivity(new Intent(ProfileScreen.this, ViewpagerActivity.class));
                        }
                    }
                } else {
                    Snackbar.make(edt_name, "Response Error" + status, Snackbar.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.GONE);
            } catch (Exception e) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    //**********************************Webservice parameter RegisterMethod*************************
    private void setImage(String selectedImagePath) {
        System.out.println("phselectedImagePath:: " + selectedImagePath);
        dummy_view.setVisibility(View.GONE);
        set_view.setVisibility(View.VISIBLE);
        int size = Methods.ReturnSize(ProfileScreen.this);
        if (selectedImagePath.contains("http")) {
            Picasso.with(ProfileScreen.this).load(selectedImagePath).placeholder(R.mipmap.default_user).into(img_photo_set);
        } else {
            Glide.with(ProfileScreen.this)
                    .load(selectedImagePath)
                    .placeholder(R.mipmap.default_user)
                    .crossFade()
                    .into(img_photo_set);
        }
        //  Picasso.with(ProfileScreen.this).load(selectedImagePath).placeholder(R.mipmap.group_icon).into(img_photo_set);

    }

    public String UserUpdate_Method(String service_type, String name,
                                    String fb_token, String token, String token_secret) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("name", name)
                .appendQueryParameter("fb_token", fb_token)
                .appendQueryParameter("token", token)
                .appendQueryParameter("token_secret", token_secret);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    /**************************************************
     * Allow Permissions
     **************************************************/
    private void AllowPermission() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(ProfileScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(ProfileScreen.this, android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(ProfileScreen.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            imagedialog();
           // startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                      //  startActivityForResult(getPickImageChooserIntent(), 200);
                        imagedialog();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(dummy_view, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
