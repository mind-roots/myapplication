package com.app.delayed;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Window;

import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.utils.ActivityStack;

/**
 * Created by Balvinder on 5/18/2016.
 */
public class LogoutAlert extends Activity {

    SharedPreferences prefs;
    DatabaseQueries dataquery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide activity title
        //setContentView(R.layout.activity_my_alert_dialog);

        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        dataquery = new DatabaseQueries(LogoutAlert.this);

        AlertDialog.Builder Builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setMessage("Your account has been logged out from this device. Please login again to continue.")
                .setTitle("Alert!")
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogoutAlert.this.finish();
                        CleanData(prefs, dataquery);
                    }
                });
        AlertDialog alertDialog = Builder.create();
        alertDialog.show();

    }

    private void CleanData(SharedPreferences prefs, DatabaseQueries dataquery){
        Dashboard.newchat=true;
        prefs.edit().clear().commit();
        dataquery.delete("user_contacts");
        dataquery.delete("grouping");
        dataquery.delete("message_threads");
        dataquery.delete("message_thread_content");
        dataquery.delete("scheduled_content_info");
        Intent intent = new Intent(LogoutAlert.this, FirstScreen.class);
        startActivity(intent);
        finish();
        for (int i = 0; i < ActivityStack.activity.size(); i++) {
            ActivityStack.activity.get(i).finish();
        }
    }

}
