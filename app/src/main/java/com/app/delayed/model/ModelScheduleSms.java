package com.app.delayed.model;

/**
 * Created by admin on 2/6/2016.
 */
public class ModelScheduleSms {
    public String message;
    public String recvd_no;
    public String group_name;
    public String schedule_date;
    public String recurring_type;
    public String contact_name;
    public String sms_id;
    public String view_type;
    public String header_date;
    public String list_type;
}
