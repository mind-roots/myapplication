package com.app.delayed.model;

/**
 * Created by Balvinder on 2/20/2016.
 */
public class ModelDelayedContact {

    public String delayd_user_id;
    public String profile_pic;
    public String last_updated_date;
    public String delayed_user;
    public String contact_no;
    public String country_code;
    public String name;

}
