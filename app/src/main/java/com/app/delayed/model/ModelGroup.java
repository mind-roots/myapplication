package com.app.delayed.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/4/2016.
 */
public class ModelGroup {

    public String grp_name;
    public int grp_id;
    //public String grp_img;
    public byte[] grp_img;
    public String grp_contact;
    public String grp_contact_name;

}
