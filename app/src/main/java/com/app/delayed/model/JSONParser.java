package com.app.delayed.model;

import android.net.Uri;

import com.app.delayed.utils.Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class JSONParser {
    // constructor
    public JSONParser() {

    }

    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getJSONFromUrl(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection con = (HttpURLConnection) url
                    .openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = con.getResponseCode();
            System.out.println(" code hh : " + responseCode);

            in = con.getInputStream();
            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }


    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getresFromUrl(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = connection.getResponseCode();
            System.out.println(" code hh : " + responseCode);
            if (responseCode == 200) {
                // response code is OK
                in = connection.getInputStream();
            }
            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }


    public String uploadFile(String ipath) {
        String charset = "UTF-8";
        File sourceFile = new File(ipath);
        String requestURL = Utils.Imgurl;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }

            multipart.addFilePart("user_pic", sourceFile);
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;






        /*String charset = "UTF-8";
        File sourceFile = new File(ipath);
        String requestURL = Utils.Imgurl;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFilePart("user_pic", sourceFile);
            List<String> response = multipart.finish();
            System.out.println("SERVER REPLIED:");
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;*/
    }

    public String updateprofile(String ipath, String auth_code, String name,
                                String fb_token, String token, String token_secret) {

        String charset = "UTF-8";
        File sourceFile = new File(ipath);
        String requestURL = Utils.base_url + "service_type=update_users";
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("auth_code", auth_code);
            multipart.addFormField("fb_token", fb_token);
            multipart.addFormField("token", token);
            multipart.addFormField("token_secret", token_secret);
            multipart.addFormField("name", name);
            multipart.addFormField("test", "");
            if (sourceFile.length()>1) {
                multipart.addFilePart("chat_icon", sourceFile);
            }
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }


    public String uploadimgchat(String auth_code, String service, int type, String content, String thread_id, String schtype, String trigerdate, String schdate, String schstatus, String content_time) {
        String st = String.valueOf(new Timestamp(System.currentTimeMillis()));

        String charset = "UTF-8";
        File sourceFile = new File(content);
//        FilePart sourceFile = new FilePart("userfile", new ByteArrayPartSource(
//                filename, imageData));


        String requestURL = Utils.base_url + "service_type=" + service;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset);
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("auth_code", auth_code);
            multipart.addFormField("thread_id", thread_id);
            multipart.addFormField("type", String.valueOf(type));
            multipart.addFormField("status", schstatus);
            multipart.addFormField("scheduled_type", schtype);
            multipart.addFormField("scheduled_date_time", schdate);
            multipart.addFormField("trigger_date_time", trigerdate);
            multipart.addFormField("content_time", content_time);
            multipart.addFilePart("content", sourceFile);
            multipart.addFormField("test", "");
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;


    }
}