package com.app.delayed.model;

/**
 * Created by Balvinder on 2/19/2016.
 */
public class ModelContacts {

    public String cont_id;
    public String cont_name;
    public String cont_number;
    public String cont_date;
    public String delayed_user;
    public String country_code;
}
