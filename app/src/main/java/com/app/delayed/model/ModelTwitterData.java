package com.app.delayed.model;

/**
 * Created by user on 2/9/2016.
 */
public class ModelTwitterData {
    public String header_date;
    public String view_type;
    public String twitter_post;
    public String recurring_type;
    public String schedule_date;
    public String twitter_id;
    public String tw_name;
    public String tw_email;
}
