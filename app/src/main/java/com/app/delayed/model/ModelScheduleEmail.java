package com.app.delayed.model;

/**
 * Created by admin on 2/8/2016.
 */
public class ModelScheduleEmail
{
    public String message;
    public String subject;
    public String to_id;
    public String cc_id;
    public String from_id;
    public String recurring_type;
    public String schedule_date;
    public String email_id;
    public String view_type;
    public String header_date;
}