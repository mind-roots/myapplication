package com.app.delayed.model;

/**
 * Created by Balvinder on 2/20/2016.
 */
public class ModelUser {

    public String user_id;
    public String user_name;
    public String user_contact;
    public String user_pic;
    public String delayedtype;
    public String user_country_code;

}
