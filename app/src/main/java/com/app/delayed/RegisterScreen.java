package com.app.delayed;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelCountry;
import com.app.delayed.utils.ActivityStack;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 1/29/2016.
 */
public class RegisterScreen extends AppCompatActivity {

    Toolbar toolbar;
    TextView title;
    EditText country, phone;
    int selected_country = 0;
    String country_id;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        init();

        ActivityStack.activity.add(RegisterScreen.this);
        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HideKayboard();
                CountryPicker(selected_country, FirstScreen.arr_cntry);
            }
        });

    }

    //************Innitialize UI elements**************
    private void init() {

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("Register");
        setTitle("");
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        country = (EditText) findViewById(R.id.country);
        phone = (EditText) findViewById(R.id.phone_no);
        phone.setFocusable(false);
        phone.setCursorVisible(false);
        phone.setFocusableInTouchMode(true);
        //   country.setText(FirstScreen.arr_cntry.get(0).country + " (" + FirstScreen.arr_cntry.get(0).code + ")");
    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKayboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

//        if (id == android.R.id.home) {
//            finish();
//            return true;
//        }

        if (id == R.id.action_next) {

            if (phone.length() == 0) {
                Snackbar.make(phone, "Please enter phone number", Snackbar.LENGTH_SHORT).show();
            } else if (country.length() == 0) {
                Snackbar.make(phone, "Please select country", Snackbar.LENGTH_SHORT).show();
            } else {
                country_id = FirstScreen.arr_cntry.get(selected_country).code;
                boolean b = Methods.isNetworkConnected(RegisterScreen.this);
                HideKayboard();
                if (!b) {
                    Methods.conDialog(RegisterScreen.this);

                } else {
                    new register(country_id).execute();
                }
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //**********Hide kayboard****************
    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //*************CountryPicker***********
    private void CountryPicker(final int country_id, ArrayList<ModelCountry> arrayList) {

        final String[] items = new String[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {

            items[i] = arrayList.get(i).country + " (" + arrayList.get(i).code + ")";
            //items[i] = arrayList.get(i).country;
        }

        new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, selected_country, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        selected_country = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button

                        country.setText(items[selected_country]);
                        /*
                        * show keyboard
                        * */
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(phone, InputMethodManager.SHOW_IMPLICIT);
                        phone.setFocusable(true);
                        phone.setCursorVisible(true);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();


    }

    //********************************************Register parsing*************************************
    public class register extends AsyncTask {
        String status = "false", result;

        /* String token_id = Settings.Secure.getString(RegisterScreen.this.getContentResolver(),
                 Settings.Secure.ANDROID_ID);*/
        String country1 ;
        String number = phone.getText().toString();
        String country_id;
        String old_user;

        public register(String country_id) {
            this.country_id = country_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            country1 = country.getText().toString();
            country1 = country1.split("\\(")[0];
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            country_id = country_id.replaceAll("[+]", "");
            String response = RegisterMethod(country_id, number,country1);

            Log.i("Register res: ",response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("status");

                if (status.equals("true")) {

//*****************saving data to  model class**************************************


                    JSONObject obj1 = obj.getJSONObject("data");
                    old_user = obj1.getString("old_user");
                    editor = prefs.edit();
                   // editor.putString("auth_code", obj1.optString("auth_code"));
                    editor.putString("old_user", obj1.optString("old_user"));
                    editor.commit();

                } else {


                    JSONObject data_obj = obj.getJSONObject("data");
                    result = data_obj.getString("result");


                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {

                startActivity(new Intent(RegisterScreen.this, VerificationScreen.class).putExtra("number",number).putExtra("country_id", country_id));

            } else {
                Snackbar.make(phone, "Responce Error", Snackbar.LENGTH_SHORT).show();
                //Utils.dialog(LoginActivity.this, "Error!", result);
            }
        }
    }
    //************************Webservice parameter RegisterMethod*************************

    public String RegisterMethod(String country_id, String number,String country1) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "register_v2")
                .appendQueryParameter("country_id", country_id)
                .appendQueryParameter("number", number)
                .appendQueryParameter("phone_type", "android")
                .appendQueryParameter("country", country1);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }


}
