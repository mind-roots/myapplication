package com.app.delayed;

import android.*;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.delayed.adapters.RecipientAdapter;
import com.app.delayed.cropimage.CropActivityP;
import com.app.delayed.databaseUtils.DatabaseQueries;

import com.app.delayed.model.ModelGroup;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.utils.CircularImageView;

import com.app.delayed.utils.Utils;
import com.bumptech.glide.Glide;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/2/2016.
 */
public class NewGroup extends AppCompatActivity {

    Toolbar toolbar;
    TextView mTitle;
    Menu newmenu;
    EditText edt_name;
    TextView txt_count;
    ImageView img_add;
    CircularImageView img_photo;
    RecyclerView my_recycler_view;
    public static ArrayList<String> arr_name = new ArrayList<>();
    public static ArrayList<String> arr_phone = new ArrayList<>();

    RecipientAdapter adapter;
    LinearLayoutManager mLayoutManager;
    View view;
    String IntentType;
    String groupname = "null", cnames, cnumbers;
    int grpid;

    DatabaseQueries dataquery;
    byte[] byteImage1 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_group);
        init();
        Utils.pimageUri = null;
        arr_name.clear();
        arr_phone.clear();
        clickevents();
        getdata();
    }

    private void getdata() {
        Intent intent = getIntent();
        IntentType = intent.getStringExtra("IntentType");
        if (IntentType.equals("update")) {
            groupname = intent.getStringExtra("groupname");
            cnames = intent.getStringExtra("cnames");
            cnumbers = intent.getStringExtra("cnumbers");
            grpid = intent.getIntExtra("grpid", 0);
            byteImage1 = intent.getByteArrayExtra("grpimage");
            edt_name.setText(groupname);
            txt_count.setText(String.valueOf(groupname.length()));
            String[] nam = cnames.split(",");
            for (String str : nam) {
                arr_name.add(str);
            }

            String[] num = cnumbers.split(",");
            for (String strn : num) {
                arr_phone.add(strn);
            }
            Glide.with(NewGroup.this)
                    .load(byteImage1)
                    .crossFade()
                    .into(img_photo);

            adapter = new RecipientAdapter(NewGroup.this, arr_name);
            my_recycler_view.setAdapter(adapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        HideKayboard();
        edt_name.clearFocus();
    }

    @Override
    protected void onPause() {
        super.onPause();

        HideKayboard();
    }

    //************Innitialize UI elements**************
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setText("New SMS Group");
        setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dataquery = new DatabaseQueries(NewGroup.this);
        edt_name = (EditText) findViewById(R.id.edt_name);
        txt_count = (TextView) findViewById(R.id.txt_count);
        img_photo = (CircularImageView) findViewById(R.id.img_photo);
        img_add = (ImageView) findViewById(R.id.img_add);
        my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        my_recycler_view.setLayoutManager(mLayoutManager);

    }


    private void clickevents() {
        edt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                // TODO Auto-generated method stub
                count = s.length();
                txt_count.setText(String.valueOf(s.length()));
                if (IntentType.equals("new")) {
                    newmenu.findItem(R.id.action_done).setVisible(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });


        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                view = v;
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermissionContact();
                }else {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, 1);
                }

            }
        });
        img_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKayboard();
                if (Build.VERSION.SDK_INT >= 23){
                    AllowPermission();
                }else {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
            }
        });

    }

    //***************************************Crop Image Methods*************************************

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //**********************************Webservice parameter RegisterMethod*************************
    private void setImage(Uri selectedImagePath) {
        System.out.println("phselectedImagePath:: " + selectedImagePath);
        //imogi.setVisibility(View.VISIBLE);
        Glide.with(NewGroup.this)
                .load(selectedImagePath)
                .crossFade()
                .into(img_photo);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_done, newmenu);
        if (IntentType.equals("update")) {
            newmenu.findItem(R.id.action_done).setVisible(true);
            newmenu.findItem(R.id.action_refresh).setVisible(false);
        } else {
            newmenu.findItem(R.id.action_done).setVisible(false);
            newmenu.findItem(R.id.action_refresh).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_done) {
            HideKayboard();
            //    GroupScreen.groupList.clear();
            int n = (int) Math.random();


            if (edt_name.length() == 0) {
                Snackbar.make(edt_name, "Enter group name.", Snackbar.LENGTH_SHORT).show();
            } else if (arr_name.size() == 0) {
                Snackbar.make(edt_name, "Add at least 1 contact.", Snackbar.LENGTH_SHORT).show();
            } else if (!groupname.equals(edt_name.getText().toString()) && dataquery.getallcount(edt_name.getText().toString()) > 0) {
                Snackbar.make(edt_name, "Group name already exist.", Snackbar.LENGTH_SHORT).show();
            } else {

                StringBuilder namebuilder = new StringBuilder();
                StringBuilder numberbuilder = new StringBuilder();
                for (int i = 0; i < arr_name.size(); i++) {
                    namebuilder.append(arr_name.get(i).toString()).append(",");
                    numberbuilder.append(arr_phone.get(i).toString()).append(",");
                }
                String cnames = namebuilder.toString();
                if (cnames.indexOf(",") != -1) {
                    cnames = cnames.substring(0, cnames.length() - 1);
                }
                String cnumbers = numberbuilder.toString();
                if (cnumbers.indexOf(",") != -1) {
                    cnumbers = cnumbers.substring(0, cnumbers.length() - 1);
                }

                if (Utils.pimageUri != null) {
                    try {
                        FileInputStream instream = new FileInputStream(getRealPathFromURI(getApplicationContext(), Utils.pimageUri));
                        BufferedInputStream bif = new BufferedInputStream(instream);
                        byteImage1 = new byte[bif.available()];
                        bif.read(byteImage1);
//                    newValues.put("image", byteImage1);
//                    long ret = myDb.insert(TABLE_NAME, null, newValues);
//                    if (ret < 0)
//                        textView.append("Error");
                    } catch (IOException e) {
                        //    textView.append("Error Exception : " + e.getMessage());
                    }
                }

                ModelGroup model = new ModelGroup();
                model.grp_name = edt_name.getText().toString();
                model.grp_img = byteImage1;
                model.grp_contact = cnumbers;
                model.grp_contact_name = cnames;
                if (IntentType.equals("update")) {
                    dialog(model,grpid );
                   // dataquery.updategroup(model, grpid);
                } else {
                    dataquery.insertgroup(model);
                    finish();
                }

                //  GroupScreen.groupList.add(model);


               /* ModelGroup model = new ModelGroup();
                model.grp_name = edt_name.getText().toString();
                model.names = arr_name;
                model.numbers = arr_phone;
                GroupScreen.groupList.add(model);*/

            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void HideKayboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                // TODO Fetch other Contact details as you want to use
                String id = c
                        .getString(c
                                .getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                String hasPhone = c
                        .getString(c
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (hasPhone.equalsIgnoreCase("1")) {
                    Cursor phones = getContentResolver()
                            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + id, null, null);
                    phones.moveToFirst();
                    String phone = phones.getString(phones
                            .getColumnIndex("data1"));


                    if (arr_name.size() >= 10) {
                        Snackbar.make(view, "You can add up to 10 recipients", Snackbar.LENGTH_SHORT).show();
                    } else {
                        if (arr_phone.size() > 0) {

                            if (arr_phone.contains(phone)) {
                                Snackbar.make(view, "Contact already exists!", Snackbar.LENGTH_SHORT).show();
                            } else {
                                arr_phone.add(phone);
                                arr_name.add(name);

                            }
                        } else {
                            arr_phone.add(phone);
                            arr_name.add(name);
                        }
                        adapter = new RecipientAdapter(NewGroup.this, arr_name);
                        my_recycler_view.setAdapter(adapter);

                    }
                }
            }
        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            Utils.selected_image = getPickImageResultUri(data);
            Log.i("imageUri: ", "" + Utils.selected_image);
            Intent in = new Intent(NewGroup.this, CropActivityP.class);
            startActivityForResult(in, 2);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            //   set_img.setImageURI(Utils.pimageUri);
            setImage(Utils.pimageUri);
        } else {
            Utils.pimageUri = null;
        }


    }

    /**************************************************
     * Allow Permissions
     * ************************************************/
    private void AllowPermission(){
        int hasLocationPermission = ActivityCompat.checkSelfPermission(NewGroup.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(NewGroup.this, android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if( hasLocationPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( android.Manifest.permission.WRITE_EXTERNAL_STORAGE );
        }

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( android.Manifest.permission.CAMERA );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(NewGroup.this,permissions.toArray(new String[permissions.size()]), 100);
        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    /**************************************************
     * Allow Permissions
     * ************************************************/
    private void AllowPermissionContact(){

        int hasSMSPermission = ActivityCompat.checkSelfPermission(NewGroup.this, android.Manifest.permission.READ_CONTACTS);
        List<String> permissions = new ArrayList<String>();

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( android.Manifest.permission.READ_CONTACTS );
        }

        if( !permissions.isEmpty() ) {
            ActivityCompat.requestPermissions(NewGroup.this, permissions.toArray(new String[permissions.size()]), 101);
        }else {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case 100: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        startActivityForResult(getPickImageChooserIntent(), 200);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d( "Permissions", "Permission Denied: " + permissions[i] );
                        Snackbar.make(edt_name,"You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;

            case 101: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        startActivityForResult(intent, 1);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d( "Permissions", "Permission Denied: " + permissions[i] );
                        Snackbar.make(edt_name,"You are not allowed to get Contacts", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;

            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }


    /**********************************************
     * Confirmation dialog
     ********************************************/
    public void dialog(final ModelGroup model, final int grpid) {
        new AlertDialog.Builder(NewGroup.this)
                .setTitle("Alert!")
                .setMessage("Please update any scheduled/recurring SMS setup for this group to reflect the group changes in the SMS.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ModelScheduleSms item = new ModelScheduleSms();
                        item.group_name = model.grp_name;
                        item.recvd_no = model.grp_contact;
                        Dashboard.newsms=true;
                        dataquery.updategroup(model, grpid);
                        dataquery.updatesmsFromGroup(item, groupname);

                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })

                .show();

    }
}
