package com.app.delayed;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.fragments.FragmentSms;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Balvinder on 1/30/2016.
 */
public class NewSMS extends AppCompatActivity {
    static final int PICK_CONTACT_REQUEST = 1;
    Toolbar toolbar;
    TextView title;
    TextView char_count, txt_credits;
    public static EditText edt_message, edt_time, edt_recurring, edt_name;
    ImageView add_contact, mTimeArrow, mSchArrow;
    //  int mYear, mMonth, mDay, hour, minute;
    int curday, curmont, curyear, curhour, curmin;
    // StringBuilder builder;
    int rec_id = 0;
    String recu_type = null;
    Calendar c, c1;
    LinearLayout lay_bottom;
    int timecount = 0;
    public static boolean set = false;
    public static List<String> contact_number;
    public static List<String> contact_name;
    RelativeLayout btn_schedule;
    TextView mSchText;
    String getmessage, getrecvd_no, getgroup_name, getschedule_date, getcontact_name, getsms_id;
    boolean network;
    String auth_code, intenttype;
    SharedPreferences prefsd, prefs;
    ProgressBar progressbar;
    public static String set_name, set_phone, set_groupname = null;
    CharSequence[] items = {"Non recurring", "Recur daily", "Recur weekdays", "Recur weekly", "Recur forthnightly", "Recur monthly", "Recur yearly"};
    String[] sentitems = {"0", "d", "wd", "w", "fn", "m", "y"};
    Date current;
    Methods method;
    boolean valid = true;

    int TIME_PICKER_INTERVAL = 5;
    boolean hourstype = false;
    int style, compareday, comparemonth;
    Menu newmenu;
    boolean setr = false;
    int uphour, upmin, upday, upmonth, upyear;
    DatabaseQueries query;
    private boolean isFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_sms);

        init();
        //   group_adapter_data();
        onClicks();

    }

    //************Initialize UI elements**************
    private void init() {
        query = new DatabaseQueries(NewSMS.this);
        prefsd = getSharedPreferences("delayed", MODE_PRIVATE);
        prefs = getSharedPreferences("name_phone_value", MODE_PRIVATE);
        network = Methods.isNetworkConnected(NewSMS.this);
        auth_code = prefsd.getString("auth_code", null);
        if (prefsd.getString("timeformat", null).equals("24")) {
            hourstype = true;
        } else {
            hourstype = false;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        c = Calendar.getInstance();
        c1 = Calendar.getInstance();
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        char_count = (TextView) findViewById(R.id.char_count);
        mSchText = (TextView) findViewById(R.id.sch_text);
        txt_credits = (TextView) findViewById(R.id.txt_credits);
        btn_schedule = (RelativeLayout) findViewById(R.id.btn_schedule);
        edt_name = (EditText) findViewById(R.id.name);
        edt_message = (EditText) findViewById(R.id.message);
        edt_time = (EditText) findViewById(R.id.set_time);
        edt_recurring = (EditText) findViewById(R.id.set_recurring);
        add_contact = (ImageView) findViewById(R.id.add_contact);
        mTimeArrow = (ImageView) findViewById(R.id.arrow_time);
        mSchArrow = (ImageView) findViewById(R.id.arrow_sch);
        method = new Methods(NewSMS.this, edt_time);
        lay_bottom = (LinearLayout) findViewById(R.id.lay_bottom);


        if (prefsd.getString("credits", null) != null || !prefsd.getString("credits", null).equals("null") || prefsd.getString("credits", null).length() != 0) {
            txt_credits.setText("" + (prefsd.getString("credits", null) + " Credits"));
        }
        curhour = c.get(Calendar.HOUR_OF_DAY);
        curmin = c.get(Calendar.MINUTE);
        curmont = c.get(Calendar.MONTH);
        curday = c.get(Calendar.DAY_OF_MONTH);
        curyear = c.get(Calendar.YEAR);

        Intent intent = getIntent();
        intenttype = intent.getStringExtra("intenttype");
        if (intenttype.equals("update") || intenttype.equals("nonedit")) {
            setTitle("");
            title.setText("Update SMS");
            getmessage = intent.getStringExtra("message");
            set_phone = intent.getStringExtra("recvd_no");
            set_groupname = intent.getStringExtra("group_name");
            getschedule_date = intent.getStringExtra("schedule_date");
            recu_type = intent.getStringExtra("recurring_type");
            set_name = intent.getStringExtra("contact_name");
            getsms_id = intent.getStringExtra("sms_id");
            edt_message.setText(getmessage);
            char_count.setText(getmessage.length() + "/145");
            edt_time.setText(setformater(getschedule_date));
            if (!set_groupname.equals("") || set_groupname.length() != 0) {
                edt_name.setText(set_groupname);
                edt_name.setSelection(set_groupname.length());
            } else if (!set_name.equals("") || set_name.length() != 0) {
                edt_name.setText(set_name);
                edt_name.setSelection(set_name.length());
            } else {
                edt_name.setText(set_phone);
                edt_name.setSelection(set_phone.length());
            }
        /*    if (recu_type.equals("0")) {
                rec_id = 0;
                edt_recurring.setText("Non Recurring");
            } else if (recu_type.equals("fn")) {
                rec_id = 1;
                edt_recurring.setText("Recurring forthnightly");
            } else if (recu_type.equals("wd")) {
                rec_id = 2;
                edt_recurring.setText("Recurring weekdays");
            } else if (recu_type.equals("d")) {
                rec_id = 3;
                edt_recurring.setText("Recurring Daily");
            } else if (recu_type.equals("w")) {
                rec_id = 4;
                edt_recurring.setText("Recurring Weekly");
            } else if (recu_type.equals("m")) {
                rec_id = 5;
                edt_recurring.setText("Recurring Monthly");
            } else if (recu_type.equals("y")) {
                rec_id = 6;
                edt_recurring.setText("Recurring Yearly");
            } else if (recu_type.equals("Sent")) {
                edt_recurring.setText("Sent");
            }*/
            if (recu_type.equals("0")) {
                rec_id = 0;
                edt_recurring.setText("Non Recurring");
            } else if (recu_type.equals("d")) {
                rec_id = 1;
                edt_recurring.setText("Recur Daily");
            } else if (recu_type.equals("wd")) {
                rec_id = 2;
                edt_recurring.setText("Recur weekdays");
            } else if (recu_type.equals("w")) {
                rec_id = 3;
                edt_recurring.setText("Recur weekly");
            } else if (recu_type.equals("fn")) {
                rec_id = 4;
                edt_recurring.setText("Recur forthnightly");
            } else if (recu_type.equals("m")) {
                rec_id = 5;
                edt_recurring.setText("Recur Monthly");
            } else if (recu_type.equals("y")) {
                rec_id = 6;
                edt_recurring.setText("Recur Yearly");
            } else if (recu_type.equals("Sent")) {
                edt_recurring.setText("Sent");
            }
            mSchText.setText("Update");


            if (intenttype.equals("nonedit")) {
                btn_schedule.setVisibility(View.INVISIBLE);
                edt_message.setEnabled(false);
                edt_message.setTextColor(Color.BLACK);
                edt_name.setEnabled(false);
                title.setText("Sent SMS");
                char_count.setVisibility(View.INVISIBLE);
                add_contact.setVisibility(View.INVISIBLE);
                mTimeArrow.setVisibility(View.INVISIBLE);
                mSchArrow.setVisibility(View.INVISIBLE);

            } else {
                char_count.setVisibility(View.VISIBLE);
                add_contact.setVisibility(View.VISIBLE);
                mTimeArrow.setVisibility(View.VISIBLE);
                mSchArrow.setVisibility(View.VISIBLE);
            }

        } else {
            setTitle("");
            title.setText("New SMS");
            mSchText.setText("Schedule");
            edt_name.setText("");
            recu_type = "0";
            rec_id = 0;
            edt_recurring.setText("Non recurring");
        }


        //*********Credits webservice call**********
      //  new ShowCredits().execute();
    }

    private void onClicks() {
        edt_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >= 145) {
                    Snackbar.make(edt_message, "Limit exeeds.", Snackbar.LENGTH_SHORT).show();
                    return;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                char_count.setText(s.length() + "/145");
            }
        });


        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard();
                if (!intenttype.equals("nonedit")) {
                    selectPicker();
                }
            }
        });


        edt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard();
           /*     hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mYear = c.get(Calendar.YEAR);*/
                int setday, setmonth, setyear;
                if (intenttype.equals("update") || intenttype.equals("nonedit")) {
                    updatevalues(edt_time.getText().toString());
                    setday = upday;
                    setmonth = upmonth;
                    setyear = upyear;
                } else {
                    setday = curday;
                    setmonth = curmont;
                    setyear = curyear;
                }
                if (!intenttype.equals("nonedit")) {
                    if (Build.VERSION.SDK_INT < 21) {
                        style = android.graphics.Color.TRANSPARENT;
                        datePickerbuildbelow(datePickerListener2, setyear, setmonth, setday);
                    } else {
                        style = R.style.DialogTheme1;
                        datePicker(datePickerListener1, setyear, setmonth, setday);
                    }
                }
               /* if (Build.VERSION.SDK_INT < 21) {
                    method.getdate(NewSMS.this, mYear, mMonth, mDay, hour, minute);
                } else {
                    builder = new StringBuilder();
                    Methods.datePicker(NewSMS.this, datePickerListener, mYear, mMonth, mDay);
                }*/


            }
        });


        edt_recurring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard();
                if (!intenttype.equals("nonedit")) {
                    RecurringPicker(NewSMS.this, edt_recurring);
                }
            }
        });
        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {

                    AllowPermissionSMS();
                } else {
                   scheduleSMS();
                }


            }
        });


        txt_credits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!intenttype.equals("nonedit")) {
                    startActivity(new Intent(NewSMS.this, BuyCredits.class).putExtra("buy", true));
                }
            }
        });
    }

    private boolean numberchk(String s) {
        boolean ret = false;
        try {
            long num = Long.parseLong(s);
            ret = true;
        } catch (Exception e) {
            ret = false;
        }
        return ret;
    }

    private void scheduleSMS(){
        if (edt_name.length() == 0) {
            Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_time.length() == 0) {
            Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else if (edt_message.length() == 0) {
            Snackbar.make(btn_schedule, "All fields are mandatory", Snackbar.LENGTH_SHORT).show();
        } else {
            String sentdate = sentformatter(edt_time.getText().toString(), 0);
            String trigdate = sentformatter(edt_time.getText().toString(), 1);
            if (set_groupname != null) {
            } else {
                set_groupname = "";
            }

            if (!network) {
                Methods.conDialog(NewSMS.this);

            } else {
                boolean isnum = numberchk(edt_name.getText().toString());
                if (isnum) {
                    isnum = false;
                    set_name = "";
                    set_phone = edt_name.getText().toString();
                    set_phone = set_phone.replaceAll(" ", "");
                }

//                        if (prefsd.getString("credits", null).equals("0")) {
//                            startActivity(new Intent(NewSMS.this, BuyCredits.class).putExtra("buy2", true));
//                            //Snackbar.make(btn_schedule,"You are out of credits, Please buy credits.", Snackbar.LENGTH_SHORT).show();
//                        } else {

                String[] str = sentdate.split(" ");
                ModelScheduleSms model = new ModelScheduleSms();
                model.recvd_no = set_phone;
                model.schedule_date = sentdate;
                model.recurring_type = recu_type;
                model.header_date = str[0];
                model.message = edt_message.getText().toString();
                model.group_name = set_groupname;
                model.contact_name = set_name;
                if (recu_type.equals("0")) {
                    model.list_type = "sch";
                } else {
                    model.list_type = "rec";
                }
                FragmentSms.adaptertype = recu_type;

               /* if (intenttype.equals("update")) {
                    model.sms_id = getsms_id;
                    query.updatesms(model);
                    dialog("Success!", "Your SMS has been updated", android.R.drawable.ic_dialog_info);

                } else {
                    query.insertsms(model);
                    dialog("Success!", "Your SMS has been scheduled", android.R.drawable.ic_dialog_info);

                }*/

                new schdulesms(model,trigdate).execute();
                //--------------commented schduling thread---------------//
                //   new schdulesms(set_groupname, edt_message.getText().toString(), sentdate, trigdate).execute();
                //}

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  group_adapter_data();\
        if (set) {
            set = false;
            if (set_groupname != null) {
                edt_name.setText(set_groupname);
            } else {
                edt_name.setText(set_name);
            }
        }
        HideKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        HideKeyboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_group, newmenu);
        if (intenttype.equals("nonedit")) {
            newmenu.findItem(R.id.action_group).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_group) {

            Intent in = new Intent(NewSMS.this, GroupScreen.class);
            in.putExtra("menu", "yes");
            startActivity(in);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void HideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /*private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                int ti = 0;

                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {

                    if (selectedHour < curhour && valid) {
                        edt_time.setText("");
                        Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else if (selectedMinute < curmin && valid) {
                        edt_time.setText("");
                        Snackbar.make(edt_time, "Please select valid time.", Snackbar.LENGTH_SHORT).show();
                    } else {

                        int set = selectedMinute;
                      *//*  if (selectedMinute > 0 && selectedMinute <= 7 || selectedMinute > 53 && selectedMinute <= 59) {
                            set = 0;
                        } else if (selectedMinute > 7 && selectedMinute <= 15 || selectedMinute > 15 && selectedMinute <= 23) {
                            set = 15;
                        } else if (selectedMinute > 23 && selectedMinute <= 30 || selectedMinute > 30 && selectedMinute <= 37) {
                            set = 30;
                        } else if (selectedMinute > 37 && selectedMinute <= 45 || selectedMinute > 45 && selectedMinute <= 53) {
                            set = 45;
                        } *//*
                        if (selectedMinute <= 15) {
                            set = 0;
                        } else if (selectedMinute <= 30) {
                            set = 15;
                        } else if (selectedMinute <= 45) {
                            set = 30;
                        } else if (selectedMinute <= 59) {
                            set = 45;
                        }

                        hour = selectedHour;
                        minute = set;

                        c.set(Calendar.HOUR_OF_DAY, hour);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
                        displayValue = timeFormatter.format(c.getTime());
                        System.out.println("value time : " + displayValue);

                        recu_type = "0";
                        rec_id = 0;
                        edt_recurring.setText("Non recurring");
                        edt_time.setText(builder.append(", " + displayValue));
                    }

                }

            };
*/
   /* private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (year < curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                edt_time.setText("");
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                timecount = 0;
                c.set(Calendar.YEAR, mYear);
                c.set(Calendar.MONTH, mMonth);
                c.set(Calendar.DAY_OF_MONTH, mDay);
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy");
                dateSet = format1.format(c.getTime());
                //dateSet = DateFormat.getDateInstance().format(c.getTime());
                System.out.println("value date : " + dateSet);
//            builder.append((dayOfMonth))
//                    .append(" ").append((monthOfYear)).append(", ").append((year));

                builder.append(dateSet);
             //   Methods.timePicker(NewSMS.this);
               Methods.timePicker(NewSMS.this, timePickerListener, hour, minute);
            }
        }
    };
*/
    //*************RecurringPicker***********
    private void RecurringPicker(Context ctx, final EditText recurring) {

        // int selected_rec=0;
        new AlertDialog.Builder(ctx, R.style.AppTheme_Dialog)
                .setSingleChoiceItems(items, rec_id, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        rec_id = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button

                        recu_type = sentitems[rec_id];

                        recurring.setText(items[rec_id]);


                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();


        // return selected_rec;
    }

    //*************RecurringPicker***********
    private void selectPicker() {

        // int selected_rec=0;
        final CharSequence[] items = {"Add from contacts", "Add group"};
        new AlertDialog.Builder(NewSMS.this, R.style.AppTheme_Dialog)

                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == 0) {
                            if (Build.VERSION.SDK_INT >= 23) {

                                AllowPermission();
                            } else {
                                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                                startActivityForResult(intent, 1);
                            }

                        } else {
                            Intent in = new Intent(NewSMS.this, GroupScreen.class);
                            in.putExtra("menu", "no");
                            startActivity(in);
                        }
                    }
                })
                .show();


        // return selected_rec;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                set_name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                // TODO Fetch other Contact details as you want to use
                String id = c
                        .getString(c
                                .getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                String hasPhone = c
                        .getString(c
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (hasPhone.equalsIgnoreCase("1")) {
                    Cursor phones = getContentResolver()
                            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + id, null, null);
                    phones.moveToFirst();
                    set_phone = phones.getString(phones
                            .getColumnIndex("data1"));
                    set_phone = set_phone.replaceAll(" ", "");
                    edt_name.setText(set_name);

                }
            }
        }
    }


    class schdulesms extends AsyncTask<String, Void, String> {
        String response;
        ModelScheduleSms item;
        String date;

        public schdulesms(ModelScheduleSms model, String time) {
            this.item = model;
            this.date = time;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            if (intenttype.equals("update")) {
                response = update_schdule_method(date);
            } else {
                response = schdule_method(date);
            }
            System.out.println("response=" + response);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                progressbar.setVisibility(View.GONE);
                JSONObject obj = new JSONObject(response);
                String status = obj.optString("status");
                if (status.equals("true")) {
                    FragmentSms.adaptertype = recu_type;

                   // new getsms().execute();
                    if (intenttype.equals("update")) {
                        item.sms_id = getsms_id;
                        query.updatesms(item);
                        dialog("Success!", "Your SMS has been updated", android.R.drawable.ic_dialog_info);
                    } else {
                        String smsId = obj.optString("last_insert_id");
                        item.sms_id = smsId;
                        query.insertsms(item);
                        dialog("Success!", "Your SMS has been scheduled", android.R.drawable.ic_dialog_info);

                    }
                } else {
                    progressbar.setVisibility(View.GONE);
                    Snackbar.make(btn_schedule, "Error : "+obj.optString("error"), Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                progressbar.setVisibility(View.GONE);
            }
        }
    }

    public String sentformatter(String sendtime, int i) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1;
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }

            Date newDate = format1.parse(sendtime);
            format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (i == 0) {
                settym = format1.format(newDate);
            } else {
                format1.setTimeZone(TimeZone.getTimeZone("UTC"));
                settym = format1.format(newDate);
            }
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    public String setformater(String sendtime) {

        // TODO Auto-generated method stub
        String settym = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);
            if (hourstype) {
                format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
            } else {
                format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
            }
            settym = format1.format(newDate);
        } catch (Exception e) {
            settym = "NA";
        }
        return settym;

    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(NewSMS.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Dashboard.newsms = true;
                            finish();
                    }
                })

                .setIcon(icon)
                .show();

    }

    public String schdule_method( String trigdate) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "add_sms")
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("recurring_type", recu_type)
                .appendQueryParameter("trigger_date", trigdate);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    public String update_schdule_method(String trigdate) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "edit_sms")
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sms_id", getsms_id)
                .appendQueryParameter("recurring_type", recu_type)
                .appendQueryParameter("trigger_date", trigdate);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }


    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermission() {

        int hasSMSPermission = ActivityCompat.checkSelfPermission(NewSMS.this, Manifest.permission.SEND_SMS);
        int hasContactPermission = ActivityCompat.checkSelfPermission(NewSMS.this, android.Manifest.permission.READ_CONTACTS);
        List<String> permissions = new ArrayList<String>();

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.SEND_SMS);
        }
        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.READ_CONTACTS);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(NewSMS.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 1);
        }
    }
    /**************************************************
     * Allow Permissions
     ************************************************/
    private void AllowPermissionSMS() {

        int hasSMSPermission = ActivityCompat.checkSelfPermission(NewSMS.this, Manifest.permission.SEND_SMS);
//        int hasContactPermission = ActivityCompat.checkSelfPermission(NewSMS.this, android.Manifest.permission.READ_CONTACTS);
        List<String> permissions = new ArrayList<String>();

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.SEND_SMS);
        }
//        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
//            permissions.add(android.Manifest.permission.READ_CONTACTS);
//        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(NewSMS.this, permissions.toArray(new String[permissions.size()]), 200);
        } else {
//            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//            startActivityForResult(intent, 1);
            scheduleSMS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        startActivityForResult(intent, 1);
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(add_contact, "Your contacts will not loaded in the app", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case 200: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
//                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                        startActivityForResult(intent, 1);
                        scheduleSMS();
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                       // Snackbar.make(add_contact, "Your contacts will not loaded in the app", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }


    //-------------------------time and date dialogss-------------------------//

    /***************************************
     * Date picker
     *************************************/
    public void datePicker(DatePickerDialog.OnDateSetListener datePickerListener, int mYear, int mMonth, int mDay) {

        DatePickerDialog dialogg = new DatePickerDialog(NewSMS.this, R.style.AppTheme_Dialog, datePickerListener, mYear, mMonth, mDay);
        dialogg.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialogg.show();

    }

    public void datePickerbuildbelow(DatePickerDialog.OnDateSetListener datePickerListener, int Year, int Month, int Day) {

        DatePickerDialog dialog = new DatePickerDialog(NewSMS.this, style, datePickerListener, Year, Month, Day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();


            }
        });

        dialog.show();

    }

    public DatePickerDialog.OnDateSetListener datePickerListener2 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {


            c1.set(Calendar.YEAR, mYear);
            c1.set(Calendar.MONTH, mMonth);
            c1.set(Calendar.DAY_OF_MONTH, mDay);
            compareday = mDay;
            comparemonth = mMonth;


        }
    };

    public void timePickerbelow() {

        isFirst=true;
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        int setmin, sethour;
        if ((intenttype.equals("update") || intenttype.equals("nonedit")) && compareday != day) {
            setmin = upmin;
            sethour = uphour;

        } else {
            setmin = minute;
            sethour = hour;
            if (setmin >= 55 && setmin <= 59) {
                setmin = 0;
                sethour = sethour + 1;
            } else {
                setmin = setmin + 5;
            }

        }

        if (minute >= 55 && minute <= 59) {
            minute = 0;
            hour = hour + 1;
        } else {
            minute = minute + 5;
        }


        final TimePickerDialogs timeee;

        timeee = new TimePickerDialogs(NewSMS.this, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int min) {
                c1.set(Calendar.HOUR_OF_DAY, selectedHour);
                if (Build.VERSION.SDK_INT < 21) {
                    if (isFirst){
                        isFirst = false;
                        c1.set(Calendar.MINUTE, min);
                    }
                } else {
                    c1.set(Calendar.MINUTE, min);
                }
                //             c1.set(Calendar.MINUTE, min);
                SimpleDateFormat timeFormatter;
                if (hourstype) {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                } else {
                    timeFormatter = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                }
                String displayValue = timeFormatter.format(c1.getTime());
                edt_time.setText(displayValue);

            }
        }, sethour, setmin, hourstype);
        timeee.setMin(hour, minute);
        timeee.show();


    }

    //********************************
    // Date change Listener
    //*********************************
    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (year < curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (monthOfYear < curmont && year == curyear) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else if (dayOfMonth < curday && year == curyear && monthOfYear == curmont) {
                valid = false;
                Snackbar.make(edt_time, "Please select valid date.", Snackbar.LENGTH_SHORT).show();
            } else {

                if (dayOfMonth == curday) {
                    valid = true;
                } else {
                    valid = false;
                }
               /* mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;*/

                c1.set(Calendar.YEAR, year);
                c1.set(Calendar.MONTH, monthOfYear);
                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                compareday = dayOfMonth;
                comparemonth = monthOfYear;
                /*if (curmin >= 55) {
                    curmin = 0;
                    curhour = curhour + 1;
                    setr = true;
                } else {
                    curmin = curmin + 5;
                }*/
                timePickerbelow();
                //   timePicker(ChattingActivity.this, timePickerListener1, curhour, curmin);


            }
        }
    };

    public class TimePickerDialogs extends TimePickerDialog {

        private TimePicker timePicker;
        private final OnTimeSetListener callback;

        private int minHour = -1;
        private int minMinute = -1;

        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private java.text.DateFormat dateFormat;

        public TimePickerDialogs(Context arg0, int dialogTheme1, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(arg0, dialogTheme1, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView);
//            this.hr = hourOfDay;
//            this.min = (minute - (minute % TIME_PICKER_INTERVAL)) + TIME_PICKER_INTERVAL;
            this.callback = callBack;
            //    ct = Calendar.getInstance();
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT);

        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
            //   super.onTimeChanged(view, hourOfDay, minute);


            boolean validTime = true;
            if (hourOfDay < minHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == minHour && (minute - 1) * 5 < minMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }

            if (hourOfDay > maxHour && compareday <= curday && comparemonth <= curmont || (hourOfDay == maxHour && minute * 5 > maxMinute) && compareday <= curday && comparemonth <= curmont) {
                validTime = false;

            }
            // if (hourOfDay==minHour&&)

            if (validTime) {
                currentHour = hourOfDay;

                currentMinute = minute;
            } else {
                int j = minMinute / 5;
                int r = minMinute % 5;
                if (r > 3) {
                    j = j + 1;
                }
                if (j < minute) {
                    currentMinute = minute;

                } else {
                    currentMinute = j;

                }

            }


            timePicker.setCurrentHour(currentHour);


            timePicker.setCurrentMinute(currentMinute);


        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            //  System.out.print("view which=" + which);
            if (which == -2) {
                dismiss();
            } else {
                if (callback != null && timePicker != null) {
                    timePicker.clearFocus();
                    callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
                            timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);

                }
            }
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field timePickerField = classForid.getField("timePicker");
                timePicker = (TimePicker) findViewById(timePickerField
                        .getInt(null));
                Field field = classForid.getField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                        .findViewById(field.getInt(null));
                mMinuteSpinner.setMinValue(0);
                mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
                List<String> displayedValues = new ArrayList<String>();
                for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                    displayedValues.add(String.format("%02d", i));
                }

                mMinuteSpinner.setDisplayedValues(displayedValues
                        .toArray(new String[0]));
                timePicker.setOnTimeChangedListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    /**************************************************
     * Update credits webservice
     ************************************************/
    private class ShowCredits extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            String response = CreditMethod(auth_code);
            Log.i("credits response: ", response);
            try {
                JSONObject obj = new JSONObject(response);
                String status = obj.getString("status");
                if (status.equals("true")) {
                    JSONObject pin_nos = obj.optJSONObject("pin_nos");
                    SharedPreferences.Editor editor = prefsd.edit();
                    editor.putString("id", pin_nos.optString("id"));
                    editor.putString("mobile_no", pin_nos.optString("mobile_no"));
                    editor.putString("country_id", pin_nos.optString("country_id"));
                    editor.putString("country", pin_nos.optString("country"));
                    editor.putString("fb_status", pin_nos.optString("status"));
                    editor.putString("pin", pin_nos.optString("pin"));
                    editor.putString("name", pin_nos.optString("name"));
                    editor.putString("setting_email", pin_nos.optString("email"));
                    editor.putString("fb_name", pin_nos.optString("fb_name"));
                    editor.putString("fb_email", pin_nos.optString("fb_email"));
                    editor.putString("fb_token", pin_nos.optString("fb_token"));
                    editor.putString("fb_token_date", pin_nos.optString("fb_token_date"));
                    editor.putString("twitter_token", pin_nos.optString("twitter_token"));
                    editor.putString("twitter_secret", pin_nos.optString("twitter_secret"));
                    editor.putString("twitter_token_date", pin_nos.optString("twitter_token_date"));
                    editor.putString("twitter_hname", pin_nos.optString("twitter_hname"));
                    editor.putString("twitter_uname", pin_nos.optString("twitter_uname"));
                    editor.putString("credits", pin_nos.optString("credits"));
                    editor.putString("flag", pin_nos.optString("flag"));
                    editor.putString("auth_code", pin_nos.optString("auth_code"));
                    editor.putString("token_id", pin_nos.optString("token_id"));
                    editor.putString("token_type", pin_nos.optString("token_type"));
                    editor.putString("chat_icon", pin_nos.optString("chat_icon"));
                    editor.putString("last_updated", pin_nos.optString("last_updated"));
                    editor.commit();

                } else {
                    // Snackbar.make(edt_name, "Response Error" + status, Snackbar.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (prefsd.getString("credits", null) != null || !prefsd.getString("credits", null).equals("null") || prefsd.getString("credits", null).length() != 0) {
                txt_credits.setText("" + (prefsd.getString("credits", null) + " Credits"));
            }

        }
    }

    //***********Network call for credits update***************
    private String CreditMethod(String auth_code) {
        String res = "";
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", "show_credits")
                .appendQueryParameter("auth_code", auth_code);

        res = new JSONParser().getJSONFromUrl(Utils.base_url, builder);
        return res;
    }

    //------------update values-----------------//
    public void updatevalues(String sendtime) {

        Date newDate;
        try {

            if (hourstype) {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, HH:mm");
                newDate = format1.parse(sendtime);
            } else {
                SimpleDateFormat format1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm a");
                newDate = format1.parse(sendtime);
            }


         /*   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format1.parse(sendtime);*/
            Calendar call = Calendar.getInstance();
            call.setTime(newDate);

            uphour = call.get(Calendar.HOUR_OF_DAY);
            upmin = call.get(Calendar.MINUTE);
            upmonth = call.get(Calendar.MONTH);
            upday = call.get(Calendar.DAY_OF_MONTH);
            upyear = call.get(Calendar.YEAR);
        } catch (Exception e) {

        }


    }

    class getsms extends AsyncTask<String, Void, String> {
        String schedule_response, recurring_response, sms_response, statuss, statusr, statust;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            //--------------------get_schedule_sms----------------------//
            //    if (schdulesmslist.size() <= 0) {
            schedule_response = Getsms_Method("get_schedule_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(schedule_response);
                statuss = jobj.optString("status");
                FragmentSms.schdulesmslist.clear();
                if (statuss.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.schdulesmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.schdulesmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            //--------------------get_recurring_sms----------------------//
            //   if (recurringsmslist.size() <= 0) {
            recurring_response = Getsms_Method("get_recurring_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(recurring_response);
                statusr = jobj.optString("status");
                FragmentSms.recurringsmslist.clear();
                if (statusr.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.recurringsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.recurringsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  }
//-----------------------------get_sms-------------------------------//
            sms_response = Getsms_Method("get_sms", auth_code);

            try {
                JSONObject jobj = new JSONObject(sms_response);
                statust = jobj.optString("status");
                FragmentSms.sentsmslist.clear();
                if (statust.equals("true")) {

                    JSONObject obj = jobj.optJSONObject("data");
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {

                        String key = iter.next();
                        ModelScheduleSms value = new ModelScheduleSms();
                        value.header_date = key;
                        value.view_type = "header";
                        FragmentSms.sentsmslist.add(value);

                        JSONArray jarr = obj.optJSONArray(key);
                        for (int i = 0; i < jarr.length(); i++) {
                            ModelScheduleSms item = new ModelScheduleSms();
                            JSONObject iobj = jarr.optJSONObject(i);
                            item.message = iobj.optString("message");
                            item.recvd_no = iobj.optString("recvd_no");
                            item.group_name = iobj.optString("group_name");
                            item.schedule_date = iobj.optString("schedule_date");
                            item.recurring_type = iobj.optString("recurring_type");
                            item.contact_name = iobj.optString("contact_name");
                            item.sms_id = iobj.optString("sms_id");
                            item.view_type = "values";
                            FragmentSms.sentsmslist.add(item);
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);
            if (intenttype.equals("update")) {
                dialog("Success!", "Your SMS has been updated", android.R.drawable.ic_dialog_info);
            } else {
                dialog("Success!", "Your SMS has been scheduled", android.R.drawable.ic_dialog_info);
            }
        }
    }

    public String Getsms_Method(String service_type, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("service_type", service_type)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.base_url, builder);
        return res;
    }
}
