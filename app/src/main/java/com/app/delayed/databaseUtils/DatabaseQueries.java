package com.app.delayed.databaseUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.app.delayed.R;
import com.app.delayed.chatExtended.Entities.Message;
import com.app.delayed.chatExtended.Entities.ModelMsgThrdContent;
import com.app.delayed.chatExtended.Entities.ModelMsgThread;
import com.app.delayed.fragments.FragmentChat;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.model.ModelGroup;
import com.app.delayed.model.ModelScheduleSms;
import com.app.delayed.model.ModelUser;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Person;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DatabaseQueries {

    public static DataBaseHelper dataBaseHelper;

    //***************Table names************
    public static String TABLE_USER = "user_contacts";
    String TABLE_GROUP = "grouping";
    String TABLE_MSG_THREAD = "message_threads";
    String TABLE_MSG_TRD_CONTENT = "message_thread_content";
    String TABLE_SCH_CONTENT_INFO = "scheduled_content_info";
    String TABLE_SMS_SENT = "sms_sent";
    String TABLE_EMAIL = "email_sync";

    String SCH_TYPE = "scheduled_type";
    String SCH_DATE = "scheduled_date_time";

    //---------email-------------//
    String EMAIL_CONTENT = "email_content";
    String EMAIL_NAME = "email_name";
    //---------group--------------//
    String G_ID = "gid";
    String G_NAME = "gname";
    String G_IMAGE = "gimage";
    String G_CONTACTS = "gcontacts";
    String G_CONTACT_NAME = "gcontnames";

    //***********User contact fields********
    public static String USER_NAME = "name";
    public static String DELAYD_USER_ID = "delayd_user_id";
    public static String USER_CONT_NO = "contact_no";
    String USER_COUNTRY_CODE = "country_code";
    public static String USER_ON_DELAYED = "delayd_user";
    public static String USER_PRO_PIC = "profile_pic";
    String USER_LAST_UPDATED = "last_updated";
    String USER_CONTACT_ID = "contact_list_id";


    //********************message_threads***********//
    String THREAD_ID = "thread_id";
    String PARTICIPANT_IDS = "participant_ids";
    String PARTICIPANT_NOS = "participant_numbers";
    String GROUP_NAME = "group_name";
    String GROUP_ICON = "group_icon";
    String THREAD_SYNC_ID = "thread_sync_id";
    String LAST_UPDATED = "last_updated";
    String NOTIFICATION_STATUS = "notification_status";
    String DELETE_STATUS = "deleted_status";
    String BLOCKED = "blocked";

    //********************message_threads content***********//
    String CONTENT_THREAD_ID = "thread_id";
    String CONTENT_ID = "content_id";
    String USER_ID = "user_id";
    String TYPE = "type";
    String CONTENT = "content";
    String THUMB_URL = "thumb_url";
    String CONTENT_STATUS = "status";
    String CONTENT_SYNC_ID = "content_sync_id";
    String CNT_LAST_UPDATED = "last_updated";
    String READ_STATUS = "read_status";
    String SCHDULED_DATE = "schduled_date";
    String CONTENT_TIME = "content_time";
    String SCHDULED_TYPE = "scheduled_type";
    String DELETED_TIME="deleted_time";
    String previus, timeset;

    //********************sms_sent detail***********//
    String SMS_ID = "sms_id";
    String RECV_NUMBER = "recv_number";
    String RECURRING_TYPE = "recurring_type";
    String HEADER_DATE = "header_date";
    String SCHDULE_DATE = "schdule_date";
    String SMS_CONTENT = "sms_content";
    String SMS_GRP_NAME = "group_name";
    String SMS_CONTACT_NAME = "contact_name";
    String LIST_TYPE = "list_type";


    public static int unreadcount = 0;
    Context ctx;

    public DatabaseQueries(Context context) {

        dataBaseHelper = new DataBaseHelper(context);
        this.ctx = context;
    }

    public static String populateItem(Cursor c, String COLUMN) {

        String item = null;
        item = c.getString(c.getColumnIndex(COLUMN));
        return item;
    }

    private byte[] populateImageItem(Cursor c, String COLUMN) {


        byte[] item;
        item = c.getBlob(c.getColumnIndex(COLUMN));
        return item;
    }

    //*****************Content Values for user contacts to insert*************
    private ContentValues CV_User_for_Insert(ModelContacts model) {

        ContentValues values = new ContentValues();
        values.put(USER_NAME, model.cont_name);
        values.put(USER_CONT_NO, model.cont_number);
        values.put(USER_CONTACT_ID, model.cont_id);
        values.put(USER_LAST_UPDATED, model.cont_date);
        values.put(USER_ON_DELAYED, "0");


        return values;
    }

    //*****************Content Values for user contacts to update*************
    private ContentValues CV_User_for_Update(ModelDelayedContact model) {

        ContentValues values = new ContentValues();
        values.put(DELAYD_USER_ID, model.delayd_user_id);
        values.put(USER_ON_DELAYED, model.delayed_user);
        values.put(USER_PRO_PIC, model.profile_pic);
        //   values.put(USER_NAME,model.name);
        values.put(USER_COUNTRY_CODE, model.country_code);


        return values;
    }

    //*****************Content Values for user contacts to update*************
    private ContentValues CV_User_for_Update2(ModelDelayedContact model) {

        ContentValues values = new ContentValues();
        values.put(DELAYD_USER_ID, model.delayd_user_id);
        values.put(USER_ON_DELAYED, model.delayed_user);
        values.put(USER_PRO_PIC, model.profile_pic);
        values.put(USER_NAME, model.name);
        values.put(USER_COUNTRY_CODE, model.country_code);


        return values;
    }

    //**********************INSERT into UserContacts Table*****************
    public void InsertContacts(ModelContacts model) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_User_for_Insert(model);
            dataBaseHelper.getWritableDatabase().insert(TABLE_USER, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**********************Update UserContacts Table*****************
    public void UpdateContacts(ModelDelayedContact model) throws Exception {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_User_for_Update(model);
            dataBaseHelper.getWritableDatabase().update(TABLE_USER, values, USER_CONT_NO + " LIKE ?", new String[]{"%" + model.contact_no});

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**********************Update UserContacts Table*****************
    public void UpdateContacts2(ModelDelayedContact model) throws Exception {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_User_for_Update2(model);
            dataBaseHelper.getWritableDatabase().update(TABLE_USER, values, USER_CONT_NO + " LIKE ?", new String[]{"%" + model.contact_no});

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //********************Update contacts using ModelContacs class*******************
    public void UpdateContactsdb(ModelContacts model) throws Exception {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_User_for_Updatedb(model);
            dataBaseHelper.getWritableDatabase().update(TABLE_USER, values, USER_CONT_NO + " LIKE ?", new String[]{"%" + model.cont_number});

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ContentValues CV_User_for_Updatedb(ModelContacts model) {

        ContentValues values = new ContentValues();
        values.put(USER_NAME, model.cont_name);
        values.put(USER_CONT_NO, model.cont_number);
        values.put(USER_CONTACT_ID, model.cont_id);
        values.put(USER_LAST_UPDATED, model.cont_date);

        return values;
    }

    //**************************Get Data from UserContacts Table ***********************
    public List<ModelUser> GetUserList(String type) throws Exception {
        List<ModelUser> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            //    String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ON_DELAYED + " = '1'";
            String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ON_DELAYED + " = '" + type + "' ORDER BY " + USER_NAME + " COLLATE NOCASE";
            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                ModelUser item = new ModelUser();
                if (type.equals("1")) {
                    item.user_name = populateItem(c, USER_NAME);
                    item.user_id = populateItem(c, DELAYD_USER_ID);
                    item.user_pic = populateItem(c, USER_PRO_PIC);
                    item.delayedtype = type;
                    item.user_country_code = populateItem(c, USER_COUNTRY_CODE);
                    item.user_contact = (populateItem(c, USER_CONT_NO));

                } else {
                    item.user_name = populateItem(c, USER_NAME);
                    item.user_id = populateItem(c, USER_CONTACT_ID);
                    item.delayedtype = type;
                    item.user_contact = (populateItem(c, USER_CONT_NO));
                    item.user_country_code = "";
                    item.user_pic = "";
                }

                if (arr.size() > 0) {
                    if (!isDuplicate(arr, item)) {
                        arr.add(item);
                    }
                } else {
                    arr.add(item);
                }

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    //**************************Get updated Data from UserContacts Table ***********************
    public List<ModelContacts> getupdateduserlist() throws Exception {
        List<ModelContacts> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ON_DELAYED + " = '0'";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                ModelContacts item = new ModelContacts();
                item.cont_name = populateItem(c, USER_NAME);
                item.cont_number = populateItem(c, USER_CONT_NO);
                item.cont_id = populateItem(c, USER_CONTACT_ID);
                item.cont_date = populateItem(c, USER_LAST_UPDATED);

                if (arr.size() > 0) {
                    if (!isDuplicateup(arr, item)) {
                        arr.add(item);
                    }
                } else {
                    arr.add(item);
                }

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
              e.printStackTrace();
        }
        return arr;
    }

    /***************************************
     * Check Duplicate entries
     *************************************/
    private boolean isDuplicate(List<ModelUser> arr, ModelUser user) {
        for (ModelUser model : arr) {

            if (model.user_id.equals(user.user_id)) {
                return true;
            }
        }

        return false;
    }

    private boolean isDuplicateup(List<ModelContacts> arr, ModelContacts user) {
        for (ModelContacts model : arr) {

            if (model.cont_number.equals(user.cont_number)) {
                return true;
            }
        }

        return false;
    }

    /**************************************************
     * Check for duplicate threads
     ************************************************/
    private boolean isDuplicateThread(List<ModelMsgThread> arr, ModelMsgThread user) {
        for (ModelMsgThread model : arr) {

            if (model.thread_id.equals(user.thread_id)) {
                return true;
            }
        }

        return false;
    }

    /*******************************
     * Get last 10 Digits
     *******************************/
    public static String Subset(String text) {
        if (text.length() == 10) {
            return text;
        } else if (text.length() > 10) {
            return text.substring(text.length() - 10);
        }
        return null;
    }


    //----------------------INSERT into Group Table------------------------//
    public void insertgroup(ModelGroup model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(G_NAME, model.grp_name);
        values.put(G_IMAGE, model.grp_img);
        values.put(G_CONTACTS, model.grp_contact);
        values.put(G_CONTACT_NAME, model.grp_contact_name);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GROUP, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------UPDATE into Group Table------------------------//
    public void updategroup(ModelGroup model, int grpid) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        //     values.put(G_ID, model.grp_id);
        values.put(G_NAME, model.grp_name);
        values.put(G_IMAGE, model.grp_img);
        values.put(G_CONTACTS, model.grp_contact);
        values.put(G_CONTACT_NAME, model.grp_contact_name);
        dataBaseHelper.getWritableDatabase().update(TABLE_GROUP, values, G_ID + " ='" + grpid + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public ArrayList<ModelGroup> getGroup() {

        ArrayList<ModelGroup> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GROUP;

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                ModelGroup model = new ModelGroup();
                model.grp_id = c.getInt(c.getColumnIndex(G_ID));
                model.grp_name = c.getString(c.getColumnIndex(G_NAME));
                model.grp_contact_name = c.getString(c.getColumnIndex(G_CONTACT_NAME));
                model.grp_contact = c.getString(c.getColumnIndex(G_CONTACTS));
                model.grp_img = c.getBlob(c.getColumnIndex(G_IMAGE));

                arr.add(model);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }


    //    **********************Get data from Order_info table***********************
    public int getallcount(String groupname) {
        String countQuery = "";
        countQuery = "SELECT  * FROM " + TABLE_GROUP + " WHERE " + G_NAME + " = '" + groupname + "'";
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    /****************************************
     * Delete Method
     **************************************/
    public void delete_value(int gid) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_GROUP, G_ID + " = ?", new String[]{String.valueOf(gid)});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /******************************************
     * Get existing message threads
     ****************************************/
    public String getexistingthread(String pids, int type) {
        String retur = "no";
        Cursor c = null;

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            // String select = "SELECT " + THREAD_ID + " FROM " + TABLE_MSG_THREAD + " WHERE " + PARTICIPANT_IDS + " ='" + str[i] + "'";
            if (type == 0) {
                String[] str = pids.split("/");
                String[] FIELDS = {THREAD_ID, GROUP_NAME};
                String WHERE = PARTICIPANT_IDS + "='" + str[0] + "' OR " + PARTICIPANT_IDS + "='" + str[1] + "'";
                c = db.query(TABLE_MSG_THREAD, FIELDS, WHERE, null, null, null, null);
            } else if (type == 1) {
                String[] FIELDS = {THREAD_ID};
                String WHERE = PARTICIPANT_IDS + "='" + pids + "' ";
                c = db.query(TABLE_MSG_THREAD, FIELDS, WHERE, null, null, null, null);
            }
            while (c.moveToNext()) {
                if (type == 0) {
                    String gp_name = c.getString(c.getColumnIndex(GROUP_NAME));
                    if (gp_name.length() != 0) {
                        retur = "no";
                    } else {
                        retur = c.getString(c.getColumnIndex(THREAD_ID));
                    }
                } else {
                    retur = c.getString(c.getColumnIndex(THREAD_ID));
                }
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return retur;
    }


    /******************************************
     * Insert message threads
     ****************************************/
    public void insertmsgthread(ModelMsgThread item) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_Msg_Thread_Insert(item);
            dataBaseHelper.getWritableDatabase().insert(TABLE_MSG_THREAD, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /******************************************
     * Content values for Insert message Threads
     ****************************************/
    private ContentValues CV_Msg_Thread_Insert(ModelMsgThread model) {
        ContentValues values = new ContentValues();
        values.put(THREAD_ID, model.thread_id);
        values.put(PARTICIPANT_IDS, model.participant_ids);
        values.put(PARTICIPANT_NOS, model.participant_numbers);
        values.put(GROUP_ICON, model.group_icon);
        values.put(GROUP_NAME, model.group_name);
        values.put(THREAD_SYNC_ID, model.thread_sync_id);
        values.put(LAST_UPDATED, model.last_updated);
        values.put(NOTIFICATION_STATUS, model.notification_status);
        values.put(DELETE_STATUS, model.deleted_status);
        values.put(BLOCKED, model.blocked_status);
        return values;
    }

    /******************************************
     * update message threads
     ****************************************/
    public void updatemsgthread(ModelMsgThread item, String type) {
        ContentValues values = null;
        try {
            dataBaseHelper.createDataBase();

            values = CV_Msg_Thread_Update(item);

            dataBaseHelper.getWritableDatabase().update(TABLE_MSG_THREAD, values, THREAD_ID + " ='" + item.thread_id + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    /******************************************
     * Content values for Update message Threads
     ****************************************/
    private ContentValues CV_Msg_Thread_Update(ModelMsgThread model) {
        ContentValues values = new ContentValues();
        values.put(PARTICIPANT_IDS, model.participant_ids);
        values.put(PARTICIPANT_NOS, model.participant_numbers);
        values.put(GROUP_ICON, model.group_icon);
        values.put(GROUP_NAME, model.group_name);
        values.put(THREAD_SYNC_ID, model.thread_sync_id);
        values.put(LAST_UPDATED, model.last_updated);

        return values;
    }

    /******************************************
     * Insert messages
     ****************************************/
    public void insertmsgthreadcontent(ModelMsgThrdContent item) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_Msg_Thread_Content_Insert(item);
            dataBaseHelper.getWritableDatabase().insert(TABLE_MSG_TRD_CONTENT, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /******************************************
     * Content values for Insert messages
     ****************************************/
    private ContentValues CV_Msg_Thread_Content_Insert(ModelMsgThrdContent model) {
        ContentValues values = new ContentValues();
        values.put(CONTENT_THREAD_ID, model.thread_id);
        values.put(CONTENT_ID, model.content_id);
        values.put(USER_ID, model.user_id);
        values.put(TYPE, model.type);
        values.put(CONTENT, model.content);
        values.put(CONTENT_TIME, model.content_time);
        values.put(THUMB_URL, model.thumb_url);
        values.put(CONTENT_STATUS, model.status);
        values.put(CONTENT_SYNC_ID, model.content_sync_id);
        values.put(CNT_LAST_UPDATED, model.last_updated);
        values.put(READ_STATUS, model.read_status);
        values.put(SCHDULED_DATE, model.schduled_date);
        values.put(DELETED_TIME,model.deleted_time);
        values.put(SCHDULED_TYPE, model.scheduled_type);
        return values;
    }

    /****************************************************
     * Get all messages
     **************************************************/
    public ArrayList<ModelMsgThread> getallmsgs(String userid, String status, String name, String icon) {
        String ids = null;
        ArrayList<ModelMsgThread> arr = new ArrayList<ModelMsgThread>();
        ArrayList<String> contarr = new ArrayList<>();
        //  unreadcount = 0;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD;//+" ORDER BY datetime("+LAST_UPDATED+") DESC";

            Cursor c = db.rawQuery(select, new String[]{});

            while (c.moveToNext()) {

                ModelMsgThread item = new ModelMsgThread();
                String threadid = populateItem(c, THREAD_ID);
                item.thread_id = threadid;
                item.readcontentids = getreadstatus(threadid, 0);
                item.participant_ids = populateItem(c, PARTICIPANT_IDS);
                item.participant_numbers = populateItem(c, PARTICIPANT_NOS);
                String lastmsg = getlastmsg(threadid, 0, status);

                item.last_msg = lastmsg;
                String[] number = populateItem(c, PARTICIPANT_NOS).split(",");
                ids = populateItem(c, PARTICIPANT_IDS);
                String[] pids = ids.split(",");
                if (populateItem(c, GROUP_NAME).length() == 0) {

                    item.isGroup = false;
                    if (ids.equals(userid)) {
                        item.chat_user_id = userid;

                        item.group_name = "You";

                        if (item.group_name.length() == 0 || item.group_name == null) {
                            item.group_name = number[0];
                        }
                        item.group_icon = icon;
                    } else if (!pids[0].equals(userid)) {
                        item.chat_user_id = pids[0];
                        //item.group_name = getname(pids[0], USER_NAME);
                        item.group_name = getUsername(number[0], USER_NAME);
                        if (item.group_name.length() == 0 || item.group_name == null) {
                            item.group_name = number[0];
                        }
                        item.group_icon = getname(pids[0], USER_PRO_PIC);

                    } else if (!pids[1].equals(userid)) {
                        item.chat_user_id = pids[1];
                        item.group_name = getUsername(number[1], USER_NAME);
                        if (item.group_name.length() == 0 || item.group_name == null) {
                            item.group_name = number[1];
                        }
                        item.group_icon = getname(pids[1], USER_PRO_PIC);
                    }

                } else {
                    item.isGroup = true;
                    item.chat_user_id = ids;
                    item.group_name = populateItem(c, GROUP_NAME);
                    item.group_icon = populateItem(c, GROUP_ICON);

                    //*********Sync unknown contacts*****
                    userid = FragmentChat.prefs.getString("id", null);
                    SyncContacts(pids, number, userid);
                }


                item.thread_sync_id = populateItem(c, THREAD_SYNC_ID);
                item.notification_status = populateItem(c, NOTIFICATION_STATUS);
                item.blocked_status = populateItem(c, BLOCKED);
                item.last_updated = getlastmsg(threadid, 1, status);
                item.last_read_status = getlastmsg(threadid, 2, status);
                if (item.last_read_status.equals("0")) {
                    contarr.add(item.last_read_status);
                }
                if (item.isGroup) {
                    if (lastmsg.equals("NA")) {

                        item.last_msg = "No Message";
                        item.last_updated = populateItem(c, LAST_UPDATED);
                        if (status.equals("0")) {
                            if (!isDuplicateThread(arr, item)) {
                                arr.add(item);
                            }
                        }
                    } else {
                        if (!isDuplicateThread(arr, item)) {
                            arr.add(item);
                        }
                    }

                } else {
                    if (!lastmsg.equals("NA")) {
                        if (!isDuplicateThread(arr, item)) {
                            arr.add(item);
                        }
                    }
                }

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        getunreadthread();
        //  unreadcount=contarr.size();
        return arr;
    }


    /*************************************************
     * Method to sync unknown contscts of group
     ***********************************************/
    private void SyncContacts(String[] participent_ids_arr, String[] nos, String userid) {

        ArrayList<ModelContacts> mContacts = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String date = format.format(new Date());

        for (int i = 0; i < participent_ids_arr.length; i++) {
            String id = participent_ids_arr[i];
            System.out.println("ids=" + id);

            if (!userid.equals(id)) {
                if (contactexist(id)) {
                } else {
                    ModelContacts user = new ModelContacts();
                    user.cont_name = nos[i];
                    user.cont_number = nos[i];
                    user.cont_id = id;
                    user.cont_date = date;
                    InsertnewContacts(user);
                    mContacts.add(user);

                }


            }

        }

        if (mContacts.size() > 0) {
            new contactsync(mContacts).execute();
        }
    }


    /****************************************************
     * Get Last message
     ****************************************************/
    private String getlastmsg(String threadid, int typeset, String status) {

        String value = "NA";
        String select;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            if (status.equals("0")) {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + " ='" + threadid + "' AND (" + CONTENT_STATUS + " = '0' OR " + CONTENT_STATUS + " = '4') ORDER BY " + LAST_UPDATED + " ASC ";
            } else {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + " ='" + threadid + "' AND " + CONTENT_STATUS + " = '" + status + "' ORDER BY " + LAST_UPDATED + " ASC ";
            }

            Cursor c = db.rawQuery(select, null);

            if (c.moveToLast()) {

                if (typeset == 0) {
                    String type = populateItem(c, TYPE);
                    String userid = FragmentChat.prefs.getString("id", null);
                    String sender_id = populateItem(c, USER_ID);
                    if (type.equals("0")) {
                        value = populateItem(c, CONTENT);
                    } else if (type.equals("1")) {
                        if (sender_id.equals(userid)) {
                            value = ctx.getString(R.string.msg_sentImage);
                        } else {
                            String name = getname(sender_id, USER_NAME);
                            value = name.concat(ctx.getString(R.string.msg_receiveImage));
                        }

                    } else if (type.equals("2")) {
                        if (sender_id.equals(userid)) {
                            value = ctx.getString(R.string.msg_sentVoice);
                        } else {
                            String name = getname(sender_id, USER_NAME);
                            value = name.concat(ctx.getString(R.string.msg_receiveVoice));
                        }
                    }
                } else if (typeset == 1) {
                    if (status.equals("0")) {
                        value = populateItem(c, CNT_LAST_UPDATED);
                    } else {
                        value = populateItem(c, SCHDULED_DATE);
                    }
                } else {
                    value = populateItem(c, READ_STATUS);
                }

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return value;
    }


    /****************************************************
     * Get User name or Group name
     **************************************************/
    public static String getname(String pid, String column) {
        String value = "";

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
           // String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ON_DELAYED + " = '1' AND " + DELAYD_USER_ID + " ='" + pid + "'";
            String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ON_DELAYED + " = '1' AND " + DELAYD_USER_ID + " ='" + pid + "'";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                value = populateItem(c, column);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /****************************************************
     * Get User name or User name
     **************************************************/
    public static String getUsername(String number, String column) {
        String value = "";

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
             String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_CONT_NO + " LIKE  '%" + Subset(number) + "'";
            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                value = populateItem(c, column);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /****************************************************
     * Get all Chat threads
     **************************************************/
    public List<Message> getthreadchat(String threadids, Context con, String userid, String oderpic, String odername, String usrpic, String usrname, String delayed) {
        List<Message> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + CONTENT_STATUS + " = '" + delayed + "'";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                Message item = new Message(con);
                item.setThread_id(populateItem(c, CONTENT_THREAD_ID));
                item.setmContentid(populateItem(c, CONTENT_ID));
                String type = populateItem(c, TYPE);
                String path = populateItem(c, THUMB_URL);
                String id = populateItem(c, USER_ID);

                item.time_text = populateItem(c, LAST_UPDATED);
                if (id.equals(userid)) {
                    item.setMine(true);
                    item.username = usrname;
                    item.userpic = usrpic;
                } else {
                    item.username = odername;
                    item.userpic = oderpic;
                    item.setMine(false);
                }
                item.setmType(Integer.parseInt(type));
                if (type.equals("0")) {
                    item.setmText(populateItem(c, CONTENT));
                } else if (type.equals("1")) {
                    item.imgurl = path;
                    item.setmText("");
                    item.originalurl = populateItem(c, CONTENT);

                } else if (type.equals("2")) {
                    item.setmText("");
                    item.imgurl = path;
                    item.setmText("");
                    item.setAudioLength(populateItem(c, CONTENT_TIME));
                    item.originalurl = populateItem(c, CONTENT);
                }
                arr.add(item);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return arr;
    }


    /****************************************************
     * Get file path from URI
     **************************************************/
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    /****************************************************
     * Get last sync ID
     **************************************************/
    public String getsynchid(String tabl, String column) {
        String value = "0";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            Cursor c = db.query(tabl, new String[]{"MAX(" + column + ")"}, null, null, null, null, null);
            try {
                c.moveToFirst();
                value = String.valueOf(c.getInt(0));

            } finally {
                c.close();
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return value;
    }

    /****************************************************
     * Date formatter
     **************************************************/
    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("dd MMM, hh:mm a");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = senddate;
        }
        return setdate;

    }


    /****************************************************
     * Insert Delayed content
     **************************************************/
    public void insertschcontent(String content_id, String schtype, String schdate) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(CONTENT_ID, content_id);
            values.put(SCH_TYPE, schtype);
            values.put(SCH_DATE, schdate);

            dataBaseHelper.getWritableDatabase().insert(TABLE_SCH_CONTENT_INFO, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public List<Message> gettfilterhreadchat(String threadids, Context con, String userid, String mPic, String mName, String userpic, String username, String mContenttype) {
        ArrayList<String> contents = new ArrayList<>();
        contents = getfiltercontent(mContenttype);
        List<Message> arr = new ArrayList<>();
        for (int i = 0; i < contents.size(); i++) {
            try {
                dataBaseHelper.createDataBase();

                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
                String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + CONTENT_ID + " ='" + contents.get(i) + "'";

                Cursor c = db.rawQuery(select, null);

                while (c.moveToNext()) {

                    Message item = new Message(con);
                    item.setThread_id(populateItem(c, CONTENT_THREAD_ID));
                    String type = populateItem(c, TYPE);
                    String path = populateItem(c, THUMB_URL);
                    String id = populateItem(c, USER_ID);

                    item.time_text = savedateformatter(populateItem(c, LAST_UPDATED));
                    if (id.equals(userid)) {
                        item.setMine(true);
                        item.username = username;
                        item.userpic = userpic;
                    } else {
                        item.username = mName;
                        item.userpic = mPic;
                        item.setMine(false);
                    }
                    item.setmType(Integer.parseInt(type));
                    if (type.equals("0")) {
                        item.setmText(populateItem(c, CONTENT));
                    } else if (type.equals("1")) {
                        item.imgurl = path;
                        item.setmText("");
                        item.originalurl = populateItem(c, CONTENT);

                    } else if (type.equals("2")) {
                        item.setmText("");
                        item.imgurl = path;
                        item.setmText("");
                        item.setAudioLength(populateItem(c, CONTENT_TIME));
                        item.originalurl = populateItem(c, CONTENT);

                    }
                    arr.add(item);


                }
                dataBaseHelper.close();
                db.close();
                c.close();

            } catch (Exception e) {
                //  e.printStackTrace();
                e.printStackTrace();
            }
        }
        return arr;
    }

    private ArrayList<String> getfiltercontent(String mContenttype) {
        ArrayList<String> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_SCH_CONTENT_INFO + " WHERE " + SCH_TYPE + "='" + mContenttype + "'";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {
                arr.add(populateItem(c, CONTENT_ID));
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return arr;
    }


    /****************************************************
     * Delete a message
     **************************************************/
    public void deleteContent(String threadids, String Contentid, String time_text) {
        String id = Contentid;
        String time = time_text;
        String thrdid = threadids;

        try {
            dataBaseHelper.createDataBase();
            if (Contentid!=null) {

                dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_TRD_CONTENT, CONTENT_THREAD_ID + "=? AND " + CONTENT_ID + " = ?", new String[]{threadids, Contentid});
            } else {
                dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_TRD_CONTENT, CONTENT_THREAD_ID + "=? AND " + DELETED_TIME + " = ?", new String[]{threadids, time_text});

            }
            dataBaseHelper.close();
        } catch (IOException e) {

        }

    }

    /****************************************************
     * Delete a Thread
     **************************************************/
    public void deleteThread(String threadids) {
        try {
            dataBaseHelper.createDataBase();
            dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_THREAD, THREAD_ID + " = ?", new String[]{threadids});
            dataBaseHelper.close();
        } catch (IOException e) {

        }

    }

    /****************************************************
     * Delete all content from a thread
     **************************************************/
    public void deleteAllContent(String threadids, String status, boolean isgroup) {
        try {
            dataBaseHelper.createDataBase();
            // dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_TRD_CONTENT, THREAD_ID + " = ?", new String[]{threadids});
            if (isgroup) {
                dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_TRD_CONTENT, THREAD_ID + " = ?", new String[]{threadids});
            } else {
                dataBaseHelper.getWritableDatabase().delete(TABLE_MSG_TRD_CONTENT, THREAD_ID + " = '" + threadids + "' AND " + CONTENT_STATUS + " = '" + status + "'", null);
            }
            dataBaseHelper.close();
        } catch (IOException e) {

        }

    }

    /****************************************************
     * Get Last updated date
     **************************************************/
    public String getlastdate() {
        String value = "NA";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            //    String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_SYNC_ID + "='" + "SELECT MAX ("+CONTENT_SYNC_ID+") FROM " + TABLE_MSG_TRD_CONTENT  + "'";
//String select="SELECT MAX ( "+CONTENT_SYNC_ID+" ) FROM " + TABLE_MSG_TRD_CONTENT;
            String select = "SELECT * FROM " + TABLE_USER;
            Cursor c = db.rawQuery(select, null);

            if (c.moveToLast()) {
                value = populateItem(c, USER_LAST_UPDATED);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return value;
    }

    /****************************************************
     * Get all contacts count
     **************************************************/
    public int getcontactcount(String number) {
        int cnt = 0;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_CONT_NO + " LIKE  '%" + Subset(number) + "'";


            Cursor c = db.rawQuery(select, null);

            cnt = c.getCount();

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    /****************************************************
     * Get notification status
     **************************************************/
    public String getNotification(String thread_id) {
        String cnt = "0";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD + " WHERE " + THREAD_ID + " = " + thread_id;


            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                cnt = c.getString(c.getColumnIndex(NOTIFICATION_STATUS));
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    /****************************************************
     * Get block status
     **************************************************/
    public String getBlock(String thread_id) {
        String cnt = null;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD + " WHERE " + THREAD_ID + " = " + thread_id;


            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                cnt = c.getString(c.getColumnIndex(BLOCKED));
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }


    /****************************************************
     * Get notification status
     **************************************************/
    public String getParticipents(String thread_id) {
        String cnt = "";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD + " WHERE " + THREAD_ID + " = " + thread_id;


            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                cnt = c.getString(c.getColumnIndex(PARTICIPANT_IDS));
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    /****************************************************
     * Check content exists or not
     **************************************************/
    public boolean contentexist(String contentid) {
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_ID + " =  '" + contentid + "'";


            Cursor c = db.rawQuery(select, null);
            if (c.getCount() > 0) {
                return true;
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /****************************************************
     * Check thread exists or not
     **************************************************/
    public boolean isthreadexist(String threadid) {
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD + " WHERE " + THREAD_ID + " =  '" + threadid + "'";


            Cursor c = db.rawQuery(select, null);
            if (c.getCount() > 0) {
                return true;
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /****************************************************
     * Check delayed contact exists or not
     **************************************************/
    public boolean contactexist(String id) {
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_USER + " WHERE " + DELAYD_USER_ID + " =  '" + id + "'";

            Cursor c = db.rawQuery(select, null);
            if (c.getCount() > 0) {
                return true;
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /****************************************************
     * insert new delayed contact
     **************************************************/
    public void InsertnewContacts(ModelContacts model) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(USER_NAME, model.cont_name);
            values.put(USER_CONT_NO, model.cont_number);
            values.put(DELAYD_USER_ID, model.cont_id);
            values.put(USER_ON_DELAYED, "1");
            dataBaseHelper.getWritableDatabase().insert(TABLE_USER, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /****************************************************
     * Update messages to insert sync id
     **************************************************/
    public void updatemsgcontent(String content_id, String content_sync_id, String status) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(CONTENT_SYNC_ID, content_sync_id);
        values.put(CONTENT_STATUS, status);
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_TRD_CONTENT, values, CONTENT_ID + " ='" + content_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /****************************************************
     * Update messages to insert sync id
     **************************************************/
    public void updatemyread(String content_id, String content_sync_id, String status) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(CONTENT_SYNC_ID, content_sync_id);
        values.put(CONTENT_STATUS, status);
        values.put(READ_STATUS, "0");
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_TRD_CONTENT, values, CONTENT_ID + " ='" + content_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /****************************************************
     * Update messages to insert local image url
     **************************************************/
    public void updateImagePath(String content_id, String path) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(CONTENT, path);
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_TRD_CONTENT, values, CONTENT_ID + " ='" + content_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /****************************************************
     * Update notification status
     **************************************************/
    public void updateNotification(String thread, String status) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_STATUS, status);
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_THREAD, values, THREAD_ID + " ='" + thread + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /****************************************************
     * Update block status
     **************************************************/
    public void updateBlock(String thread, String status) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(BLOCKED, status);
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_THREAD, values, THREAD_ID + " ='" + thread + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //********************Get Images url from Content table*************
    public ArrayList<String> GetImages(String thread) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            // e.printStackTrace();
        }

        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + THREAD_ID + " = '" + thread + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<String> arr = new ArrayList<>();

        while (c.moveToNext()) {

            String url = populateItem(c, CONTENT);
            if (url.endsWith("png") || url.endsWith("jpg")
                    || url.endsWith("jpeg") || url.endsWith("JPEG")
                    || url.endsWith("PNG")) {
                arr.add(url);
            }
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return arr;
    }

     /*//****************Delete row from Group Table*************
     public void deleteGroup(String name) {
     try {
     dataBaseHelper.createDataBase();
     } catch (IOException e) {
     //  e.printStackTrace();
     }
     dataBaseHelper.getWritableDatabase().delete(TABLE_GROUP, GRP_NAME + " = ?", new String[]{name});
     try {
     dataBaseHelper.close();
     } catch (Exception e) {
     //  e.printStackTrace();
     }
     }*/

    //-----------------update content read status---------------//
    public void updatestatus(String ids) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            //     values.put(G_ID, model.grp_id);
            values.put(READ_STATUS, 1);
            dataBaseHelper.getWritableDatabase().update(TABLE_MSG_TRD_CONTENT, values, CONTENT_ID + " ='" + ids + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //---------------------unread content ids--------------//
    public ArrayList<String> getreadstatus(String threadids, int type) {
        ArrayList<String> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();
            String select = "";
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            if (type == 0) {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + READ_STATUS + " = '0'";
            } else {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + CONTENT_STATUS + " = '" + Utils.chat_section + "'";

            }
            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                arr.add(populateItem(c, CONTENT_ID));

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return arr;
    }

    public void delete(String table) {
        try {
            dataBaseHelper.createDataBase();
            dataBaseHelper.getWritableDatabase().delete(table, null, null);
            dataBaseHelper.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
    }

    //------------------limit-offset query---------------------//
    public ArrayList<String> getlimit() {
        ArrayList<String> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " ORDER BY content_id DESC LIMIT 0, 10";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {
                String id = populateItem(c, CONTENT_ID);
                arr.add(populateItem(c, CONTENT_ID));

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return arr;
    }

    public List<Message> getlimitthreadchat(String threadids, Context con, String userid, String oderpic, String odername, String usrpic, String usrname, String delayed, int limit, int offset) {
        List<Message> arr = new ArrayList<>();
        previus = "null";
        timeset = "";
        Cursor c;
        //ORDER BY last_updated DESC LIMIT
        try {
            dataBaseHelper.createDataBase();
            String select = "";
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            if (delayed.equals("0")) {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND (" + CONTENT_STATUS + " = '0' OR " + CONTENT_STATUS + " = '4') ORDER BY last_updated DESC LIMIT " + String.valueOf(limit) + ", " + String.valueOf(offset);
                c = db.rawQuery(select, null);
            } else {
                select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + CONTENT_STATUS + " = '" + delayed + "' ORDER BY last_updated DESC LIMIT " + String.valueOf(limit) + ", " + String.valueOf(offset);
                c = db.rawQuery(select, null);
            }

            //DESC
            //  String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "' AND " + CONTENT_STATUS + " = '" + delayed + "' ORDER BY content_id DESC LIMIT " + String.valueOf(20) + ", " + String.valueOf(20);

            //  String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + threadids + "'";


            int count = c.getCount();
            System.out.println("count=" + count);
            //  c.moveToLast();
            c.moveToPosition(count);
            while (c.moveToPrevious()) {

                Message item = new Message(con);
                item.setThread_id(populateItem(c, CONTENT_THREAD_ID));
                String conteid = populateItem(c, CONTENT_ID);
                item.setmContentid(populateItem(c, CONTENT_ID));
                String type = populateItem(c, TYPE);
                String path = populateItem(c, THUMB_URL);
                String id = populateItem(c, USER_ID);
                timeset = populateItem(c, LAST_UPDATED);
                item.time_text = populateItem(c, LAST_UPDATED);
                item.displaydate = false;
               /* if (delayed.equals("0")) {

                } else {
                    timeset = populateItem(c, SCHDULED_DATE);
                    item.time_text = populateItem(c, SCHDULED_DATE);
                }*/
                //   item.time_text=timeset;
                item.schtype = populateItem(c, SCHDULED_TYPE);
                String str[] = timeset.split(" ");
                if (str[0].equals(previus)) {
                    item.headerdate = "0";
                } else {
                    item.headerdate = "1";
                    String matchtime = Methods.tochatnormaltime(timeset, "24", 0);
                    if (Utils.daterr.contains(matchtime)) {
                        for (int i = 0; i < Utils.daterr.size(); i++) {
                            if (Utils.daterr.get(i).equals(matchtime)) {
                                Utils.coarr.set(i, conteid);
                            }
                        }
                    } else {
                        Utils.daterr.add(Methods.tochatnormaltime(timeset, "24", 0));
                        Utils.coarr.add(conteid);
                    }

                }
                previus = str[0];
                if (id.equals(userid)) {
                    item.setMine(true);
                    item.username = usrname;
                    item.userpic = usrpic;
                } else {
                    // item.username = odername;
                    if (getname(id, USER_NAME).length() > 0) {
                        item.username = getname(id, USER_NAME);
                    } else {
                        item.username = odername;
                    }

                    item.userpic = getname(id, USER_PRO_PIC);
                    item.setMine(false);
                }
                item.setmType(Integer.parseInt(type));
                if (type.equals("0")) {
                    item.setmText(populateItem(c, CONTENT));
                    System.out.println("count text=" + populateItem(c, CONTENT));
                } else if (type.equals("1")) {
                    item.imgurl = path;
                    item.setmText("");
                    item.originalurl = populateItem(c, CONTENT);
                   /* File file = new File(path);
                    Uri uri = Uri.fromFile(file);
                    Image image = new Image(con, uri);
                    //   String image_path = getRealPathFromURI(con, uri);

                    item.setByteArray(image.bitmapToByteArray(image.getBitmapFromUri(uri)));
                    item.setFilePath(path);
                    item.setFileName(image.getFileName());
                    item.setFileSize(image.getFileSize());*/
                } else if (type.equals("2")) {
                    item.setmText("");
                    item.imgurl = path;
                    item.setmText("");
                    item.setAudioLength(populateItem(c, CONTENT_TIME));
                    item.originalurl = populateItem(c, CONTENT);
//                    File file = new File(path);
//                    Uri uri = Uri.fromFile(file);
//                    String fileURL = RealPathUtil.getRealPathFromURI(con, uri);
                 /*   MediaFile audioFile = new MediaFile(con, path, Message.AUDIO_MESSAGE);
                    item.setByteArray(audioFile.fileToByteArray());
                    item.setAudioLength("0");
                    item.setFileName(audioFile.getFileName());
                    item.setFilePath(audioFile.getFilePath());*/
                }
                //**********added on 25-05-16**********
                item.schstatus = populateItem(c, CONTENT_STATUS);

                arr.add(item);

                //   String number = populateItem(c, USER_CONT_NO);

                // ChattingActivity.refreshList(item, true);
            }
            dataBaseHelper.close();
            db.close();
            c.close();
           /* if (arr.size() > 0) {

                Collections.reverse(arr);
            }*/

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return arr;
    }

    //-----------------delayed user update--------------//
    public void delayedupdate(ModelDelayedContact model) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(USER_PRO_PIC, model.profile_pic);
        values.put(USER_LAST_UPDATED, model.last_updated_date);
        dataBaseHelper.getWritableDatabase().update(TABLE_USER, values, DELAYD_USER_ID + " ='" + model.delayd_user_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }


    public int getcontent(String thread_id) {
        int count = 0;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + CONTENT_THREAD_ID + "='" + thread_id + "' ";

            Cursor c = db.rawQuery(select, null);
            count = c.getCount();

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }

        return count;
    }

    public String getnotification(String getthread_id) {
        String status = "no";

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_MSG_THREAD + " WHERE " + THREAD_ID + "='" + getthread_id + "' ";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {
                status = populateItem(c, NOTIFICATION_STATUS);
            }


            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return status;
    }

    public void updategrpmsgthread(ModelMsgThread model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(PARTICIPANT_IDS, model.participant_ids);
        values.put(PARTICIPANT_NOS, model.participant_numbers);
        values.put(GROUP_NAME, model.group_name);
        values.put(GROUP_ICON, model.group_icon);
        dataBaseHelper.getWritableDatabase().update(TABLE_MSG_THREAD, values, THREAD_ID + " ='" + model.thread_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-----------------delayed user update name--------------//
    public void delayedupdatename(String name, String id) throws Exception {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(USER_NAME, name);
            dataBaseHelper.getWritableDatabase().update(TABLE_USER, values, DELAYD_USER_ID + " ='" + id + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    /******************************************
     * Insert SMS
     ****************************************/

    public void insertsms(ModelScheduleSms model) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_SMS_Insert(model);
            dataBaseHelper.getWritableDatabase().insert(TABLE_SMS_SENT, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /******************************************
     * Update SMS
     ****************************************/

    public void updatesms(ModelScheduleSms model) {

        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_SMS_Update(model);
            dataBaseHelper.getWritableDatabase().update(TABLE_SMS_SENT, values, SMS_ID + " ='" + model.sms_id + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }


    }

    /******************************************
     * Update SMS
     ****************************************/

    public void updatesmsFromGroup(ModelScheduleSms model, String old_name) {

        try {
            dataBaseHelper.createDataBase();

            ContentValues values = CV_SMS_UpdateFromGroup(model);
            dataBaseHelper.getWritableDatabase().update(TABLE_SMS_SENT, values, SMS_GRP_NAME + " ='" + old_name + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }


    }

    private ContentValues CV_SMS_UpdateFromGroup(ModelScheduleSms model) {
        ContentValues values = new ContentValues();
        values.put(RECV_NUMBER, model.recvd_no);
        values.put(SMS_GRP_NAME, model.group_name);
        return values;
    }

    /******************************************
     * Content values for Insert SMS
     ****************************************/

    private ContentValues CV_SMS_Insert(ModelScheduleSms model) {
        ContentValues values = new ContentValues();
        values.put(SMS_ID, model.sms_id);
        values.put(RECV_NUMBER, model.recvd_no);
        values.put(SMS_CONTACT_NAME, model.contact_name);
        values.put(SMS_GRP_NAME, model.group_name);
        values.put(RECURRING_TYPE, model.recurring_type);
        values.put(HEADER_DATE, model.header_date);
        values.put(SCHDULE_DATE, model.schedule_date);
        values.put(SMS_CONTENT, model.message);
        values.put(LIST_TYPE, model.list_type);

        return values;
    }

    /******************************************
     * Content values for Insert SMS
     ****************************************/

    private ContentValues CV_SMS_Update(ModelScheduleSms model) {
        ContentValues values = new ContentValues();
        // values.put(ALARM_ID, model.sms_id);


        values.put(RECV_NUMBER, model.recvd_no);
        values.put(SMS_CONTACT_NAME, model.contact_name);
        values.put(SMS_GRP_NAME, model.group_name);
        values.put(RECURRING_TYPE, model.recurring_type);
        values.put(HEADER_DATE, model.header_date);
        values.put(SCHDULE_DATE, model.schedule_date);
        values.put(SMS_CONTENT, model.message);
        values.put(LIST_TYPE, model.list_type);

        return values;
    }

    //-----------getting sms---------------//
    public ArrayList<ModelScheduleSms> getsms(String type) {

        String select = "SELECT * FROM " + TABLE_SMS_SENT + " WHERE " + LIST_TYPE + " = '" + type + "' ORDER BY " + HEADER_DATE;

        ArrayList<ModelScheduleSms> arr = new ArrayList<>();
        ArrayList<ModelScheduleSms> storearr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {
                ModelScheduleSms item = new ModelScheduleSms();
                item.sms_id = populateItem(c, SMS_ID);
                item.recvd_no = populateItem(c, RECV_NUMBER);
                item.contact_name = populateItem(c, SMS_CONTACT_NAME);
                item.group_name = populateItem(c, SMS_GRP_NAME);
                item.recurring_type = populateItem(c, RECURRING_TYPE);
                item.header_date = populateItem(c, HEADER_DATE);
                item.schedule_date = populateItem(c, SCHDULE_DATE);
                item.message = populateItem(c, SMS_CONTENT);
                arr.add(item);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }


        JSONObject main = new JSONObject();
        JSONArray namearr = null;
        for (int i = 0; i < arr.size(); i++) {
            try {
                String headerDate = arr.get(i).header_date;
                if (!main.has(headerDate)) {
                    namearr = new JSONArray();
                }
                JSONObject obj = new JSONObject();
                obj.put("recvd_no", arr.get(i).recvd_no);
                obj.put("contact_name", arr.get(i).contact_name);
                obj.put("sms_id", arr.get(i).sms_id);
                obj.put("group_name", arr.get(i).group_name);
                obj.put("recurring_type", arr.get(i).recurring_type);
                obj.put("schedule_date", arr.get(i).schedule_date);
                obj.put("message", arr.get(i).message);

                namearr.put(obj);
                try {
                    if (!main.has(arr.get(i + 1).header_date)) {
                        main.put(headerDate, namearr);
                    }
                } catch (IndexOutOfBoundsException io) {
                    main.put(headerDate, namearr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            Iterator<String> iter = main.keys();
            while (iter.hasNext()) {

                String key = iter.next();
                ModelScheduleSms value = new ModelScheduleSms();
                value.header_date = key;
                value.view_type = "header";
                storearr.add(value);

                JSONArray jarr = main.optJSONArray(key);
                for (int i = 0; i < jarr.length(); i++) {
                    ModelScheduleSms all = new ModelScheduleSms();
                    JSONObject obj1 = jarr.optJSONObject(i);
                    all.sms_id = obj1.getString("sms_id");
                    all.header_date = key;
                    all.recvd_no = obj1.getString("recvd_no");
                    all.contact_name = obj1.getString("contact_name");
                    all.group_name = obj1.getString("group_name");
                    all.recurring_type = obj1.getString("recurring_type");
                    all.schedule_date = obj1.getString("schedule_date");
                    all.message = obj1.getString("message");
                    all.view_type = "values";
                    storearr.add(all);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return storearr;
    }

    public ArrayList<ModelScheduleSms> getschsms() {
        String select = "SELECT * FROM " + TABLE_SMS_SENT + " ORDER BY " + SCHDULE_DATE;

        ArrayList<ModelScheduleSms> arr = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {
                ModelScheduleSms item = new ModelScheduleSms();
                item.sms_id = populateItem(c, SMS_ID);
                item.contact_name = populateItem(c, SMS_CONTACT_NAME);
                item.group_name = populateItem(c, SMS_GRP_NAME);
                item.recurring_type = populateItem(c, RECURRING_TYPE);
                item.header_date = populateItem(c, HEADER_DATE);
                item.schedule_date = populateItem(c, SCHDULE_DATE);
                item.message = populateItem(c, SMS_CONTENT);
                item.recvd_no = populateItem(c, RECV_NUMBER);

                arr.add(item);

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    public void deletesms(String smsid) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_SMS_SENT, SMS_ID + " = ?", new String[]{String.valueOf(smsid)});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatesmsdate(String smsid, String update) {
        try {
            String str[] = update.split(" ");
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(SCHDULE_DATE, update);
            values.put(HEADER_DATE, str[0]);
            dataBaseHelper.getWritableDatabase().update(TABLE_SMS_SENT, values, SMS_ID + " ='" + smsid + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public boolean msgexist() {
        String select = "SELECT * FROM " + TABLE_SMS_SENT;
        try {
            dataBaseHelper.createDataBase();
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            Cursor c = db.rawQuery(select, null);
            if (c.getCount() == 1) {
                return true;
            }
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return false;
    }

    public boolean emailexist(String email) {

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String select = "SELECT * FROM " + TABLE_EMAIL + " WHERE " + EMAIL_CONTENT + " =  '" + email + "'";


            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {
                return true;
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void insertemail(String name, String email) {
        try {
            dataBaseHelper.createDataBase();
            ContentValues values = new ContentValues();
            values.put(EMAIL_CONTENT, email);
            values.put(EMAIL_NAME, name);
            dataBaseHelper.getWritableDatabase().insert(TABLE_EMAIL, null, values);
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Person> getemail() {
        ArrayList<Person> userlist = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_EMAIL;

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {
                String email = c.getString(c.getColumnIndex(EMAIL_CONTENT));
                String name = c.getString(c.getColumnIndex(EMAIL_NAME));
                Person item = new Person(email, email);
                userlist.add(item);

            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userlist;

    }

    public int getunreadthread() {
        unreadcount = 0;
        try {
            dataBaseHelper.createDataBase();
            String select = "";
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            select = "SELECT * FROM " + TABLE_MSG_TRD_CONTENT + " WHERE " + READ_STATUS + " = '0'";

            Cursor c = db.rawQuery(select, null);
            unreadcount = c.getCount();

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            e.printStackTrace();
        }
        return unreadcount;

    }

    public void delete_sms(String grp_name) {
        try {
            dataBaseHelper.createDataBase();
            dataBaseHelper.getWritableDatabase().delete(TABLE_SMS_SENT, SMS_GRP_NAME + " = ?", new String[]{String.valueOf(grp_name)});
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getsmsid(String grp_name) {

        ArrayList<String> userlist = new ArrayList<>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_SMS_SENT + " WHERE " + SMS_GRP_NAME + "='" + grp_name + "'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                userlist.add(c.getString(c.getColumnIndex(SMS_ID)));

            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userlist;
    }

    /*************************************
     * Update contacts
     ***********************************/
    class contactsync extends AsyncTask<String, Void, String> {
        ArrayList<ModelContacts> arr_cont;

        public contactsync(ArrayList<ModelContacts> mContacts) {
            this.arr_cont = mContacts;
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if (arr_cont.size() > 0) {
                    String json = new Gson().toJson(arr_cont);
                    //
                    String auth_code = FragmentChat.prefs.getString("auth_code", null);
                    String response = ResponseMethod(auth_code, json);
                    Log.i("JSON: ", "" + response);


                    JSONObject object = new JSONObject(response);

                    String status = object.getString("status");
                    if (status.equals("true")) {

                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject obj = jsonArray.getJSONObject(i);
                            ModelDelayedContact model = new ModelDelayedContact();

                            model.delayd_user_id = obj.getString("delayd_user_id");
                            model.name = obj.getString("name");
                            model.profile_pic = obj.getString("profile_pic");
                            model.last_updated_date = obj.getString("last_updated_date");
                            model.contact_no = obj.getString("no");
                            model.country_code = obj.getString("country_code");
                            model.delayed_user = "1";

                            //delayedContacts.add(model);
                            UpdateContacts2(model);

                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    private String ResponseMethod(String auth_code, String data) {
        String res = "";
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }

}
