package com.app.delayed;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.delayed.adapters.DelayedContactAdapter;
import com.app.delayed.chatExtended.CreateNewGroup;
import com.app.delayed.databaseUtils.DatabaseQueries;
import com.app.delayed.model.JSONParser;
import com.app.delayed.model.ModelContacts;
import com.app.delayed.model.ModelDelayedContact;
import com.app.delayed.model.ModelUser;
import com.app.delayed.utils.Methods;
import com.app.delayed.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balvinder on 2/19/2016.
 */
public class ChatContactsScreen extends AppCompatActivity {
    Toolbar toolbar;
    TextView title;
    RecyclerView contact_list;
    DelayedContactAdapter adapter;
    List<ModelContacts> arr_cont = new ArrayList<>();
    public static List<ModelUser> delayedContacts = new ArrayList<>();
    DatabaseQueries query;
    SharedPreferences prefs;
    Menu optionsMenu;
    SearchView searchView;
    String userid,mobile_no,username,userpic,userCountryCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_contact_screen);

        init();

    }

    private void init() {
        prefs = getSharedPreferences("delayed", MODE_PRIVATE);
        userid = prefs.getString("id", null);
        username=prefs.getString("name", null);
        mobile_no= prefs.getString("mobile_no", null);
        userCountryCode= prefs.getString("country_id", null);
        userpic=prefs.getString("chat_icon", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("Contacts");
        query = new DatabaseQueries(ChatContactsScreen.this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        contact_list = (RecyclerView) findViewById(R.id.contact_list);

        getcontactsfromdb();


        LinearLayoutManager llm = new LinearLayoutManager(ChatContactsScreen.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        contact_list.setLayoutManager(llm);


        new LoadContacts().execute();
    }


    private class LoadContacts extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {

            Methods.FetchContacts(ChatContactsScreen.this);
            try {

                arr_cont = query.getupdateduserlist();

            }catch (Exception e){

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                if (arr_cont.size() > 0) {
                    new refreshcontacts().execute();
                }
            }catch (Exception e){

            }

        }
    }

    private void getcontactsfromdb() {

        try {
            delayedContacts = query.GetUserList("1");
            delayedContacts.addAll(query.GetUserList("0"));
            ModelUser item1 = new ModelUser();
            item1.user_contact = mobile_no;
            //item1.user_name = "Send to Yourself";
            item1.user_name = "You";
            item1.user_id=userid;
            item1.user_pic = userpic;
            item1.user_country_code=userCountryCode;
            item1.delayedtype="2";
            delayedContacts.add(0, item1);
//            ModelUser item2 = new ModelUser();
//            item2.user_contact = "New group";
//            item2.user_name = "Add new group";
//            item2.user_pic = "";
//            item2.delayedtype="3";
//            delayedContacts.add(0, item2);
            adapter = new DelayedContactAdapter(ChatContactsScreen.this, delayedContacts);
            contact_list.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_done, optionsMenu);
        optionsMenu.findItem(R.id.action_refresh).setVisible(true);
        optionsMenu.findItem(R.id.action_done).setVisible(false);
        menu.findItem(R.id.search).setVisible(true);
        final MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
//                prodrecyclerview.setVisibility(View.VISIBLE);
//                recyclerview.setVisibility(View.GONE);

                    adapter.filter(s);

                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.action_refresh) {
//            setRefreshActionButtonState(true);
//            try {
//                Methods.FetchContacts(ChatContactsScreen.this);
//                arr_cont = query.getupdateduserlist();
//                if (arr_cont.size() > 0) {
//                    new refreshcontacts().execute();
//                }else {
//                    setRefreshActionButtonState(false);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            Intent intent = new Intent(ChatContactsScreen.this, CreateNewGroup.class);
            intent.putExtra("isUpdate", false);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);

    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.action_refresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.menu_refresh_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    //-------------------refreshing contact thread----------------------//
    class refreshcontacts extends AsyncTask<String, Void, String> {
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String json = new Gson().toJson(arr_cont);
            //    System.out.println("json=" + json);
            Log.i("dgdg", "" + json);
            String auth_code = prefs.getString("auth_code", null);

            String response = ResponseMethod(auth_code, json);
            Log.i("JSON: ", "" + response);

            try {
                JSONObject object = new JSONObject(response);

                status = object.getString("status");
                if (status.equals("true")) {

                    JSONArray jsonArray = object.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject obj = jsonArray.getJSONObject(i);
                        ModelDelayedContact model = new ModelDelayedContact();

                        model.delayd_user_id = obj.getString("delayd_user_id");
                        model.profile_pic = obj.getString("profile_pic");
                        model.last_updated_date = obj.getString("last_updated_date");
                        model.contact_no = obj.getString("no");
                        model.country_code = obj.getString("country_code");
                        model.delayed_user = "1";
                        //delayedContacts.add(model);
                        query.UpdateContacts(model);

                    }

                } else {

                    // Do something when status false
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
         //   setRefreshActionButtonState(false);
            try {
                getcontactsfromdb();
                if (status.equals("true")) {

                  //  Toast.makeText(ChatContactsScreen.this,"Your contact list has been updated",Toast.LENGTH_LONG).show();
                    //    adapter.notifyDataSetChanged();
                }else {
                  //  Toast.makeText(ChatContactsScreen.this,"No new contact found",Toast.LENGTH_LONG).show();
                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private String ResponseMethod(String auth_code, String data) {
        String res = "";

        Uri.Builder builder = new Uri.Builder().appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("contact_no", data);
        res = new JSONParser().getJSONFromUrl(Utils.getAll_users, builder);

        return res;
    }
}
