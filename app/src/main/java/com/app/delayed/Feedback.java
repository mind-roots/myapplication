package com.app.delayed;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Balvinder on 4/6/2016.
 */
public class Feedback extends Activity {

    TextView mBetter, mLoveIt;
    LinearLayout mClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

        init();

        mLoveIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mBetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void init(){

        mBetter = (TextView)findViewById(R.id.could_be_better);
        mLoveIt = (TextView)findViewById(R.id.love_it);
        mClose = (LinearLayout)findViewById(R.id.close);

    }
}
